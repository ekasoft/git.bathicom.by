<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?><div class="base-page" id="contacts">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Контактная информация</h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="bg-white contacts-wrapper">
					<div class="row info-row">
						 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"address",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "address",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "9",
		"IBLOCK_TYPE" => "info",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Слайды",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"LINK",2=>"",),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>
						<div class="col-xs-12 col-sm-6 col-md-4">
 <span class="contact-row-name">Телефоны:</span>
							<div class="phones-row main-phones">
								 <? if (tplvar('mts')): ?>
								<div class="one-phone mts">
 <span class="phone"><span class="code"><?= tplvar('mts-code', false); ?> </span><?= tplvar('mts', false); ?></span>
								</div>
								 <? endif; ?> <? if (tplvar('mts2')): ?>
								<div class="one-phone mts">
 <span class="phone"><span class="code"><?= tplvar('mts2-code', false); ?> </span><?= tplvar('mts2', false); ?></span>
								</div>
								 <? endif; ?> <? if (tplvar('velcom')): ?>
								<div class="one-phone velcom">
 <span class="phone"><span class="code"><?= tplvar('velcom-code', false); ?> </span><?= tplvar('velcom', false); ?></span>
								</div>
								 <? endif; ?> <? if (tplvar('velcom2')): ?>
								<div class="one-phone velcom">
 <span class="phone"><span class="code"><?= tplvar('velcom2-code', false); ?> </span><?= tplvar('velcom2', false); ?></span>
								</div>
								 <? endif; ?> <? if (tplvar('life')): ?>
								<div class="one-phone life">
 <span class="phone"><span class="code"><?= tplvar('life-code', false); ?> </span><?= tplvar('life', false); ?></span>
								</div>
								 <? endif; ?> <? if (tplvar('life2')): ?>
								<div class="one-phone life">
 <span class="phone"><span class="code"><?= tplvar('life2-code', false); ?> </span><?= tplvar('life2', false); ?></span>
								</div>
								 <? endif; ?> <? if (tplvar('city')): ?>
								<div class="one-phone btc">
 <span class="phone"><span class="code"><?= tplvar('city-code', false); ?> </span><?= tplvar('city', false); ?></span>
								</div>
								 <? endif; ?>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-2">
 <span class="contact-row-name">Отдел продаж:</span>
							<div class="phones-row">
								 <? if (tplvar('mts-sale')): ?>
								<div class="one-phone mts">
 <span class="phone"><span class="code"><?= tplvar('mts-sale-code', false); ?> </span><?= tplvar('mts-sale', false); ?></span>
								</div>
								 <? endif; ?> <? if (tplvar('velcom-sale')): ?>
								<div class="one-phone velcom">
 <span class="phone"><span class="code"><?= tplvar('velcom-sale-code', false); ?> </span><?= tplvar('velcom-sale', false); ?></span>
								</div>
								 <? endif; ?> <? if (tplvar('life-sale')): ?>
								<div class="one-phone life">
 <span class="phone"><span class="code"><?= tplvar('life-sale-code', false); ?> </span><?= tplvar('life-sale', false); ?></span>
								</div>
								 <? endif; ?>
							</div>
						</div>
						 <? if (tplvar('fax')): ?>
						<div class="col-xs-12 col-sm-6 col-md-2">
 <span class="contact-row-name">Факс:</span>
							<div class="phones-row">
								<div class="one-phone btc">
 <span class="phone"><span class="code"><?= tplvar('fax-code', false); ?> </span><?= tplvar('fax', false); ?></span>
								</div>
							</div>
						</div>
						 <? endif; ?>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="map">
								 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	".default",
	Array(
		"COMPONENT_TEMPLATE" => ".default",
		"CONTROLS" => array(0=>"ZOOM",1=>"TYPECONTROL",2=>"SCALELINE",),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:53.9420199999964;s:10:\"yandex_lon\";d:27.68783800000001;s:12:\"yandex_scale\";i:15;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:27.687677067459;s:3:\"LAT\";d:53.942080132834;s:4:\"TEXT\";s:42:\"Минск, ул. Руссиянова, 14\";}}}",
		"MAP_HEIGHT" => "290",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(0=>"ENABLE_DBLCLICK_ZOOM",1=>"ENABLE_DRAGGING",)
	)
);?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>