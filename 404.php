<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>
    <div id="error-404" class="base-page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-10">
                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/404.png" class="img-responsive" alt="">
                </div>
                <div class="col-xs-12 col-sm-12 col-md-offset-3 col-md-9 main-info">
                    <span class="title_h2">Извините, страница не найдена</span>
                    <p>Неправильно набран адрес либо такой страницы не существует.</p>
                    <p>Вы можете вернуться на <a href="/">главную страницу</a> либо посетить <a href="/katalog/">каталог сайта</a></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-offset-3 col-md-9 add-info">
                    <span class="title_h4">Возможно, вы искали:</span>
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:catalog.section.list",
                        "404",
                        array(
                            "COMPONENT_TEMPLATE" => "tree",
                            "IBLOCK_TYPE" => "catalog",
                            "IBLOCK_ID" => "10",
                            "SECTION_ID" => $_REQUEST["SECTION_ID"],
                            "SECTION_CODE" => "",
                            "COUNT_ELEMENTS" => "N",
                            "TOP_DEPTH" => "1",
                            "SECTION_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "SECTION_USER_FIELDS" => array(
                                0 => "",
                                1 => "",
                            ),
                            "VIEW_MODE" => "LIST",
                            "SHOW_PARENT_NAME" => "N",
                            "SECTION_URL" => "",
                            "CACHE_TYPE" => "N",
                            "CACHE_TIME" => "36000000",
                            "CACHE_GROUPS" => "Y",
                            "ADD_SECTIONS_CHAIN" => "N"
                        ),
                        $component
                    );?>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>