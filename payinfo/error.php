<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оплата заказа");?><!-- wsb_order_num=44&wsb_tid=385413297 -->
<div class="base-page">
    <div class="container m-b-40">
        <div class="row">
            <div class="col-md-12">
                <div class="bg-white b-r_2">
                    <div class="row pay-result">
                        <div class="col-md-12">
                            <p><span class="ok"><?
                                    if ($_REQUEST['wsb_order_num']){
                                        echo 'Ошибка при оплате заказа № '.$_REQUEST['wsb_order_num'];
                                    } elseif ($_REQUEST['ordernumber']){
                                        echo 'Для завершения оплаты воспользуйтесь данными, отправленными вам на email. Номер вашего заказа - '.$_REQUEST['ordernumber'];
                                    }
                                    ?></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>