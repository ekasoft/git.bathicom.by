<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Оплата заказа");

$arParams['M_ID'] = '470351';
$arParams['ordernumber'] = htmlspecialchars($_REQUEST['ordernumber']);
// Номер транзакции
$arParams['billnumber'] = htmlspecialchars($_REQUEST['billnumber']);

if($arParams['billnumber']) {
    
    // Определяем начальную дату проверки билета
    $dates = array();
    $dates['Y'] = date('Y', strtotime('-3 day'));
    $dates['m'] = date('m', strtotime('-3 day'));
    $dates['d'] = date('d', strtotime('-3 day'));
    
    $url = 'https://test.paysec.by/orderstate/orderstate.cfm';
    $params = array(
        'Ordernumber' => $arParams['ordernumber'],
        'Merchant_ID' => $arParams['M_ID'],
        'Login' => 'bathicom7577_sale',
        'Password' => 'mva7500500',
        'Startyear' => $dates['Y'],
        'Startmonth' => $dates['m'],
        'Startday' => $dates['d'],
        'Starthour' => '00',
        'Startmin' => '00',
        'Endyear' => date('Y'),
        'Endmonth' => date('m'),
        'Endday' => date('d'),
        'Endhour' => '23',
        'Endmin' => '59',
        'Format' => '3',
    );
    $result = file_get_contents($url, false, stream_context_create(array(
        'http' => array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)
        )
    )));
    
    $xml = simplexml_load_string($result);
    $json = json_encode($xml);
    $arResult['ORDER_CHECK']['PAY_SYSTEM'] = json_decode($json,TRUE);
    if($arResult['ORDER_CHECK']['PAY_SYSTEM']['order']['orderstate']) {
        //prent($arResult['ORDER_CHECK']['PAY_SYSTEM']);
        $arOrder = CSaleOrder::GetByID($arResult['ORDER_CHECK']['PAY_SYSTEM']['order']['ordernumber']);
        if ($arOrder) {
            //prent($arOrder);
            //prent(Date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("FULL", LANG))));
            $arFields = array(
                "PAYED" => ($arResult['ORDER_CHECK']['PAY_SYSTEM']['order']['orderstate'] == 'Approved')?'Y':'N',
                "DATE_PAYED" => Date(CDatabase::DateFormatToPHP(CLang::GetDateFormat("FULL", LANG))),
                "USER_ID" => $arOrder["USER_ID"],
                "EMP_PAYED_ID" => $USER->GetID()
            );
            //prent($arFields);
            CSaleOrder::Update($arResult['ORDER_CHECK']['PAY_SYSTEM']['order']['ordernumber'], $arFields);
        }
    }
}

?>
    <div class="base-page">
        <?
        if (CModule::IncludeModule("sale")) {
            if ($_REQUEST['wsb_tid'] || $_REQUEST['billnumber']) {
                ?>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <span class="title_h1">Заказ №<?
                                if ($_REQUEST['wsb_order_num']){
                                    echo $_REQUEST['wsb_order_num'];
                                } elseif ($_REQUEST['ordernumber']){
                                    echo $_REQUEST['ordernumber'];
                                }
                                ?>
                                успешно оплачен</span>
                        </div>
                    </div>
                </div>
                <div class="container m-b-40">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bg-white b-r_2">
                                <div class="row pay-result">
                                    <div class="col-md-12">
                                        <p><span class="ok">
                                                 Код транзакции <?
                                                if($_REQUEST['wsb_tid']){
                                                    echo $_REQUEST['wsb_tid'];
                                                } elseif ($_REQUEST['billnumber']){
                                                    echo $_REQUEST['billnumber'];
                                                } ?>
                                               </span></p>

                                        <p><span class="ok">
                                    Информация об оплате в скором времени будет подтвеждена администратором.
                            </span></p>

                                        <?
                                        if (!CSaleOrder::PayOrder($_REQUEST['wsb_order_num'], "Y", True, True, 0, array("PAY_VOUCHER_NUM" => $_REQUEST['wsb_tid']))) {
                                            /*?>
                                            <p><span class="ok">Ошибка обновления информации о заказе.</span></p>
                                        <?*/
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?
            } else {
                ?>
                <div class="container m-b-40">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="bg-white b-r_2">
                                <div class="row pay-result">
                                    <div class="col-md-12">
                                        <p><span class="ok">Не переданы параметры.</span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?
            }
        }
        ?>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>