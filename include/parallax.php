<div class="parallax-box">
    <div class="main">
        <ul id="scene">
            <li class="layer" data-depth="0.20"><img src="<?= SITE_TEMPLATE_PATH ?>/img/square1-min.png" alt=""></li>
            <li class="layer" data-depth="0.20"><img src="<?= SITE_TEMPLATE_PATH ?>/img/square2-min.png" alt=""></li>
            <li class="layer" data-depth="0.60"><img src="<?= SITE_TEMPLATE_PATH ?>/img/square3-min.png" alt=""></li>
            <li class="layer" data-depth="0.40"><img src="<?= SITE_TEMPLATE_PATH ?>/img/square4-min.png" alt=""></li>
            <li class="layer" data-depth="0.10"><img src="<?= SITE_TEMPLATE_PATH ?>/img/square5-min.png" alt=""></li>
            <li class="layer" data-depth="0.50"><img src="<?= SITE_TEMPLATE_PATH ?>/img/square6-min.png" alt=""></li>
            <li class="layer" data-depth="0.20"><img src="<?= SITE_TEMPLATE_PATH ?>/img/square7-min.png" alt=""></li>
        </ul>
    </div>
</div>