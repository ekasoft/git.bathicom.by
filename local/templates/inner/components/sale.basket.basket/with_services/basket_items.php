<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
    echo $arResult["ERROR_MESSAGE"];

$bDelayColumn = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn = false;
$bPriceType = false;

if ($normalCount > 0):
    $currencyCode = CCurrency::GetBaseCurrency();
    ?>
    <div class="bg-white b-r_2" id="basket_items_list">
    <?
    foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
        $arHeaders[] = $arHeader["id"];
    endforeach;
    ?>
    <input type="hidden" id="column_headers" value="<?= CUtil::JSEscape(implode($arHeaders, ",")) ?>"/>
    <input type="hidden" id="offers_props" value="<?= CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ",")) ?>"/>
    <input type="hidden" id="action_var" value="<?= CUtil::JSEscape($arParams["ACTION_VARIABLE"]) ?>"/>
    <input type="hidden" id="quantity_float" value="<?= $arParams["QUANTITY_FLOAT"] ?>"/>
    <input type="hidden" id="count_discount_4_all_quantity"
           value="<?= ($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="price_vat_show_value"
           value="<?= ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="hide_coupon" value="<?= ($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N" ?>"/>
    <input type="hidden" id="use_prepayment" value="<?= ($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N" ?>"/>

    <div class="row basket-list-title-row">
        <div class="col-xs-12 col-sm-12 col-md-4">
            <span>Наименование товара</span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2 text-center-to-left">
            <span>Количество</span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4">
            <span>Дополнительные услуги</span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-2 text-right">
            <span>Стоимость</span>
        </div>
    </div>
    <div id="basket_items">
    <?
    $liftServices = getLiftServices();
foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
        if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
            $product_res = CCatalogProduct::GetByIDEx($arItem["PRODUCT_ID"]);
            if (!$product_res || $product_res['IBLOCK_ID'] == SERVICES_IBLOCK) {
                continue;
            }
            ?>
            <div class="row one-product delay_04s" id="<?= $arItem["ID"] ?>">
            <div class="col-xs-12 col-sm-12 col-md-4">
                <?
                $imageId = 0;
                if (intval($arItem["PREVIEW_PICTURE"]) > 0):
                    $imageId = $arItem["PREVIEW_PICTURE"];
                elseif (strlen($arItem["DETAIL_PICTURE"]) > 0):
                    $imageId = $arItem["DETAIL_PICTURE"];
                endif;
                if ($imageId == 0) {
                    $resProductImage = CIBlockElement::GetList(array(), array(
                        "ID" => $arItem['PRODUCT_ID']
                    ), false, false, array());
                    if ($obProductImage = $resProductImage->GetNextElement()) {
                        $arFieldsImage = $obProductImage->GetFields();
                        if ($arFieldsImage['IBLOCK_ID'] == OFFERS_IBLOCK) {
                            $arPropertiesCML2_LINK = $obProductImage->GetProperties(false, array(
                                "CODE" => "CML2_LINK"
                            ));
                            $obProductImage = CCatalogProduct::GetByIDEx($arPropertiesCML2_LINK['CML2_LINK']['VALUE']);
                            $imageId = intval($obProductImage['PROPERTIES']['MORE_PHOTO']['VALUE']);
                        } else {
                            $arPropertiesImage = $obProductImage->GetProperties(false, array(
                                "CODE" => "MORE_PHOTO"
                            ));
                            $imageId = intval(current($arPropertiesImage['MORE_PHOTO']['VALUE']));
                        }
                    }
                }
                if ($imageId > 0) {
                    $y = CFile::ResizeImageGet($imageId, array("width" => 50, "height" => 90), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false);
                    $image = $y['src'];
                } else {
                    $image = $templateFolder . "/images/no_photo.png";
                }
                unset($imageId);
                ?>
                <img src="<?= $image ?>" alt="">
                <? if (strlen($arItem["DETAIL_PAGE_URL"]) > 0): ?>
                <a class="show-product-popup" data-toggle="modal"
                   data-target="#product-detail-popup-<?= $arItem['ID'] ?>">
                    <!--<a class="show-product-popup" href="<?= $arItem["DETAIL_PAGE_URL"] ?>">!-->
                    <? endif; ?>
                    <span><?= $arItem["NAME"] ?></span>
                    <? if (strlen($arItem["DETAIL_PAGE_URL"]) > 0): ?></a><? endif; ?>
                <?foreach ($arItem['PROPS'] as $PROP) {
                    if ($PROP['CODE'] == "SERVICES") continue;
                    ?><br/><span><?= $PROP['NAME'] ?>: <?= $PROP['VALUE'] ?></span><?
                }?>
                <input type="button" class="delete-product delay_04s" value="Удалить из корзины"
                       data-toggle="modal" data-target="#delete-ask-popup"
                       data-href="<?= str_replace("#ID#", $arItem["ID"], $arUrls["delete"]) ?>">

                <div class="clearfix"></div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 text-center-to-left number-of-product"
                 id="basket_quantity_control">
                <?
                $ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
                $max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"" . $arItem["AVAILABLE_QUANTITY"] . "\"" : "";
                $useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
                $useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
                if (!isset($arItem["MEASURE_RATIO"])) {
                    $arItem["MEASURE_RATIO"] = 1;
                } ?>
                <? if (floatval($arItem["MEASURE_RATIO"]) != 0): ?>
                    <input type="button" class="minus" value="-"
                           onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'down', <?= $useFloatQuantityJS ?>);">
                <? endif; ?>
                <input class="number-product number-only"
                       type="text"
                       id="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                       name="QUANTITY_INPUT_<?= $arItem["ID"] ?>"
                       size="2"
                       maxlength="18"
                       min="0"
                    <?= $max ?>
                       step="<?= $ratio ?>"
                       value="<?= $arItem["QUANTITY"] ?>"
                       onchange="updateQuantity('QUANTITY_INPUT_<?= $arItem["ID"] ?>', '<?= $arItem["ID"] ?>', <?= $ratio ?>, <?= $useFloatQuantityJS ?>)"
                    >
                <? if (floatval($arItem["MEASURE_RATIO"]) != 0): ?>
                    <input type="button" class="plus" value="+"
                           onclick="setQuantity(<?= $arItem["ID"] ?>, <?= $arItem["MEASURE_RATIO"] ?>, 'up', <?= $useFloatQuantityJS ?>);">
                <? endif; ?>
                <input type="hidden" id="QUANTITY_<?= $arItem['ID'] ?>" name="QUANTITY_<?= $arItem['ID'] ?>"
                       value="<?= $arItem["QUANTITY"] ?>"/>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 additional-service">
                <?
                // получаем прикрепленные услуги товара
                $ITEM_SERVICES_CART = "";
                foreach ($arItem['PROPS'] as $PROP) {
                    if ($PROP['CODE'] == "SERVICES") {
                        $ITEM_SERVICES_CART = $PROP['VALUE'];
                        break;
                    }
                }
                // установленные ID всех услуг товара через запятую
                $ITEM_SERVICES_CART_IDS = explode(',', $ITEM_SERVICES_CART);
                $ITEM_SERVICES_CART_IDS = array_diff($ITEM_SERVICES_CART_IDS, array('', null));
                // установленные PRODUCT_ID всех услуг товара
                $ITEM_SERVICES_CART_PRODUCT_IDS = array();
                $ITEM_SERVICES_CART_PRODUCT_PROPS = array();
                $ITEM_SERVICES_CART_PRODUCT_PRICES = array();
                foreach ($arResult["GRID"]["ROWS"] as $arItemBasket) {
                    if (in_array($arItemBasket['ID'], $ITEM_SERVICES_CART_IDS)) {
                        $ITEM_SERVICES_CART_PRODUCT_IDS[] = $arItemBasket['PRODUCT_ID'];
                        foreach ($arItemBasket['PROPS'] as $PROP) {
                            $ITEM_SERVICES_CART_PRODUCT_PROPS[$arItemBasket['PRODUCT_ID']][$PROP['NAME']] = $PROP['VALUE'];
                        }
                        $ITEM_SERVICES_CART_PRODUCT_PRICES['CURRENCY'] = $arItemBasket['CURRENCY'];
                        $ITEM_SERVICES_CART_PRODUCT_PRICES['PRICE'][] = $arItemBasket['PRICE'];
                    }
                }

                if ($product_res['IBLOCK_ID'] == OFFERS_IBLOCK) {
                    $product_res = CCatalogProduct::GetByIDEx($product_res['PROPERTIES']['CML2_LINK']['VALUE']);
                }
                if (intval($product_res['IBLOCK_SECTION_ID']) > 0):
                    $services = getServicesBySectionId(intval($product_res['IBLOCK_SECTION_ID']));
                    if ($services) {
                        ?>
                        <div class="check-control">
                            <div class="group-checkboxes">
                                <? foreach ($services as $service): ?>
                                    <? if (empty($service['PROPERTIES']['RISE_ON_THE_FLOOR']['VALUE'])): ?>
                                        <?
                                        $currentPrice = current($service['PRICES']);
                                        $convertPrice = CCurrencyRates::ConvertCurrency($currentPrice['PRICE'], $currentPrice['CURRENCY'], $currencyCode);
                                        $formatPrice = CurrencyFormat($convertPrice, $currencyCode);
                                        ?>
                                        <div class="custom_check">
                                            <input type="checkbox" class="SERVICE_CHECK"
                                                   <? if (in_array($service['ID'], $ITEM_SERVICES_CART_PRODUCT_IDS)): ?>checked="checked"<? endif; ?>
                                                   name="SERVICE_CHECK[<?= $arItem["ID"] ?>][<?= $service['ID'] ?>]"
                                                   id="SERVICE_CHECK_<?= $arItem["ID"] ?>_<?= $service['ID'] ?>">
                                            <label
                                                for="SERVICE_CHECK_<?= $arItem["ID"] ?>_<?= $service['ID'] ?>"><?= $service['NAME'] ?>
                                                <? if (!empty($formatPrice)): ?>(<?= $formatPrice ?>)<? endif; ?></label>
                                        </div>
                                    <? endif; ?>
                                <? endforeach ?>
                            </div>
                            <div class="lifting-check-box">
                                <? foreach ($services as $service): ?>
                                    <?
                                    $currentPrice = current($service['PRICES']);
                                    $convertPrice = CCurrencyRates::ConvertCurrency($currentPrice['PRICE'], $currentPrice['CURRENCY'], $currencyCode);
                                    $formatPrice = CurrencyFormat($convertPrice, $currencyCode);
                                    ?>
                                    <? if (!empty($service['PROPERTIES']['RISE_ON_THE_FLOOR']['VALUE'])): ?>
                                        <?
                                        $isNoRise = ("Не требуется" == $ITEM_SERVICES_CART_PRODUCT_PROPS[$service['ID']]['Услуга']);
                                        $isRise = (in_array($service['ID'], $ITEM_SERVICES_CART_PRODUCT_IDS));
                                        ?>
                                        <div class="custom_check slope">
                                            <input type="checkbox" class="rise SERVICE_CHECK"
                                                   <? if ($isRise && !$isNoRise): ?>checked="checked"<? endif; ?>
                                                   name="SERVICE_CHECK[<?= $arItem["ID"] ?>][<?= $service['ID'] ?>]"
                                                   id="SERVICE_CHECK_<?= $arItem["ID"] ?>_<?= $service['ID'] ?>">
                                            <label
                                                for="SERVICE_CHECK_<?= $arItem["ID"] ?>_<?= $service['ID'] ?>"><?= $service['NAME'] ?>
                                                <? if (!empty($formatPrice) && $currentPrice['PRICE'] > 0): ?>(<?= $formatPrice ?>)<? endif; ?></label>
                                        </div>
                                        <div
                                            class="slope-param <? if ($isRise && !$isNoRise): ?>active<? endif; ?>">
                                            <!--inner checkboxes-->
                                            <? foreach ($liftServices as $liftService): ?>
                                                <div class="custom_check slope">
                                                    <?
                                                    $isLiftService = ($liftService['UF_NAME'] == $ITEM_SERVICES_CART_PRODUCT_PROPS[$service['ID']]['Лифт']);
                                                    ?>
                                                    <input type="checkbox" class="SERVICE_CHECK_LIFT"
                                                           <? if ($isLiftService && !$isNoRise): ?>checked="checked"<? endif; ?>
                                                           name="SERVICE_CHECK_LIFT[<?= $arItem["ID"] ?>][<?= $service['ID'] ?>][<?= $liftService['ID'] ?>]"
                                                           id="SERVICE_CHECK_LIFT_<?= $arItem["ID"] ?>_<?= $service['ID'] ?>_<?= $liftService['ID'] ?>">
                                                    <label
                                                        for="SERVICE_CHECK_LIFT_<?= $arItem["ID"] ?>_<?= $service['ID'] ?>_<?= $liftService['ID'] ?>">
                                                        <?= $liftService['UF_NAME'] ?></label>
                                                </div>
                                            <? endforeach; ?>
                                            <div class="floor-box">
                                                <label for="floor1-<?= $arItem["ID"] ?>">Этаж</label>
                                                <input type="text" maxlength="2"
                                                       value="<?=
                                                       (!$isNoRise) ?
                                                           (intval($ITEM_SERVICES_CART_PRODUCT_PROPS[$service['ID']]['Этаж']) > 0 ?
                                                               $ITEM_SERVICES_CART_PRODUCT_PROPS[$service['ID']]['Этаж'] : 1)
                                                           : "" ?>"
                                                       class="form-control number-only SERVICE_CHECK_FLOOR"
                                                       name="SERVICE_CHECK_FLOOR[<?= $arItem["ID"] ?>][<?= $service['ID'] ?>]"
                                                       id="SERVICE_CHECK_FLOOR_<?= $arItem["ID"] ?>_<?= $service['ID'] ?>">
                                            </div>
                                        </div>
                                    <? endif; ?>
                                <? endforeach ?>
                            </div>
                        </div>
                        <div class="custom_check">
                            <input type="checkbox" class="no-rise SERVICE_CHECK_NORISE"
                                   <? if ($isNoRise): ?>checked="checked"<? endif; ?>
                                   name="SERVICE_CHECK_NORISE[<?= $arItem["ID"] ?>][<?= $service['ID'] ?>]"
                                   id="SERVICE_CHECK_NORISE_<?= $arItem["ID"] ?>_<?= $service['ID'] ?>">
                            <label
                                for="SERVICE_CHECK_NORISE_<?= $arItem["ID"] ?>_<?= $service['ID'] ?>">Не
                                требуется</label>
                        </div>
                    <? } ?>
                <? endif; ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 text-right cost-col"
                 id="sum_<?= $arItem["ID"] ?>">
                <?
                $serviceCurrencyCode = !empty($ITEM_SERVICES_CART_PRODUCT_PRICES['CURRENCY']) ?
                    $ITEM_SERVICES_CART_PRODUCT_PRICES['CURRENCY'] : $currencyCode;
                if (!empty($ITEM_SERVICES_CART_PRODUCT_PRICES['PRICE'])) {
                    $sumServices = CCurrencyRates::ConvertCurrency(array_sum($ITEM_SERVICES_CART_PRODUCT_PRICES['PRICE']), $serviceCurrencyCode, $currencyCode);
                } else {
                    $sumServices = 0;
                }
                $formatPrice = CurrencyFormat($arItem['PRICE'] * $arItem['QUANTITY'] + $sumServices, $currencyCode);
                $priceCode = getPriceCode($formatPrice, $currencyCode);
                ?>
                <span class="cost"><span class="prod-cost"><? echo $priceCode['price']; ?></span>
                    <? echo $priceCode['code']; ?></span>
                <? unset($priceCode); ?>
            </div>
            </div>

            <? if ($product_res['IBLOCK_ID'] == GOODS_IBLOCK): ?>
            <?
            $productId = $product_res['ID'];
            ?>
            <?$APPLICATION->IncludeComponent(
                "bitrix:catalog.element",
                "cart",
                array(
                    "IBLOCK_TYPE" => "catalog",
                    "IBLOCK_ID" => "2",
                    "PROPERTY_CODE" => array(
                        0 => "BRAND_REF",
                        1 => "COLLECTION_REF",
                        2 => "",
                    ),
                    "META_KEYWORDS" => "",
                    "META_DESCRIPTION" => "",
                    "BROWSER_TITLE" => "",
                    "SET_CANONICAL_URL" => "N",
                    "BASKET_URL" => "/cart/",
                    "ACTION_VARIABLE" => "action",
                    "PRODUCT_ID_VARIABLE" => "id",
                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                    "CHECK_SECTION_ID_VARIABLE" => "N",
                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                    "PRODUCT_PROPS_VARIABLE" => "prop",
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "36000000",
                    "CACHE_GROUPS" => "Y",
                    "SET_TITLE" => "N",
                    "SET_BROWSER_TITLE" => "N",
                    "SET_META_KEYWORDS" => "N",
                    "SET_META_DESCRIPTION" => "N",
                    "SET_LAST_MODIFIED" => "N",
                    "MESSAGE_404" => "",
                    "SET_STATUS_404" => "N",
                    "SHOW_404" => "N",
                    "FILE_404" => "",
                    "PRICE_CODE" => array(
                        0 => "BASE",
                    ),
                    "USE_PRICE_COUNT" => "N",
                    "SHOW_PRICE_COUNT" => "1",
                    "PRICE_VAT_INCLUDE" => "Y",
                    "PRICE_VAT_SHOW_VALUE" => "N",
                    "USE_PRODUCT_QUANTITY" => "N",
                    "PRODUCT_PROPERTIES" => array(),
                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                    "PARTIAL_PRODUCT_PROPERTIES" => "Y",
                    "LINK_IBLOCK_TYPE" => "",
                    "LINK_IBLOCK_ID" => "",
                    "LINK_PROPERTY_SID" => "USEFUL_PRODUCTS",
                    "LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",

                    "OFFERS_CART_PROPERTIES" => array(
                        0 => "PACKAGE_REF",
                        1 => "SIZES_BATHS",
                    ),
                    "OFFERS_FIELD_CODE" => $arParams["DETAIL_OFFERS_FIELD_CODE"],
                    "OFFERS_PROPERTY_CODE" => array(
                        0 => "ARTNUMBER",
                        1 => "PACKAGE_REF",
                        2 => "SIZES_BATHS",
                        3 => "",
                    ),
                    "OFFERS_SORT_FIELD" => "sort",
                    "OFFERS_SORT_ORDER" => "desc",
                    "OFFERS_SORT_FIELD2" => "id",
                    "OFFERS_SORT_ORDER2" => "desc",

                    "ELEMENT_ID" => $productId,
                    "ELEMENT_CODE" => "",
                    "SECTION_ID" => "",
                    "SECTION_CODE" => "",
                    "SECTION_URL" => "/catalog/#SECTION_CODE#/",
                    "DETAIL_URL" => "/catalog/#SECTION_CODE#/#ELEMENT_CODE#/",
                    'CONVERT_CURRENCY' => "Y",
                    'CURRENCY_ID' => "BYR",
                    'HIDE_NOT_AVAILABLE' => "N",
                    'USE_ELEMENT_COUNTER' => "N",
                    'SHOW_DEACTIVATED' => "N",
                    "USE_MAIN_ELEMENT_SECTION" => "N",

                    'ADD_PICT_PROP' => "MORE_PHOTO",
                    'LABEL_PROP' => "-",
                    'OFFER_ADD_PICT_PROP' => "-",
                    'OFFER_TREE_PROPS' => array(
                        0 => "PACKAGE_REF",
                        1 => "SIZES_BATHS",
                    ),
                    'PRODUCT_SUBSCRIPTION' => "",
                    'SHOW_DISCOUNT_PERCENT' => "N",
                    'SHOW_OLD_PRICE' => "N",
                    'SHOW_MAX_QUANTITY' => "Y",
                    'MESS_BTN_BUY' => "Добавить в корзину",
                    'MESS_BTN_ADD_TO_BASKET' => "Добавить в корзину",
                    'MESS_BTN_SUBSCRIBE' => "",
                    'MESS_BTN_COMPARE' => "Сравнение",
                    'MESS_NOT_AVAILABLE' => "Нет в наличии",
                    'USE_VOTE_RATING' => "N",
                    'VOTE_DISPLAY_AS_RATING' => "rating",
                    'USE_COMMENTS' => "N",
                    'BLOG_USE' => "Y",
                    'BLOG_URL' => "catalog_comments",
                    'BLOG_EMAIL_NOTIFY' => "N",
                    'VK_USE' => "N",
                    'VK_API_ID' => "",
                    'FB_USE' => "Y",
                    'FB_APP_ID' => "",
                    'BRAND_USE' => "Y",
                    'BRAND_PROP_CODE' => array(
                        0 => "BRAND_REF",
                        1 => "",
                    ),
                    'DISPLAY_NAME' => "Y",
                    'ADD_DETAIL_TO_SLIDER' => "Y",
                    'TEMPLATE_THEME' => "site",
                    "ADD_SECTIONS_CHAIN" => "N",
                    "ADD_ELEMENT_CHAIN" => "N",
                    "DISPLAY_PREVIEW_TEXT_MODE" => "E",
                    "DETAIL_PICTURE_MODE" => "IMG",
                    'ADD_TO_BASKET_ACTION' => array(
                        0 => "BUY",
                    ),
                    'SHOW_CLOSE_POPUP' => "N",
                    'DISPLAY_COMPARE' => "N",
                    'COMPARE_PATH' => "/catalog/compare/",
                    'SHOW_BASIS_PRICE' => "Y",
                    'BASKET_ID' => $arItem['ID']
                ),
                $component,
                array("HIDE_ICONS" => "Y")
            );?>
        <? endif; ?>
        <?
        endif;
    endforeach;
    ?>
    </div>
    <!---->
    <div class="row total-row">
        <div class="col-sm-6 col-sm-push-6 text-right cost">
            <?
            $priceCode = getPriceCode($arResult["allSum_FORMATED"], $currencyCode);
            ?>
            <span class="total-cost-wrapper" id="allSum_FORMATED">Итого: <span id="total-cost">
                                        <? echo $priceCode['price']; ?></span><span
                    class="currency-name"><? echo $priceCode['code']; ?></span></span>
            <? unset($priceCode); ?>
        </div>
        <div class="col-sm-6 col-sm-pull-6">
            <a href="/catalog/" class="to-catalogue"><span>Перейти к каталогу</span></a>
        </div>
    </div>
    </div>
    <div class="row step-row">
        <div class="col-xs-4 col-sm-6 col-md-6">
            <span class="step-name">Шаг 1 из 3</span>
        </div>
        <div class="col-xs-8 col-sm-6 col-md-6 text-right">
            <a href="javascript:void(0)" onclick="checkOut();" class="main-blue-btn next-step delay_04s">Следующий
                шаг</a>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.one-product .delete-product').on('click', function () {
                var href = $(this).data('href');
                $deleteAskPopupYes = $('#delete-ask-popup .yes');
                $deleteAskPopupYes.attr('href', href);
            });
        });
    </script>
    <div class="modal modal-vcenter fade" id="delete-ask-popup">
        <div class="modal-dialog mid-popup">
            <div class="modal-content">
                <div class="modal-cont-wrap">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <span class="modal-title">Вы действительно хотите удалить товар из корзины?</span>
                    </div>
                    <div class="modal-body">
                        <a class="yes delay_04s">Удалить</a>
                        <a class="no delay_04s" data-dismiss="modal" aria-hidden="true">Отмена</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?
else:
    ?>
    <div class="bg-white b-r_2">
        <div class="row no-result">
            <div class="col-md-12">
                <p><span class="sorry"><?= GetMessage("SALE_NO_ITEMS"); ?></span></p>

                <p><span class="sorry"><a href="/catalog/">Вернуться к покупкам</a></span></p>
            </div>
        </div>
    </div>
<?
endif;
?>