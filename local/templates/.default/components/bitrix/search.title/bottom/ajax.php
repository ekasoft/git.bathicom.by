<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(!empty($arResult["CATEGORIES"])):?>
	<table class="title-search-result">
		<?foreach($arResult["CATEGORIES"] as $category_id => $arCategory):?>
			<tr class="title-search-line">
				<th class="title-search-separator">&nbsp;</th>
				<td class="title-search-separator">&nbsp;</td>
			</tr>
			<?foreach($arCategory["ITEMS"] as $i => $arItem):?>
			<tr>
				<?if($i == 0):?>
					<th>&nbsp;<?echo $arCategory["TITLE"]?></th>
				<?else:?>
					<th>&nbsp;</th>
				<?endif?>

				<?if($category_id === "all"):?>
					<td class="title-search-all"><a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></td>
				<?elseif(isset($arItem["ICON"])):?>
				<?php
					$arElement = GetIBlockElement($arItem['ITEM_ID']);
					$image_id = $arElement['PROPERTIES']['PHOTO']['VALUE'];
					$image_id = $arElement['DETAIL_PICTURE'];
					$width = 50;
					$height = 50;
					$y = CFile::ResizeImageGet($image_id, array("width" => $width, "height" => $height), BX_RESIZE_IMAGE_PROPORTIONAL_ALT , true );
					$image = $y['src'];
					if (!$image_id){
						$image = SITE_TEMPLATE_PATH."/img/noimage_50.jpg";
					}
				?>
					<td class="title-search-item">
						<a href="<?echo $arItem["URL"]?>">
							<table class="product">
								<tr>
									<td rowspan="2" class="img"><img src="<?=$image?>" alt="<?=$arItem["NAME"]?>" /></td>
									<td class="name"><?=$arItem["NAME"]?></td>
								</tr>
								<tr>
									<td class="desc"><?=$arElement['~PREVIEW_TEXT']?></td>
								</tr>
							</table>
						</a>
					</td>
				<?else:?>
					<td class="title-search-more"><a href="<?echo $arItem["URL"]?>"><?echo $arItem["NAME"]?></td>
				<?endif;?>
			</tr>
			<?endforeach;?>
		<?endforeach;?>
	</table>
<?endif;

?>
<script type="text/javascript">
	var offsetSearch = $("#search").offset();
	var widthSearch = $("#search").width();
	$(".searchtd").css({"z-index":"300","position":"relative"})
	$(".title-search-result").css({"left":3+offsetSearch.left+"px","top":40+offsetSearch.top+"px","width":widthSearch-8+"px"});
</script>