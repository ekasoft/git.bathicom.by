$(document).ready(function() {
    var cb = function(event){
        event.preventDefault();
        var self = $(this);
        var form = self.closest("form");
        var data = form.serialize();
        var ajaxCb = function(){
            loader_Custom_ajax_shadow(form);
            $.get("/local/include/scripts/search.php", data).success(function(result) {
                $(result).insertAfter("header");
                updateBlock('body');
                initSearchPopup();
                checkForAjaxForms();
                loader_Custom_ajax_remove();
            });
        };

        if (event.type == 'keyup' && event.keyCode == 13) {
            ajaxCb();
        }

        if (event.type == 'click' && self.is('input[type="button"]')){
            ajaxCb();
        }
    };

    $("#search-form").off("keyup").on("keyup", cb);
    $("#search-form input[type='button']").on('click', cb);

    $("#search-form").on("submit", function() {
        event.preventDefault();
        return false;
    });

    $("body").on("onAjaxReload", function(event, obj) {
        checkForAjaxForms();
        initSearchPopup(obj);
    });
});