<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
//prent($_REQUEST);
if(htmlspecialchars($_REQUEST['gopay']) == 'Y') {
    ?>
    <script>
        setTimeout(function(){ window.location = <?='"/order/payment/?ORDER_ID='.$arResult["ORDER"]["ID"]?>"; }, 3000);
    </script>
    <?
}
if (!empty($arResult["ORDER"])) {
    ?>
    <div class="base-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="title_h1">Поздравляем, Ваш заказ принят!</span>
                </div>
            </div>
        </div>
        <div class="container m-b-40">
            <div class="row">
                <div class="col-md-12">
                    <div class="bg-white b-r_2">
                        <div class="row pay-result">
                            <div class="col-md-12">
                                <p><span class="ok">
                                        В течение нескольких минут Вам перезвонит наш оператор для подтверждения списка заказа и времени доставки и поступит оповещение на e-mail с подтверждением о приеме заказа.
                                        <br/>Желаем приятных покупок!<br/>
                                    </span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?
    if (!empty($arResult["PAY_SYSTEM"])) {
        ?>
        <div class="base-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <span class="title_h1"><?= GetMessage("SOA_TEMPL_PAY") ?></span>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="bg-white b-r_2">
                            <div class="row pay-result">
                                <div class="col-md-12">
                                    <p><span class="ok"><?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"])) ?></span></p>
                                    <p><span class="ok"><?= $arResult["PAY_SYSTEM"]["NAME"] ?></span></p>
                                </div>
                                <?
                                if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0) {
                                    ?>
                                    <div class="col-md-12">
                                        <p><span class="ok">
                                            <?
                                            if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y") {
                                                ?>
                                                <script language="JavaScript">
                                                    window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]))?>');
                                                </script>
                                                <?= GetMessage("SOA_TEMPL_PAY_LINK", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"])))) ?>
                                                <?
                                                if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE'])) {
                                                    ?>
                                                    <?= GetMessage("SOA_TEMPL_PAY_PDF", Array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"])) . "&pdf=1&DOWNLOAD=Y")) ?>
                                                <?
                                                }
                                            } else {
                                                if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]) > 0) {
                                                    try {
                                                        include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                                                    } catch (\Bitrix\Main\SystemException $e) {
                                                        if ($e->getCode() == CSalePaySystemAction::GET_PARAM_VALUE)
                                                            $message = GetMessage("SOA_TEMPL_ORDER_PS_ERROR");
                                                        else
                                                            $message = $e->getMessage();

                                                        echo '<span style="color:red;">' . $message . '</span>';
                                                    }
                                                }
                                            }
                                            ?>
                                        </span></p>
                                    </div>
                                <?
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?
    }?>

<?
} else {
    ?>
    <div class="base-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="title_h1"><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></span>
                </div>
            </div>
        </div>
        <div class="container m-b-40">
            <div class="row">
                <div class="col-md-12">
                    <div class="bg-white b-r_2">
                        <div class="row no-result">
                            <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ACCOUNT_NUMBER"])) ?>
                            <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?
}
?>
