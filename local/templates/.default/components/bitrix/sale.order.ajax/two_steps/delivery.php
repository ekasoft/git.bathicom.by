<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$delivery_id_checked = 0;
?>
<div class="col-xs-12">
<? if (!empty($arResult["DELIVERY"])): ?>
    <div class="pay-box-wrapper no-border">
        <span class="title_h4">Вид доставки:</span>
        <?foreach ($arResult["DELIVERY"] as $delivery_id => $arDelivery): ?>
            <?
            if ($delivery_id !== 0 && intval($delivery_id) <= 0) {
                foreach ($arDelivery["PROFILES"] as $profile_id => $arProfile) {
                    ?>
                    <div class="custom_radio">
                        <input type="radio"
                               name="<?= htmlspecialcharsbx($arProfile["FIELD_NAME"]) ?>"
                               value="<?= $delivery_id . ":" . $profile_id; ?>"
                               id="ID_DELIVERY_<?= $delivery_id ?>_<?= $profile_id ?>"
                            <?= $arProfile["CHECKED"] == "Y" ? "checked=\"checked\"" : ""; ?>
                               onclick="submitForm();"
                            />
                        <label for="ID_DELIVERY_<?= $delivery_id ?>_<?= $profile_id ?>"
                               onclick="BX('ID_DELIVERY_<?= $delivery_id ?>_<?= $profile_id ?>').checked=true; submitForm();">
                            <?= htmlspecialcharsbx($arDelivery["TITLE"]) . " (" . htmlspecialcharsbx($arProfile["TITLE"]) . ")"; ?>
                        </label>
                    </div>
                <?
                } // endforeach
            } else // stores and courier
            {
                if (count($arDelivery["STORE"]) > 0)
                    $clickHandler = "onClick = \"fShowStore('" . $arDelivery["ID"] . "','" . $arParams["SHOW_STORES_IMAGES"] . "','" . $width . "','" . SITE_ID . "')\";";
                else
                    $clickHandler = "onClick = \"BX('ID_DELIVERY_ID_" . $arDelivery["ID"] . "').checked=true;submitForm();\"";
                if ($arDelivery["CHECKED"] == "Y") {
                    $delivery_id_checked = $arDelivery["ID"];
                }
                ?>
                <div class="custom_radio">
                    <input type="radio"
                           id="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>"
                           name="<?= htmlspecialcharsbx($arDelivery["FIELD_NAME"]) ?>"
                           value="<?= $arDelivery["ID"] ?>"<? if ($arDelivery["CHECKED"] == "Y") echo " checked"; ?>
                           onclick="submitForm();"
                        />
                    <label for="ID_DELIVERY_ID_<?= $arDelivery["ID"] ?>" <?= $clickHandler ?>>
                        <?= htmlspecialcharsbx($arDelivery["NAME"]) ?>
                        (<?= (int)$arDelivery["PRICE"] ? ($arDelivery["PRICE_FORMATED"].' <span class="byr">'.oldPrice($arDelivery["PRICE"], 'Y').'</span>') : "БЕСПЛАТНО" ?>)
                    </label>
                    <p><?=$arDelivery["DESCRIPTION"]?></p>
                </div>
            <?
            }
            ?>
        <? endforeach; ?>
    </div>
<? endif; ?>
<script type="text/javascript">
    function fShowStore(id, showImages, formWidth, siteId) {
        var strUrl = '<?=$templateFolder?>' + '/map.php';
        var strUrlPost = 'delivery=' + id + '&showImages=' + showImages + '&siteId=' + siteId;

        var storeForm = new BX.CDialog({
            'title': '<?=GetMessage('SOA_ORDER_GIVE')?>',
            head: '',
            'content_url': strUrl,
            'content_post': strUrlPost,
            'width': formWidth,
            'height': 450,
            'resizable': false,
            'draggable': false
        });

        var button = [
            {
                title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
                id: 'crmOk',
                'action': function () {
                    GetBuyerStore();
                    BX.WindowManager.Get().Close();
                }
            },
            BX.CDialog.btnCancel
        ];
        storeForm.ClearButtons();
        storeForm.SetButtons(button);
        storeForm.Show();
    }

    function GetBuyerStore() {
        BX('BUYER_STORE').value = BX('POPUP_STORE_ID').value;
        //BX('ORDER_DESCRIPTION').value = '
//        <?=GetMessage("SOA_ORDER_GIVE_TITLE")?>:
//        '+BX('
//        POPUP_STORE_NAME
//        ').value;
        BX('store_desc').innerHTML = BX('POPUP_STORE_NAME').value;
        BX.show(BX('select_store'));
    }

    function showExtraParamsDialog(deliveryId) {
        var strUrl = '<?=$templateFolder?>' + '/delivery_extra_params.php';
        var formName = 'extra_params_form';
        var strUrlPost = 'deliveryId=' + deliveryId + '&formName=' + formName;

        if (window.BX.SaleDeliveryExtraParams) {
            for (var i in window.BX.SaleDeliveryExtraParams) {
                strUrlPost += '&' + encodeURI(i) + '=' + encodeURI(window.BX.SaleDeliveryExtraParams[i]);
            }
        }

        var paramsDialog = new BX.CDialog({
            'title': '<?=GetMessage('SOA_ORDER_DELIVERY_EXTRA_PARAMS')?>',
            head: '',
            'content_url': strUrl,
            'content_post': strUrlPost,
            'width': 500,
            'height': 200,
            'resizable': true,
            'draggable': false
        });

        var button = [
            {
                title: '<?=GetMessage('SOA_POPUP_SAVE')?>',
                id: 'saleDeliveryExtraParamsOk',
                'action': function () {
                    insertParamsToForm(deliveryId, formName);
                    BX.WindowManager.Get().Close();
                }
            },
            BX.CDialog.btnCancel
        ];

        paramsDialog.ClearButtons();
        paramsDialog.SetButtons(button);
        //paramsDialog.adjustSizeEx();
        paramsDialog.Show();
    }

    function insertParamsToForm(deliveryId, paramsFormName) {
        var orderForm = BX("ORDER_FORM"),
            paramsForm = BX(paramsFormName);
        wrapDivId = deliveryId + "_extra_params";

        var wrapDiv = BX(wrapDivId);
        window.BX.SaleDeliveryExtraParams = {};

        if (wrapDiv)
            wrapDiv.parentNode.removeChild(wrapDiv);

        wrapDiv = BX.create('div', {props: { id: wrapDivId}});

        for (var i = paramsForm.elements.length - 1; i >= 0; i--) {
            var input = BX.create('input', {
                    props: {
                        type: 'hidden',
                        name: 'DELIVERY_EXTRA[' + deliveryId + '][' + paramsForm.elements[i].name + ']',
                        value: paramsForm.elements[i].value
                    }
                }
            );

            window.BX.SaleDeliveryExtraParams[paramsForm.elements[i].name] = paramsForm.elements[i].value;

            wrapDiv.appendChild(input);
        }

        orderForm.appendChild(wrapDiv);

        BX.onCustomEvent('onSaleDeliveryGetExtraParams', [window.BX.SaleDeliveryExtraParams]);
    }
</script>

<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?= $arResult["BUYER_STORE"] ?>"/>

<?
$ADDRESS_PROP = false;
$ADDRESS_LOCATION = false;
$STREET_PROP = false;
$ENTRANCE_PROP = false;
$CORP_PROP = false;
$APARTMENT_PROP = false;
$HOUSE_PROP = false;
$DELIVERY_DATE_PROP = false;
$DELIVERY_TIME_FROM_PROP = false;
$DELIVERY_TIME_TO_PROP = false;
$arrGroupProps = array("USER_PROPS_N", "USER_PROPS_Y");
foreach ($arrGroupProps as $arrGroupProp) {
    foreach ($arResult["ORDER_PROP"][$arrGroupProp] as $prop) {
        switch ($prop['CODE']) {
            case "ADDRESS_LOCATION":
                $ADDRESS_LOCATION = $prop;
                break;
            case "ADDRESS":
                $ADDRESS_PROP = $prop;
                break;
            case "STREET":
                $STREET_PROP = $prop;
                break;
            case "ENTRANCE":
                $ENTRANCE_PROP = $prop;
                break;
            case "CORP":
                $CORP_PROP = $prop;
                break;
            case "APARTMENT":
                $APARTMENT_PROP = $prop;
                break;
            case "HOUSE":
                $HOUSE_PROP = $prop;
                break;
            case "DELIVERY_DATE":
                $DELIVERY_DATE_PROP = $prop;
                break;
            case "DELIVERY_TIME_FROM":
                $DELIVERY_TIME_FROM_PROP = $prop;
                break;
            case "DELIVERY_TIME_TO":
                $DELIVERY_TIME_TO_PROP = $prop;
                break;
        }
    }
}
?>
<input type="hidden" name="ADDRESS_NAME" value="ORDER_PROP_<?= $ADDRESS_PROP['ID'] ?>"/>
<? if (in_array($delivery_id_checked, array(2, 4))): ?>
    <div class="pay-box-wrapper-inner inner-no-delivery" style="display: block;">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 from-where" id="no-delivery-form">
                <span class="param-name">Из города</span>
                <select class="selectpicker form-control" name="ORDER_PROP_<?= $ADDRESS_PROP['ID'] ?>">
                    <?
                    $resAddress = CIBlockElement::GetList(array(), array(
                        "IBLOCK_ID" => ADDRESS_IBLOCK,
                        "!PROPERTY_PARAGRAPH_EX" => false
                    ), false, false, array(
                        "ID", "NAME", "PREVIEW_TEXT", "PROPERTY_ADDRESS_STORAGE", "PROPERTY_OPENING_TIMES"
                    ));
                    while ($obAddress = $resAddress->GetNextElement()) {
                        $arFieldsAddress = $obAddress->GetFields();
                        ?>
                        <option data-id="<?= $arFieldsAddress['ID'] ?>"
                                value="<?= $arFieldsAddress['~NAME']; ?>, <?= $arFieldsAddress['~PROPERTY_ADDRESS_STORAGE_VALUE']; ?>"><?= $arFieldsAddress['NAME']; ?></option><?
                    }
                    ?>
                </select>
                <div class="param-box street">
                    <span class="param-name">Дата самовывоза</span>
                    <input type="text" class="form-control calendar" maxlength="10"
                           name="<?= $DELIVERY_DATE_PROP['FIELD_NAME'] ?>"
                           value="<?= $DELIVERY_DATE_PROP['VALUE'] ?>"
                           id="calendar-no-delivery">
                </div>
            </div>
            <?
            $resAddress = CIBlockElement::GetList(array(), array(
                "IBLOCK_ID" => ADDRESS_IBLOCK,
                "!PROPERTY_PARAGRAPH_EX" => false
            ), false, false, array(
                "ID", "NAME", "PREVIEW_TEXT",
                "PROPERTY_ADDRESS_STORAGE",
                "PROPERTY_OPENING_TIMES",
                "PROPERTY_PARAGRAPH_NAME",
            ));
            $first = true;
            while ($obAddress = $resAddress->GetNextElement()) {
                $arFieldsAddress = $obAddress->GetFields();
                ?>
            <div class="col-xs-12 col-sm-6 col-md-9 address-tab"
                 style="display: <? if ($first): $first = false; ?>block<? else: ?>none<?endif; ?>;"
                 data-address-id="<?= $arFieldsAddress['ID'] ?>">
                <? if (!empty($arFieldsAddress['~PROPERTY_PARAGRAPH_NAME_VALUE'])): ?>
                    <span class="title_h4"><?= $arFieldsAddress['~PROPERTY_PARAGRAPH_NAME_VALUE']; ?></span>
                <? endif; ?>
                <div class="row">
                    <? if (!empty($arFieldsAddress['~PROPERTY_ADDRESS_STORAGE_VALUE'])): ?>
                        <div class="col-xs-6 col-sm-6 col-md-3">
                            <span class="title_h5">Адрес склада:</span>

                            <p><?= $arFieldsAddress['~PROPERTY_ADDRESS_STORAGE_VALUE']; ?></p>
                        </div>
                    <? endif; ?>
                    <? if (!empty($arFieldsAddress['~PROPERTY_OPENING_TIMES_VALUE']['TEXT'])): ?>
                        <div class="col-xs-6 col-sm-6 col-md-3">
                            <span class="title_h5">Время работы:</span>
                            <?= $arFieldsAddress['~PROPERTY_OPENING_TIMES_VALUE']['TEXT']; ?>
                        </div>
                    <? endif; ?>
                </div>
                </div><?
            }
            ?>
        </div>
    </div>
<? else: ?>
    <div class="pay-box-wrapper-inner inner-delivery" style="display: block;">
        <div class="row">
            <div class="col-md-12" id="delivery-form">
                <p>* - поля, обязательные к заполнению</p>
                <div class="param-box street">
                    <span class="param-name">Город:<sup>*</sup></span>
                    <?//$ADDRESS_LOCATION?>
                    <? /*?><select class="selectpicker form-control" name="<?= $ADDRESS_PROP['FIELD_NAME'] ?>">
                        <?
                        $resAddress = CIBlockElement::GetList(array(), array(
                            "IBLOCK_ID" => ADDRESS_IBLOCK
                        ), false, false, array(
                            "ID", "NAME"
                        ));
                        while ($obAddress = $resAddress->GetNextElement()) {
                            $arFieldsAddress = $obAddress->GetFields();
                            ?>
                            <option value="<?= $arFieldsAddress['~NAME']; ?>"><?= $arFieldsAddress['NAME']; ?></option><?
                        }
                        ?>
                    </select><?*/?>
                    <?$GLOBALS["APPLICATION"]->IncludeComponent(
                        'bitrix:sale.ajax.locations',
                        "popup",
                        array(
                            "AJAX_CALL" => "N",
                            "COUNTRY_INPUT_NAME" => "COUNTRY",
                            "REGION_INPUT_NAME" => "REGION",
                            "CITY_INPUT_NAME" => $ADDRESS_LOCATION['FIELD_NAME'], //
                            "CITY_OUT_LOCATION" => "Y",
                            "LOCATION_VALUE" => !empty($ADDRESS_LOCATION['VALUE']) ? $ADDRESS_LOCATION['VALUE'] : $ADDRESS_LOCATION['DEFAULT_VALUE'], //
                            "ORDER_PROPS_ID" => $ADDRESS_LOCATION['ID'], //
                            "ONCITYCHANGE" => "",
                            "SIZE1" => $ADDRESS_LOCATION['SIZE1'],
                        ),
                        null,
                        array('HIDE_ICONS' => 'Y')
                    );?>
                </div>
                <div class="param-box street">
                    <span class="param-name">Улица:<sup>*</sup></span>
                    <input type="text" class="form-control"
                           name="<?= $STREET_PROP['FIELD_NAME'] ?>"
                           value="<?= $STREET_PROP['VALUE'] ?>"
                           id="street-delivery">
                </div>
                <div class="param-box">
                    <span class="param-name">Номер дома:<sup>*</sup></span>
                    <input type="text" class="form-control number-only" maxlength="3"
                           name="<?= $HOUSE_PROP['FIELD_NAME'] ?>"
                           value="<?= $HOUSE_PROP['VALUE'] ?>"
                           id="house-number-delivery">
                </div>
                <div class="param-box">
                    <span class="param-name">Номер квартиры:</span>
                    <input type="text" class="form-control number-only" maxlength="3"
                           name="<?= $APARTMENT_PROP['FIELD_NAME'] ?>"
                           value="<?= $APARTMENT_PROP['VALUE'] ?>"
                           id="flat-number-delivery">
                </div>
                <div class="param-box">
                    <span class="param-name">Корпус:</span>
                    <input type="text" class="form-control number-only" maxlength="2"
                           name="<?= $CORP_PROP['FIELD_NAME'] ?>"
                           value="<?= $CORP_PROP['VALUE'] ?>"
                           id="building-number-delivery">
                </div>
                <div class="param-box">
                    <span class="param-name">Номер подъезда:</span>
                    <input type="text" class="form-control number-only" maxlength="2"
                           name="<?= $ENTRANCE_PROP['FIELD_NAME'] ?>"
                           value="<?= $ENTRANCE_PROP['VALUE'] ?>"
                           id="entrance-number-delivery">
                </div>
                <div class="param-box">
                    <span class="param-name">Дата доставки:<sup>*</sup></span>
                    <input type="text" class="form-control calendar" maxlength="10"
                           name="<?= $DELIVERY_DATE_PROP['FIELD_NAME'] ?>"
                           value="<?= $DELIVERY_DATE_PROP['VALUE'] ?>"
                           id="calendar-delivery">
                </div>
                <div class="param-box">
                    <span class="param-name">Желаемое время доставки:<sup>*</sup></span>

                    <div class="time-wrap">
                        <span>с&nbsp;</span>
                        <input type="text" class="form-control time"
                               name="<?= $DELIVERY_TIME_FROM_PROP['FIELD_NAME'] ?>"
                               value="<?= $DELIVERY_TIME_FROM_PROP['VALUE'] ?>"
                               id="time1-delivery">
                        &nbsp;
                        <span>до&nbsp;</span>
                        <input type="text" class="form-control time"
                               name="<?= $DELIVERY_TIME_TO_PROP['FIELD_NAME'] ?>"
                               value="<?= $DELIVERY_TIME_TO_PROP['VALUE'] ?>"
                               id="time2-delivery">
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endif; ?>
</div>