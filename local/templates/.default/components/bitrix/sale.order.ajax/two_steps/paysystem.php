<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<div class="col-xs-12">
<div class="pay-box-wrapper">
    <span class="title_h4">Способ оплаты:</span>
    <?
    uasort($arResult["PAY_SYSTEM"], "cmpBySort"); // resort arrays according to SORT value
     //prent($arResult);
    $flag_rassrochki = true;
    foreach($arResult['BASKET_ITEMS'] as $k => $v){
        if($v['NOTES'] != 'RASSROCHKA'){
            $flag_rassrochki = false;
        }
    }
    
    foreach ($arResult["PAY_SYSTEM"] as $arPaySystem) {
        if (strlen(trim(str_replace("<br />", "", $arPaySystem["DESCRIPTION"]))) > 0 || intval($arPaySystem["PRICE"]) > 0) {
            if (count($arResult["PAY_SYSTEM"]) == 1) {
                ?>
                <div class="custom_radio">
                    <input type="hidden" name="PAY_SYSTEM_ID" value="<?= $arPaySystem["ID"] ?>">
                    <input type="radio"
                           id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                           name="PAY_SYSTEM_ID"
                           value="<?= $arPaySystem["ID"] ?>"
                        <? if ($arPaySystem["CHECKED"] == "Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) echo " checked=\"checked\""; ?>
                           onclick="changePaySystem();"
                        />
                    <label for="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                           onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">
                        <?= $arPaySystem["PSA_NAME"]; ?>
                        <?if (intval($arPaySystem["PRICE"]) > 0) {
                            echo " (".str_replace("#PAYSYSTEM_PRICE#",
                                SaleFormatCurrency(roundEx($arPaySystem["PRICE"],
                                        SALE_VALUE_PRECISION),
                                    $arResult["BASE_LANG_CURRENCY"]),
                                GetMessage("SOA_TEMPL_PAYSYSTEM_PRICE")).")";
                        }?>
                    </label>
                </div>
            <?
            } else // more than one
            {
                ?>
                <?if(!$flag_rassrochki){?>
                    <div class="custom_radio">
                        <input type="radio"
                               id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                               name="PAY_SYSTEM_ID"
                               value="<?= $arPaySystem["ID"] ?>"
                            <? if ($arPaySystem["CHECKED"] == "Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) echo " checked=\"checked\""; ?>
                               onclick="changePaySystem();"/>
                        <label for="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                               onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">
                            <?= $arPaySystem["PSA_NAME"]; ?>
                            <?if (intval($arPaySystem["PRICE"]) > 0) {
                                echo " (".str_replace("#PAYSYSTEM_PRICE#",
                                    SaleFormatCurrency(roundEx($arPaySystem["PRICE"],
                                        SALE_VALUE_PRECISION),
                                        $arResult["BASE_LANG_CURRENCY"]),
                                    GetMessage("SOA_TEMPL_PAYSYSTEM_PRICE")).")";
                            }?>
                        </label>
                    </div>
                <?}elseif($arPaySystem["ID"] == 1){?>
                    <div class="custom_radio">
                        <input type="radio"
                               id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                               name="PAY_SYSTEM_ID"
                               value="<?= $arPaySystem["ID"] ?>"
                            <? if ($arPaySystem["CHECKED"] == "Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) echo " checked=\"checked\""; ?>
                               onclick="changePaySystem();"/>
                        <label for="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                               onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">
                            <?= $arPaySystem["PSA_NAME"]; ?>
                            <?if (intval($arPaySystem["PRICE"]) > 0) {
                                echo " (".str_replace("#PAYSYSTEM_PRICE#",
                                    SaleFormatCurrency(roundEx($arPaySystem["PRICE"],
                                        SALE_VALUE_PRECISION),
                                        $arResult["BASE_LANG_CURRENCY"]),
                                    GetMessage("SOA_TEMPL_PAYSYSTEM_PRICE")).")";
                            }?>
                        </label>
                    </div>
                <?}?>
            <?
            }
        }

        if (strlen(trim(str_replace("<br />", "", $arPaySystem["DESCRIPTION"]))) == 0 && intval($arPaySystem["PRICE"]) == 0) {
            if (count($arResult["PAY_SYSTEM"]) == 1) {
                ?>
                <div class="custom_radio">
                    <input type="hidden" name="PAY_SYSTEM_ID" value="<?= $arPaySystem["ID"] ?>">
                    <input type="radio"
                           id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                           name="PAY_SYSTEM_ID"
                           value="<?= $arPaySystem["ID"] ?>"
                        <? if ($arPaySystem["CHECKED"] == "Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) echo " checked=\"checked\""; ?>
                           onclick="changePaySystem();"
                        />
                    <label for="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                           onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">
                        <?= $arPaySystem["PSA_NAME"]; ?>
                    </label>
                </div>
            <?
            } else // more than one
            {
                ?>
                <?if(!$flag_rassrochki){?>
                    <div class="custom_radio test">
                        <input type="radio"
                               id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                               name="PAY_SYSTEM_ID"
                               value="<?= $arPaySystem["ID"] ?>"
                            <? if ($arPaySystem["CHECKED"] == "Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) echo " checked=\"checked\""; ?>
                               onclick="changePaySystem();"/>
                        <label for="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                               onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">
                            <?= $arPaySystem["PSA_NAME"]; ?>
                        </label>
                    </div>
                <?}elseif($arPaySystem["ID"] == 1){?>
                    <div class="custom_radio test">
                        <input type="radio"
                               id="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                               name="PAY_SYSTEM_ID"
                               value="<?= $arPaySystem["ID"] ?>"
                            <? if ($arPaySystem["CHECKED"] == "Y" && !($arParams["ONLY_FULL_PAY_FROM_ACCOUNT"] == "Y" && $arResult["USER_VALS"]["PAY_CURRENT_ACCOUNT"] == "Y")) echo " checked=\"checked\""; ?>
                               onclick="changePaySystem();"/>
                        <label for="ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>"
                               onclick="BX('ID_PAY_SYSTEM_ID_<?= $arPaySystem["ID"] ?>').checked=true;changePaySystem();">
                            <?= $arPaySystem["PSA_NAME"]; ?>
                        </label>
                    </div>
                <?}?>
            <?
            }
        }
    }
    ?>
</div>
<script type="text/javascript">
    function changePaySystem(param) {
        if (BX("account_only") && BX("account_only").value == 'Y') // PAY_CURRENT_ACCOUNT checkbox should act as radio
        {
            if (param == 'account') {
                if (BX("PAY_CURRENT_ACCOUNT")) {
                    BX("PAY_CURRENT_ACCOUNT").checked = true;
                    BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                    BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');

                    // deselect all other
                    var el = document.getElementsByName("PAY_SYSTEM_ID");
                    for (var i = 0; i < el.length; i++)
                        el[i].checked = false;
                }
            }
            else {
                BX("PAY_CURRENT_ACCOUNT").checked = false;
                BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
            }
        }
        else if (BX("account_only") && BX("account_only").value == 'N') {
            if (param == 'account') {
                if (BX("PAY_CURRENT_ACCOUNT")) {
                    BX("PAY_CURRENT_ACCOUNT").checked = !BX("PAY_CURRENT_ACCOUNT").checked;

                    if (BX("PAY_CURRENT_ACCOUNT").checked) {
                        BX("PAY_CURRENT_ACCOUNT").setAttribute("checked", "checked");
                        BX.addClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                    }
                    else {
                        BX("PAY_CURRENT_ACCOUNT").removeAttribute("checked");
                        BX.removeClass(BX("PAY_CURRENT_ACCOUNT_LABEL"), 'selected');
                    }
                }
            }
        }

        submitForm();

    }
</script>
</div>
