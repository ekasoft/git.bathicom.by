<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$currencyCode = CCurrency::GetBaseCurrency();
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Корзина товаров</h1>
        </div>
    </div>
</div>
<div class="container m-b-40">
    <div class="row">
        <div class="col-md-12">
            <div class="bg-white b-r_2">
                <div class="row basket-list-title-row">
                    <div class="col-xs-12 col-sm-12 col-md-3">
                        <span>Наименование товара</span>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 text-center-to-left">
                        <span>Стоимость</span>
                    </div>
                    <?if($_REQUEST['debug'] == 'Y'){?>
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <span>Первый взнос</span>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-2">
                            <span>Ежемесячный платёж</span>
                        </div>
                    <?}else{?>
                        <div class="col-xs-12 col-sm-12 col-md-4">
                            <span>Дополнительные услуги</span>
                        </div>
                    <?}?>
                    <div class="col-xs-12 col-sm-12 col-md-3 text-right">
                        <span>Итоговая стоимость</span>
                    </div>
                </div>
                <? foreach ($arResult["GRID"]["ROWS"] as $k => $arData): ?>
                    <?
                    $arItem = $arData["data"];
                    if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
                        $product_res = CCatalogProduct::GetByIDEx($arItem["PRODUCT_ID"]);
                        if (!$product_res || $product_res['IBLOCK_ID'] == SERVICES_IBLOCK) {
                            continue;
                        }
                        ?>
                        <div class="row one-product delay_04s">
                            <div class="col-xs-12 col-sm-12 col-md-3">
                                <? if (strlen($arItem["DETAIL_PAGE_URL"]) > 0): ?><a class="show-product-popup"
                                                                                     href="<?= $arItem["DETAIL_PAGE_URL"] ?>"><? endif; ?>
                                    <span><?= $arItem["NAME"] ?></span>
                                    <? if (strlen($arItem["DETAIL_PAGE_URL"]) > 0): ?></a><? endif; ?>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-2 text-center-to-left cost-col">
                                <?
                                $priceCode = getPriceCode($arItem['PRICE_FORMATED'], $arItem['CURRENCY']);
                                $priceCodeOld = oldprice($arItem['PRICE_FORMATED'],'Y');
                                ?>
                                <span class="cost">
                                    <span class="prod-cost"><? echo $priceCode['price']; ?></span>
                                    <? echo $priceCode['code']; ?>
                                    <?/*<span class="prod-cost-old byr"><? echo $priceCodeOld; ?></span>*/?>
                                </span>
                                <? unset($priceCode); ?>
                            </div>
                            
                            <?if($_REQUEST['debug'] == 'Y' && $arItem['NOTES'] == 'RASSROCHKA'){?>
                                <div class="col-xs-12 col-sm-12 col-md-2"><?=number_format($arItem['PRICE']*$arItem['QUANTITY']*FIRST_PAY_PERCENT/100, 2, '.', ' ')?> руб.</div>
                                <div class="col-xs-12 col-sm-12 col-md-2"><?=number_format(($arItem['PRICE']*$arItem['QUANTITY']-$arItem['PRICE']*$arItem['QUANTITY']*FIRST_PAY_PERCENT/100)/RASSROCH_MONTH, 2, '.', ' ')?> руб.</div>
                            <?}else{?>
                                <div class="col-xs-12 col-sm-12 col-md-4 additional-service">
                                    <?
                                    // получаем прикрепленные услуги товара
                                    $ITEM_SERVICES_CART = "";
                                    foreach ($arItem['PROPS'] as $PROP) {
                                        if ($PROP['CODE'] == "SERVICES") {
                                            $ITEM_SERVICES_CART = $PROP['VALUE'];
                                            break;
                                        }
                                    }
                                    // установленные ID всех услуг товара через запятую
                                    $ITEM_SERVICES_CART_IDS = explode(',', $ITEM_SERVICES_CART);
                                    $ITEM_SERVICES_CART_IDS = array_diff($ITEM_SERVICES_CART_IDS, array('', null));
                                    $ITEM_SERVICES_CART_PRODUCT_PRICES = array();
                                    foreach ($arResult["GRID"]["ROWS"] as $arItemBasket) {
                                        if (in_array($arItemBasket['id'], $ITEM_SERVICES_CART_IDS)) {
                                            $arItemBasketData = $arItemBasket["data"];
                                            $convertPrice = CCurrencyRates::ConvertCurrency($arItemBasketData['PRICE'], $arItemBasketData['CURRENCY'], $currencyCode);
                                            $formatPrice = CurrencyFormat($convertPrice, $currencyCode);
                                            $isNoRise = false;
                                            foreach($arItemBasketData['PROPS'] as $PROP){
                                                if ($PROP['VALUE'] == "Не требуется"){
                                                    $isNoRise = true;
                                                    break;
                                                }
                                            }
                                            ?>
                                            <span><?= (!$isNoRise) ? $arItemBasketData['NAME'] : "Не требуется" ?>
                                                <? if (!empty($formatPrice) && $arItemBasketData['PRICE'] > 0): ?><span
                                                    class="add-cost">(<?= $formatPrice ?>)</span><? endif; ?>
                                            </span>
                                            <?
                                            $ITEM_SERVICES_CART_PRODUCT_PRICES['CURRENCY'] = $arItemBasketData['CURRENCY'];
                                            $ITEM_SERVICES_CART_PRODUCT_PRICES['PRICE'][] = $arItemBasketData['PRICE'];
                                        }
                                    }
                                    ?>
                                </div>
                            <?}?>
                            <div class="col-xs-12 col-sm-12 col-md-3 text-right cost-col">
                                <?
                                $serviceCurrencyCode = !empty($ITEM_SERVICES_CART_PRODUCT_PRICES['CURRENCY']) ?
                                    $ITEM_SERVICES_CART_PRODUCT_PRICES['CURRENCY'] : $currencyCode;
                                if (!empty($ITEM_SERVICES_CART_PRODUCT_PRICES['PRICE'])) {
                                    $sumServices = CCurrencyRates::ConvertCurrency(array_sum($ITEM_SERVICES_CART_PRODUCT_PRICES['PRICE']), $serviceCurrencyCode, $currencyCode);
                                } else {
                                    $sumServices = 0;
                                }
                                $formatPrice = CurrencyFormat($arItem['PRICE'] * $arItem['QUANTITY'] + $sumServices, $currencyCode);
                                $priceCode = getPriceCode($formatPrice, $currencyCode);
                                ?>
                                <span class="cost">
                                    <span class="prod-cost"><? echo $priceCode['price']; ?></span>
                                    <? echo $priceCode['code']; ?>
                                    <?/*<span class="prod-cost-old byr"><? echo oldPrice($priceCode['price'],'Y'); ?></span>*/?>
                                </span>
                                <? unset($priceCode); ?>
                            </div>
                        </div>
                    <? endif; ?>
                <? endforeach; ?>
                <!---->
                <div class="row total-price-row">
                    <div class="col-md-12 text-right cost">
                        <?
                        $priceCode = getPriceCode($arResult["ORDER_PRICE_FORMATED"], $currencyCode);
                        ?>
                        <span class="total-cost-wrapper">Товаров на: 
                            <span id="total-cost"><? echo $priceCode['price']; ?></span>
                            <span class="currency-name"><? echo $priceCode['code']; ?></span>
                            <?/*<span id="total-cost-old" class="byr"><? echo oldPrice($priceCode['price'],'Y'); ?></span>*/?>
                        </span>
                        <? unset($priceCode); ?>
                    </div>
                </div>
                <?
                if (doubleval($arResult["DISCOUNT_PRICE"]) > 0) {
                    ?>
                    <div class="row total-price-row">
                        <div class="col-md-12 text-right cost">
                            <?
                            $priceCode = getPriceCode($arResult["DISCOUNT_PRICE_FORMATED"], $currencyCode);
                            ?>
                            <span class="total-cost-wrapper"><?= GetMessage("SOA_TEMPL_SUM_DISCOUNT") ?>
                                <? if (strLen($arResult["DISCOUNT_PERCENT_FORMATED"]) > 0): ?> (<? echo $arResult["DISCOUNT_PERCENT_FORMATED"]; ?>)<? endif; ?>
                                :<span id="total-cost"><? echo $priceCode['price']; ?></span>
                            <span class="currency-name"><? echo $priceCode['code']; ?></span>
                        </span>
                            <? unset($priceCode); ?>
                        </div>
                    </div>
                <?
                }
                if (!empty($arResult["TAX_LIST"])) {
                    foreach ($arResult["TAX_LIST"] as $val) {
                        ?>
                        <div class="row total-price-row">
                            <div class="col-md-12 text-right cost">
                                <?
                                $priceCode = getPriceCode($val["VALUE_MONEY_FORMATED"], $currencyCode);
                                ?>
                                <span class="total-cost-wrapper"><?= $val["NAME"] ?> <?= $val["VALUE_FORMATED"] ?>:<span
                                        id="total-cost"><? echo $priceCode['price']; ?></span>
                            <span class="currency-name"><? echo $priceCode['code']; ?></span>
                        </span>
                                <? unset($priceCode); ?>
                            </div>
                        </div>
                    <?
                    }
                }
                if (doubleval($arResult["DELIVERY_PRICE"]) > 0) {
                    ?>
                    <div class="row total-price-row">
                        <div class="col-md-12 text-right cost">
                            <?
                            $priceCode = getPriceCode($arResult["DELIVERY_PRICE_FORMATED"], $currencyCode);
                            ?>
                            <span class="total-cost-wrapper"><?= GetMessage("SOA_TEMPL_SUM_DELIVERY") ?> 
                                <span id="total-cost"><? echo $priceCode['price']; ?></span>
                                <span class="currency-name"><? echo $priceCode['code']; ?></span>
                                <?/*<span id="total-cost-old" class="byr"><? echo oldPrice($priceCode['price'],'Y'); ?></span>*/?>
                            </span>
                            <? unset($priceCode); ?>
                        </div>
                    </div>
                <?
                }
                if (strlen($arResult["PAYED_FROM_ACCOUNT_FORMATED"]) > 0) {
                    ?>
                    <div class="row total-price-row">
                        <div class="col-md-12 text-right cost">
                            <?
                            $priceCode = getPriceCode($arResult["PAYED_FROM_ACCOUNT_FORMATED"], $currencyCode);
                            ?>
                            <span class="total-cost-wrapper"><?= GetMessage("SOA_TEMPL_SUM_PAYED") ?><span
                                    id="total-cost"><? echo $priceCode['price']; ?></span>
                            <span class="currency-name"><? echo $priceCode['code']; ?></span>
                        </span>
                            <? unset($priceCode); ?>
                        </div>
                    </div>
                <?
                }?>
                <div class="row total-row">
                    <div class="col-md-12 text-right cost">
                        <?
                        $priceCode = getPriceCode($arResult["ORDER_TOTAL_PRICE_FORMATED"], $currencyCode);
                        ?>
                        <span class="total-cost-wrapper">Итого: 
                            <span id="total-cost"><? echo $priceCode['price']; ?></span>
                            <span class="currency-name"><? echo $priceCode['code']; ?></span>
                            <?/*<span id="total-cost-old" class="byr"><? echo oldPrice($priceCode['price'],'Y'); ?></span>*/?>
                        </span>
                        <? unset($priceCode); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
