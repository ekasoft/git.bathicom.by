<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
if ($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y") {
    if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
        if (strlen($arResult["REDIRECT_URL"]) > 0) {
            $APPLICATION->RestartBuffer();
            ?>
            <script type="text/javascript">
                window.top.location.href = '<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
            </script>
            <?
            die();
        }

    }
}

$APPLICATION->SetAdditionalCSS($templateFolder . "/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder . "/style.css");
CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));

if (!function_exists("getColumnName")) {
    function getColumnName($arHeader)
    {
        return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_" . $arHeader["id"]);
    }
}

if (!function_exists("cmpBySort")) {
    function cmpBySort($array1, $array2)
    {
        if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
            return -1;

        if ($array1["SORT"] > $array2["SORT"])
            return 1;

        if ($array1["SORT"] < $array2["SORT"])
            return -1;

        if ($array1["SORT"] == $array2["SORT"])
            return 0;
    }
}
?>
<div id="order_form_div"><?
if (!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N") {
if (!empty($arResult["ERROR"])) {
    ?>
    <div class="base-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="bg-white b-r_2">
                        <div class="row no-result">
                            <?
                            foreach ($arResult["ERROR"] as $v) {
                                ?>
                                <div class="col-md-12">
                                    <p><span class="sorry"><?= $v ?></span></p>
                                </div>
                            <?
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?
} elseif (!empty($arResult["OK_MESSAGE"])) {
    ?>
    <div class="base-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="bg-white b-r_2">
                        <div class="row no-result">
                            <?
                            foreach ($arResult["OK_MESSAGE"] as $v) {
                                ?>
                                <div class="col-md-12">
                                    <p><span class="sorry"><?= $v ?></span></p>
                                </div>
                            <?
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?
}

include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/auth.php");
} else {
if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
    if (strlen($arResult["REDIRECT_URL"]) == 0) {
        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/confirm.php");
    }
} else {
?>
    <script type="text/javascript">

        <?if(CSaleLocation::isLocationProEnabled()):?>

        <?
        // spike: for children of cities we place this prompt
        $city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
        ?>

/*        BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
					'source' => $this->__component->getPath().'/get.php',
					'cityTypeId' => intval($city['ID']),
					'messages' => array(
						'otherLocation' => '--- '.GetMessage('SOA_OTHER_LOCATION'),
						'moreInfoLocation' => '--- '.GetMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
						'notFoundPrompt' => '<div class="-bx-popup-special-prompt">'.GetMessage('SOA_LOCATION_NOT_FOUND').'.<br />'.GetMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
							'#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
							'#ANCHOR_END#' => '</a>'
						)).'</div>'
					)
				))?>);*/

        <?endif?>

        function showLoader(){
            $('.loading').show();
        }
        function hideLoader(){
            $('.loading').hide();
        }

        var BXFormPosting = false;
        
        function ajaxResult(res) {
            hideLoader();
            var orderForm = BX('ORDER_FORM');
            try {
                // if json came, it obviously a successfull order submit

                var json = JSON.parse(res);
                BX.closeWait();

                if (json.error) {
                    BXFormPosting = false;
                    return;
                }
                else if (json.redirect) {
                    window.top.location.href = json.redirect;
                }
            }
            catch (e) {
                // json parse failed, so it is a simple chunk of html

                BXFormPosting = false;
                BX('order_form_content').innerHTML = res;

                <?if(CSaleLocation::isLocationProEnabled()):?>
//                BX.saleOrderAjax.initDeferredControl();
                <?endif?>
            }

            BX.closeWait();
            BX.onCustomEvent(orderForm, 'onAjaxSuccess');

            $(".phone").mask("+375(99)999-99-99");
            if (window.location.hash == "#step3") {
                $("#basket-step-2").hide();
                $("#basket-step-3").show();
            } else {
                $("#basket-step-2").show();
                $("#basket-step-3").hide();
            }

            /*if ($('#ID_PAY_SYSTEM_ID_1').is(':checked')){
                $('#ID_DELIVERY_ID_2').attr('disabled', 'disabled');
                $('#ID_DELIVERY_ID_2').parent().find('label').attr('onclick', 'javascript:void(0);');
            }*/
            $('.inner-delivery .selectpicker').selectpicker('refresh');
            $('.inner-no-delivery .selectpicker').selectpicker('refresh');

            $.mask.definitions['H']='[012]';
            $.mask.definitions['M']='[012345]';
            $('.time').mask('H9:M9');

            var datesArray = [];
            var d = new Date();
            for (i = -1; i > -390; i--) {
                var tempDay = new Date(); tempDay.setHours(0,0,0,0);
                tempDay.setDate(d.getDate()+i);
                datesArray.push(tempDay.getTime());
            }
            $(function () {
                $('.calendar').pickmeup({
                    position : 'top',
                    hide_on_select : true,
                    //default_date : false,
                    render: function(date) {
                        if ($.inArray(date.getTime(), datesArray) > -1){
                            return {
                                disabled   : true,
                                class_name : 'disabled'
                            }
                        }
                    }
                });
            });
        }

        function SetContact(profileId) {
            BX("profile_change").value = "Y";
            submitForm();
        }
    </script>
<?if ($_POST["is_ajax_post"] != "Y")
{
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form action="<?= $APPLICATION->GetCurPage(); ?>" method="POST" name="ORDER_FORM" id="ORDER_FORM"
                      enctype="multipart/form-data">
                    <?= bitrix_sessid_post() ?>
                    <div id="order_form_content">
                        <?
                        }
                        else {
                            $APPLICATION->RestartBuffer();
                        }

                        if ($_REQUEST['PERMANENT_MODE_STEPS'] == 1) {
                            ?>
                            <input type="hidden" name="PERMANENT_MODE_STEPS" value="1"/>
                        <?
                        }

                        if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y") {
                            ?>
                            <div class="base-page">
                                <div class="container m-b-40">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="bg-white b-r_2">
                                                <div class="row no-result">
                                                    <?
                                                    foreach ($arResult["ERROR"] as $v) {
                                                        ?>
                                                        <div class="col-md-12">
                                                            <p><span class="sorry"><?= $v ?></span></p>
                                                        </div>
                                                    <?
                                                    }
                                                    ?>
                                                    <script type="text/javascript">
                                                        top.BX.scrollToNode(top.BX('ORDER_FORM'));
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?
                        }

                        ?>

                        <div class="base-page" id="basket-step-2">
                            <? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/summary.php"); ?>
                            <!---->
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span class="title_h1">Оплата заказа</span>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="bg-white b-r_2">
                                            <div class="row pay-row">
                                                <?if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d") {
                                                    include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
                                                    include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php");
                                                } else {
                                                    include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/delivery.php");
                                                    include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/paysystem.php");
                                                }?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row step-row">
                                    <div class="col-xs-4 col-sm-6 col-md-6">
                                        <a href="/cart/" class="step-back delay_04s">Шаг назад</a>
                                    </div>
                                    <div class="col-xs-8 col-sm-6 col-md-6 text-right">
                                        <span class="step-name">Шаг 2 из 3</span>
                                        <a href="javascript:void(0);" onclick="step3();"
                                           class="main-blue-btn next-step delay_04s">Следующий
                                            шаг</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="base-page" id="basket-step-3">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h1>Оформление заказа</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <? include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/person_type.php"); ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="bg-white b-r_2">
                                            <div class="row order-placement">
                                                <?
                                                include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/props.php");
                                                ?>
                                            </div>
                                        </div>
                                        <div class="row step-row">
                                            <div class="col-xs-4 col-sm-6 col-md-6">
                                                <a href="javascript:void(0);" onclick="step2();" class="step-back delay_04s">Шаг назад</a>
                                            </div>
                                            <div class="col-xs-8 col-sm-6 col-md-6 text-right">
                                                <span class="step-name">Шаг 3 из 3</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?
                        //                include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/related_props.php");
                        if (strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
                            echo $arResult["PREPAY_ADIT_FIELDS"];
                        ?>

                        <?if ($_POST["is_ajax_post"] != "Y")
                        {
                        ?>
                    </div>
                    <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
                    <input type="hidden" name="profile_change" id="profile_change" value="N">
                    <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
                    <input type="hidden" name="json" value="Y">
                    <script>
                        $(document).ready(function () {
                            if (window.location.hash == "#step3") {
                                $("#basket-step-2").hide();
                                $("#basket-step-3").show();
                            } else {
                                $("#basket-step-2").show();
                                $("#basket-step-3").hide();
                            }
                        });

                        function step2() {
                            $("#basket-step-2").show();
                            $("#basket-step-3").hide();
                            window.location.hash = '#step2';
                            top.BX.scrollToNode(top.BX('basket-step-2'));
                        }
                        function step3() {
                            $("#ORDER_FORM").valid();
                            var isValid = false;
                            if ($('.inner-no-delivery').length > 0){
                                if ($('#no-delivery-form input.error').length == 0){
                                    isValid = true;
                                }
                            }  else if ($('.inner-delivery').length > 0) {
                                if ($('#delivery-form input.error').length == 0){
                                    isValid = true;
                                }
                            }
                            if (isValid){
                                $('#sale_order_props input.error').removeClass('error');
                                $("#basket-step-2").hide();
                                $("#basket-step-3").show();
                                window.location.hash = 'step3';
                                top.BX.scrollToNode(top.BX('basket-step-3'));
                            }
                        }
                    </script>
                    <div class="loading">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/loader3.gif" alt=""/>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?
} else {
?>
    <script type="text/javascript">
        top.BX('confirmorder').value = 'Y';
        top.BX('profile_change').value = 'N';
    </script>
    <?
    die();
}
}
}
?>

</div>