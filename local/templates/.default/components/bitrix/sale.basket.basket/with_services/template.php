<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixBasketComponent $component */
$curPage = $APPLICATION->GetCurPage() . '?' . $arParams["ACTION_VARIABLE"] . '=';
// удаляем услуги удаленных товаров
if (count($arResult["ITEMS"]["nAnCanBuy"]) > 0){
    CModule::IncludeModule('sale');
    foreach ($arResult["ITEMS"]["nAnCanBuy"] as $k => $arItem){
        CSaleBasket::Delete($arItem['ID']);
    }
    LocalRedirect($APPLICATION->GetCurPage());
}

$arUrls = array(
    "delete" => $curPage . "delete&id=#ID#",
    "delay" => $curPage . "delay&id=#ID#",
    "add" => $curPage . "add&id=#ID#",
);
unset($curPage);

$arBasketJSParams = array(
    'SALE_DELETE' => GetMessage("SALE_DELETE"),
    'SALE_DELAY' => GetMessage("SALE_DELAY"),
    'SALE_TYPE' => GetMessage("SALE_TYPE"),
    'TEMPLATE_FOLDER' => $templateFolder,
    'DELETE_URL' => $arUrls["delete"],
    'DELAY_URL' => $arUrls["delay"],
    'ADD_URL' => $arUrls["add"]
);
$APPLICATION->AddHeadScript($templateFolder . "/script.js");
?>
<script type="text/javascript">
    var basketJSParams =
    <?=CUtil::PhpToJSObject($arBasketJSParams);?>
</script>

<div class="base-page" id="basket-step-1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Корзина товаров</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="bg-white b-r_2">
                    <div class="row basket-error">
                        <div class="col-md-12">
                            <p><span class="sorry" id="warning_message"></span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?
        if (strlen($arResult["ERROR_MESSAGE"]) <= 0) {
        ?>
        <?
        if (!empty($arResult["WARNING_MESSAGE"]) && is_array($arResult["WARNING_MESSAGE"])) {
            ?>
            <div class="row">
            <div class="col-md-12">
                <div class="bg-white b-r_2">
                    <div class="row no-result">
                        <div class="col-md-12">
                            <p><span class="sorry">
                                    <?
                                    foreach ($arResult["WARNING_MESSAGE"] as $v)
                                        ShowError($v);
                                    ?>
                            </span></p>
                        </div>
                    </div>
                </div>
            </div>
            </div><?
        }
        ?>
    </div><?

    $normalCount = count($arResult["ITEMS"]["AnDelCanBuy"]);
    $normalHidden = ($normalCount == 0) ? 'style="display:none;"' : '';

    $delayCount = count($arResult["ITEMS"]["DelDelCanBuy"]);
    $delayHidden = ($delayCount == 0) ? 'style="display:none;"' : '';

    $subscribeCount = count($arResult["ITEMS"]["ProdSubscribe"]);
    $subscribeHidden = ($subscribeCount == 0) ? 'style="display:none;"' : '';

    $naCount = count($arResult["ITEMS"]["nAnCanBuy"]);
    $naHidden = ($naCount == 0) ? 'style="display:none;"' : '';

    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form method="post" action="<?= POST_FORM_ACTION_URI ?>" name="basket_form" id="basket_form">
                    <div id="basket_form_container">
                        <?
                        include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items.php");
                        //include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items_delayed.php");
                        //include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items_subscribed.php");
                        //include($_SERVER["DOCUMENT_ROOT"] . $templateFolder . "/basket_items_not_available.php");
                        ?>
                    </div>
                    <input type="hidden" name="BasketOrder" value="BasketOrder"/>
                    <!-- <input type="hidden" name="ajax_post" id="ajax_post" value="Y"> -->
                    <div class="loading">
                        <img src="<?= SITE_TEMPLATE_PATH ?>/img/loader3.gif" alt=""/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
//        var TEMPLATE_BASKET_PATH = '<?=SITE_TEMPLATE_PATH?>/components/bitrix/sale.basket.basket/with_services';
          var TEMPLATE_BASKET_PATH = '/local/templates/.default/components/bitrix/sale.basket.basket/with_services';
    </script>

    <?
    } else {
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="bg-white b-r_2">
                <div class="row no-result">
                    <div class="col-md-12">
                        <p><span class="sorry"><?= $arResult["ERROR_MESSAGE"] ?></span></p>
						<p><span class="sorry"><a href="/katalog/plitka/">Вернуться к покупкам</a></span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><?
}
?>
</div>