<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="search-result-block" style="display: block;">
    <?$APPLICATION->IncludeComponent("ekasoft.base:search", "", array(
        "ELEMENTS_ID" => $arResult["ELEMENTS_ID"],
        "SEARCH_SECTION_ID" => $_REQUEST["SEARCH_SECTION_ID"],
        "QUERY" => $arResult["REQUEST"]["QUERY"],
        "AJAX_MODE" => "Y",
        "AJAX_OPTION_HISTORY" => "N"
        ))?>
</div>
