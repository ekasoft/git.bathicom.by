<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="base-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?=$arResult["NAME"]?></h1>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="bg-white padd-30 b-r_2">
                    <?echo $arResult["PREVIEW_TEXT"];?>
                </div>
            </div>
        </div>
    </div>
</div>