<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

if (0 < $arResult["SECTIONS_COUNT"]) {
    ?>
    <div class="container main-buttons-box ind-buttons-box">
        <div class="row">
            <?
            $countSectionLevel1 = 0;
            foreach ($arResult['SECTIONS'] as $k => &$arSection) {
                $class = ($countSectionLevel1 >= 12) ? "more" : "";
                if ($arSection['DEPTH_LEVEL'] > 1) {
                    continue;
                }
                $countSectionLevel1++;
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                $color = !empty($arSection['UF_COLOR']) ? $arSection['UF_COLOR'] : 'blue';

                $img_white = "";
                if ($arSection['UF_IMAGE_WHITE'] > 0) {
                    $y = CFile::ResizeImageGet($arSection['UF_IMAGE_WHITE'], array("width" => 40, "height" => 40), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false);
                    $img_white = $y['src'];
                }
                $img_color = "";
                if ($arSection['UF_IMAGE_COLOR'] > 0) {
                    $y = CFile::ResizeImageGet($arSection['UF_IMAGE_COLOR'], array("width" => 40, "height" => 40), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false);
                    $img_color = $y['src'];
                }

                $depthLevelExist = (isset($arResult['SECTIONS'][$k + 1]) &&
                    intval($arResult['SECTIONS'][$k + 1]['DEPTH_LEVEL']) > 1
                );
                ?>
                <div class="col-xs-12 col-sm-6 col-md-3 <?= $class ?>"
                     id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                    <a
                        <? if (!$depthLevelExist): ?>href="<?= $arSection['SECTION_PAGE_URL'] ?>"<? endif; ?>
                        class="but but<?= $k + 1 ?> but-<?= $color ?> bg bg-<?= $color ?> delay_04s">
                        <!-- <span class="img"></span> -->
                        <div class="img-block">
                            <img src="<?= $img_white ?>" alt="">
                            <img src="<?= $img_color ?>" alt="">
                        </div>
                        <span class="text"><? echo $arSection['NAME']; ?></span>
                        <span class="clearfix"></span>
                    </a>

                    <?
                    if ($depthLevelExist) {
                        ?>
                        <div class="sub-cat-box bg-<?= $color ?> active">
                            <ul>
                                <? for ($i = $k + 1; $i < count($arResult['SECTIONS']); $i++): ?>
                                    <?
                                    $sectionLevel2 = $arResult['SECTIONS'][$i];
                                    $this->AddEditAction($sectionLevel2['ID'], $sectionLevel2['EDIT_LINK'], $strSectionEdit);
                                    $this->AddDeleteAction($sectionLevel2['ID'], $sectionLevel2['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                                    if ($sectionLevel2['DEPTH_LEVEL'] <= 1) {
                                        break;
                                    }
                                    ?>
                                    <li class="col-xs-12 col-sm-6 col-md-2"
                                        id="<? echo $this->GetEditAreaId($sectionLevel2['ID']); ?>">
                                        <a href="<?= $sectionLevel2['SECTION_PAGE_URL'] ?>">
                                            <?= $sectionLevel2['NAME'] ?></a>
                                    </li>
                                <? endfor; ?>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    <?
                    }
                    ?>
                </div>
            <?
            }
            unset($arSection);
            ?>
        </div>
    </div>

    <div class="container mosaic-button-row">
        <div class="row">
            <div class="col-md-12">
                <div class="button-wrapper">
                    <? if ($countSectionLevel1 > 12): ?>
                        <!-- mosaic button -->
                        <a class="delay_04s">
                            <span>Показать все позиции</span>
                        </a>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </div>
<?
}
?>
<!--  -->