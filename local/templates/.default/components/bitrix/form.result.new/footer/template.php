<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>
<div class="modal modal-vcenter fade" id="<?= $arResult['WEB_FORM_NAME'] ?>-popup">
    <? if ($_REQUEST['formresult'] == "addok" && $arResult["isFormErrors"] != "Y"): ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#ok-popup').modal('show');
            });
        </script>
    <? endif; ?>
    <? if ($arResult["isFormErrors"] == "Y"): ?>
        <?//=$arResult["FORM_ERRORS_TEXT"]; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#ok-popup').modal('show');
            });
        </script>
    <? endif; ?>

    <div class="modal-dialog min-popup">
        <div class="modal-content">
            <div class="modal-cont-wrap">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="modal-title"><?= $arResult['FORM_TITLE'] ?></span>
                </div>
                <div class="modal-body">
                    <?
                    $formH = $arResult["FORM_HEADER"];
                    $formH = str_replace('<form', '<form id="' . $arResult['WEB_FORM_NAME'] . '-popup-form"', $formH);
                    ?>
                    <?= $formH ?>
                    <span class="warning-message">Все поля обязательны для заполнения</span>
                    <? foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion): ?>
                        <? if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'): ?>
                            <?= $arQuestion["HTML_CODE"] ?>
                        <? else: ?>
                            <label><?= $arQuestion["CAPTION"] ?></label>
                            <?= $arQuestion["HTML_CODE"] ?>
                        <? endif; ?>
                    <? endforeach; ?>
                    <a class="<?=$arResult['WEB_FORM_NAME']?> main_btn btn_validate delay_04s">
                                    <span class="text">
                                        <span class="img"></span>
                                        <?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>
                                    </span>
                    </a>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#<?=$arResult['WEB_FORM_NAME'].'-popup-form a.'.$arResult['WEB_FORM_NAME']?>').click(function(){
                                $form = $('#<?=$arResult['WEB_FORM_NAME'].'-popup-form'?>');
                                $form.submit();
                            });
                        });
                    </script>
                    <input type="hidden" name="web_form_submit"
                           value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"/>
                    <?= $arResult["FORM_FOOTER"] ?>
                </div>
            </div>
        </div>
    </div>
</div>
