<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS'])) {
    $strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
    $strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
    $arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="bg-white b-r_2">
                    <div class="row">
                        <div class="col-xs-12 similar-goods-slider-wrapper">
                            <span class="title_h2">Аксессуары и полезные товары</span>

                            <div id="add-product-slider">
                                <?
                                foreach ($arResult['ITEMS'] as $key => $arItem) {
                                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
                                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
                                    $strMainID = $this->GetEditAreaId($arItem['ID']);

                                    $productTitle = (
                                    isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
                                        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
                                        : $arItem['NAME']
                                    );
                                    $imgTitle = (
                                    isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
                                        ? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
                                        : $arItem['NAME']
                                    );

                                    $minPrice = false;
                                    $price = "";
                                    if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
                                        $minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);

                                    if (!empty($minPrice)) {
                                        if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
                                            $price = GetMessage(
                                                'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
                                                array(
                                                    '#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE'],
                                                    '#MEASURE#' => GetMessage(
                                                        'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
                                                        array(
                                                            '#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
                                                            '#UNIT#' => $minPrice['CATALOG_MEASURE_NAME']
                                                        )
                                                    )
                                                )
                                            );
                                        } else {
                                            $currencyCode = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE']['CURRENCY'] : $arItem['MIN_PRICE']['CURRENCY']);
                                            $priceCode = getPriceCode($minPrice['PRINT_DISCOUNT_VALUE'], $currencyCode);
                                            $price = $priceCode['price'] . "<span>" . $priceCode['code'] . "</span>";
                                        }
                                    }
                                    unset($minPrice);
                                    $isDiscount = (!empty($arItem['PROPERTIES']['SPECIALOFFER']['VALUE']));
                                    $previewText = $arItem['~PREVIEW_TEXT'];

                                    $arFirstPhoto = $arItem['PRODUCT_PREVIEW'];
                                    if (isset($arFirstPhoto['ID'])) {
                                        $width = 240;
                                        $height = 274;
                                        $y = CFile::ResizeImageGet($arFirstPhoto['ID'], array("width" => $width, "height" => $height), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false);
                                        $big_photo = $y['src'];
                                    } else {
                                        $big_photo = $arFirstPhoto['SRC'];
                                    }

                                    /*if (isset($arItem['OFFERS']) && !empty($arItem['OFFERS'])) {
                                        $offerSelected = $arItem['OFFERS'][$arItem['OFFERS_SELECTED']];
                                        $isAvailable = (intval($offerSelected['CATALOG_QUANTITY']) > 0);
                                        unset($offerSelected);
                                    } else {
                                        $isAvailable = (intval($arItem['CATALOG_QUANTITY']) > 0);
                                    }*/
                                    $isAvailable = (!empty($arItem['PROPERTIES']['AVAILABLE']['VALUE']));
                                    ?>
                                    <div class="slide" id="<? echo $strMainID; ?>">
                                        <div class="product-card-min delay_04s">
                                            <div class="top-row">
                                                <span
                                                    class="discount <? if ($isDiscount): ?>active<? endif; ?>">%</span>
                    <span
                        class="status status-green <? if ($isAvailable): ?>active<? endif; ?>">В наличии</span>
                    <span
                        class="status status-orange <? if (!$isAvailable): ?>active<? endif; ?>">Под заказ</span>
                                            </div>
                                            <div class="img">
                                                <img src="<?= $big_photo ?>" alt="<? echo $imgTitle; ?>">
                                            </div>
                                            <div class="description-box">
                                                <div class="product-name-wrapper">
                                                    <span
                                                        class="product-name delay_04s"><? echo $productTitle; ?></span>
                                                </div>
                                                <span class="product-description"><?= $previewText ?></span>
                                                <span class="cost"><?= $price ?></span>
                                            </div>
                                            <a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"></a>
                                        </div>
                                    </div>
                                <?
                                } ?>
                            </div>
                            <a id="add-product-slide-prev" href="#"></a>
                            <a id="add-product-slide-next" href="#"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?
}