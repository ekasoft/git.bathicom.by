<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
CModule::IncludeModule('currency');

function showProp($arItem, $key)
{
    if ($arItem["ID"] == "50"){ // color
        $arItem['DISPLAY_TYPE'] = "R";
    }
    if (isset($arItem["PRICE"]) &&
        $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] > 0
    ) { // PRICE
        $key = $arItem["ENCODED_ID"];
        $precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
        ?>
        <div class="filter-item cost-item">
            <span class="filter-name">Цена:</span>

            <div class="ui-cost-wrapper">
                <span class="sub-val-min left">от<span class="ui-val-min" id="val-min-<?= $key ?>"></span></span>
                <span class="sub-val-max right">до<span class="ui-val-max" id="val-max-<?= $key ?>"></span></span>

                <div class="clearfix"></div>
                <div class="cost-slider" id="cost-slider-<?= $key ?>"></div>
                <?
                $currency = CCurrency::GetBaseCurrency();
                $currencyLang = CCurrencyLang::GetByID($currency, LANGUAGE_ID);
                $currencyCode = str_replace("#", "", $currencyLang['FORMAT_STRING']);
                ?>
                <span class="currency-name"><?= $currencyCode ?></span>
            </div>
            <input
                type="hidden"
                name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
                value="<? echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                />
            <input
                type="hidden"
                name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                value="<? echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                />

            <script type="text/javascript">
                $(document).ready(function () {
                    /* === NOUISLIDER === */
                    if ($(".cost-slider#cost-slider-<?=$key?>").length) {
                        var opt_min = <?=intval($arItem["VALUES"]["MIN"]["VALUE"])?>,
                            opt_max = <?=intval($arItem["VALUES"]["MAX"]["VALUE"])?>,
                            opt_start_min = <?=(intval($arItem["VALUES"]["MIN"]["HTML_VALUE"]) > 0) ? intval($arItem["VALUES"]["MIN"]["HTML_VALUE"]) : intval($arItem["VALUES"]["MIN"]["VALUE"])?>,
                            opt_start_max = <?=(intval($arItem["VALUES"]["MAX"]["HTML_VALUE"]) > 0) ? intval($arItem["VALUES"]["MAX"]["HTML_VALUE"]) : intval($arItem["VALUES"]["MAX"]["VALUE"])?>;
                        $("#cost-slider-<?=$key?>").noUiSlider({
                            start: [ opt_start_min, opt_start_max ],
                            connect: true,
                            range: {
                                'min': [ opt_min ],
                                'max': [ opt_max ]
                            },
                            step: 1000,
                            serialization: {
                                lower: [
                                    $.Link({
                                        target: $("#val-min-<?=$key?>")
                                    })
                                ],
                                upper: [
                                    $.Link({
                                        target: $("#val-max-<?=$key?>")
                                    })
                                ],
                                format: {
                                    mark: ',',
                                    decimals: <?=$precision?>
                                }
                            }
                        });
                        $("#cost-slider-<?=$key?>").on('slide', function (event, values) {
                            if (values[0] == opt_min) {
                                $("#<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>").val("");
                            } else {
                                $("#<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>").val(values[0]);
                            }
                            if (values[1] == opt_max) {
                                $("#<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>").val("");
                            } else {
                                $("#<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>").val(values[1]);
                            }
                        });
                        /*reset ui-slider*/
                        /*$('#but1-main-filter').find('input.reset').on('click', function (e) {
                         e.preventDefault();
                         $("#cost-slider-
                        <?=$key?>").val([opt_min, opt_max], true);
                         });*/
                    }
                });
            </script>
        </div>
        <?
        return true;
    } else {
        if (!empty($arItem["VALUES"]) &&
            !(
                ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0)) ||
                ($arItem["DISPLAY_TYPE"] == "B" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
            )
        ) {
            $arCur = current($arItem["VALUES"]);
            switch ($arItem["DISPLAY_TYPE"]) {
                case "A": //NUMBERS_WITH_SLIDER
                case "B": //NUMBERS
                    ?>
                    <?
                    $stepCount = 5;
                    $step = (intval($arItem["VALUES"]["MAX"]["VALUE"]) - intval($arItem["VALUES"]["MIN"]["VALUE"])) / $stepCount;
                    ?>
                    <div class="filter-item">
                        <span class="filter-name"><?= $arItem["NAME"] ?>:</span>

                        <div class="box">
                            <span class="sub-name">от</span>
                            <select class="selectpicker form-control small-select min"
                                    name="<? echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                    id="<? echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>">
                                <? for ($i = $arItem["VALUES"]["MIN"]["VALUE"]; $i <= $arItem["VALUES"]["MAX"]["VALUE"]; $i += $step): ?>
                                    <? $selected = ($arItem["VALUES"]["MIN"]["HTML_VALUE"] == $i && !empty($arItem["VALUES"]["MIN"]["HTML_VALUE"])) ? 'selected="selected"' : '' ?>
                                    <? if ($i + $step > intval($arItem["VALUES"]["MAX"]["VALUE"])): ?>
                                        <option <?= $selected ?>
                                            value="<?= intval($arItem["VALUES"]["MAX"]["VALUE"]) ?>"><?= intval($arItem["VALUES"]["MAX"]["VALUE"]) ?></option>
                                    <? else : ?>
                                        <option <?= $selected ?> value="<?= intval($i) ?>"><?= intval($i) ?></option>
                                    <?endif; ?>
                                <? endfor; ?>
                            </select>
                            <span class="sub-name">до</span>
                            <select class="selectpicker form-control small-select max"
                                    name="<? echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                    id="<? echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>">
                                <? for ($i = $arItem["VALUES"]["MAX"]["VALUE"]; $i >= $arItem["VALUES"]["MIN"]["VALUE"]; $i -= $step): ?>
                                    <? $selected = ($arItem["VALUES"]["MAX"]["HTML_VALUE"] == $i && !empty($arItem["VALUES"]["MAX"]["HTML_VALUE"])) ? 'selected="selected"' : '' ?>
                                    <? if ($i - $step < intval($arItem["VALUES"]["MIN"]["VALUE"])): ?>
                                        <option <?= $selected ?>
                                            value="<?= intval($arItem["VALUES"]["MIN"]["VALUE"]) ?>"><?= intval($arItem["VALUES"]["MIN"]["VALUE"]) ?></option>
                                    <? else : ?>
                                        <option <?= $selected ?> value="<?= intval($i) ?>"><?= intval($i) ?></option>
                                    <?endif; ?>
                                <? endfor; ?>
                            </select>
                            <span class="sub-name"></span>
                        </div>
                    </div>
                    <?
                    break;
                case "F": //CHECKBOXES
                case "G": //CHECKBOXES_WITH_PICTURES
                case "H": //CHECKBOXES_WITH_PICTURES_AND_LABELS
                    foreach ($arItem["VALUES"] as $val => $ar): ?>
                        <div class="filter-item">
                            <div class="custom_check">
                                <input
                                    type="checkbox"
                                    name="<?= $ar["CONTROL_NAME"] ?>"
                                    id="<?= $ar["CONTROL_ID"] ?>"
                                    value="<?= $ar["HTML_VALUE"] ?>"
                                    <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                    />
                                <label for="<?= $ar["CONTROL_ID"] ?>"><?= $arItem["NAME"] ?></label>
                            </div>
                        </div>
                    <? endforeach ?>
                    <?
                    break;
                case "K": //RADIO_BUTTONS
                case "P": //DROPDOWN
                    ?>
                    <div class="filter-item">
                        <span class="filter-name"><?= $arItem["NAME"] ?>:</span>
                        <select class="selectpicker form-control" name="<?= $arCur["CONTROL_NAME_ALT"] ?>">
                            <option value="">Не выбрано</option>
                            <? foreach ($arItem["VALUES"] as $val => $ar): ?>
                                <option
                                    value="<?= $ar['HTML_VALUE_ALT'] ?>" <? echo $ar["CHECKED"] ? 'selected="selected"' : '' ?>>
                                    <? echo $ar["VALUE"] ?>
                                </option>
                            <? endforeach ?>
                        </select>
                    </div>
                    <?
                    break;
                case "R": //DROPDOWN_WITH_PICTURES_AND_LABELS // COLOR
                    ?>
                    <div class="filter-item">
                        <span class="filter-name"><?= $arItem["NAME"] ?>:</span>
                        <?
                        //                        $colors = getColors();
                        $checkedColor = array();
                        foreach ($arItem["VALUES"] as $val => $ar):
                            if ($ar["CHECKED"]) {
                                $checkedColor = $ar;
                                break;
                            }
                        endforeach ?>
                        <input type="hidden" name="<?= $arCur["CONTROL_NAME_ALT"] ?>"
                               value="<?= $checkedColor['HTML_VALUE_ALT'] ?>"/>

                        <div class="color-select-box">
                            <div class="select-color">
                                <span class="selected">
                                    <? if (empty($checkedColor)): ?>
                                        <a><span class="color-name">Не выбран</span></a>
                                    <? else: ?>
                                        <a>
                                            <? if (strpos($checkedColor['URL_ID'], "#") !== false): ?>
                                                <span class="color"
                                                      style="background: <?= $checkedColor['URL_ID'] ?>;"></span>
                                            <? endif; ?>
                                            <span class="color-name"><?= $checkedColor['VALUE'] ?></span>
                                        </a>
                                    <?endif; ?>
                                </span>
                                <span class="caret"></span>

                                <div class="select-opt-color custom-scroll">
                                    <a data-value=""><span class="color-name">Не выбран</span></a>
                                    <? foreach ($arItem["VALUES"] as $val => $ar): ?>
                                        <a data-value="<?= $ar['HTML_VALUE_ALT'] ?>">
                                            <? if (strpos($ar['URL_ID'], "#") !== false): ?>
                                                <span class="color delay_04s"
                                                      style="background: <?= $ar['URL_ID'] ?>;"></span>
                                            <? endif; ?>
                                            <span class="color-name"><?= $ar["VALUE"] ?></span>
                                        </a>
                                    <? endforeach ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?
                    break;
                case "U": //CALENDAR
                    break;
                default:
                    break;
            }
            return true;
        }
    }
    return false;
}


$isFilterApply = isset($_REQUEST['set_filter']);
$arItemProps = array();
?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="filter-wrapper">
                <form id="but1-main-filter" name="<? echo $arResult["FILTER_NAME"] . "_form" ?>"
                      action="<? echo $arResult["FORM_ACTION"] ?>" method="get">
                    <? foreach ($arResult["HIDDEN"] as $arItem): ?>
                        <input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>"
                               id="<? echo $arItem["CONTROL_ID"] ?>" value="<? echo $arItem["HTML_VALUE"] ?>"/>
                    <? endforeach; ?>
                    <div class="main-catalogue-filter">
                        <span class="title_h3">Фильтр</span>

                        <div class="filter-items-row">
                            <?
                            foreach ($arResult["ITEMS"] as $key => $arItem) {
                                if ($arItem['CODE'] == "BRAND_REF") {
                                    $arItemProps['BRAND'] = array(
                                        'item' => $arItem,
                                        'key' => $key
                                    );
                                } elseif ($arItem['CODE'] == "COLLECTION_REF") {
                                    $arItemProps['COLLECTION'] = array(
                                        'item' => $arItem,
                                        'key' => $key
                                    );
                                } elseif ($arItem['CODE'] == "SPECIALOFFER") {
                                    $arItemProps['SPECIALOFFER'] = array(
                                        'item' => $arItem,
                                        'key' => $key
                                    );
                                } elseif (isset($arItem["PRICE"])) {
                                    $arItemProps['PRICE'] = array(
                                        'item' => $arItem,
                                        'key' => $key
                                    );
                                } else {
                                    $arItemProps['OTHER'][] = array(
                                        'item' => $arItem,
                                        'key' => $key
                                    );
                                }
                            }

                            if (!empty($arItemProps['BRAND'])) {
                                $arItemProps['BRAND']['result'] = showProp($arItemProps['BRAND']['item'], $arItemProps['BRAND']['key']);
                            }
                            if (!empty($arItemProps['COLLECTION'])) {
                                $arItemProps['COLLECTION']['result'] = showProp($arItemProps['COLLECTION']['item'], $arItemProps['BRAND']['key']);
                            }
                            ?>

                            <input class="main-blue-btn delay_04s" type="submit" id="set_filter" name="set_filter"
                                   value="Применить"/>
                            <? if ($isFilterApply): ?>
                                <input class="reset delay_04s" type="submit" id="del_filter" name="del_filter"
                                       value="Очистить фильтр"/>
                            <? endif; ?>
                            <?
                            if ((empty($arItemProps['BRAND']) || !$arItemProps['BRAND']['result']) &&
                                (empty($arItemProps['COLLECTION']) || !$arItemProps['COLLECTION']['result'])
                            ) {
                                $isFilterApply = true;
                            }
                            ?>
                            <a class="show-add-filter right <? if ($isFilterApply): ?>active<? endif; ?>"><span>Расширенный фильтр</span></a>

                            <div class="clearfix"></div>
                        </div>

                    </div>
                    <div class="additional-filter-wrapper"
                         <? if ($isFilterApply): ?>style="display: block;"<? endif; ?>>
                        <? if (!empty($arItemProps['OTHER'])): ?>
                            <div class="filter-items-row">
                                <?
                                foreach ($arItemProps['OTHER'] as $key => $arProp) {
                                    showProp($arProp['item'], $arProp['key']);
                                }
                                ?>
                            </div>
                        <? endif; ?>

                        <? if (!empty($arItemProps['PRICE']) || !empty($arItemProps['SPECIALOFFER'])): ?>
                            <div class="filter-items-row">
                                <?
                                if (!empty($arItemProps['PRICE'])) {
                                    showProp($arItemProps['PRICE']['item'], $arItemProps['PRICE']['key']);
                                }
                                if (!empty($arItemProps['SPECIALOFFER'])) {
                                    showProp($arItemProps['SPECIALOFFER']['item'], $arItemProps['SPECIALOFFER']['key']);
                                }
                                ?>
                            </div>
                        <? endif; ?>
                        <!--<div class="filter-items-row">
                            <div class="filter-item">
                                <input class="main-blue-btn delay_04s" type="submit" id="set_filter" name="set_filter"
                                       value="Применить"/>
                                <input class="reset delay_04s" type="submit" id="del_filter" name="del_filter"
                                       value="Очистить фильтр"/>
                            </div>
                        </div>!-->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    //var smartFilter = new JCSmartFilter('
    <?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '
    <?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>',
    <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>