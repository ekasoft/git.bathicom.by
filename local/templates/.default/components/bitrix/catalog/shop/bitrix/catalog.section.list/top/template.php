<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

$sectionPath = false;
if (!empty($arParams['CURRENT_SECTION'])){
    $db_list_section = CIBlockSection::GetList(array($by => $order), array(
        'IBLOCK_ID' => $arParams["IBLOCK_ID"],
        'CODE' => $arParams['CURRENT_SECTION']
    ), false);
    $sections = array();
    if ($ar_result_section = $db_list_section->GetNext()) {
        $nav = CIBlockSection::GetNavChain(false, $ar_result_section['ID']);
        while ($arItem = $nav->Fetch()) {
            $sections[] = $arItem['CODE'];
        }
        $sectionPath = implode("/", $sections);
    }
}

if (0 < $arResult["SECTIONS_COUNT"]) {
    ?>
    <div class="container main-buttons-box">
        <div class="row">
            <?
            foreach ($arResult['SECTIONS'] as $k => &$arSection) {
                $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
                $color = !empty($arSection['UF_COLOR']) ? $arSection['UF_COLOR'] : 'blue';
                $active = (stripos($sectionPath, $arSection['CODE']) !== false) ? 'active' : '';

                $img_white = "";
                if ($arSection['UF_IMAGE_WHITE'] > 0){
                    $y = CFile::ResizeImageGet($arSection['UF_IMAGE_WHITE'], array("width" => 40, "height" => 40), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false );
                    $img_white = $y['src'];
                }
                $img_color = "";
                if ($arSection['UF_IMAGE_COLOR'] > 0){
                    $y = CFile::ResizeImageGet($arSection['UF_IMAGE_COLOR'], array("width" => 40, "height" => 40), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false );
                    $img_color = $y['src'];
                }
                ?>
                <div class="col-xs-12 col-sm-6 col-md-3" id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                    <a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
                       class="but but<?= $k + 1 ?> but-<?= $color ?> delay_02s <?= $active ?>">
                        <div class="img-block">
                            <img src="<?= $img_white ?>" alt="">
                            <img src="<?= $img_color ?>" alt="">
                        </div>
                        <span class="text"><? echo $arSection['NAME']; ?></span>
                        <span class="clearfix"></span>
                    </a>
                </div>
            <?
            }
            unset($arSection);
            ?>
        </div>
    </div>
<?
}
?>