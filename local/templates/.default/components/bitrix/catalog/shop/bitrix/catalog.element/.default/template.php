<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder() . '/themes/' . $arParams['TEMPLATE_THEME'] . '/style.css',
    'TEMPLATE_CLASS' => 'bx_' . $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID . '_pict',
    'DISCOUNT_PICT_ID' => $strMainID . '_dsc_pict',
    'STICKER_ID' => $strMainID . '_sticker',
    'BIG_SLIDER_ID' => $strMainID . '_big_slider',
    'BIG_IMG_CONT_ID' => $strMainID . '_bigimg_cont',
    'SLIDER_CONT_ID' => $strMainID . '_slider_cont',
    'SLIDER_LIST' => $strMainID . '_slider_list',
    'SLIDER_LEFT' => $strMainID . '_slider_left',
    'SLIDER_RIGHT' => $strMainID . '_slider_right',
    'OLD_PRICE' => $strMainID . '_old_price',
    'PRICE' => $strMainID . '_price',
    'DISCOUNT_PRICE' => $strMainID . '_price_discount',
    'SLIDER_CONT_OF_ID' => $strMainID . '_slider_cont_',
    'SLIDER_LIST_OF_ID' => $strMainID . '_slider_list_',
    'SLIDER_LEFT_OF_ID' => $strMainID . '_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID . '_slider_right_',
    'QUANTITY' => $strMainID . '_quantity',
    'QUANTITY_DOWN' => $strMainID . '_quant_down',
    'QUANTITY_UP' => $strMainID . '_quant_up',
    'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
    'QUANTITY_LIMIT' => $strMainID . '_quant_limit',
    'BASIS_PRICE' => $strMainID . '_basis_price',
    'BUY_LINK' => $strMainID . '_buy_link',
    'ADD_BASKET_LINK' => $strMainID . '_add_basket_link',
    'BASKET_ACTIONS' => $strMainID . '_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
    'COMPARE_LINK' => $strMainID . '_compare_link',
    'PROP' => $strMainID . '_prop_',
    'PROP_DIV' => $strMainID . '_skudiv',
    'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
    'OFFER_GROUP' => $strMainID . '_set_group_',
    'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
    : $arResult['NAME']
);
$strAlt = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
    : $arResult['NAME']
);

if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
    $canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
} else {
    $canBuy = $arResult['CAN_BUY'];
}
$buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
$addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
$notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

$showSubscribeBtn = false;
$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));

?>

<div class="container" id="<? echo $arItemIDs['ID']; ?>">
<div class="row">
<div class="col-md-12">
<div class="bg-white b-r_2">
<div class="row product-card">
<?
$isNoPhoto = false;
reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);
?>
<div class="col-xs-12 col-sm-12 col-md-4 product-slider-wrapper"
     id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
    <div id="carousel-wrapper">
        <div id="<? echo $arItemIDs['BIG_IMG_CONT_ID']; ?>">
            <div id="carousel">
                <? foreach ($arResult['MORE_PHOTO'] as $key => &$arOnePhoto): ?>
                    <?
                    if (isset($arOnePhoto['ID'])) {
                        $width = 355;
                        $height = 400;
                        $y = CFile::ResizeImageGet($arOnePhoto['ID'], array("width" => $width, "height" => $height), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false);
                        $big_photo = $y['src'];
                    } else {
                        $big_photo = $arOnePhoto['SRC'];
                    }
                    $isNoPhoto = (strpos($big_photo, "no_photo") !== false);
                    ?>
                    <a id="prod_slide<?= ++$key ?>" class="example-image-link"
                       href="<?= (!$isNoPhoto) ? $arOnePhoto['SRC'] : "javascript:void(0);"; ?>"
                        <? if (!$isNoPhoto): ?>
                            data-lightbox="prod-img"
                        <? endif; ?>
                       data-title="<? echo $strTitle; ?>">
                        <img src="<? echo $big_photo; ?>" class="example-image"
                             alt="<? echo $strAlt; ?>"
                             title="<? echo $strTitle; ?>"
                             <? if ($key == 1): ?>id="<? echo $arItemIDs['PICT']; ?>"<? endif; ?>
                            >
                        <? if (!$isNoPhoto): ?>
                            <span class="zoom"></span>
                        <? endif; ?>
                    </a>
                <? endforeach; ?>
                <? unset($arOnePhoto); ?>
            </div>
        </div>
    </div>
    <? if (!$isNoPhoto): ?>
        <div id="thumbs-wrapper">
            <div id="thumbs">
                <? foreach ($arResult['MORE_PHOTO'] as $key => &$arOnePhoto): ?>
                    <?
                    if (isset($arOnePhoto['ID'])) {
                        $width = 68;
                        $height = 100;
                        $y = CFile::ResizeImageGet($arOnePhoto['ID'], array("width" => $width, "height" => $height), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false);
                        $small_photo = $y['src'];
                    } else {
                        $small_photo = $arOnePhoto['SRC'];
                    }
                    ?>
                    <a data-value="<? echo $arOnePhoto['ID']; ?>" href="#prod_slide<?= ++$key ?>"
                       class="<? if ($key == 0): ?>selected<? endif; ?>"><img
                            src="<? echo $small_photo; ?>"/></a>
                <? endforeach; ?>
                <? unset($arOnePhoto); ?>

            </div>
            <a id="prev" href="#"></a>
            <a id="next" href="#"></a>
        </div>
    <? endif; ?>
</div>
<div class="col-xs-12 col-sm-12 col-md-8 description-wrapper">
    <div class="row">
        <div class="col-md-12 status-box">
            <span class="status status-green <?
            /*if (intval($arResult['CATALOG_QUANTITY']) > 0):*/
            if (!empty($arResult['PROPERTIES']['AVAILABLE']['VALUE'])):?>active<? endif; ?>">В наличии</span>
            <span class="status status-orange <?
            /*if (intval($arResult['CATALOG_QUANTITY']) <= 0):*/
            if (empty($arResult['PROPERTIES']['AVAILABLE']['VALUE'])):?>active<? endif; ?>">Под заказ</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <h1><?
                echo(
                isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
                    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
                    : $arResult["NAME"]
                ); ?></h1>
        </div>
    </div>

    <?
    if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) && !empty($arResult['OFFERS_PROP'])) {
        $arSkuProps = array();
        $offerSelected = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']];
        ?>
        <div id="<? echo $arItemIDs['PROP_DIV']; ?>">
            <?
            foreach ($arResult['SKU_PROPS'] as $offerPropCode => &$arProp) {
                if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
                    continue;
                $arSkuProps[] = array(
                    'ID' => $arProp['ID'],
                    'SHOW_MODE' => $arProp['SHOW_MODE'],
                    'VALUES_COUNT' => $arProp['VALUES_COUNT']
                );
                if ($offerPropCode == "PACKAGE_REF") {
                    ?>
                    <div id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_cont">
                        <div class="row set-list nav nav-tabs" id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_list">
                            <?
                            $packages = $arResult['PROPERTIES']['PACKAGES']['VALUE'];
                            $c = 0;
                            foreach ($arProp['VALUES'] as $arOneValue) {
                                $arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
                                $active = ($offerSelected['TREE']['PROP_' . $arProp['ID']] == $arOneValue['ID']) ? "active" : "";
                                ?>
                                <div
                                    class="col-xs-12 col-sm-6 col-md-3 set-item package <? //=$active?> delay_04s"
                                    data-treevalue="<? echo $arProp['ID'] . '_' . $arOneValue['ID']; ?>"
                                    data-onevalue="<? echo $arOneValue['ID']; ?>"
                                    <? //if ($arOneValue['ID'] == 0): ?>style="display: none;"<? //endif; ?>>
                                    <!--data-target="#param-set-item1" data-toggle="tab"!-->
                                    <span class="title delay_04s"><span><? echo $arOneValue['NAME']; ?></span></span>
                                    <?= $packages[$c]['TEXT'] ?>
                                </div>
                                <?
                                $c++;
                            }
                            ?>
                        </div>
                        <span id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_left"
                              data-treevalue="<? echo $arProp['ID']; ?>"></span>
                        <span id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_right"
                              data-treevalue="<? echo $arProp['ID']; ?>"></span>
                    </div>
                <?
                } else {
                    ?>
                    <div class="row param-set-list tab-content"
                         id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_cont">
                        <!-- tab item-->
                        <div class="col-md-12 param-set-item tab-pane active" id="param-set-item1">
                            <div class="row param-row">
                                <div class="col-xs-6 col-sm-4 col-md-3 param-name no-bg">
                                    <span class="name-wrap"><span><? echo htmlspecialcharsex($arProp['NAME']); ?></span></span>
                                </div>
                                <div class="col-xs-6 col-sm-8 col-md-9 param-info">
                                    <select class="selectpicker form-control"
                                            id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_list">
                                        <?
                                        $c = 0;
                                        foreach ($arProp['VALUES'] as $arOneValue) {
                                            $arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
                                            $selected = ($offerSelected['TREE']['PROP_' . $arProp['ID']] == $arOneValue['ID']) ? 'selected="selected"' : "";
                                            ?>
                                            <option <? //=$selected?>
                                                data-treevalue="<? echo $arProp['ID'] . '_' . $arOneValue['ID']; ?>"
                                                data-onevalue="<? echo $arOneValue['ID']; ?>"
                                                <? //if ($arOneValue['ID'] == 0): ?>style="display: none;"<? //endif; ?>>
                                                <? echo $arOneValue['NAME']; ?></option>
                                            <?
                                            $c++;
                                        }
                                        ?>
                                    </select>
                                    <span id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_left"
                                          data-treevalue="<? echo $arProp['ID']; ?>"></span>
                                    <span id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_right"
                                          data-treevalue="<? echo $arProp['ID']; ?>"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?
                }
            }
            unset($arProp);
            ?>
        </div>
    <?
    }
    ?>

    <div class="row param-set-list tab-content">
        <!-- tab item-->
        <div class="col-md-12 param-set-item tab-pane active" id="param-set-item2">
            <?
            foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp) {
                ?>
                <div class="row param-row">
                <div class="col-xs-6 col-sm-4 col-md-3 param-name">
                    <span class="name-wrap"><span><? echo $arOneProp['NAME']; ?></span></span>
                </div>
                <div class="col-xs-6 col-sm-8 col-md-9 param-info"><span><?
                        echo(
                        is_array($arOneProp['DISPLAY_VALUE'])
                            ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                            : $arOneProp['DISPLAY_VALUE']
                        ); ?></span>
                </div>
                </div><?
            }
            unset($arOneProp);
            ?>
            <? if ($arResult['SHOW_OFFERS_PROPS']): ?>
                <div id="<? echo $arItemIDs['DISPLAY_PROP_DIV'] ?>" style="display: none;"></div>
            <? endif; ?>
            <? if ($arResult['DETAIL_TEXT'] || $arResult['PREVIEW_TEXT']): ?>
                <div class="row param-row">
                    <div class="col-xs-12">
                        <?
                        if ('' != $arResult['PREVIEW_TEXT']) {
                            if ('S' == $arParams['DISPLAY_PREVIEW_TEXT_MODE']
                                || ('E' == $arParams['DISPLAY_PREVIEW_TEXT_MODE'] && '' == $arResult['DETAIL_TEXT'])
                            ) {
                                ?>
                                <?= ('html' == $arResult['PREVIEW_TEXT_TYPE'] ? $arResult['PREVIEW_TEXT'] : '<p>' . $arResult['PREVIEW_TEXT'] . '</p>') ?>
                            <?
                            }
                        }
                        if ('' != $arResult['DETAIL_TEXT']) {
                            ?>
                            <?= ('html' == $arResult['DETAIL_TEXT_TYPE'] ? $arResult['DETAIL_TEXT'] : '<p>' . $arResult['DETAIL_TEXT'] . '</p>') ?>
                        <?
                        }
                        ?>
                    </div>
                </div>
            <? endif; ?>
            <div class="row total-cost-row">
                <div class="col-xs-12 total-cost">
                    <?
                    $currencyCode = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE']['CURRENCY'] : $arResult['MIN_PRICE']['CURRENCY']);
                    $minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
                    $priceCode = getPriceCode($minPrice['PRINT_DISCOUNT_VALUE'], $currencyCode);
                    if (!empty($minPrice['PRINT_DISCOUNT_VALUE'])):
                        ?>
                        <span class="cost-name">Стоимость:</span>
                        <span class="cost"
                              id="<? echo $arItemIDs['PRICE']; ?>"><? echo $priceCode['price']; ?>
                            <span><? echo $priceCode['code']; ?></span></span>
                    <?
                    endif;
                    unset($minPrice);
                    unset($priceCode);
                    unset($currencyCode);
                    ?>
                </div>
            </div>
        </div>
    </div>
		<span id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo($canBuy ? 'block' : 'none'); ?>;">
<? if ($showBuyBtn): ?>
    <a href="javascript:void(0);" id="<? echo $arItemIDs['BUY_LINK']; ?>" class="main-blue-btn add-to-basket delay_04s">
        <span class="img"></span>
        <span class="text"><? echo $buyBtnMessage; ?></span></a>
<? endif; ?>
            <? if ($showAddBtn): ?>
                <a href="javascript:void(0);" id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"
                   class="main-blue-btn add-to-basket delay_04s">
                    <span class="img"></span>
                    <img src="<?= SITE_TEMPLATE_PATH ?>/img/loading_small3.gif" style="display: none;"/>
                    <span class="text"><? echo $addToBasketBtnMessage; ?></span></a>
            <? endif; ?>
		</span>
    <? unset($showAddBtn, $showBuyBtn); ?>
</div>
</div>
</div>
</div>
</div>
</div>


<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
    foreach ($arResult['JS_OFFERS'] as &$arOneJS) {
        // обнуляем слайдер картинок для торговых предложений
        $arOneJS['SLIDER_COUNT'] = 0;
        $currencyCode = $arOneJS['PRICE']['CURRENCY'];
        $currencyLang = CCurrencyLang::GetByID($currencyCode, LANGUAGE_ID);
        $arOneJS['PRICE']['CURRENCY_FORMAT_STRING'] = $currencyLang['FORMAT_STRING'];
        $arOneJS['BASIS_PRICE']['CURRENCY_FORMAT_STRING'] = $currencyLang['FORMAT_STRING'];

        if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE']) {
            $arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
            $arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
        }
        $strProps = '';
        if ($arResult['SHOW_OFFERS_PROPS']) {
            if (!empty($arOneJS['DISPLAY_PROPERTIES'])) {
                foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp) {
                    $strProps .= '
                            <div class="row param-row">
                                <div class="col-xs-6 col-sm-4 col-md-3 param-name">
                                    <span class="name-wrap"><span>' . $arOneProp['NAME'] . '</span></span>
                                </div>
                                <div class="col-xs-6 col-sm-8 col-md-9 param-info">
                                        <span>' . (
                        is_array($arOneProp['VALUE'])
                            ? implode(' / ', $arOneProp['VALUE'])
                            : $arOneProp['VALUE']
                        ) . '</span>
                                </div>
                            </div>';
                }
            }
        }
        $arOneJS['DISPLAY_PROPERTIES'] = $strProps;
    }
    if (isset($arOneJS))
        unset($arOneJS);
    $arJSParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => true,
            'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
            'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
            'OFFER_GROUP' => $arResult['OFFER_GROUP'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'VISUAL' => array(
            'ID' => $arItemIDs['ID'],
        ),
        'DEFAULT_PICTURE' => array(
            'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
            'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
        ),
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'NAME' => $arResult['~NAME'],
            'IS_AVAILABLE' => !empty($arResult['PROPERTIES']['AVAILABLE']['VALUE']),
        ),
        'BASKET' => array(
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'BASKET_URL' => $arParams['BASKET_URL'],
            'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        ),
        'OFFERS' => $arResult['JS_OFFERS'],
        'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
        'TREE_PROPS' => $arSkuProps
    );
    if ($arParams['DISPLAY_COMPARE']) {
        $arJSParams['COMPARE'] = array(
            'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
            'COMPARE_PATH' => $arParams['COMPARE_PATH']
        );
    }
} else {
    $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
    /*if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties) {
        ?>
        <div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
            <?
            if (!empty($arResult['PRODUCT_PROPERTIES_FILL'])) {
                foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
                    ?>
                    <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
                           value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
                    <?
                    if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
                        unset($arResult['PRODUCT_PROPERTIES'][$propID]);
                }
            }
            $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
            if (!$emptyProductProperties) {
                ?>
                <table>
                    <?
                    foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
                        ?>
                        <tr>
                            <td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
                            <td>
                                <?
                                if (
                                    'L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE']
                                    && 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']
                                ) {
                                    foreach ($propInfo['VALUES'] as $valueID => $value) {
                                        ?><label><input type="radio"
                                                        name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
                                                        value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?>
                                        </label><br><?
                                    }
                                } else {
                                    ?><select
                                    name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
                                    foreach ($propInfo['VALUES'] as $valueID => $value) {
                                        ?>
                                        <option
                                        value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
                                    }
                                    ?></select><?
                                }
                                ?>
                            </td>
                        </tr>
                    <?
                    }
                    ?>
                </table>
            <?
            }
            ?>
        </div>
    <?
    }*/
    if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE']) {
        $arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
        $arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
    }

    $currencyCode = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE']['CURRENCY'] : $arResult['MIN_PRICE']['CURRENCY']);
    $currencyLang = CCurrencyLang::GetByID($currencyCode, LANGUAGE_ID);
    $arResult['MIN_BASIS_PRICE']['CURRENCY_FORMAT_STRING'] = $currencyLang['FORMAT_STRING'];

    $arJSParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
            'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
            'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y')
        ),
        'VISUAL' => array(
            'ID' => $arItemIDs['ID'],
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'PICT' => $arFirstPhoto,
            'NAME' => $arResult['~NAME'],
            'SUBSCRIPTION' => true,
            'PRICE' => $arResult['MIN_PRICE'],
            'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
//            'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
            'SLIDER_COUNT' => 0,
//            'SLIDER' => $arResult['MORE_PHOTO'],
            'SLIDER' => array(),
            'CAN_BUY' => $arResult['CAN_BUY'],
            'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
            'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
            'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
            'IS_AVAILABLE' => !empty($arResult['PROPERTIES']['AVAILABLE']['VALUE']),
            'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
        ),
        'BASKET' => array(
            'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => $emptyProductProperties,
            'BASKET_URL' => $arParams['BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        )
    );
    if ($arParams['DISPLAY_COMPARE']) {
        $arJSParams['COMPARE'] = array(
            'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
            'COMPARE_PATH' => $arParams['COMPARE_PATH']
        );
    }
    unset($emptyProductProperties);
}
?>
<script type="text/javascript">
    var <? echo $strObName; ?> =
    new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
    BX.message({
        ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
        BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
        TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
        TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
        BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
        BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
        BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
        BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
        BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
        TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
        COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
        COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
        COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
        BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
        SITE_ID: '<? echo SITE_ID; ?>'
    });
</script>