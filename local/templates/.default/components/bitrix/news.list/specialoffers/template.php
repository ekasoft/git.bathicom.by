<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0):
    ?>    <div class="container actions">
        <div class="row">
            <div class="col-md-12">
                <span class="title_h1">Спецпредложение!</span>
            </div>
        </div>
        <div class="row">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                $y = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array("width" => 460, "height" => 406),
                    BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false);
                $image = $y['src'];

                $link = $arItem['PROPERTIES']['LINK']['VALUE'];
                $isLink = !empty($link);
                ?>
                <div class="col-xs-12 col-sm-4 col-ms-4 one-action" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <? if ($isLink): ?>
                    <a href="<?= $link ?>">
                        <? endif; ?>
                        <span class="img">
                                <img src="<?= $image ?>" class="img-responsive delay_04s"
                                     alt="">
                                <span class="mask"></span>
                            </span>
                            <span class="text">
                                <?= $arItem['NAME'] ?>
                            </span>
                        <? if ($isLink): ?>
                    </a>
                <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? endif; ?>