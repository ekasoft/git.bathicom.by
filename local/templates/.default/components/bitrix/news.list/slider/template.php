<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0):
    $upload = COption::GetOptionString("main", "upload_dir", "upload");
    ?>
    <div class="wrapper-main-slider">
        <div class="main-slider">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                $db_file = CFile::GetByID($arItem['~PREVIEW_PICTURE']);
                $fileElement = $db_file->GetNext();
                $file_info = '/' . $upload . '/' . $fileElement['SUBDIR'] . '/' . $fileElement['FILE_NAME'];

                $link = $arItem['PROPERTIES']['LINK']['VALUE'];
                $isLink = !empty($link);
                ?>
                <div class="slide" style="background: url(<?= $file_info ?>);" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <div class="slide-info-wrap">
                        <div class="slide-info delay_04s">
                            <span class="slide-title"><?= $arItem['NAME'] ?></span>

                            <p><?= $arItem['~PREVIEW_TEXT'] ?></p>
                            <? if ($isLink): ?>
                                <a href="<?= $link ?>" class="more delay_04s">Подробнее</a>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <div class="container slider-buttons">
            <div class="row">
                <a id="slide-prev" class="delay_04s"><span></span></a>
                <a id="slide-next" class="delay_04s"><span></span></a>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<? endif; ?>
