<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0):
    ?>
    <div class="col-xs-12 col-sm-6 col-md-4 adress-box">
        <span class="contact-row-name">Адрес:</span>
        <form>
            <select id="citySelect" class="selectpicker form-control">
                <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
                    <option value='<?= $k ?>'><?= $arItem['NAME'] ?></option>
                <? endforeach; ?>
            </select>
        </form>
        <ul class="nav nav-tabs" id="cityTab">
            <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
                <li <? if ($k == 0): ?>class="active"<? endif; ?>>
                    <a href="#city<?= $k ?>"><?= $arItem['NAME'] ?></a></li>
            <? endforeach; ?>
        </ul>
        <div class="tab-content">
            <? foreach ($arResult["ITEMS"] as $k => $arItem): ?>
                <div class="tab-pane <? if ($k == 0): ?>active<? endif; ?>" id="city<?= $k ?>">
                    <p><?=$arItem['~PREVIEW_TEXT']?></p>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? endif; ?>
