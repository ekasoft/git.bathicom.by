<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (count($arResult["ITEMS"]) > 0):
    ?>
    <div class="col-xs-12 col-sm-12 col-md-4 sections-block">
        <div class="row">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                $y = CFile::ResizeImageGet($arItem['~PREVIEW_PICTURE'], array("width" => 360, "height" => 150),
                    BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true, false);
                $image = $y['src'];

                $link = "";
                $articleId = $arItem['PROPERTIES']['PAGE']['VALUE'];
                $res = CIBlockElement::GetByID($articleId);
                if($ar_res = $res->GetNext())
                    $link = $ar_res['DETAIL_PAGE_URL'];
                $isLink = !empty($link);
                ?>
                <div class="col-md-12 one-section" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                    <? if ($isLink): ?>
                    <a href="<?= $link ?>">
                        <? endif; ?>
                        <img src="<?= $image ?>" class="delay_04s" alt="">
                        <span><?= $arItem['NAME'] ?></span>
                        <? if ($isLink): ?>
                    </a>
                <? endif; ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? endif; ?>
