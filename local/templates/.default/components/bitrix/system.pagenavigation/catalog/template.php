<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

//echo "<pre>"; print_r($arResult);echo "</pre>";

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>
<!-- pagination -->

<div class="pagination-wrapper">

    <? if ($arResult["bDescPageNumbering"] === true): ?>
        <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
            <? if ($arResult["bSavePage"]): ?>
                <div class="left-part">
                    <a title="<?= GetMessage("nav_prev") ?>"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"></a>
                </div>
            <? else: ?>
                <? if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"] + 1)): ?>
                    <div class="left-part">
                        <a title="<?= GetMessage("nav_prev") ?>"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"></a>
                    </div>
                <? else: ?>
                    <div class="left-part">
                        <a title="<?= GetMessage("nav_prev") ?>"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"></a>
                    </div>
                <? endif ?>
            <?endif ?>
        <?
        else: /*?>
		 <a class="page-direction page-prev" href="javascript:void(0);"><?=GetMessage("nav_prev")?></a>
	<?*/ endif?>

        <div class="center-part">
            <? while ($arResult["nStartPage"] >= $arResult["nEndPage"]): ?>
                <? $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1; ?>

                <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                    <a href="javascript:void(0);" class="active delay_04s"><?= $NavRecordGroupPrint ?></a>
                <? elseif ($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false): ?>
                    <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>" class="delay_04s">
                        <?= $NavRecordGroupPrint ?>
                    </a>
                <?
                else: ?>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"
                       class="delay_04s">
                        <?= $NavRecordGroupPrint ?></a>
                <?endif ?>
                <? $arResult["nStartPage"]-- ?>
            <? endwhile ?>
        </div>

        <? if ($arResult["NavPageNomer"] > 1): ?>
            <div class="right-part">
                <a title="<?= GetMessage("nav_next") ?>"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
            </div>
        <?
        else: /*?>
		 <a class="page-direction page-next" href="javascript:void(0);"><?=GetMessage("nav_next")?></a>
	<? */ endif?>

    <? else: ?>


        <? if ($arResult["NavPageNomer"] > 1): ?>

            <? if ($arResult["bSavePage"]): ?>
                <div class="left-part">
                    <a title="<?= GetMessage("nav_prev") ?>"
                       href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
                </div>
            <? else: ?>
                <? if ($arResult["NavPageNomer"] > 2): ?>
                    <div class="left-part">
                        <a title="<?= GetMessage("nav_prev") ?>"
                           href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"></a>
                    </div>
                <? else: ?>
                    <div class="left-part">
                        <a title="<?= GetMessage("nav_prev") ?>"
                           href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"></a>
                    </div>
                <?endif ?>
            <?endif ?>

        <?
        else: /*?>
		 <a class="page-direction page-prev" href="javascript:void(0);"><?=GetMessage("nav_prev")?></a>
	<?*/ endif?>

        <div class="center-part">
            <? while ($arResult["nStartPage"] <= $arResult["nEndPage"]): ?>

                <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                    <a href="javascript:void(0);" class="active delay_04s"><?= $arResult["nStartPage"] ?></a>
                <? elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false): ?>
                    <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"
                       class="delay_04s"><?= $arResult["nStartPage"] ?></a>
                <?
                else: ?>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"
                       class="delay_04s">
                        <?= $arResult["nStartPage"] ?></a>
                <?endif ?>
                <? $arResult["nStartPage"]++ ?>
            <? endwhile ?>
        </div>

        <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
            <div class="right-part">
                <a title="<?= GetMessage("nav_next") ?>"
                   href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"></a>
            </div>
        <?
        else: /*?>
		<a class="page-direction page-next" href="javascript:void(0);"><?=GetMessage("nav_next")?></a>
	<?*/ endif?>

    <?endif ?>


    <div class="clearfix"></div>
</div>
