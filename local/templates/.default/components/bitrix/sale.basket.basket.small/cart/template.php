<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?php
$count = 0;
$ids = array();
foreach ($arResult['ITEMS'] as $key => $value) {
    $ids[] = $value['PRODUCT_ID'];
}

CModule::IncludeModule('catalog');
$goodsNoServices = array();
$db_res = CCatalogProduct::GetList(
    array(),
    array(
        "!ELEMENT_IBLOCK_ID" => SERVICES_IBLOCK,
        "ID" => $ids
    ),
    false,
    false
);
while ($ar_res = $db_res->Fetch()) {
    $goodsNoServices[] = $ar_res['ID'];
}

foreach ($arResult['ITEMS'] as $key => $value) {
    if (in_array($value['PRODUCT_ID'], $goodsNoServices)){
        $count += $value["QUANTITY"];
    }
}
?>
<div id="basket" class="delay_04s">
    <a href="<?= $arParams["PATH_TO_BASKET"] ?>">
        <span id="idBasket"><?= $count ?></span>
    </a>
</div>


