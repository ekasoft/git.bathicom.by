<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BCatalogRoute extends \BaseModuleComponent {

    public function onPrepareComponentParams( $arParams ) {
        $arParams["TEMPLATE"] = "tovar";

        /**
         * If element not found
         */
        /*if (!$_REQUEST["ELEMENT_CODE"])
        {
            var_dump(\EkaSoft\Base\Core\Structure::getInstance()->getIDByCode($_REQUEST["ELEMENT_CODE"]));
            exit;
        }*/

        /**
         * If section not found
         */
        
        if (!\EkaSoft\Base\Core\Structure::getInstance()->getIDByCode($_REQUEST["SECTION_CODE"]) || $_REQUEST['BRAND'])
        {   
            $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
            $arFilter = Array("IBLOCK_ID" => 15, "CODE" => htmlspecialchars($_REQUEST["BRAND"]), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($ob = $res->GetNext())
            {
                $_GET['set_filter'] = 'Показать';
                $_GET['arrFilter_129_'.abs(crc32($ob['ID']))] = 'Y';
            //    $_GET['STORE'] = 'N';
            //    $_REQUEST['STORE'] = 'N';
            }
        }
        
        
        global $APPLICATION;
        //$store = $APPLICATION->get_cookie("STORE");
        $arParams['STORE'] = 'Y';
        if($_REQUEST["STORE"]) {
            $arParams['STORE'] = $_REQUEST["STORE"];
        } elseif($APPLICATION->get_cookie("STORE")) {
            $arParams['STORE'] = $APPLICATION->get_cookie("STORE");
            //prent($APPLICATION->get_cookie("STORE"));
        }
        $arFilter = Array('IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'CODE'=>$_REQUEST["SECTION_CODE"]);
        $arSelect = array('ID');
        $db_list = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
        if($ar_result = $db_list->GetNext())
        {
            $sectCode = $ar_result['ID'];
        } 
        if ( $sectCode && !isset($_REQUEST["ELEMENT_CODE"]) ) {
        //if ( \EkaSoft\Base\Core\Structure::getInstance()->getIDByCode($_REQUEST["SECTION_CODE"]) && !isset($_REQUEST["ELEMENT_CODE"]) ) {
            $arParams["TEMPLATE"] = 'catalog';
        }

        $dtSectionCode = ($_REQUEST["SECTION_CODE"]) ? $_REQUEST["SECTION_CODE"] : $_REQUEST["PREPARE_SECTION_CODE"] ;

        //prent(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->getIDByCode($dtSectionCode);
        //exit();
        $arParams['IBLOCK_ID'] = $_REQUEST['IBLOCK_ID'];
        $arParams['TP_SECTION_CODE'] = $_REQUEST['TP_SECTION_CODE'];
        if($arParams['IBLOCK_ID'] == 11) {
            $arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'CODE' => $_REQUEST["TP_SECTION_CODE"]);
            $arSelect = array('ID');
            $db_list = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
            if($section = $db_list->GetNext())
            {
                $arParams["SECTION_ID"] = $section['ID'];
            }
            //$arParams["SECTION_ID"] = getSectionIdByCode($_REQUEST["TP_SECTION_CODE"],$arParams['IBLOCK_ID']);
        } else {
            $arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'CODE' => $dtSectionCode);
            $arSelect = array('ID');
            $db_list = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
            if($section = $db_list->GetNext())
            {
                $arParams["SECTION_ID"] = $section['ID'];
            }
            //$arParams["SECTION_ID"] = \EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->getIDByCode($dtSectionCode);
        }
        //prent($arParams);
        
        if ( !isset($_REQUEST["ajax"]["template"]) ) {
            if ( !isset($_SESSION["EKASOFT"]["AJAX"]["TEMPLATE"][$arParams["SECTION_ID"]])/* || $_SESSION["EKASOFT"]["AJAX"]["TEMPLATE"][$arParams["SECTION_ID"]] == ''*/ ) {
                $arParams["AJAX"]["TEMPLATE"] = $_SESSION["EKASOFT"]["AJAX"]["TEMPLATE"][$arParams["SECTION_ID"]] = 'table';
            } else {
                $arParams["AJAX"]["TEMPLATE"] = $_SESSION["EKASOFT"]["AJAX"]["TEMPLATE"][$arParams["SECTION_ID"]];
            }
        } else {
            $arParams["AJAX"]["TEMPLATE"] = $_SESSION["EKASOFT"]["AJAX"]["TEMPLATE"][$arParams["SECTION_ID"]] = $_REQUEST["ajax"]["template"];
        }

        if ( !isset($_REQUEST["ajax"]["sorted"]) ) {
            if ( !isset($_SESSION["EKASOFT"]["AJAX"]["SORTED"][$arParams["SECTION_ID"]]) || $_SESSION["EKASOFT"]["AJAX"]["SORTED"][$arParams["SECTION_ID"]] == '' ) {
                $_SESSION["EKASOFT"]["AJAX"]["SORTED"][$arParams["SECTION_ID"]] = $arParams["AJAX"]["SORTED"] = 1;
            } else {
                $arParams["AJAX"]["SORTED"] = $_SESSION["EKASOFT"]["AJAX"]["SORTED"][$arParams["SECTION_ID"]];
            }
        } else {
            $arParams["AJAX"]["SORTED"] = $_SESSION["EKASOFT"]["AJAX"]["SORTED"][$arParams["SECTION_ID"]] = $_REQUEST["ajax"]["sorted"];
        }
        //prent($arParams);
        return $arParams;
    }

    public function executeComponent() {
        if ( isset($_REQUEST["ajax"]) ) {
            $GLOBALS["APPLICATION"]->RestartBuffer();
            unset($_REQUEST["ajax"], $_GET["ajax"]);
            global $APPLICATION;
            $APPLICATION->GetCurPageParam("", array("ajax"));
        }
        //$this->arResult["SECTION"] = EkaSoft\Base\Core\Structure::getInstance()->getSection($this->arParams["SECTION_ID"]);
        $res = CIBlockSection::GetByID($this->arParams["SECTION_ID"]);
        if($ar_res = $res->GetNext()){
            $this->arResult["SECTION"] = $ar_res;
        }
        //prent($this->arParams["SECTION_ID"],true);
        //prent($this->arResult["SECTION"],true);
        //prent($this->arParams["TEMPLATE"],true);
        //exit();
        $this->includeComponentTemplate($this->arParams["TEMPLATE"]);
    }

}
