<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Простое меню",
    "DESCRIPTION" => "Вывод меню с разделов инфоблока",
    "ICON" => "/images/strings.gif",
    "PATH" => array(
        "ID" => "ekasoft",
        "NAME" => "Ека-софт",
        "CHILD" => array(
            "ID" => "structure",
            "NAME" => "Структура"
        )
    ),
);
?>
