<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BMenuSectionsTovar extends \BaseModuleComponent {

    private $IDs = array();

    public function decorateFiles($row) {
        unset($row["ID"], $row["TIMESTAMP_X"], $row["MODULE_ID"], $row["HEIGHT"], $row["WIDTH"], $row["CONTENT_TYPE"], $row["FILE_SIZE"], $row["SUBDIR"], $row["FILE_NAME"], $row["ORIGINAL_NAME"], $row["DESCRIPTION"], $row["HANDLER_ID"], $row["EXTERNAL_ID"]);
        return $row;
    }
    
    public function decorateItem($row) {
        $row["KEY"] = $row["IBLOCK_SECTION_ID"];
        return $row;
    }
    
    public function onPrepareComponentParams( $arParams ) {
        \CModule::IncludeModule("ekasoft.base");
        $arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::TOVAR;
        $arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::TOVAR;
        $arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::TOVARSKU;

        return parent::onPrepareComponentParams($arParams);
    }

    protected function getTovarInSections() {
        $this->objModel->setOrder(array("CREATED_DATE" => "DESC", "SORT" => "SORT"));
        
        $this->objModel->addFilter(array("SECTION_ID" => \EkaSoft\Base\Core\Structure::getInstance()->getChild($this->arParams["SECTION_ID"]), "PROPERTY_IN_MENU_VALUE" => "Y"));

        $this->arResult["SECTIONS_GOODS"] = $this->objEntity->getItems(2);
        $this->addFiles($this->objEntity);
    }

//    protected function getOffers() {
//        $this->objDecorator = new \EkaSoft\Base\Decorator\Tovar($this);
//        $this->objModel = new \Ekasoft\Base\Model\Tovar($this->objDecorator);
//        $this->objModel->setFilter(array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "=PROPERTY_CML2_LINK" => $this->IDs));
//        $this->objEntity = new EkaSoft\Base\Entity\TovarSKU($this->objModel, $this->objDecorator);
//        $this->objDecorator->initEntity($this->objEntity);
//        
//        $this->arResult["OFFERS"] = $this->objEntity->getItems();
//        $this->addFiles($this->objEntity);
//    }

    protected function getSections() {
        $this->arResult["SECTION"] = \EkaSoft\Base\Core\Structure::getInstance()->getSection($this->arParams["SECTION_ID"]);
        $this->arResult["SECTIONS"] = \EkaSoft\Base\Core\Structure::getInstance()->getChild($this->arParams["SECTION_ID"]);
        foreach( $this->arResult["SECTIONS"] as $id ) {
            $this->arResult["SECTIONS_L2"][$id] = \EkaSoft\Base\Core\Structure::getInstance()->getChild($id);
        }
        $this->addFiles(\EkaSoft\Base\Core\Structure::getInstance());
    }
    
    public function getQuantity($IDs) {
        \CModule::IncludeModule("catalog");
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()));
        while( $row = $dbl->fetch() ) {
            $this->arResult["QUANTITIES"][$row["ID"]] = $row;
        }
    }
    
    private function addFiles($obj) {
        if ( count($obj->getFiles()) > 0 ) {
            $this->arResult["FILES"] += $obj->getFiles();
        }
    }

    protected function getHits() {
        $this->objModel->delFilterByCode("SECTION_ID");
        $this->objModel->addFilter(array("INCLUDE_SUBSECTIONS" => "Y", "SECTION_ID" => $this->arParams["SECTION_ID"], "PROPERTY_IN_HIT_VALUE" => "Y"));
        $this->arResult["HITS"] = $this->objEntity->getResult();
       // $this->IDs = $this->objEntity->getIDs();
        $this->addFiles($this->objEntity);
        $this->getQuantity($this->objEntity->getIDs());
    }

    public function executeComponent() {
        if ( $this->StartResultCache() ) {
            
            //$this->getTovarInSections();
            $this->getHits();
            //$this->getOffers();
            $this->getSections();            
            
            $this->IncludeComponentTemplate();
            
        }    
    }

}
