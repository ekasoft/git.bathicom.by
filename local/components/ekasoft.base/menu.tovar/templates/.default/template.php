<div class="col-xs-12 col-sm-6 col-md-3 js-top-menu-element posinit">
    <?//prent($arResult['HITS']);?>
    <a  href="<?= $arResult["SECTION"]["SECTION_PAGE_URL"] ?>" data-href="#<?= $arResult["SECTION"]["CODE"]; ?>" class="but but1 but-<?= $arParams["COLORS"][$arResult["SECTION"]["UF_COLOR"]]["XML_ID"]; ?> bg bg-<?= $arParams["COLORS"][$arResult["SECTION"]["UF_COLOR"]]["XML_ID"]; ?> delay_04s1 a-url" title="<?= $arResult["SECTION"]["NAME"]; ?>">
        <div class="img-block">
            <img src="<?= $arResult["FILES"][$arResult["SECTION"]["UF_IMAGE_WHITE"]]["SRC"]; ?>" alt="<?= $arResult["SECTION"]["NAME"]; ?>" />
            <img src="<?= $arResult["FILES"][$arResult["SECTION"]["UF_IMAGE_COLOR"]]["SRC"]; ?>" alt="<?= $arResult["SECTION"]["NAME"]; ?>" />
        </div>
        <span class="text"><?= $arResult["SECTION"]["NAME"]; ?></span>
        <span class="clearfix"></span>
    </a>
    <nav class="header-menu-top <?= $arResult["SECTION"]["CODE"]; ?>" id="<?= $arResult["SECTION"]["CODE"]; ?>">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-2">
                    <div class="title-menu"><?= $arResult["SECTION"]["NAME"]; ?> <i class="fa fa-caret-right"></i></div>
                    <? if ( isset($arResult["SECTIONS"]) && count($arResult["SECTIONS"]) > 0 ): ?>
                        <ul class="menu-first-level2 menu-level">
                            <? foreach ( $arResult["SECTIONS"] as $id ): ?>
                                <li>
                                    <? $arItem = \EkaSoft\Base\Core\Structure::getInstance()->getSection($id); ?>
                                    <a href="<?= $arItem["SECTION_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>" data-code="<?=$arItem['CODE']?>"><?= $arItem["NAME"]; ?></a>
                                    <div class="topmenu-innermenu">
                                        <div class="col-lg-4 col-md-11">
                                            <div class="title-menu"<?= $arItem["NAME"]; ?><i class="fa fa-caret-right"></i></div>
                                            <ul class="menu-level menu-second-level2">
                                                <? foreach ( $arResult["SECTIONS_L2"][$id] as $_id ):?>
                                                    <? $arSection = \EkaSoft\Base\Core\Structure::getInstance()->getSection($_id); ?>
                                                    <li>
                                                        <a href="<?= $arSection["SECTION_PAGE_URL"]; ?>" title="<?= $arSection["NAME"]; ?>"><?= $arSection["NAME"]; ?></a>
                                                        <div class="topmenu-innermenu2">
                                                            <div class="title-menu-hit">Хиты продаж<i class="fa fa-caret-right"></i></div>
                                                            <div class="categories-list-tovar-e products-list">

                                                            </div>
                                                        </div>
                                                    </li>
                                                <? endforeach; ?>
                                            </ul>
                                        </div>

                                        <ul class="menu-level">
                                            <? foreach ( $arResult["SECTIONS_GOODS"][$arItem["ID"]] as $arSectionsGoods ): ?>
                                                <li>
                                                    <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>"><?= $arSectionsGoods["NAME"]; ?></a>
                                                    <div class="topmenu-innermenu2">
                                                        <div class="title-colection"><?= $arSectionsGoods["NAME"]; ?></div>
                                                        <div class="slider-colection">
                                                            <div class="slide-block">
                                                                <div class="item-slid">
                                                                    <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>">
                                                                        <img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200, "MODE" => "inv"), $arResult["FILES"][$arSectionsGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arSectionsGoods["NAME"]; ?>" class="img-responsive" />
                                                                        <div class="info-block">
                                                                            <p class="title-info"><?= $arSectionsGoods["NAME"]; ?></p>
                                                                            <p class="price">от 30 500 <span>000</span></p>
                                                                        </div>
                                                                    </a>
                                                                    <div class="topmenu-innermenu3">
                                                                        <div class="col-md-12">
                                                                            <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>" class="close-bt"></a>
                                                                            <div class="title-colection"><?= $arSectionsGoods["NAME"]; ?></div>
                                                                            <div class="row">
                                                                                <div class="col-md-7">
                                                                                    <div>
                                                                                        <img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200, "MODE" => "inv"), $arResult["FILES"][$arSectionsGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" class="img-responsive" />
                                                                                    </div>
                                                                                    <div class="colection-description"> 
                                                                                        <?= substr($arSectionsGoods["PREVIEW_TEXT"], 0, 280); ?>
                                                                                    </div>
                                                                                    <div>
                                                                                        <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>" class="bt-show-colection">Посмотреть коллекцию</a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5 jq-listcoll">
                                                                                    <div class="menu-list-colection">
                                                                                        <div class="title-colection2">Плитка в <?= $arSectionsGoods["NAME"]; ?></div>
                                                                                        <ul>
                                                                                            <? foreach ( $arResult["OFFERS"][$arSectionsGoods["ID"]] as $arOffer ): ?>
                                                                                                <li>
                                                                                                    <a href="<?= $arOffer["DETAIL_PAGE_URL"]; ?>" title="<?= $arOffer["NAME"]; ?>">
                                                                                                        <span class="img-warp"><img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200, "MODE" => "inv"), $arResult["FILES"][$arSectionsGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arOffer["NAME"]; ?>" /></span>
                                                                                                        <span class="name"><?= $arOffer["NAME"]; ?></span>
                                                                                                    </a>
                                                                                                </li>
                                                                                            <? endforeach; ?>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                          
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <? endforeach; ?>
                                        </ul>
                                    </div>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>
                </div>
                <div class="subs"></div>
                <div class="col-md-8 col-lg-10 js-hit-towarov">
                    <div class="title-menu-hit">Хиты продаж<i class="fa fa-caret-right"></i></div>
                    <div class="products-list products_list-list new-style-list">
                        <?foreach( $arResult["HITS"] as $arItem):?>
                            <?$APPLICATION->IncludeComponent(
	"ekasoft.view:element", 
	"item", 
	array(
		"RESULT" => $arItem,
		"FILES" => $arResult["FILES"],
		"TEMPLATE" => "vertical",
		"QUANTITY" => $arResult["QUANTITIES"][$arItem["ID"]],
		"COMPONENT_TEMPLATE" => "item",
		"GET_SECTION_CODE" => "Y"
	),
	false
);?>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>