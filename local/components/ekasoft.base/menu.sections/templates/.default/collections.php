<div class="col-xs-12 col-sm-6 col-md-3 js-top-menu-element posinit">
    <a  href="<?= $arItems["SECTION_PAGE_URL"] ?>" data-href="#<?= $arItems["CODE"]; ?>" class="but but1 but-<?= $arResult["COLORS"][$arItems["UF_COLOR"]]["XML_ID"]; ?> bg bg-<?= $arResult["COLORS"][$arItems["UF_COLOR"]]["XML_ID"]; ?> delay_04s1 a-url" title="<?= $arItems["NAME"]; ?>">
        <div class="img-block">
            <img src="<?= $arResult["FILES"][$arItems["UF_IMAGE_WHITE"]]["SRC"]; ?>" alt="<?= $arItems["NAME"]; ?>" />
            <img src="<?= $arResult["FILES"][$arItems["UF_IMAGE_COLOR"]]["SRC"]; ?>" alt="<?= $arItems["NAME"]; ?>" />
        </div>
        <span class="text"><?= $arItems["NAME"]; ?></span>
        <span class="clearfix"></span>
    </a>

    <nav class="header-menu-top <?= $arItems["CODE"]; ?>" id="<?= $arItems["CODE"]; ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-2">
                    <div class="title-menu"><?= $arItems["NAME"]; ?> <i class="fa fa-caret-right"></i></div>
                    <? if ( isset($arResult["ITEMS"][$arItems["ID"]]) && count($arResult["ITEMS"][$arItems["ID"]]) > 0 ): ?>
                        <ul class="menu-first-level menu-level">
                            <?
                            $first = true;
                            foreach ( $arResult["ITEMS"][$arItems["ID"]] as $pid => $arItem ):
                                ?>
                                <li <? if ( $first ): ?>class="active"<?
                                endif;
                                $first = false;
                                ?>>
                                    <a href="<?= $arItem["SECTION_URL_PAGE"]; ?>" title="<?= $arItem["NAME"]; ?>"><?= $arItem["NAME"]; ?></a>
                                    <div class="topmenu-innermenu">
                                        <div class="title-menu">Коллекции <?= $arItems["NAME"]; ?><i class="fa fa-caret-right"></i></div>
                                        <ul class="menu-level">
                                            <?
                                            $first1 = true;
                                            foreach ( $arResult["SECTIONS_GOODS"][$pid] as $arSectionsGoods ):
                                                ?>
                                                <li <? if ( $first1 ): ?>class="active"<?
                                                endif;
                                                $first1 = false;
                                                ?>>
                                                    <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>"><?= $arSectionsGoods["NAME"]; ?></a>
                                                    <div class="topmenu-innermenu2">
                                                        <div class="title-colection"><?= $arSectionsGoods["NAME"]; ?></div>
                                                        <div class="slider-colection">
                                                            <div class="slide-block">
                                                                <div class="item-slid">
                                                                    <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>">
                                                                        <img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200), $arResult["FILES"][$arSectionsGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arSectionsGoods["NAME"]; ?>" class="img-responsive" />
                                                                        <div class="info-block">
                                                                            <p class="title-info"><?= $arSectionsGoods["NAME"]; ?></p>
                                                                            <p class="price">от 30 500 <span>000</span></p>
                                                                        </div>
                                                                    </a>
                                                                    <div class="topmenu-innermenu3">
                                                                        <div class="col-md-12">
                                                                            <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>" class="close-bt"></a>
                                                                            <div class="title-colection"><?= $arSectionsGoods["NAME"]; ?></div>
                                                                            <div class="row">
                                                                                <div class="col-md-7">
                                                                                    <div>
                                                                                        <img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200), $arResult["FILES"][$arSectionsGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" class="img-responsive" />
                                                                                    </div>
                                                                                    <div class="colection-description"> 
                                                                                        <?= $arSectionsGoods["PREVIEW_TEXT"]; ?>
                                                                                    </div>
                                                                                    <div>
                                                                                        <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>" class="bt-show-colection">Посмотреть коллекцию</a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-5 jq-listcoll">
                                                                                    <div class="menu-list-colection">
                                                                                        <div class="title-colection2">Плитка в <?= $arSectionsGoods["NAME"]; ?></div>
                                                                                        <ul>
                                                                                            <? foreach ( $arResult["OFFERS"][$arSectionsGoods["ID"]] as $arOffer ): ?>
                                                                                                <li>
                                                                                                    <a href="<?= $arOffer["DETAIL_PAGE_URL"]; ?>" title="<?= $arOffer["NAME"]; ?>">
                                                                                                        <span class="img-warp"><img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200), $arResult["FILES"][$arSectionsGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arOffer["NAME"]; ?>" /></span>
                                                                                                        <span class="name"><?= $arOffer["NAME"]; ?></span>
                                                                                                    </a>
                                                                                                </li>
                                                                                            <? endforeach; ?>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>                                          
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <? endforeach; ?>
                                        </ul>
                                    </div>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>
                </div>
                <div class="col-sm-5 col-lg-2">
                    <div class="menu-second-level" id="colection1">
                        <div class="title-menu">Коллекции <i class="fa fa-caret-right"></i></div>
                        <ul class="menu-level">
                            <?
                            $first2 = true;
                            foreach ( $arResult["SECTIONS_GOODS"][$arItem["ID"]] as $arGoods ):
                                ?>
                                <li <? if ( $first2 ): ?>class="active"<?
                                endif;
                                $first2 = false;
                                ?>>
                                    <a href="<?= $arGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arGoods["NAME"]; ?>"><?= $arGoods["NAME"]; ?></a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                        <div class="all-colect"><a href="<?= $arItems["SECTION_PAGE_URL"]; ?>">Все коллекции  <i class="fa fa-caret-right"></i></a></div>
                    </div>
                </div>
                <div class="col-lg-offset-1 col-lg-7 col-sm-1 jq-showmenublock col-xs-offset-0 col-sm-offset-0 col-xs-12 visible-lg-inline-block" >
                    <div class="col-md-12">
                        <div class="title-colection">Популярные коллекции</div>
                        <div class="slider-colection">
                            <div class="slide-block">
                                <? foreach ( $arResult["HITS"] as $arGoods ): ?>
                                    <div class="item-slid">
                                        <a href="<?= $arGoods["DETAIL_URL_PAGE"]; ?>" title="<?= $arGoods["NAME"]; ?>">
                                            <img src="<?= $arResult["FILES"][$arGoods["PREVIEW_PICTURE"]]["SRC"]; ?>" alt="<?= $arGoods["NAME"]; ?>" class="img-responsive" />
                                            <div class="info-block">
                                                <p class="title-info"><?= $arGoods["NAME"]; ?></p>
                                                <p class="price">от 30 500 <span>000</span></p>
                                            </div>
                                        </a>
                                        <div class="topmenu-innermenu3">
                                            <div class="col-md-12">
                                                <a href="#" class="close-bt"></a>
                                                <div class="title-colection">Настенная Berlingo</div>
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <div>
                                                            <img src="../images/pic-ber.jpg" alt="" class="img-responsive" />
                                                        </div>
                                                        <div class="colection-description"> 
                                                            Серия Bellagio позволяет зрительно преобразовать пространство. Благодаря натуральным оттенкам и оригинальным орнаментам, глянцевой поверхности и необычным эффектам, плитка серии Adora помогает создать яркие дизайнерские решения.
                                                        </div>
                                                        <div>
                                                            <a href="#" class="bt-show-colection">Посмотреть коллекцию</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5 jq-listcoll">
                                                        <div class="menu-list-colection">
                                                            <div class="title-colection2">Товары в коллекции Настенная (8)</div>
                                                            <ul>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="img-warp"><img src="../images/plate.jpg" alt="" /></span>
                                                                        <span class="name">Напольная плитка Керамин Калипсо 7С 400x275</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="img-warp"><img src="../images/plate.jpg" alt="" /></span>
                                                                        <span class="name">Напольная плитка Керамин Калипсо 7С 400x275</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="img-warp"><img src="../images/plate.jpg" alt="" /></span>
                                                                        <span class="name">Напольная плитка Керамин Калипсо 7С 400x275</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="img-warp"><img src="../images/plate.jpg" alt="" /></span>
                                                                        <span class="name">Напольная плитка Керамин Калипсо 7С 400x275</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="img-warp"><img src="../images/plate.jpg" alt="" /></span>
                                                                        <span class="name">Напольная плитка Керамин Калипсо 7С 400x275</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="img-warp"><img src="../images/plate.jpg" alt="" /></span>
                                                                        <span class="name">Напольная плитка Керамин Калипсо 7С 400x275</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="img-warp"><img src="../images/plate.jpg" alt="" /></span>
                                                                        <span class="name">Напольная плитка Керамин Калипсо 7С 400x275</span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <span class="img-warp"><img src="../images/plate.jpg" alt="" /></span>
                                                                        <span class="name">Напольная плитка Керамин Калипсо 7С 400x275</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                          
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </nav>
</div>