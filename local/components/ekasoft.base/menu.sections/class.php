<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BMenuSections extends \BaseModuleComponent {

    public function onPrepareComponentParams( $arParams ) {
        \CModule::IncludeModule("ekasoft.base");
        $arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::SECTION;
        $arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::SECTION;
        $arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::STRUCTURE;

        return parent::onPrepareComponentParams($arParams);
    }

    public function executeComponent() {
        //if ( $this->startResultCache() ) {

            $this->objModel->addSelect(array("UF_IMAGE_WHITE", "UF_IMAGE_COLOR", "UF_COLOR"));

            // получили разделы, подразделы и картинки к ним
            $this->arResult = $this->getElements();
            $this->arResult["COLORS"] = getSectionColors();
/*
            $objDecorator = new \EkaSoft\Base\Decorator\ElementsFull($this);
            $objModel = new \Ekasoft\Base\Model\ElementsFull($objDecorator);
            $objModel->initParams($this->arParams);

            $objModel->setOrder(array("CREATED_DATE" => "DESC", "SORT" => "SORT"));
            $objModel->setNavParams(array("nTopCount" => 10));
            $objEntity = new \EkaSoft\Base\Entity\Tovar($objModel, $objDecorator);
            $objDecorator->initEntity($objEntity);

            $objModel->addFilter(array("PROPERTY_IN_MENU_VALUE" => "Y"));
            $this->arResult["HITS"] = $objEntity->getItems();
            if ( count($objEntity->getFiles()) > 0 ) {
                $this->arResult["FILES"] += $objEntity->getFiles();
            }
            
            $arIDsGoods = array();
            $arIDsGoods = count($objEntity->getID()) > 0 ? $objEntity->getID() : array();
            $objModel->setFilterByCode("PROPERTY_IN_MENU_VALUE", "");
            
            foreach ( $this->arResult["ITEMS"][0] as $arItems ) {
                foreach ( $this->arResult["ITEMS"][$arItems["ID"]] as $arItem ) {
                    $objModel->setFilterByCode("SECTION_ID", $arItem["ID"]);
                    $this->arResult["SECTIONS_GOODS"][$arItem["ID"]] = $objEntity->getItems();
                    if ( count($objEntity->getFiles()) > 0 ) {
                        $this->arResult["FILES"] += $objEntity->getFiles();
                    }
                    if( $objEntity->getID() ) {
                        $arIDsGoods += $objEntity->getID();
                    }
                }
            }

            $objDecoratorSKU = new \EkaSoft\Base\Decorator\Tovar($this);
            $objModelSKU = new \Ekasoft\Base\Model\Tovar($objDecoratorSKU);
            $objModelSKU->setFilter(array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "=PROPERTY_CML2_LINK" => $arIDsGoods));
            $objEntitySKU = new EkaSoft\Base\Entity\TovarSKU($objModelSKU, $objDecoratorSKU);
            $objDecoratorSKU->initEntity($objEntitySKU);

            $this->arResult["OFFERS"] = $objEntitySKU->getItems();
*/
       // }
        
        $this->initStructure();
        $this->includeComponentTemplate();
        
        
    }

    public function initStructure() {
        foreach ( $this->arResult["RESULT"] as $arItem ) {
            \EkaSoft\Base\Core\Structure::getInstance()->addStructure($arItem);
        }
        \EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->setStructure($this->arResult["ITEMS"]);
        \EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->setFiles($this->arResult["FILES"]);
    }

}
