<div class="col-xs-12 col-sm-6 col-md-3 js-top-menu-element posinit">
    <a  href="<?= $arResult["SECTION"]["SECTION_PAGE_URL"] ?>" data-href="#<?= $arResult["SECTION"]["CODE"]; ?>" class="but but1 but-<?= $arParams["COLORS"][$arResult["SECTION"]["UF_COLOR"]]["XML_ID"]; ?> bg bg-<?= $arParams["COLORS"][$arResult["SECTION"]["UF_COLOR"]]["XML_ID"]; ?> delay_04s1 a-url" title="<?= $arResult["SECTION"]["NAME"]; ?>">
        <div class="img-block">
            <img src="<?= $arResult["FILES"][$arResult["SECTION"]["UF_IMAGE_WHITE"]]["SRC"]; ?>" alt="<?= $arResult["SECTION"]["NAME"]; ?>" />
            <img src="<?= $arResult["FILES"][$arResult["SECTION"]["UF_IMAGE_COLOR"]]["SRC"]; ?>" alt="<?= $arResult["SECTION"]["NAME"]; ?>" />
        </div>
        <span class="text"><?= $arResult["SECTION"]["NAME"]; ?></span>
        <span class="clearfix"></span>
    </a>
    <nav class="header-menu-top <?= $arResult["SECTION"]["CODE"]; ?>" id="<?= $arResult["SECTION"]["CODE"]; ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-2">
                    <div class="title-menu"><?= $arResult["SECTION"]["NAME"]; ?> <i class="fa fa-caret-right"></i></div>
                    <? if ( isset($arResult["SECTIONS"]) && count($arResult["SECTIONS"]) > 0 ): ?>
                        <ul class="menu-first-level menu-level">
                            <? foreach ( $arResult["SECTIONS"] as $id ): ?>
                                <li>
                                    <? $arItem = \EkaSoft\Base\Core\Structure::getInstance()->getSection($id); ?>
                                    <a href="<?= $arItem["SECTION_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>"><?= $arItem["NAME"]; ?></a>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>
                </div>
                <div class="col-lg-offset-1 col-lg-9 col-sm-6 jq-showmenublock col-xs-offset-0 col-sm-offset-0 col-xs-12 visible-lg-inline-block" >
                    <div class="col-md-12">
                        <div class="infoCollection">
                            
                        </div>
                        <div class="popuplarCollection">
                            <div class="title-colection">Популярные1 коллекции</div>
                            <div class="slider-colection">
                                <div class="slide-block">
                                    <div class="slide-block slide-block-menu-wrapper">
                                        <? foreach ( $arResult["HITS"] as $arGoods ): ?>
                                            <div class="item-slid">
                                                <a href="<?= $arGoods["DETAIL_URL_PAGE"]; ?>" title="<?= $arGoods["NAME"]; ?>">
                                                    <img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200, "MODE" => "inv"), $arResult["FILES"][$arGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arGoods["NAME"]; ?>" class="img-responsive" />

                                                        <img class="hit-col" src="/local/img/hit-icon.png">
                                                        <?if($arGoods['PROPERTIES']['SALE_ICON']['VALUE'] == 'Y'){?>
                                                             <img class="hit-col" src="/local/img/sale-icon.png">
                                                        <?}?>
                                                        <?if($arGoods['ICON_COUNTRY']){?>
                                                             <img class="hit-col" src="<?=$arGoods['ICON_COUNTRY'];?>">
                                                        <?}?>     

                                                    <div class="info-block">
                                                        <p class="title-info"><?= $arGoods["NAME"]; ?></p>
                                                        <? if ( $arGoods["PROPERTIES"]["PRICE"]["VALUE"] != '' && $arGoods["PROPERTIES"]["PRICE"]["VALUE"] > 0 ): ?>
                                                            <p class="price">
                                                                от <?= number_format($arGoods["PROPERTIES"]["PRICE"]["VALUE"], 2, " руб.", " "); ?> коп.
                                                                <span class="byr">от <?= number_format($arGoods["PROPERTIES"]["PRICE"]["VALUE"] * 10, 0, "", " "); ?> <span>000</span> руб.</span>
                                                            </p>
                                                        <? endif; ?>
                                                    </div>
                                                </a>
                                                <div class="topmenu-innermenu3">
                                                    <div class="col-md-12">
                                                        <a href="<?= $arGoods["DETAIL_PAGE_URL"]; ?>" class="close-bt"></a>
                                                        <div class="title-colection"><?= $arGoods["NAME"]; ?></div>
                                                        <div class="row">
                                                            <div class="col-md-7">
                                                                <div>
                                                                    <img src="<?= imageResize(array("WIDTH" => 360, "HEIGHT" => 174, "MODE" => "inv"), $arResult["FILES"][$arGoods["DETAIL_PICTURE"]]["SRC"]); ?>" alt="" class="img-responsive" />
                                                                </div>
                                                                <div class="colection-description"> 
                                                                    <?= substr($arGoods["PREVIEW_TEXT"], 0, 280) . '...'; ?>
                                                                </div>
                                                                <div>
                                                                    <a href="<?= $arGoods["DETAIL_PAGE_URL"]; ?>" class="bt-show-colection">Посмотреть коллекцию</a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5 jq-listcoll">
                                                                <div class="menu-list-colection">
                                                                    <div class="title-colection2">Товары в коллекции <?= $arGoods["NAME"]; ?> (<?= count($arResult["OFFERS"][$arGoods["ID"]]); ?>)</div>
                                                                    <ul>
                                                                        <? foreach ( $arResult["OFFERS"][$arGoods["ID"]] as $arOffer ): ?>
                                                                            <li>
                                                                                <a href="<?= $arOffer["DETAIL_PAGE_URL"]; ?>" title="<?= $arOffer["NAME"]; ?>">
                                                                                    <span class="img-warp"><img src="<?= imageResize(array("WIDTH" => 24, "HEIGHT" => 48, "MODE" => "inv"), $arResult["FILES"][$arOffer["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arOffer["NAME"]; ?>" /></span>
                                                                                    <span class="name"><?= $arOffer["NAME"]; ?></span>
                                                                                </a>
                                                                            </li>
                                                                        <? endforeach; ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>