<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BMenuSectionsCollections extends \BaseModuleComponent {

    private $IDs = array();

    public function decorateFiles($row) {
        unset($row["ID"], $row["TIMESTAMP_X"], $row["MODULE_ID"], $row["HEIGHT"], $row["WIDTH"], $row["CONTENT_TYPE"], $row["FILE_SIZE"], $row["SUBDIR"], $row["FILE_NAME"], $row["ORIGINAL_NAME"], $row["DESCRIPTION"], $row["HANDLER_ID"], $row["EXTERNAL_ID"]);
        return $row;
    }
    
    public function decorateItem($row) {
        $row["KEY"] = $row["IBLOCK_SECTION_ID"];
        return $row;
    }
    
    public function onPrepareComponentParams( $arParams ) {
        \CModule::IncludeModule("ekasoft.base");
        $arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::ELEMENT_FULL;
        $arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::ELEMENT_FULL;
        $arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::TOVAR;

        return parent::onPrepareComponentParams($arParams);
    }

    protected function getTovarInSections() {
        $this->objModel->setOrder(array("CREATED_DATE" => "DESC", "SORT" => "SORT"));
        //$this->objModel->setNavParams(array("nTopCount" => 10));
        
        $this->objModel->addFilter(array("SECTION_ID" => \EkaSoft\Base\Core\Structure::getInstance()->getChild($this->arParams["SECTION_ID"]), "PROPERTY_IN_MENU_VALUE" => "Y"));
        //foreach ( \EkaSoft\Base\Core\Structure::getInstance()->getChild($this->arParams["SECTION_ID"]) as $arSection ) {
        //    $this->objModel->setFilterByCode("SECTION_ID", $arSection);
            $this->arResult["SECTIONS_GOODS"] = $this->objEntity->getItems(2);
            $this->addFiles($this->objEntity);
        //}
    }

    protected function getOffers() {
        if($this->IDs) {
            $this->objDecorator = new \EkaSoft\Base\Decorator\Tovar($this);
            $this->objModel = new \Ekasoft\Base\Model\Tovar($this->objDecorator);
            $this->objModel->setFilter(array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "=PROPERTY_CML2_LINK" => $this->IDs));
            $this->objEntity = new EkaSoft\Base\Entity\TovarSKU($this->objModel, $this->objDecorator);
            $this->objDecorator->initEntity($this->objEntity);

            $this->arResult["OFFERS"] = $this->objEntity->getItems();
            $this->addFiles($this->objEntity);
        }
    }

    protected function getSections() {
        $this->arResult["SECTION"] = \EkaSoft\Base\Core\Structure::getInstance()->getSection($this->arParams["SECTION_ID"]);
        $this->arResult["SECTIONS"] = \EkaSoft\Base\Core\Structure::getInstance()->getChild($this->arParams["SECTION_ID"]);
        $this->addFiles(\EkaSoft\Base\Core\Structure::getInstance());
    }
    
    private function addFiles($obj) {
        if ( count($obj->getFiles()) > 0 ) {
            $this->arResult["FILES"] += $obj->getFiles();
        }
    }

    protected function getHits() {
        $this->objModel->delFilterByCode("SECTION_ID");
        $this->objModel->delFilterByCode("PROPERTY_IN_MENU_VALUE");
        $this->objModel->addFilter(array("INCLUDE_SUBSECTIONS" => "Y", "SECTION_ID" => $this->arResult["SECTIONS"], "PROPERTY_IN_HIT_VALUE" => "Y"));
        $this->arResult["HITS"] = $this->objEntity->getItems();
        foreach($this->arResult["HITS"] as $k => $v){
            if($v['PROPERTIES']['COUNTRY']['VALUE_ENUM']){
                $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $v['PROPERTIES']['COUNTRY']['VALUE_ENUM']);
                $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($object = $nr->GetNextElement())
                {
                    $arF = $object->GetFields();
                    if($arF['DETAIL_PICTURE']){
                       $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                       $this->arResult["HITS"][$k]['ICON_COUNTRY'] = $image['SRC'];
                    }
                }
            }
        }
        $this->IDs = $this->objEntity->getIDs();
        $this->addFiles($this->objEntity);
    }

    public function executeComponent() {
        define("QQ", 'Y');
        if ( $this->StartResultCache(10) ) {

            //$this->getTovarInSections();
            $this->getSections();
            $this->getHits();
            $this->getOffers();unset($GLOBALS['QQ']);
			//$this->getSections();            
			//prent($this->arResult);die;
            $this->IncludeComponentTemplate();
            
        }    
    }

}
