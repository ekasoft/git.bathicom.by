<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BSearch extends \BaseModuleComponent {

    protected $obEntitySKU;
    protected $sectionBysecondLevelSectionID;

    public function onPrepareComponentParams( $arParams ) {
        $arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::TOVAR;
        $arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::TOVAR;
        $arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::TOVARSKU;
        //$arParams["IBLOCK_ID"] = 11;
        $arParams["ELEMENTS_ID"] = (array)$arParams["ELEMENTS_ID"];
        $arParams["SEARCH_SECTION_ID"] = (int)$arParams["SEARCH_SECTION_ID"];

        return parent::onPrepareComponentParams($arParams);
    }

    public function executeComponent() {
        $this->objModel->addSelect(array("CATALOG_GROUP_1"));

        if (empty($this->arParams["ELEMENTS_ID"])) {
            $this->includeComponentTemplate("notfound");
            return;
        }

        if( $this->arParams["ELEMENTS_ID"]) {
            $this->objModel->addFilter(array("ID" => $this->arParams["ELEMENTS_ID"], "!CATALOG_TYPE" => 3));
        }


        $this->arResult["ITEMS"] = $this->objEntity->getResult();
        $this->arResult["FILES"] = $this->objEntity->getFiles();
/*        $objDecoratorSKU = new EkaSoft\Base\Decorator\Tovar($this);
        $objModel = new Ekasoft\Base\Model\Tovar($objDecoratorSKU);

        $objModel->setFilter(array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "=PROPERTY_CML2_LINK" => $this->objEntity->getIDs()));
        
        $objEntitySKU = new EkaSoft\Base\Entity\TovarSKU($objModel, $objDecoratorSKU);
        $objDecoratorSKU->initEntity($objEntitySKU);

        $this->arResult["OFFERS"] = $objEntitySKU->getItems();
        $this->arResult["OTHER_OFFERS"] = $objEntitySKU->getOffers();
        
        $this->arResult["FILES"] = $this->arResult["FILES"] + $objEntitySKU->getFiles();
*/
        $this->getSections();
        $this->getQuantity($this->objEntity->getIDs());
        $this->filterItemsBySectionID();
        if (empty($this->arResult["ITEMS"])) {
            $this->includeComponentTemplate("notfound");
            return;
        }
        //prent(array_shift($this->arResult["ITEMS"]));
        $this->includeComponentTemplate();
    }

    private function getSections() {
        foreach ($this->arResult["ITEMS"] as $item) {
            if ($item["IBLOCK_SECTION_ID"]) {
                $sectionsID[] = $item["IBLOCK_SECTION_ID"];
            }
        }
        if (empty($sectionsID)) return;

        $sectionsID = array_unique($sectionsID);
        $secondLevelSectionsID = array();
        foreach ($sectionsID as $id) {
            $dbl = CIBlockSection::GetNavChain(10, $id);
            while ($res = $dbl->Fetch()) {
                if ($res["DEPTH_LEVEL"] == 2) {
                    $this->sectionBysecondLevelSectionID[$id] = $res["ID"];
                    $secondLevelSectionsID[$res["ID"]] = $res["ID"];
                }
            }
        }
        
        $sort = array("SORT" => "ASC");
        $filter = array("ID" => $secondLevelSectionsID, "IBLOCK_ID" => 10);
        $select = array("ID", "NAME");
        $dbl = CIBlockSection::GetList($sort, $filter, false, $select);
        while ($res = $dbl->Fetch()) {
            $this->arResult["SECTIONS"][$res["ID"]] = $res["NAME"];
        }
    }
    
    public function getQuantity( $IDs ) {
        \CModule::IncludeModule("catalog");
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()));
        while ( $row = $dbl->fetch() ) {
            $this->arResult["QUANTITIES"][$row["ID"]] = $row;
        }
    }

    private function filterItemsBySectionID() {
        $sectionID = $this->arParams["SEARCH_SECTION_ID"];
        if (empty($sectionID)) return;

        $sectionBysecondLevelSectionID = $this->sectionBysecondLevelSectionID;

        $this->arResult["ITEMS"] = array_filter($this->arResult["ITEMS"], function($a) use ($sectionID, $sectionBysecondLevelSectionID) {
            return $sectionBysecondLevelSectionID[$a["IBLOCK_SECTION_ID"]] == $sectionID;
        });
    }

}
