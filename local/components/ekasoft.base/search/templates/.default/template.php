<a class="js-search-close-popup search-close-popup" href="#" title="Закрыть форму"></a>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="#" class="replaceFormBitixAjax search-form">
                <input type="hidden" name="FORM_ACTION" value="<?= $APPLICATION->GetCurPageParam("", array("q", "SEARCH_SECTION_ID", "bxajaxid")) ?>"/>
                <input type="hidden" name="bxajaxidjQuery"
                       value="<?= $arParams["PARENT_AJAX_ID"] ? $arParams["PARENT_AJAX_ID"] : $arParams["AJAX_ID"]; ?>"/>
                <input type="hidden" name="AJAX_CALLjQuery" value="Y"/>
                <input id="search-input" name="q" type="text" value="<?= $arParams["QUERY"] ?>" />
                <?//<span class="cancel-bt"></span>?>
            </form>
        </div>
    </div>
    <div class="row search-result-block-new-row">
        <div class="col-md-3">
            <? if ( empty($arParams["SEARCH_SECTION_ID"]) ): ?>
                <div class="title-menu">Все категории <i class="fa fa-caret-right"></i></div>
            <? else: ?>
                <? foreach ( $arResult["SECTIONS"] as $id => $name ): ?>
                    <? if ( $arParams["SEARCH_SECTION_ID"] == $id ): ?>
                        <div class="title-menu"><?= $name ?> <i class="fa fa-caret-right"></i></div>
                    <? endif ?>
                <? endforeach ?>
            <? endif ?>
            <? if ( $arResult["SECTIONS"] ): ?>
                <ul class="menu-first-level menu-level">
                    <li class="<?= empty($arParams["SEARCH_SECTION_ID"]) ? "active" : "" ?>"><a href="<?= $APPLICATION->GetCurPageParam("", array("SEARCH_SECTION_ID")) ?>">Все категории</a></li>
                    <? foreach ( $arResult["SECTIONS"] as $id => $name ): ?>
                        <li class="<?= $arParams["SEARCH_SECTION_ID"] == $id ? "active" : "" ?>"><a href="<?= $APPLICATION->GetCurPageParam("SEARCH_SECTION_ID=$id&q={$arParams["QUERY"]}", array("SEARCH_SECTION_ID")) ?>"><?= $name ?></a></li>
                    <? endforeach ?>
                </ul>
            <? endif ?>
        </div>
        <div class="col-md-9 search-result-block-new-col">
            <div class="products-list products_list-list new-style-list">
                <? foreach ( $arResult["ITEMS"] as $item ):
                    if( isset($item["PROPERTIES"]["CML2_LINK"]) && $item["PROPERTIES"]["CML2_LINK"]["VALUE"] == '' ) {
                        continue;
                    }
                ?>
                    <?
                    $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array(
                    "RESULT" => $item,
                    "FILES" => $arResult["FILES"],
                    "TEMPLATE" => "vertical_row",
                    "PROPER_VIEW" => $arResult["PROPER_VIEW"],
                    "QUANTITY" => $arResult["QUANTITIES"][$item["ID"]])
                    );?>
                    <? /* <div class="row product-item">
                      <div class="col-sm-3">
                      <div class="img-wrap">
                      <img src="/local/images/product-prev-01.png" alt="img" class="img-responsive" />
                      </div>
                      </div>
                      <div class="col-sm-4">
                      <h5><a href="#"><?=$item["NAME"]?></a></h5>
                      <div class="description">
                      <p>Страна: Польша</p>
                      <p>Тип: Напольный</p>
                      <p>Подвод воды: Боковой</p>
                      <p>Выпуск: Косой</p>
                      </div>
                      </div>
                      <div class="col-sm-5">
                      <div class="price"></div>
                      <div class="share">
                      <span class="old"><?=$item["CATALOG_PRICE_1"]?></span>
                      <span class="discount">Экономия 1 000 000 000</span>
                      </div>
                      <div class="parameters">
                      <input class="count-input" min="1" value="1" type="number" placeholder=""/>
                      <div class="units">
                      <ul>
                      <li>м2</li>
                      <li class="active">шт</li>
                      </ul>
                      </div>
                      <a class="button" href="#">В корзину</a>
                      </div>
                      <div class="buy-one-click">
                      <a class="buy" href="#">Купить в один клик</a>
                      <form>
                      <input type="submit" value="Купить" /><input type="text" class="masktel" placeholder="+375 (XX) XXX-XX-XX" />
                      <span class="bt-close-one-click"></span>
                      </form>
                      </div>
                      </div>
                      </div> */
                    ?>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>
