<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="#" class="replaceFormBitixAjax search-form">
                <input type="hidden" name="FORM_ACTION" value="<?= $APPLICATION->GetCurPageParam("", array("q", "SEARCH_SECTION_ID", "bxajaxid")) ?>"/>
                <input type="hidden" name="bxajaxidjQuery"
                       value="<?= $arParams["PARENT_AJAX_ID"] ? $arParams["PARENT_AJAX_ID"] : $arParams["AJAX_ID"]; ?>"/>
                <input type="hidden" name="AJAX_CALLjQuery" value="Y"/>
                <input id="search-input" name="q" type="text" value="<?= $arParams["QUERY"] ?>" />
                <span class="cancel-bt"></span>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?if (empty($arParams["SEARCH_SECTION_ID"])):?>
                <div class="title-menu">Все категории <i class="fa fa-caret-right"></i></div>
            <?else:?>
                <?foreach ($arResult["SECTIONS"] as $id => $name):?>
                    <?if($arParams["SEARCH_SECTION_ID"] == $id):?>
                        <div class="title-menu"><?=$name?> <i class="fa fa-caret-right"></i></div>
                    <?endif?>
                <?endforeach?>
            <?endif?>
            <?if ($arResult["SECTIONS"]):?>
                <ul class="menu-first-level menu-level">
                    <li class="<?=empty($arParams["SEARCH_SECTION_ID"]) ? "active" : ""?>"><a href="<?=$APPLICATION->GetCurPageParam("", array("SEARCH_SECTION_ID"))?>">Все категории</a></li>
                    <?foreach ($arResult["SECTIONS"] as $id => $name):?>
                        <li class="<?=$arParams["SEARCH_SECTION_ID"] == $id ? "active" : ""?>"><a href="<?=$APPLICATION->GetCurPageParam("SEARCH_SECTION_ID=$id", array("SEARCH_SECTION_ID"))?>"><?=$name?></a></li>
                    <?endforeach?>
                </ul>
            <?endif?>
        </div>
        <div class="col-md-9">
            <div class="products-list products_list-list ">
                Ничего не найдено
            </div>
        </div>
    </div>
</div>
