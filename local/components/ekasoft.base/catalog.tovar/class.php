<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BCatalogTovar extends \BaseModuleComponent {

    protected $obEntitySKU;

    public function onPrepareComponentParams( $arParams ) {
        //prent($arParams);
        $arParams['CACHE_TIME'] = 86400*30;
        $arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::TOVAR;
        $arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::TOVAR;
        $arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::TOVARSKU;
        $url_first = explode('?',$_SERVER['REQUEST_URI']);
        $arParams['URL'] = $url_first[0];

        if($_REQUEST['IBLOCK_ID']) {
            $arParams['IBLOCK_ID'] = $_REQUEST['IBLOCK_ID'];
        }
        if($arParams['IBLOCK_ID'] == 11) {
            //$arParams["SECTION_ID"] = 268;
            $arParams["SECTION_ID"] = getSectionIdByCode($_REQUEST['TP_SECTION_CODE'],$arParams['IBLOCK_ID']);
            /*
            // Получаем id основных товаров
            if($_REQUEST['SECTION_ID']) {
                $arFilter = array('IBLOCK_ID' => 10, 'ACTIVE' => 'Y', 'SECTION_ID' => $_REQUEST['SECTION_ID'], 'INCLUDE_SUBSECTIONS' => 'Y');
                $arSelect = array('IBLOCK_ID','ID');
                $res = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
                $ids = array();
                while($ob = $res->GetNextElement()) {
                    $item = $ob->GetFields();
                    $ids[$item['ID']] = $item['ID'];
                }
                //prent($ids);
            }
            // Получаем id торговых предложений
            if($ids) {
                $arFilter = array('IBLOCK_ID' => 11, 'ACTIVE' => 'Y', 'PROPERTY_CML2_LINK' => $ids);
                $arSelect = array('IBLOCK_ID','ID');
                $res = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
                $idsTp = array();
                while($ob = $res->GetNextElement()) {
                    $item = $ob->GetFields();
                    $idsTp[$item['ID']] = $item['ID'];
                }
                //prent($idsTp);
                if($idsTp) {
                    $arParams['ID'] = $idsTp;
                }
            }*/
        } else {
            //$arParams["SECTION_ID"] = \EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->getIDByCode($_REQUEST["SECTION_CODE"]);
            $arFilter = Array('IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'CODE'=>$_REQUEST["SECTION_CODE"]);
            $arSelect = array('ID');
            $db_list = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
            if($ar_result = $db_list->GetNext()) {
                $arParams["SECTION_ID"] = $ar_result['ID'];
            }
        }
        
        $arParams["ELEMENT_CODE"] = $_REQUEST["ELEMENT_CODE"];
        //prent($arParams);
        if ( $_REQUEST["ajax"]["count"] != '' ) {
            $arParams["ELEMENT_COUNT"] = (int) $_REQUEST["ajax"]["count"];
        } else {
            $arParams["ELEMENT_COUNT"] = 12;
        }

//        if( $_REQUEST["ajax"]["sorted"] > 0 ) {
//            $arParams["AJAX"]["SORT"] = (int) $_REQUEST["ajax"]["sorted"];
//        }
        //prent($_SESSION["EKASOFT"]);
        if($_REQUEST['SECTION_ID']) {
            $arParams["AJAX"]["SORT"] = $_SESSION["EKASOFT"]["AJAX"]["SORTED"][$_REQUEST['SECTION_ID']];
        } else {
            $arParams["AJAX"]["SORT"] = $_SESSION["EKASOFT"]["AJAX"]["SORTED"][$arParams["SECTION_ID"]];
        }
        
        $arParams["GOODS"] = $_REQUEST["SKU_CODE"];
        
        $arParams["PAGEN"] = ($_REQUEST["PAGEN_1"]) ? (int) $_REQUEST["PAGEN_1"] : 1;

        $arParams["SORT"] = array(
          1 => array("SORT" => "ASC"),
          2 => array("CATALOG_PRICE_1" => "ASC"),
          3 => array("CATALOG_PRICE_1" => "DESC")
        );
        
        //prent($arParams);
        //exit();
        return parent::onPrepareComponentParams($arParams);
    }

    public function sortAjax() {
        if( !isset($this->arParams["SORT"][$this->arParams["AJAX"]["SORT"]]) ) {
            return false;
        }
        
        $this->objModel->setOrder($this->arParams["SORT"][$this->arParams["AJAX"]["SORT"]]);
    }
    
    public function executeComponent() {
        
        if ( $this->startResultCache() ) {
            if( isset($this->arParams["AJAX"]["SORT"]) ) {

                $this->sortAjax();
            }

            $this->objFilter = new \EkaSoft\Base\Filter\SmartFilter($this->arParams);
            //prent($this->arParams);
            $this->objModel->addSelect(array("CATALOG_GROUP_1"));
            $this->objModel->setFilter($this->objFilter->getFilter());
            $this->arResult["ITEMS"] = $this->objEntity->getResult();
            $this->arResult["FILES"] = $this->objEntity->getFiles();
            
            $this->getQuantity($this->objEntity->getIDs());
            
            if($this->arParams['IBLOCK_ID'] == 10) {
                $resId = CIBlockSection::GetByID($this->arParams["SECTION_ID"]);
                if($ar_res = $resId->GetNext()) {
                    $this->arResult["SECTION"] = $ar_res;
                }
            } elseif($this->arParams['IBLOCK_ID'] == 11) {
                $resId = CIBlockSection::GetByID($this->arParams["SECTION_ID"]);
                if($ar_res = $resId->GetNext()) {
                    $this->arResult["SECTION"] = $ar_res;
                }
            }

            $this->arResult["PROPER_VIEW"] = array("BRAND", "COLLECTION_REF");
            
            $this->arResult["NAV_PARAMS"] = $this->objModel->getCDBResult();
            global $APPLICATION;
            $APPLICATION->SetPageProperty("count_elements", $this->objModel->SelectedRowsCount());

                    if($_REQUEST['PAGEN_1'] > 1)
                    {
                            list($can,) = explode('?', getenv('REQUEST_URI'), 2);
                            $GLOBALS['APPLICATION']->AddHeadString('<link rel="canonical" href="http://bathicom.by'.$can.'" />', true);
                    }
            
            
            
            $this->includeComponentTemplate();
        }
        

        
        $this->seoParams();

        






    }

    public function getQuantity( $IDs ) {
        \CModule::IncludeModule("catalog");
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()));
        while ( $row = $dbl->fetch() ) {
            $this->arResult["QUANTITIES"][$row["ID"]] = $row;
        }
    }

}
