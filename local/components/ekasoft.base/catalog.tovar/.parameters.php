<?

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

if ( !CModule::IncludeModule("iblock") )
    return;

use Bitrix\Iblock;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-" => " "));

$arIBlocks = Array();
$db_iblock = CIBlock::GetList(Array("SORT" => "ASC"), Array("SITE_ID" => $_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "-" ? $arCurrentValues["IBLOCK_TYPE"] : "")));
while ( $arRes = $db_iblock->Fetch() )
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$arComponentParameters = array(
  "GROUPS" => array(
  ),
  "PARAMETERS" => array(
    "AJAX_MODE" => array(),
    "IBLOCK_TYPE" => Array(
      "PARENT" => "BASE",
      "NAME" => "Где лежит инфоблока",
      "TYPE" => "LIST",
      "VALUES" => $arTypesEx,
      "DEFAULT" => "news",
      "REFRESH" => "Y",
     ),
    "IBLOCK_ID" => Array(
      "PARENT" => "BASE",
      "NAME" => "Инфоблок",
      "TYPE" => "LIST",
      "VALUES" => $arIBlocks,
      "DEFAULT" => '={$_REQUEST["ID"]}',
      "ADDITIONAL_VALUES" => "Y",
      "REFRESH" => "Y",
    ),
    "CACHE_TIME" => Array("DEFAULT" => 36000000),
  )
);