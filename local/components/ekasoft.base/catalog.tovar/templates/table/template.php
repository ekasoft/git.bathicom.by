<div class="products-list categories-list-tovar-e">
    <div class="row">
    <?if( count($arResult["ITEMS"]) == 0 ):?>
        <?$APPLICATION->IncludeFile("/local/include/custom_users/emptyfilter.php", array(), array("MODE"=>"html"));?>
    <?else:?>
        <?foreach( $arResult["ITEMS"] as $arItem ):?>
            <div class="col-lg-4 col-md-6">          
            <?$APPLICATION->IncludeComponent("ekasoft.view:element", "item",  
                    Array(
                      "RESULT" => $arItem,
                      "FILES" => $arResult["FILES"],
                      "PROPER_VIEW" => $arResult["PROPER_VIEW"], 
                      "QUANTITY" => $arResult["QUANTITIES"][$arItem["ID"]])
                    );?>
            </div>
        <?endforeach;?>
    <?endif;?>
    </div>
</div>
<?$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', 'collections', array(
    'NAV_RESULT' => $arResult["NAV_PARAMS"],
));?>