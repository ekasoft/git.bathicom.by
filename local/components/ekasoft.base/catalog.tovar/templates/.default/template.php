<div class="products-list products_list-list new-style-list">
    <?if( count($arResult["ITEMS"]) == 0 ):?>
        <?$APPLICATION->IncludeFile("/local/include/custom_users/emptyfilter.php", array(), array("MODE"=>"html"));?>
    <?else:?>
        <?foreach( $arResult["ITEMS"] as $arItem ):?>
            <?$APPLICATION->IncludeComponent("ekasoft.view:element", "item",  
                    Array(
                      "RESULT" => $arItem,
                      "FILES" => $arResult["FILES"],
                      "TEMPLATE" => "vertical",
                      "PROPER_VIEW" => $arResult["PROPER_VIEW"], 
                      "QUANTITY" => $arResult["QUANTITIES"][$arItem["ID"]])
                    );?>
        <?endforeach;?>
    <?endif;?>
</div>
<?$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', 'collections', array(
    'NAV_RESULT' => $arResult["NAV_PARAMS"],
));?>