<div class="seo-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="seoslider">
                    <?foreach($arResult["ITEMS"][0]["GALLERY"] as $iGallery):?>
                        <img src="<?= $arResult["FILES"][$iGallery]["SRC"];?>" alt="<?= $arResult["ITEMS"][0]["NAME"]?>" class="img-responsive" />
                    <?endforeach;?>
                </div>
            </div>
            <div class="col-md-6">
                <h3><?= $arResult["SECTION"]["NAME"];?></h3>
                <p><?= $arResult["SECTION"]["DESCRIPTION"];?></p>
            </div>
        </div>
    </div>
</div>
<? $countOffers = count($arResult["OFFERS"]);
if( $countOffers > 0 ):?>
    <section class="categories-section-e">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">Товары коллекции <span><?= $countOffers;?></span></div>
                </div>
                <div class="col-md-12">
                    <div class="categories-list-tovar-e products-list">
                        <div class="row">
                            <?foreach($arResult["OFFERS"] as $arItem):?>
                                <?$APPLICATION->IncludeComponent("ekasoft.view:element", "plitka", Array("RESULT" => $arItem, "FILES" => $arResult["FILES"]));?>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?endif;?>