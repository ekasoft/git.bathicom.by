<div class="colections-list row">
    <? foreach ( $arResult["ITEMS"] as $arItem ): ?>
        <div class="col-lg-4 col-md-6">
            <div class="colections-item">
                <div class="img-warp">
                    <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>">
                        <img src="<?= imageResize(array("WIDTH" => 260, "HEIGHT" => 199, "MODE" => "inv"), $arResult["FILES"][$arItem["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arItem["NAME"]; ?>" class="img-responsive" />
                    </a>
                </div>
                <div class="text-warp">
                    <div class="title-colectino-item">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>"><?= $arItem["NAME"]; ?></a>
                    </div>
                    <div class="price">от 58 000 <span>00</span></div>
                </div>
                <div class="colections-item-hover">
                    <div class="js-left-part">
                        <div class="img-warp">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>">
                                <img src="<?= imageResize(array("WIDTH" => 260, "HEIGHT" => 199, "MODE" => "in"), $arResult["FILES"][$arItem["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arItem["NAME"]; ?>" class="img-responsive" />
                            </a>
                        </div>
                        <div class="text-warp">
                            <div class="title-colectino-item"><a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>"><?= $arItem["NAME"]; ?></a></div>
                            <div class="price">от 58 000 <span>00</span></div>
                        </div>
                    </div>
                    <? if ( isset($arResult["OFFERS"][$arItem["ID"]]) ): ?>
                        <div class="colections-item-slider">
                            <? foreach ( $arResult["OFFERS"][$arItem["ID"]] as $key => $arItemSKU ): ?>
                                <? if ( $key == 0 || $key == 6 ): ?>
                                    <div class="slide-item">
                                    <? endif; ?>    
                                    <div class="colections-item-slider-item">
                                        <a href="<?= $arItemSKU["DETAIL_PAGE_URL"]; ?>" title="<?= $arItemSKU["NAME"]; ?>">
                                            <img src="<?= imageResize(array("WIDTH" => 24, "HEIGHT" => 48), $arResult["FILES"][$arItemSKU["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arItemSKU["NAME"]; ?>"  />
                                        </a>
                                        <div class="showtovar">
                                            <div class="categories-list-tovar-e product-list">
                                                <div class="product-item" style="border: none; background: none;">
                                                    <div class="img-wrap">
                                                        <img src="<?= imageResize(array("WIDTH" => 176, "HEIGHT" => 92, "MODE" => "in"), $arResult["FILES"][$arItemSKU["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="img" class="img-responsive" />
                                                    </div>
                                                    <div class="text-warp">
                                                        <span class="h5">
                                                            <a href="<?= $arItemSKU["DETAIL_PAGE_URL"]; ?>" title="<?= $arItemSKU["NAME"]; ?>"><?= $arItemSKU["NAME"]; ?></a>
                                                        </span>
                                                        <div class="rating-wrap">
                                                            <div class="rating">5</div>
                                                            <span class="count">15</span>
                                                        </div>
                                                        <div class="price">8 999 990 <span class="small">000</span></div>
                                                        <div class="share">
                                                            <span class="old">9 999 900 000</span>
                                                            <span class="discount">Экономия 1 000 000 000</span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <? if ( $key == 5 || $key == count($arResult["OFFERS"][$arItem["ID"]]) - 1 ): ?>
                                    </div>
                                <? endif; ?>
                            <? endforeach; ?>
                        </div>
                    <? endif; ?>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>
<?$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', 'collections', array(
    'NAV_RESULT' => $arResult["NAV_PARAMS"],
));?>
