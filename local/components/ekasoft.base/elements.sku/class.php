<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BElementsSKU extends \BaseModuleComponent {
    
    public function onPrepareComponentParams( $arParams ) {
      /*  $arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::TOVAR;
        $arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::TOVAR;
        $arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::TOVARSKU;*/
        $arParams['SECTION_CODE'] = $_REQUEST['SECTION_CODE']?htmlspecialchars($_REQUEST["SECTION_CODE"]):htmlspecialchars($_REQUEST["PREV_SECTION"]);
        //prent($_REQUEST["SECTION_CODE"]);
       
        //prent($arParams['SECTION_ID']);

        $arParams['CACHE_TIME'] = 86400*30;
       /* $arParams["SECTION_ID"] = \EkaSoft\Base\Core\Structure::getInstance()->getIDByCode($_REQUEST["SECTION_CODE"]) 
                ? \EkaSoft\Base\Core\Structure::getInstance()->getIDByCode($_REQUEST["SECTION_CODE"])
                : \EkaSoft\Base\Core\Structure::getInstance()->getIDByCode($_REQUEST["PREV_SECTION"]); */
        //prent($arParams["SECTION_ID"]);
        $arParams["ELEMENT_CODE"] = isset($_REQUEST["ELEMENT_CODE"]) ? $_REQUEST["ELEMENT_CODE"] : $_REQUEST["SECTION_CODE"];
        
        $arParams["GOODS"] = $_REQUEST["SKU_CODE"];
        $arParams["TYPE"] = htmlspecialchars($_REQUEST['TYPE']);
        
        $arParams["DEBUG"] = htmlspecialchars($_REQUEST['debug']);
        
        //$arParams["VIEW_MAIN_PROP"] = array("BRAND", "COLLECTION_REF");
        //$arParams["VIEW_PROP"] = array("ARTNUMBER", "WIDTH", "COLOR", "LENGTH", "HEIGHT", "ADDRESS_PRODUCER", "NAME_SUPPLIER", "MEASURE", "COLOR_CODE");
        //prent($arParams);
       
        
            //prent($arParams);

        return parent::onPrepareComponentParams($arParams);
    }
    
    public function decorateItem($row) {
        if( $row["DETAIL_PICTURE"] == '' ) {
            $row["DETAIL_PICTURE"] = $row["PREVIEW_PICTURE"];
        }
        
        return $row;
    }
    public function getQuantity($IDs) {
        \CModule::IncludeModule("catalog");
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()));
        while( $row = $dbl->fetch() ) {
            $this->arResult["QUANTITIES"][$row["ID"]] = $row;
        }
    }
    
    public function seoTitle() {
        global $APPLICATION;
        $first = $this->arResult["ITEMS"];
        if( !isset($this->arResult["ITEMS"]["NAME"]) ) {
            $first = array_shift($this->arResult["ITEMS"]);
        }
        
        //print_r($this->arResult);

        $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($first["IBLOCK_ID"], $first["ID"]);
        $this->arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();

        //print_r($this->arResult["IPROPERTY_VALUES"]);

        if ($this->arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != "")
                $APPLICATION->SetPageProperty("title", $this->arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]);
        else
                $APPLICATION->SetPageProperty("title", $first["NAME"]);

        $browserTitle = \Bitrix\Main\Type\Collection::firstNotEmpty(
                $first["PROPERTIES"], array($arParams["BROWSER_TITLE"], "VALUE")
                ,$first, $arParams["BROWSER_TITLE"]
                ,$this->arResult["IPROPERTY_VALUES"], "ELEMENT_META_TITLE"
        );
        if (is_array($browserTitle))
                $APPLICATION->SetTitle(implode(" ", $browserTitle));
        elseif ($browserTitle != "")
                $APPLICATION->SetTitle($browserTitle);

        $metaKeywords = \Bitrix\Main\Type\Collection::firstNotEmpty(
                $first["PROPERTIES"], array($arParams["META_KEYWORDS"], "VALUE")
                ,$this->arResult["IPROPERTY_VALUES"], "ELEMENT_META_KEYWORDS"
        );
        if (is_array($metaKeywords))
                $APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords));
        elseif ($metaKeywords != "")
                $APPLICATION->SetPageProperty("keywords", $metaKeywords);

        $metaDescription = \Bitrix\Main\Type\Collection::firstNotEmpty(
                $first["PROPERTIES"], array($arParams["META_DESCRIPTION"], "VALUE")
                ,$this->arResult["IPROPERTY_VALUES"], "ELEMENT_META_DESCRIPTION"
        );
        if (is_array($metaDescription))
                $APPLICATION->SetPageProperty("description", implode(" ", $metaDescription));
        elseif ($metaDescription != "")
                $APPLICATION->SetPageProperty("description", $metaDescription);

        //$APPLICATION->SetTitle($first["NAME"]);
    }
    
    public function getSoput($tp,$main) {
        $ids == array();
        foreach($tp as $v) {
            $ids[] = $v;
        }
        foreach($main as $v) {
            $ids[] = $v;
        }
        if($ids) {
            $objDecorator = new \EkaSoft\Base\Decorator\Tovar($this);
            $objModel = new \Ekasoft\Base\Model\Tovar($objDecorator);
            $objModel->addFilter(array("ACTIVE" => "Y", "ID" => $ids));
            $objEntity = new \EkaSoft\Base\Entity\TovarSKU($objModel, $objDecorator);
            $objDecorator->initEntity($objEntity);

            $objEntity->setOfferActive("CODE", $this->arParams["GOODS"]);

            $this->arResult["SOPUT"] = $objEntity->getResult();
            /*
            foreach($ids as $v) {
                $this->arResult["SOPUT"][$v] = $this->arResult["SOPUT_TEMP"][$v];
            }*/

            $this->arResult["FILES"] += $objEntity->getFiles();

            $this->getQuantity($objEntity->getIDs());
        }
    }
    
    public function getOtherSizes($ids) {
        if($ids) {
            $objDecorator = new \EkaSoft\Base\Decorator\Tovar($this);
            $objModel = new \Ekasoft\Base\Model\Tovar($objDecorator);
            $objModel->addFilter(array("ACTIVE" => "Y", "ID" => $ids));
            $objEntity = new \EkaSoft\Base\Entity\TovarSKU($objModel, $objDecorator);
            $objDecorator->initEntity($objEntity);
            //$objEntity->setOfferActive("CODE", $this->arParams["GOODS"]);
            $this->arResult["OTHER_SIZES"] = $objEntity->getResult();
            foreach($this->arResult["OTHER_SIZES"] as $k => $v) {
                $sizes = array($v['PROPERTIES']['LENGTH']['VALUE'],$v['PROPERTIES']['WIDTH']['VALUE'],$v['PROPERTIES']['HEIGHT']['VALUE'],$v['DEPTH']['VALUE']);
                foreach($sizes as $key => $val) {
                    if(empty($val)) {
                        unset($sizes[$key]);
                    }
                }
                $this->arResult["OTHER_SIZES"][$k]['SIZES_ARRAY'] = $sizes;
            }
            $this->getQuantity($objEntity->getIDs());
        }
    }
    
    public function addViewed($id) {
        global $USER;
        $user_id = CSaleBasket::GetBasketUserID();
        if($user_id  > 0) {
           \Bitrix\Catalog\CatalogViewedProductTable::refresh($id, $user_id);
        }
    }
    
    public function getPropertyGroups($myArray) {
            if($myArray){
            //prent($myArray);
             // Запрос свойств текущего раздела
        
            $this->arResult['CURR_PROPS'] = CIBlockSectionPropertyLink::GetArray($this->arParams['IBLOCK_ID'],$this->arParams['SECTION_ID']);
            foreach($this->arResult['CURR_PROPS'] as $k => $v) {
                $res = CIBlockProperty::GetByID($v['PROPERTY_ID']);
                if($ar_res = $res->GetNext()) {
                    //prent($ar_res['CODE']);
                    $this->arParams["VIEW_PROP"][] = $ar_res['CODE'];
                    $this->arParams['SHOW_PROPS'][$ar_res['ID']] = $ar_res['CODE'];
                    $this->arResult['CURR_PROPS'][$k]['NAME'] = $ar_res['NAME'];
                }
            }
            
            
            CModule::IncludeModule('redsign.grupper');
            $arGroups = array();	
            $rsGroups = CRSGGroups::GetList(array("SORT"=>"ASC","ID"=>"ASC"),array());
            //забираем айдишники группировок
            while($arGroup = $rsGroups->Fetch())
            {
                $groupIds[$arGroup['ID']] = $arGroup['ID'];
                $resultGroup[$arGroup['ID']]['NAME'] = $arGroup['NAME'];
            }
            if($groupIds){
                //забраем свойства, принадлежащие группировке
                $rsBinds = CRSGBinds::GetList(array("ID"=>"ASC"),array("GROUP_ID"=>$groupIds));
                while($arBind = $rsBinds->Fetch())
                {
                    $resultGroup[$arBind['GROUP_ID']]['CHILDREN'][$arBind['ID']] = $arBind;
                }
            }
            //формируем новый массив, где ключами будут айдишники свойств
            $newArray = array();
            foreach($myArray as $k => $v){
                if($this->arParams['SHOW_PROPS'][$v['ID']] && $v['VALUE']){    
                $newArray[$v['ID']]['NAME'] = $v['NAME'];
                $newArray[$v['ID']]['CODE'] = $v['CODE'];
                $newArray[$v['ID']]['VALUE'] = $v['VALUE'];
                }
            }
            
            foreach($resultGroup as $k => $v){
                foreach ($v['CHILDREN'] as $key => $val){
                    if($newArray[$val['IBLOCK_PROPERTY_ID']]['VALUE']){
                        $resultGroup[$k]['SHOW'] = 'Y';
                        $resultGroup[$k]['CHILDREN'][$key]['NAME'] = $newArray[$val['IBLOCK_PROPERTY_ID']]['NAME'];
                        $resultGroup[$k]['CHILDREN'][$key]['CODE'] = $newArray[$val['IBLOCK_PROPERTY_ID']]['CODE'];
                        $resultGroup[$k]['CHILDREN'][$key]['VALUE'] = $newArray[$val['IBLOCK_PROPERTY_ID']]['VALUE'];
                    }
                    unset($newArray[$val['IBLOCK_PROPERTY_ID']]);
                }
            }      
            
            
            
           
            $resultGroup['NO_GROUP']['CHILDREN'] = $newArray; 

            //prent($resultGroup);
            $this->arResult['PROPERTIES_TO_SHOW'] = $resultGroup;            
            
            //иконки
            if($myArray['SALE_ICON']['VALUE'] == 'Y'){
                $this->arResult['ICONS'][1] = '/local/img/sale-icon.png';
            }
            
            if($myArray['IN_HIT']['VALUE'] == 'Y'){
                $this->arResult['ICONS'][2] = '/local/img/hit-icon.png';
            }

            if($myArray['COUNTRY']['VALUE_ENUM']){
                $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $myArray['COUNTRY']['VALUE_ENUM']);
                $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($object = $nr->GetNextElement())
                {
                    $arF = $object->GetFields();
                    if($arF['DETAIL_PICTURE']){
                       $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                       $this->arResult['ICONS'][3] = $image['SRC'];
                    }
                }
            }
            
        }                           
    }
    
    
    public function executeComponent() {
        if( $this->startResultCache() ) {
            $this->arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::TOVAR;
            $this->arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::TOVAR;
            $this->arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::TOVARSKU;
            
            $this->objModel->addSelect(array("CATALOG_GROUP_1"));
            $this->objModel->addFilter(array("CODE" => $this->arParams["ELEMENT_CODE"]));
            
                                     
            $arFilter = Array('IBLOCK_ID'=>$this->arParams['IBLOCK_ID'], 'CODE' => $this->arParams['SECTION_CODE']);
            $arSelect = array('ID');
            $db_list = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
            if($section = $db_list->GetNext())
            {
                $this->arParams['SECTION_ID'] = $section['ID'];
            } 
        
            $this->arResult["ITEMS"] = $this->objEntity->getResult(false);
            $this->arResult["ITEMS"] = $this->arResult["ITEMS"][0];
            $this->arResult["FILES"] = $this->objEntity->getFiles();
            $this->arResult["SECTION"] = EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->getSection($this->arParams["SECTION_ID"]);
            $this->arResult["QUANTITY"] = $this->getQuantity($this->objEntity->getIDs());
            $this->arResult["BASKET"] = $this->getBasket();
            $this->getSoput($this->arResult["ITEMS"]["PROPERTIES"]["SOPUT_TP"]["VALUE"],$this->arResult["ITEMS"]["PROPERTIES"]["SOPUT_MAIN"]["VALUE"]);
            $this->getOtherSizes($this->arResult["ITEMS"]["PROPERTIES"]["OTHER_SIZES"]["VALUE"]);
            $this->getPropertyGroups($this->arResult['ITEMS']['PROPERTIES']);
            
            $this->includeComponentTemplate();
            $this->endResultCache();
        }
        $this->addViewed($this->arResult["ITEMS"]["ID"]);
        $this->seoParams();
    }
    
}