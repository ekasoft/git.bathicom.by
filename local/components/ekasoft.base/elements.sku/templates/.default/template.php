<section class="detail-section-01">
    <?//prent($arResult);
    //prent($arParams["VIEW_PROP"]);?>
    <div class="container">
        <div class="row product-item jsDetailElement">
            <div class="col-lg-4 col-sm-6">
                <div class="product-viewer-wrap">
                    <div class="prod-photo">
                        <?
                        $width = $height = $widthBig = $heightBig = 0;
                        /*if ( $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["WIDTH"] > 358 ) {
                            $delta = $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["WIDTH"] - 358;
                            $width = 358;
                            $widthBig = $width * 2;
                            $height = $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["HEIGHT"] - $delta;
                        } else if ( $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["HEIGHT"] > 358 ) {
                            $delta = $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["HEIGHT"] - 358;
                            $height = 358;
                            $heightBig = $height * 2;
                            $width = $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["WIDTH"] - $delta;
                        }*/if ( $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["WIDTH"] > $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["HEIGHT"] ) {
                            $width = 358;
                            $widthBig = $width * 2;
                        } else {
                            $height = 358;
                            $heightBig = $height * 2;
                        }
                        $_img = imageResize(array("WIDTH" => $width, "HEIGHT" => $height, "MODE" => "inv"), $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["SRC"]);
                        $_imgBig = imageResize(array("WIDTH" => $widthBig, "HEIGHT" => $heightBig, "MODE" => "inv"), $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["SRC"]);
                        ?>
                        <?if($arResult['ITEMS']['PROPERTIES']['VIDEO']['VALUE']){?>
                            <?$i=0;?>
                            <?foreach($arResult['ITEMS']['PROPERTIES']['VIDEO']['VALUE'] as $k => $v){?>
                                <a rel="fancybox-video" class="tube-video num-<?=$i;?>" style="display: none;" href="https://www.youtube.com/watch?v=<?=$v;?>" data-img="http://img.youtube.com/vi/<?=$v?>/0.jpg">
                                    <img src="http://img.youtube.com/vi/<?=$v;?>/1.jpg" alt="img" class="img-responsive zoom-img"/>
                                    <div class="big-vid"></div>
                                </a>
                            <?$i++;?>
                            <?}?>    
                                <a href="<?= imageResize(array("WIDTH" => 358 * 2, "HEIGHT" => 358 * 2, "MODE" => "in"), $_imgBig); ?>" class="cloud-zoom" id="zoom1" rel="adjustX: 20">
                                    <img src="<?= imageResize(array("WIDTH" => 358, "HEIGHT" => 358), $_img); ?>" alt="img" class="img-responsive zoom-img"/>
                                </a>   
                        <?}else{?>
                            <a href="<?= imageResize(array("WIDTH" => 358 * 2, "HEIGHT" => 358 * 2, "MODE" => "in"), $_imgBig); ?>" class="cloud-zoom" id="zoom1" rel="adjustX: 20">
                                <img src="<?= imageResize(array("WIDTH" => 358, "HEIGHT" => 358), $_img); ?>" alt="img" class="img-responsive zoom-img"/>
                            </a>
                        <?}?>
                        <? if ( in_array($arResult["ITEMS"]["ID"], $arResult["BASKET"]) ): ?>
                            <div class="mark green">Товар в корзине</div>
                        <? elseif ( $arResult["QUANTITIES"][$arResult["ITEMS"]["ID"]]["QUANTITY"] == 0 ): ?>
                            <div class="mark gray">Нет в наличии</div>
                        <? endif; ?>  
                            <div>
                            <?if($arResult['ITEMS']['PRICES']['DISCOUNT']['PERCENT']){?>
                                <div class="discount-perc"><span>-<?=$arResult['ITEMS']['PRICES']['DISCOUNT']['PERCENT'];?>%</span></div>
                            <?}?>
                            <?foreach ($arResult['ICONS'] as $keya => $valya){?>
                                <img src="<?=$valya;?>" class="cart-sticker">
                            <?}?>
                            </div>
                    </div>
                    <? //if ( count($arResult["ITEMS"]["GALLERY"]) > 1 ): ?>
                        <div id="gallery" class="products-preview">
                            <ul>
                                <?if($arResult['ITEMS']['PROPERTIES']['VIDEO']['VALUE']){?>
                                    <?$i=0;?>
                                    <?foreach($arResult['ITEMS']['PROPERTIES']['VIDEO']['VALUE'] as $k => $v){?>
                                        <li>
                                            <a href="http://img.youtube.com/vi/<?=$v;?>/1.jpg" class="video" data-num="<?=$i;?>" data-href="https://www.youtube.com/watch?v=<?=$v;?>" data-image="http://img.youtube.com/vi/<?=$v;?>/0.jpg" style="position: relative">
                                                <img src="http://img.youtube.com/vi/<?=$v;?>/1.jpg" alt="img" width="65" height="65">
                                                <div class="vid"></div>
                                            </a>
                                        </li>
                                        <?$i++;?>
                                    <?}?>
                                <?}?>
                                <? $i = 1; ?>
                                <? foreach ( $arResult["ITEMS"]["GALLERY"] as $value ): ?>
                                    <li>
                                        <?
                                        $width = $height = $widthBig = $heightBig = 0;
                                        if ( $arResult["FILES"][$value]["WIDTH"] > 358 ) {
                                            $delta = $arResult["FILES"][$value]["WIDTH"] - 358;
                                            $width = 358;
                                            $widthBig = $width * 2;
                                            $height = $arResult["FILES"][$value]["HEIGHT"] - $delta;
                                        } else if ( $arResult["FILES"][$value]["HEIGHT"] > 358 ) {
                                            $delta = $arResult["FILES"][$value]["HEIGHT"] - 358;
                                            $height = 358;
                                            $heightBig = $height * 2;
                                            $width = $arResult["FILES"][$value]["WIDTH"] - $delta;
                                        } else if ( $arResult["FILES"][$value]["WIDTH"] > $arResult["FILES"][$value]["HEIGHT"] ) {
                                            $width = 358;
                                            $widthBig = $width * 2;
                                        } else {
                                            $height = 358;
                                            $heightBig = $height * 2;
                                        }
                                        $_img = imageResize(array("WIDTH" => $width, "HEIGHT" => $height, "MODE" => "inv"), $arResult["FILES"][$value]["SRC"]);
                                        $_imgBig = imageResize(array("WIDTH" => $widthBig, "HEIGHT" => $heightBig, "MODE" => "inv"), $arResult["FILES"][$value]["SRC"]);
                                        ?>
                                        <a href="<?= imageResize(array("WIDTH" => 385 * 2, "HEIGHT" => 385 * 2, "MODE" => "in"), $_imgBig); ?>" class="cloud-zoom-gallery" rel="useZoom: 'zoom1', smallImage: '<?= imageResize(array("WIDTH" => 385 * 2, "HEIGHT" => 385 * 2, "MODE" => "in"), $_imgBig); ?>'" data-image="<?= imageResize(array("WIDTH" => 385 * 2, "HEIGHT" => 385 * 2, "MODE" => "in"), $_imgBig); ?>">
                                            <img src="<?= imageResize(array("WIDTH" => 385 * 2, "HEIGHT" => 385 * 2, "MODE" => "in"), $_imgBig); ?>" alt="img" width="65" height="65">
                                        </a>
                                    </li>
                                    <? $i++; ?>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    <? //endif; ?>
                </div>
            </div>
            <div class="col-lg-8 col-sm-6">
                <div class="top-info">
                    <div class="row">
                        <div class="col-lg-7 col-md-12 paddbot20">
                            <h1><?= $arResult["ITEMS"]["NAME"]; ?></h1>
                            <? if($arResult['OTHER_SIZES']) { ?>
                            <div class="other-sizes">
                                <div class="head">Другие размеры:</div>
                                <? foreach($arResult['OTHER_SIZES'] as $k => $v) { ?>
                                <? if($v['ID'] != $arResult['ITEMS']['ID']) { ?>
                                <div class="item">
                                    <a href="<?=$v['DETAIL_PAGE_URL']?>">
                                        <?=implode('x', $v['SIZES_ARRAY'])?><?=$v['PRICES']['HTML']['PRICE']?' - '.$v['PRICES']['HTML']['PRICE']:''?>
                                    </a>
                                </div>
                                <? } ?>
                                <div class=""></div>
                                <? } ?>
                            </div>
                            <? } ?>
                            <ul>
                                <?
                                foreach ( $arParams["VIEW_MAIN_PROP"] as $code ):
                                    if ( $arResult["ITEMS"]["PROPERTIES"][$code]["VALUE"] != "" ) {
                                        continue;
                                    }
                                    ?>
                                    <li><?= $arResult["ITEMS"]["PROPERTIES"][$code]["NAME"] == "Коллекция" ? "Бренд" : $arResult["ITEMS"]["PROPERTIES"][$code]["NAME"]; ?>: <?= $arResult["ITEMS"]["PROPERTIES"]["BRAND"]["VALUE"]; ?></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                        <div class="col-lg-5 col-md-12">
                            <div class="buy-info-wrapper">
                                <div class="buy-info">
                                    <!--div class="buy-head">
                                        <div class="rating-wrap">
                                            <div class="rating"></div>
                                        </div>
                                        <span class="reviews">15</span>
                                        <a class="view" href="#rewievs">Посмотреть отзывы</a>
                                    </div-->
                                    <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arResult["ITEMS"], "FILES" => $arResult["FILES"], "TEMPLATE" => "price")); ?>
                                    <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arResult["ITEMS"], "TEMPLATE" => "basket", "TYPE" => $arParams["TYPE"], "QUANTITY" => $arResult["QUANTITIES"][$arResult["ITEMS"]["ID"]])); ?>                            
                                </div>                               
                            </div>
                            <div class="halva-block">
                                <a><span>«Халва»</span></a>
                                <img src="/local/images/halva-pic.png">
                            </div>
                            <?if($arResult['ITEMS']['EXTERNAL_ID'] != $arResult['ITEMS']['ID']){?>
                                <div class="rassrochka-block">
                                    <a><span>Рассрочка</span></a>
                                    <img src="/local/images/rasscrochka-img.png">
                                </div>
                            <?}?>
                        </div>
                    </div>
                </div>

                
                <div class="product-feature">
                    <span class="h4">Характеристики</span>
                    <?/*?>
                    <table>
                        <?
                        foreach ( $arParams["VIEW_PROP"] as $code ):
                            if ( \Ekasoft\Base\Tools\Validate::isEmpty($arResult["ITEMS"]["PROPERTIES"][$code]["VALUE"]) ) {
                                continue;
                            }
                            ?>
                            <tr>
                                <th>
                                    <span><?= $arResult["ITEMS"]["PROPERTIES"][$code]["NAME"] ?>:</span>
                                </th>
                                <td> <?= $arResult["ITEMS"]["PROPERTIES"][$code]["VALUE"]; ?></td>
                            </tr>
                        <? endforeach; ?>
                    </table> */?>

                    <table>
                        <?foreach($arResult['PROPERTIES_TO_SHOW'] as $k => $v){?>
                            <?if($v['NAME'] && $v['SHOW'] == 'Y'){?>
                            <tr><th colspan="2" class="prop-group-name"><?=$v['NAME'];?></th></tr>
                            <?}?>
                            <?foreach($v['CHILDREN'] as $key => $val){?>
                                <?if($val['NAME'] && $val['VALUE']){?>
                                    <tr>
                                        <th>
                                            <span><?=$val['NAME'];?>:</span>
                                        </th>
                                        <td><?=$val['VALUE'];?></td>
                                    </tr>
                                <?}?>
                            <?}?>
                                    <tr><td></td><td></td></tr>        
                        <?}?>
                    </table>    

                </div>
                
                <? if ( $arResult["ITEMS"]["DETAIL_TEXT"] != '' ): ?>
                    <div class="product-description">
                        <span class="h4">
                            <a class="title active" href="#">Описание</a>
                        </span>
                        <div class="text">
                            <?= $arResult["ITEMS"]["DETAIL_TEXT"]; ?>
                        </div>
                    </div>
                <? endif; ?>
                
            </div>
        </div>
    </div>
</section>

<? if($arResult['SOPUT']) { ?>
<section class="related-products categories-list-tovar-e products-list">
    <div class="container">
        <h3><a href="#" title="">Сопутствующие товары</a> <span class="count"><?= count($arResult["SOPUT"]); ?></span></h3>
        <div class="related-slider">
            <?
            foreach ( $arResult["SOPUT"] as $arOffer ):
                if ( $arOffer["ID"] == $arResult["OFFER_ID"] ) {
                    continue;
                }
                ?>
                <div class="slide">
                    <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arOffer, "FILES" => $arResult["FILES"], "BASKET_BLOCK" => "N", "QUANTITY" => $arResult["QUANTITIES"][$arOffer["ID"]])); ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</section>
<? } ?>