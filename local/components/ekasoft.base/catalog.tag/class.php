<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BCatalog extends \BaseModuleComponent {

    protected $obEntitySKU;

    public function onPrepareComponentParams( $arParams ) {
        $arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::ELEMENT_FULL;
        $arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::ELEMENT_FULL;
        $arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::TOVAR;

		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DETAIL_TEXT", "PROPERTY_*");
		$arFilter = Array("IBLOCK_ID"=>14, "CODE"=>$_REQUEST['ELEMENT_CODE']);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
		if($ob = $res->GetNextElement()){ 
			$arParams['TAG'] = $ob->GetFields();  
			$arParams['TAG']['PROPERTIES'] = $ob->GetProperties();
		}

        if ( $_REQUEST["ajax"]["count"] != '' ) {
            $arParams["ELEMENT_COUNT"] = (int) $_REQUEST["ajax"]["count"];
        } else {
            $arParams["ELEMENT_COUNT"] = 24;
        }

		if(empty($arParams["TAG"]["ID"]))
		{
			if (Bitrix\Main\Loader::includeModule('iblock'))
			{
				\Bitrix\Iblock\Component\Tools::process404(
					'����� ����������� � ��������'
					,true
					,true
					,true
					,'/404.php'
				);
			}
		}
		
		if($_REQUEST['PAGEN_1'] > 1)
		{
			list($can,) = explode('?', getenv('REQUEST_URI'), 2);
			$GLOBALS['APPLICATION']->AddHeadString('<link rel="canonical" href="http://bathicom.by'.$can.'" />', true);
		}
//if(getenv('REMOTE_ADDR') == '188.162.14.7')
//{
//	 print_r($arParams); exit;
//}

        return parent::onPrepareComponentParams($arParams);
    }

    public function getQuantity($IDs) {
        \CModule::IncludeModule("catalog");
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()));
        while( $row = $dbl->fetch() ) {
            $this->arResult["QUANTITIES"][$row["ID"]] = $row;
        }
    }
    
    public function executeComponent() {
        if ( $this->startResultCache() ) {

            $this->objFilter = new \EkaSoft\Base\Filter\SmartFilter($this->arParams);

            $this->objModel->addSelect(array("CATALOG_GROUP_1"));

            $this->objModel->setFilter($this->objFilter->getFilter());

            $this->objModel->addFilter(array("PROPERTY_TAGS" => array($this->arParams["TAG"]["ID"])));
//            unset($this->objModel->arFilter['CATALOG_SHOP_QUANTITY_1']);
//            print_r($this->objModel->arFilter);
            $this->arResult = $this->getElements( true );
            //print_r($this->arResult);

            $objDecoratorSKU = new EkaSoft\Base\Decorator\Tovar($this);
            $objModel = new Ekasoft\Base\Model\Tovar($objDecoratorSKU);
			
			if(!empty($this->objEntity->getIDs()))
			{
	            $objModel->setFilter(array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "=PROPERTY_CML2_LINK" => $this->objEntity->getIDs()));

	            $objEntitySKU = new EkaSoft\Base\Entity\TovarSKU($objModel, $objDecoratorSKU);
	            $objDecoratorSKU->initEntity($objEntitySKU);

	            $objEntitySKU->setOfferActive("CODE", $this->arParams["GOODS"]);

	            $this->arResult["OFFERS"] = $objEntitySKU->getItems();
	            $this->arResult["OTHER_OFFERS"] = $objEntitySKU->getOffers();
	            $this->getQuantity( $objEntitySKU->getIDs());
	            $this->arResult["FILES"] = $this->arResult["FILES"] + $objEntitySKU->getFiles();

	            $this->arResult["SECTION"] = EkaSoft\Base\Core\Structure::getInstance()->getSection($this->arParams["SECTION_ID"]);

	            $this->arResult["NAV_PARAMS"] = $this->objModel->getCDBResult();
	        }
            $this->includeComponentTemplate();
        }

        global $APPLICATION;
        $APPLICATION->SetPageProperty("count_elements", $this->objModel->SelectedRowsCount());

		$APPLICATION->SetPageProperty("title", $this->arParams["TAG"]["PROPERTIES"]["SEO_H1"]["VALUE"]);
		$APPLICATION->SetTitle($this->arParams["TAG"]["PROPERTIES"]["SEO_TITLE"]["VALUE"]);
		$APPLICATION->SetPageProperty("keywords", $this->arParams["TAG"]["PROPERTIES"]["SEO_KEYWORDS"]["VALUE"]);
		$APPLICATION->SetPageProperty("description", $this->arParams["TAG"]["PROPERTIES"]["SEO_DESCRIPTION"]["VALUE"]);
    }

}
