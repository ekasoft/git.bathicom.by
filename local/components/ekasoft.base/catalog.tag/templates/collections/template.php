<div class="colections-list row">
    <?if( count($arResult["ITEMS"]) == 0 ):?>
        <?$APPLICATION->IncludeFile("/local/include/custom_users/emptyfilter.php", array(), array("MODE"=>"html"));?>
    <?else:?>
        <? foreach ( $arResult["ITEMS"] as $arItem ): ?>
            <div class="col-lg-4 col-md-6">
                <div class="colections-item">
                    <div class="img-warp">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>">
                            <img src="<?= imageResize(array("WIDTH" => 260, "HEIGHT" => 200, "MODE" => "inv"), $arResult["FILES"][$arItem["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arItem["NAME"]; ?>" class="img-responsive" />
                        </a>
                    </div>
                    <div class="text-warp">
                        <div class="title-colectino-item">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>"><?= $arItem["NAME"]; ?></a>
                        </div>
                        <? if ( $arItem["PROPERTIES"]["PRICE"]["VALUE"] != '' && $arItem["PROPERTIES"]["PRICE"]["VALUE"] > 0 ): ?>
                            <div class="price">
                                от <?= number_format($arItem["PROPERTIES"]["PRICE"]["VALUE"], 2, " руб. ", " "); ?> коп.
                                <?/*<span class="byr">от <?= number_format(round($arItem["PROPERTIES"]["PRICE"]["VALUE"]*10000/100)*100, 0, "", " "); ?> руб.</span>*/?>
                            </div>
                        <? endif; ?>
                    </div>
                    <div class="colections-item-hover">
                        <div class="js-left-part">
                            <div class="img-warp">
                                <a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>">
                                    <img src="<?= imageResize(array("WIDTH" => 260, "HEIGHT" => 200, "MODE" => "in"), $arResult["FILES"][$arItem["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arItem["NAME"]; ?>" class="img-responsive" />
                                </a>
                            </div>
                            <div class="text-warp">
                                <div class="title-colectino-item"><a href="<?= $arItem["DETAIL_PAGE_URL"]; ?>" title="<?= $arItem["NAME"]; ?>"><?= $arItem["NAME"]; ?></a></div>
                                <? if ( $arItem["PROPERTIES"]["PRICE"]["VALUE"] != '' && $arItem["PROPERTIES"]["PRICE"]["VALUE"] > 0 ): ?>
                                    <div class="price">
                                        от <?= number_format($arItem["PROPERTIES"]["PRICE"]["VALUE"], 2, " руб. ", " "); ?> коп.
                                        <?/*<span class="byr">от <?= number_format(round($arItem["PROPERTIES"]["PRICE"]["VALUE"]*10000/100)*100, 0, "", " "); ?> руб.</span>*/?>
                                    </div>
                                <? endif; ?>
                            </div>
                        </div>
                        <? if ( isset($arResult["OFFERS"][$arItem["ID"]]) ): ?>
                            <div class="collection-item-new-wrap">
                                <div class="colections-item-slider">
                                    <?
                                    $index = 0;
                                    foreach ( $arResult["OFFERS"][$arItem["ID"]] as $key => $arItemSKU ):
                                        ?>
                                        <? if ( $index == 0 || $index == 6 ): ?>
                                            <div class="slide-item">
                                            <? endif; ?>    
                                            <div class="colections-item-slider-item">
                                                <a href="<?= $arItemSKU["DETAIL_PAGE_URL"]; ?>" title="<?= $arItemSKU["NAME"]; ?>">
                                                    <img src="<?= imageResize(array("WIDTH" => 24, "HEIGHT" => 48), $arResult["FILES"][$arItemSKU["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arItemSKU["NAME"]; ?>"  />
                                                </a>
                                                <div class="showtovar">
                                                    <div class="categories-list-tovar-e product-list">
                                                        <div class="product-item" style="border: none; background: none;">
                                                            <div class="img-wrap">
                                                                <img src="<?= imageResize(array("WIDTH" => 260, "HEIGHT" => 200, "MODE" => "in"), $arResult["FILES"][$arItemSKU["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="img" class="img-responsive" />
                                                            </div>
                                                            <div class="text-warp">
                                                                <span class="h5">
                                                                    <a href="<?= $arItemSKU["DETAIL_PAGE_URL"]; ?>" title="<?= $arItemSKU["NAME"]; ?>"><?= $arItemSKU["NAME"]; ?></a>
                                                                </span>
                                                                <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arItemSKU, "TEMPLATE" => "price_block")); ?>
                                                                <!--div class="rating-wrap">
                                                                    <div class="rating">5</div>
                                                                    <span class="count">15</span>
                                                                </div-->
                                                                <!--div class="price">8 999 990 <span class="small">000</span></div>
                                                                <div class="share">
                                                                    <span class="old">9 999 900 000</span>
                                                                    <span class="discount">Экономия 1 000 000 000</span>
                                                                </div-->

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <? if ( $index == 5 || $index == count($arResult["OFFERS"][$arItem["ID"]]) - 1 ): ?>
                                            </div>
                                        <? endif; ?>
                                        <?
                                        ++$index;
                                    endforeach;
                                    ?>
                                </div>
                                <a href="javascript:void(0);" title="Заркыть" class="collection-item-new-close-btn"></a>
                            </div>
                        <? endif; ?>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    <?endif;?>
</div>
<?
$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', 'collections', array(
  'NAV_RESULT' => $arResult["NAV_PARAMS"],
));
								//	print_r($arParams);
?>
<?= $arParams["~TAG"]["DETAIL_TEXT"]; ?>