<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Каталог товаров",
    "DESCRIPTION" => "Вывод каталога товара с торговыми предложениями",
    "ICON" => "/images/strings.gif",
    "PATH" => array(
        "ID" => "ekasoft",
        "NAME" => "Ека-софт",
        "CHILD" => array(
            "ID" => "catalog",
            "NAME" => "Каталог"
        )
    ),
);
?>
