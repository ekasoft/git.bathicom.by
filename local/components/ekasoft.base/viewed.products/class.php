<?php
if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

//\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BCatalogTovar extends \BaseModuleComponent {

    public function onPrepareComponentParams( $arParams ) {
        $arParams["DEBUG"] = htmlspecialchars($_REQUEST["debug"]);
        $arParams["ELEMENT_CODE"] = htmlspecialchars($_REQUEST["ELEMENT_CODE"]);
        $arParams["SKU_CODE"] = htmlspecialchars($_REQUEST["SKU_CODE"]);
        return parent::onPrepareComponentParams($arParams);
    }
    
    
    public function clearViewed() {
        \Bitrix\Catalog\CatalogViewedProductTable::clear(7);
    }
    
    public function getViewed() {
        $siteId = $this->getSiteId();
        $basketUserId = (int)CSaleBasket::GetBasketUserID(false);

        $filter = array('=FUSER_ID' => $basketUserId, '=SITE_ID' => $siteId);

        $viewedIterator = Bitrix\Catalog\CatalogViewedProductTable::GetList(array(
                'select' => array('PRODUCT_ID', 'ELEMENT_ID', 'DATE_VISIT'),
                'filter' => $filter,
                'order' => array('DATE_VISIT' => 'DESC'),
                'limit' => $this->arParams['PAGE_ELEMENT_COUNT']
        ));

        while ($viewedProduct = $viewedIterator->fetch()) {
            //prent($viewedProduct);
                $viewedProduct['ELEMENT_ID'] = (int)$viewedProduct['ELEMENT_ID'];
                $viewedProduct['PRODUCT_ID'] = (int)$viewedProduct['PRODUCT_ID'];
                $map[$viewedProduct['PRODUCT_ID']] = $viewedProduct['PRODUCT_ID'];
                /*
                if ($viewedProduct['ELEMENT_ID'] <= 0)
                        $emptyProducts[] = $viewedProduct['PRODUCT_ID'];*/
        }
        
        if($map) {
            $objDecorator = new \EkaSoft\Base\Decorator\Tovar($this);
            $objModel = new \Ekasoft\Base\Model\Tovar($objDecorator);
            $objModel->addFilter(array("ACTIVE" => "Y", "ID" => $map, "!=CODE" => $this->arParams["ELEMENT_CODE"]));
            $objEntity = new \EkaSoft\Base\Entity\TovarSKU($objModel, $objDecorator);
            $objDecorator->initEntity($objEntity);
            $this->arResult["VIEWED_TEMP"] = $objEntity->getResult();
            foreach($map as $k => $v) {
                $this->arResult["VIEWED"][$k] = $this->arResult["VIEWED_TEMP"][$v];
            }
            $this->arResult["FILES"] += $objEntity->getFiles();
            $this->arResult["QUANTITY"] = $this->getQuantity($map);
        }
    }
    
    public function getQuantity($IDs) {
        \CModule::IncludeModule("catalog");
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()));
        while( $row = $dbl->fetch() ) {
            $this->arResult["QUANTITIES"][$row["ID"]] = $row;
        }
    }
    
    public function executeComponent() {
        if ( $this->startResultCache() ) {
            $this->clearViewed();
            $this->getViewed();
            
            $this->includeComponentTemplate();
        }
    }
}
