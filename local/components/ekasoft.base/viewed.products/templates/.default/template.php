<? if($arResult["VIEWED"]) { ?>
<? //prent($arResult["VIEWED"]); ?>
<section class="related-products categories-list-tovar-e products-list">
    <div class="container">
        <h3><a href="#" title="">Просмотренные товары</a> <span class="count"><?= count($arResult["VIEWED"]); ?></span></h3>
        <div class="related-slider">
            <?
            foreach ( $arResult["VIEWED"] as $arOffer ):
                if ( $arOffer["ID"] == $arResult["OFFER_ID"] ) {
                    continue;
                }
                ?>
                <div class="slide">
                    <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arOffer, "FILES" => $arResult["FILES"], "BASKET_BLOCK" => "N", "QUANTITY" => $arResult["QUANTITIES"][$arOffer["ID"]])); ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</section>
<? } ?>