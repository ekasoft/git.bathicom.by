<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Просмотренные товары",
    "DESCRIPTION" => "Просмотренные товары",
    "ICON" => "/images/strings.gif",
    "PATH" => array(
        "ID" => "ekasoft",
        "NAME" => "Гиперлинк",
        "CHILD" => array(
            "ID" => "catalog",
            "NAME" => "Каталог"
        )
    ),
);
?>
