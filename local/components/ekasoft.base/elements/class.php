<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BElements extends \BaseModuleComponent {

    public function onPrepareComponentParams( $arParams ) {
        $arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::ELEMENT_FULL;
        $arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::ELEMENT_FULL;
        $arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::TOVAR;

        //$arParams["SECTION_ID"] = \EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->getIDByCode($_REQUEST["SECTION_CODE"]);
        $arParams['SECTION_CODE'] = htmlspecialchars($_REQUEST['SECTION_CODE']);
        
        $arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'CODE' => $arParams['SECTION_CODE']);
        $arSelect = array('ID');
        $db_list = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
        if($section = $db_list->GetNext())
        {
            $arParams['SECTION_ID'] = $section['ID'];
        }
        
        $arParams["ELEMENT_CODE"] = $_REQUEST["ELEMENT_CODE"];

        $arParams["GOODS"] = $_REQUEST["SKU_CODE"];
        $arParams["TYPE"] = htmlspecialchars($_REQUEST['TYPE']);
        
        $arParams["DEBUG"] = htmlspecialchars($_REQUEST['debug']);
        
        $arParams['TP_IBLOCK_ID'] = 11;

        //$arParams["VIEW_MAIN_PROP"] = array("BRANDTP", "COLLECTION_REF");
        //$arParams["VIEW_PROP"] = array("ARTNUMBER", "WIDTH", "COLOR", "LENGTH", "HEIGHT", "ADDRESS_PRODUCER", "NAME_SUPPLIER", "MEASURE", "COLOR_CODE");


        return parent::onPrepareComponentParams($arParams);
    }

    public function decorateItem( $row ) {
        if ( $row["CODE"] == $this->arParams["GOODS"] ) {
            $this->arResult["OFFER_ID"] = $row["ID"];
        }

        if ( $row["DETAIL_PICTURE"] == '' ) {
            $row["DETAIL_PICTURE"] = $row["PREVIEW_PICTURE"];
        }

        return $row;
    }
    
    public function getCollection() {
        $objDecorator = new \EkaSoft\Base\Decorator\Tovar($this);
        $objModel = new \Ekasoft\Base\Model\Tovar($objDecorator);
        $objModel->addFilter(array("IBLOCK_ID" => $this->arParams['IBLOCK_ID'], "ACTIVE" => "Y", "=CODE" => $this->arParams['ELEMENT_CODE']));
        $objEntity = new \EkaSoft\Base\Entity\TovarSKU($objModel, $objDecorator);
        $objDecorator->initEntity($objEntity);
        return $objEntity->getResult();
    }

    public function getOffers() {
        $objDecorator = new \EkaSoft\Base\Decorator\Tovar($this);
        $objModel = new \Ekasoft\Base\Model\Tovar($objDecorator);
        $objModel->addFilter(array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "=PROPERTY_CML2_LINK" => $this->objEntity->getIDs()));
        $objEntity = new \EkaSoft\Base\Entity\TovarSKU($objModel, $objDecorator);
        $objDecorator->initEntity($objEntity);
        $objEntity->setOfferActive("CODE", $this->arParams["GOODS"]);
        $this->arResult["OFFERS"] = $objEntity->getResult();
        $this->arResult["COLLECTION"] = $this->getCollection();
        $this->arResult["COLLECTION_PRICE"] = array();
        
        foreach($this->arResult["OFFERS"] as $k => $v) {
            if(empty($this->arResult["COLLECTION_PRICE"])) {
                $this->arResult["COLLECTION_PRICE"] = $v['PRICES'];
            } elseif($v['PRICES']['PRICE'] < $this->arResult["COLLECTION_PRICE"]['PRICE']) {
                $this->arResult["COLLECTION_PRICE"] = $v['PRICES'];
            }
            $this->arResult["COLLECTION_PRICE"] = $v['PRICES'];
            if($v['CODE'] == $this->arParams['GOODS']) {
                $this->arResult["DESCRIPTION"] = $v["DETAIL_TEXT"];
            }
        }
        $this->arResult["FILES"] += $objEntity->getFiles();
        $this->getQuantity($objEntity->getIDs());
    }
    
    public function getSoput($ids) {
        //prent($ids);
        if($ids) {
            $objDecorator = new \EkaSoft\Base\Decorator\Tovar($this);
            $objModel = new \Ekasoft\Base\Model\Tovar($objDecorator);
            $objModel->addFilter(array("ACTIVE" => "Y", "ID" => $ids));
            $objEntity = new \EkaSoft\Base\Entity\TovarSKU($objModel, $objDecorator);
            $objDecorator->initEntity($objEntity);
            $objEntity->setOfferActive("CODE", $this->arParams["GOODS"]);
            $this->arResult["SOPUT"] = $objEntity->getResult();
            $this->arResult["FILES"] += $objEntity->getFiles();
            $this->getQuantity($objEntity->getIDs());
        }
    }

    public function getQuantity( $IDs ) {
        \CModule::IncludeModule("catalog");
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()));
        while ( $row = $dbl->fetch() ) {
            $this->arResult["QUANTITIES"][$row["ID"]] = $row;
        }
    }

    public function seoTitle() {
        global $APPLICATION;
        $APPLICATION->SetTitle($this->arResult["OFFERS"][$this->arResult["OFFER_ID"]]["NAME"]);
    }
    
    public function addViewed($id) {
        global $USER;
        $user_id = CSaleBasket::GetBasketUserID();
        if($user_id  > 0) {
           \Bitrix\Catalog\CatalogViewedProductTable::refresh($id, $user_id);
        }
    }
    
    public function getPropertyGroups($myArray) {
            if($myArray){
            //prent($myArray);
           
             // Запрос свойств текущего раздела
        
            $this->arResult['CURR_PROPS'] = CIBlockSectionPropertyLink::GetArray($this->arParams['TP_IBLOCK_ID'],$this->arParams['SECTION_ID']);
            
                // Запрос свойств текущего раздела
                $this->arResult['CURR_PROPS'] = CIBlockSectionPropertyLink::GetArray($this->arParams['TP_IBLOCK_ID'],$this->arParams['SECTION_ID']);
                foreach($this->arResult['CURR_PROPS'] as $k => $v) {
                    $res = CIBlockProperty::GetByID($v['PROPERTY_ID']);
                    if($ar_res = $res->GetNext()) {
                        //prent($ar_res);
                        $this->arParams["VIEW_PROP"][] = $ar_res['CODE'];
                        $this->arParams["SHOW_PROPS"][$ar_res['ID']] = $ar_res['CODE'];
                        $this->arResult['CURR_PROPS'][$k]['NAME'] = $ar_res['NAME'];
                    }
                }
   
            
            CModule::IncludeModule('redsign.grupper');
            $arGroups = array();	
            $rsGroups = CRSGGroups::GetList(array("SORT"=>"ASC","ID"=>"ASC"),array());
            //забираем айдишники группировок
            while($arGroup = $rsGroups->Fetch())
            {
                $groupIds[$arGroup['ID']] = $arGroup['ID'];
                $resultGroup[$arGroup['ID']]['NAME'] = $arGroup['NAME'];
            }
            if($groupIds){
                //забраем свойства, принадлежащие группировке
                $rsBinds = CRSGBinds::GetList(array("ID"=>"ASC"),array("GROUP_ID"=>$groupIds));
                while($arBind = $rsBinds->Fetch())
                {
                    $resultGroup[$arBind['GROUP_ID']]['CHILDREN'][$arBind['ID']] = $arBind;
                }
            }
            //формируем новый массив, где ключами будут айдишники свойств
            $newArray = array();
            foreach($myArray as $k => $v){
                if($this->arParams['SHOW_PROPS'][$v['ID']] && $v['VALUE']){
                $newArray[$v['ID']]['NAME'] = $v['NAME'];
                $newArray[$v['ID']]['CODE'] = $v['CODE'];
                $newArray[$v['ID']]['VALUE'] = $v['VALUE'];
                }
            }

            foreach($resultGroup as $k => $v){
                foreach ($v['CHILDREN'] as $key => $val){
                    if($newArray[$val['IBLOCK_PROPERTY_ID']]['VALUE']){
                        $resultGroup[$k]['SHOW'] = 'Y';
                        $resultGroup[$k]['CHILDREN'][$key]['NAME'] = $newArray[$val['IBLOCK_PROPERTY_ID']]['NAME'];
                        $resultGroup[$k]['CHILDREN'][$key]['CODE'] = $newArray[$val['IBLOCK_PROPERTY_ID']]['CODE'];
                        $resultGroup[$k]['CHILDREN'][$key]['VALUE'] = $newArray[$val['IBLOCK_PROPERTY_ID']]['VALUE'];
                    }
                    unset($newArray[$val['IBLOCK_PROPERTY_ID']]);
                }
            }      
           
            $resultGroup['NO_GROUP']['CHILDREN'] = $newArray; 

            //prent($resultGroup);
            $this->arResult['PROPERTIES_TO_SHOW'] = $resultGroup; 
            
             //иконки
            if($myArray['SALE_ICON']['VALUE'] == 'Y'){
                $this->arResult['ICONS'][1] = '/local/img/sale-icon.png';
            }
            
            if($myArray['IN_HIT']['VALUE'] == 'Y'){
                $this->arResult['ICONS'][2] = '/local/img/hit-icon.png';
            }

            if($myArray['COUNTRY']['VALUE_ENUM']){
                $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $myArray['COUNTRY']['VALUE_ENUM']);
                $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($object = $nr->GetNextElement())
                {
                    $arF = $object->GetFields();
                    if($arF['DETAIL_PICTURE']){
                       $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                       $this->arResult['ICONS'][3] = $image['SRC'];
                    }
                }
            }
   
        }                           
    }
    
    
    public function executeComponent() {
        if ( $this->startResultCache() ) {
            $this->objModel->addSelect(array("CATALOG_GROUP_1"));
            $this->objModel->addFilter(array("CODE" => $this->arParams["ELEMENT_CODE"]));

            $this->arResult = $this->objEntity->getResult(false);
            $this->arResult["SECTION"] = EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->getSection($this->arParams["SECTION_ID"]);
            $this->arResult["BASKET"] = $this->getBasket();
            $this->getOffers();
            $ids = array();
            foreach($this->arResult["OFFERS"][$this->arResult["OFFER_ID"]]["PROPERTIES"]["SOPUT_TP"]["VALUE"] as $k => $v) {
                $ids[$v] = $v;
            }
            foreach($this->arResult["OFFERS"][$this->arResult["OFFER_ID"]]["PROPERTIES"]["SOPUT_MAIN"]["VALUE"] as $k => $v) {
                $ids[$v] = $v;
            }
            $this->getSoput($ids);
            $this->endResultCache();
            
        }
        $this->getPropertyGroups($this->arResult["OFFERS"][$this->arResult["OFFER_ID"]]["PROPERTIES"]);
        $this->includeComponentTemplate();
        $this->addViewed($this->arResult["OFFER_ID"]);

        $this->seoParams();
    }

}
