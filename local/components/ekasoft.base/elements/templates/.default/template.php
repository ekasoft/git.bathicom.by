<section class="detail-section-01">
    <?//prent($arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"]['VIDEO']['VALUE']);?>
    <div class="container">
        <div class="row product-item jsDetailElement">
            <div class="col-lg-4 col-sm-6">
                <div class="product-viewer-wrap">
                    <div class="prod-photo">
                        <?
                        $width = $height = $widthBig = $heightBig = 0;
                        /*if ( $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["WIDTH"] > 358 ) {
                            $delta = $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["WIDTH"] - 358;
                            $width = 358;
                            $widthBig = $width * 2;
                            $height = $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["HEIGHT"] - $delta;
                        } else if ( $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["HEIGHT"] > 358 ) {
                            $delta = $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["HEIGHT"] - 358;
                            $height = 358;
                            $heightBig = $height * 2;
                            $width = $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["WIDTH"] - $delta;
                        } else*/ if ( $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["WIDTH"] > $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["HEIGHT"] ) {
                            $width = 358;
                            $widthBig = $width * 2;
                        } else {
                            $height = 358;
                            $heightBig = $height * 2;
                        }
                        $_img = imageResize(array("WIDTH" => $width, "HEIGHT" => $height, "MODE" => "inv"), $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["SRC"]);
                        $_imgBig = imageResize(array("WIDTH" => $widthBig, "HEIGHT" => $heightBig, "MODE" => "inv"), $arResult["FILES"][$arResult["OFFERS"][$arResult["OFFER_ID"]]["DETAIL_PICTURE"]]["SRC"]);
                        ?>
                        <?if($arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"]['VIDEO']['VALUE']){?>
                            <?$i=0;?>
                            <?foreach($arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"]['VIDEO']['VALUE'] as $k => $v){?>
                                <a rel="fancybox-video" class="tube-video num-<?=$i;?>" style="display: none;" href="https://www.youtube.com/watch?v=<?=$v;?>" data-img="http://img.youtube.com/vi/<?=$v?>/0.jpg">
                                    <img src="http://img.youtube.com/vi/<?=$v;?>/1.jpg" alt="img" class="img-responsive zoom-img"/>
                                    <div class="big-vid"></div>
                                </a>
                            <?$i++;?>
                            <?}?>    
                                <a href="<?= imageResize(array("WIDTH" => 358 * 2, "HEIGHT" => 358 * 2, "MODE" => "in"), $_imgBig); ?>" class="cloud-zoom" id="zoom1" rel="adjustX: 20">
                                    <img src="<?= imageResize(array("WIDTH" => 358, "HEIGHT" => 358), $_img); ?>" alt="img" class="img-responsive zoom-img"/>
                                </a>   
                        <?}else{?>
                        <a href="<?= imageResize(array("WIDTH" => 358 * 2, "HEIGHT" => 358 * 2, "MODE" => "in"), $_imgBig); ?>" class="cloud-zoom" id="zoom1" rel="adjustX: 20">
                            <img src="<?= imageResize(array("WIDTH" => 358, "HEIGHT" => 358), $_img); ?>" alt="img" class="img-responsive zoom-img"/>
                        </a>
                        <?}?>
                        <? if ( $arResult["QUANTITIES"][$arResult["OFFER_ID"]]["QUANTITY"] == 0 ): ?>
                            <div class="mark gray">Нет в наличии</div>
                        <? elseif ( in_array($arResult["OFFER_ID"], $arResult["BASKET"]) ): ?>
                            <div class="mark green">Товар в корзине</div>
                        <? endif; ?>
                            <div>
                            <?if($arResult['ITEMS']['PRICES']['DISCOUNT']['PERCENT']){?>
                                <div class="discount-perc"><span>-<?=$arResult['ITEMS']['PRICES']['DISCOUNT']['PERCENT'];?>%</span></div>
                            <?}?>
                            <?foreach ($arResult['ICONS'] as $keya => $valya){?>
                                <img src="<?=$valya;?>" class="cart-sticker">
                            <?}?>
                            </div>   
                    </div>
                    <? //if ( count($arResult["OFFERS"][$arResult["OFFER_ID"]]["GALLERY"]) > 1 ): ?>
                        <div id="gallery" class="products-preview">
                            <ul>
                                <?if($arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"]['VIDEO']['VALUE']){?>
                                    <?$i=0;?>
                                    <?foreach($arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"]['VIDEO']['VALUE'] as $k => $v){?>
                                        <li>
                                            <a href="http://img.youtube.com/vi/<?=$v;?>/1.jpg" class="video" data-num="<?=$i;?>" data-href="https://www.youtube.com/watch?v=<?=$v;?>" data-image="http://img.youtube.com/vi/<?=$v;?>/0.jpg" style="position: relative">
                                                <img src="http://img.youtube.com/vi/<?=$v;?>/1.jpg" alt="img" width="65" height="65">
                                                <div class="vid"></div>
                                            </a>
                                        </li>
                                        <?$i++;?>
                                    <?}?>
                                <?}?>
                                <? foreach ( $arResult["OFFERS"][$arResult["OFFER_ID"]]["GALLERY"] as $value ): ?>
                                    <li>
                                        <?
                                        $width = $height = $widthBig = $heightBig = 0;
                                        if ( $arResult["FILES"][$value]["WIDTH"] > 358 ) {
                                            $delta = $arResult["FILES"][$value]["WIDTH"] - 358;
                                            $width = 358;
                                            $widthBig = $width * 2;
                                            $height = $arResult["FILES"][$value]["HEIGHT"] - $delta;
                                        } else if ( $arResult["FILES"][$value]["HEIGHT"] > 358 ) {
                                            $delta = $arResult["FILES"][$value]["HEIGHT"] - 358;
                                            $height = 358;
                                            $heightBig = $height * 2;
                                            $width = $arResult["FILES"][$value]["WIDTH"] - $delta;
                                        } else if ( $arResult["FILES"][$value]["WIDTH"] > $arResult["FILES"][$value]["HEIGHT"] ) {
                                            $width = 358;
                                            $widthBig = $width * 2;
                                        } else {
                                            $height = 358;
                                            $heightBig = $height * 2;
                                        }
                                        $_img = imageResize(array("WIDTH" => $width, "HEIGHT" => $height, "MODE" => "inv"), $arResult["FILES"][$value]["SRC"]);
                                        $_imgBig = imageResize(array("WIDTH" => $widthBig, "HEIGHT" => $heightBig, "MODE" => "inv"), $arResult["FILES"][$value]["SRC"]);
                                        ?>
                                        <a href="<?= imageResize(array("WIDTH" => 385 * 2, "HEIGHT" => 385 * 2, "MODE" => "in"), $_imgBig); ?>"  class="cloud-zoom-gallery" rel="useZoom: 'zoom1', smallImage: '<?= imageResize(array("WIDTH" => 385 * 2, "HEIGHT" => 385 * 2, "MODE" => "in"), $_imgBig); ?>'" data-image="<?= imageResize(array("WIDTH" => 385 * 2, "HEIGHT" => 385 * 2, "MODE" => "in"), $_imgBig); ?>">
                                            <img src="<?= imageResize(array("WIDTH" => 65, "HEIGHT" => 65, "MODE" => "in"), $_imgBig); ?>" alt="img">
                                        </a>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    <? //endif; ?>
                </div>
            </div>
            <div class="col-lg-8 col-sm-6">
                <div class="top-info">
                    <div class="row">
                        <div class="col-lg-7 col-md-12 paddbot20">
                            <h1><?= $arResult["OFFERS"][$arResult["OFFER_ID"]]["NAME"]; ?></h1>
                            <ul>
                                <?
                                foreach ( $arParams["VIEW_MAIN_PROP"] as $code ):
                                    if ( $arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"]["PROPERTIES"][$code]["VALUE"] != "" ) {
                                        continue;
                                    }
                                    ?>
                                    <li><?= $arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"][$code]["NAME"]; ?>: <?= $arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"][$code]["VALUE"]; ?></li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                        <div class="col-lg-5 col-md-12">
                            <div class="buy-info-wrapper">
                                <div class="buy-info">
                                    <!--div class="buy-head">
                                        <div class="rating-wrap">
                                            <div class="rating"></div>
                                        </div>
                                        <span class="reviews">15</span>
                                        <a class="view" href="#rewievs">Посмотреть отзывы</a>
                                    </div-->
                                    <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arResult["OFFERS"][$arResult["OFFER_ID"]], "FILES" => $arResult["FILES"], "TEMPLATE" => "price")); ?>
                                    <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arResult["OFFERS"][$arResult["OFFER_ID"]], "TEMPLATE" => "basket", "TYPE" => $arParams["TYPE"], "QUANTITY" => $arResult["QUANTITIES"][$arResult["OFFER_ID"]])); ?>
                                </div>
                            </div>
                            <div class="halva-block">
                                <a><span>«Халва»</span></a>
                                <img src="/local/images/halva-pic.png">
                            </div>
                            <?if($arResult['OFFERS'][$arResult['OFFER_ID']]['EXTERNAL_ID'] != $arResult['OFFER_ID']){?>                              
                                <div class="rassrochka-block">
                                    <a><span>Рассрочка</span></a>
                                    <img src="/local/images/rasscrochka-img.png">
                                </div>
                            <?}?>
                        </div>
                    </div>
                </div>
                <? if ( $arResult["DESCRIPTION"] != '' ): ?>
                    <div class="product-description">
                        <span class="h4">
                            <a class="title active" href="#">Описание</a>
                        </span>
                        <div class="text">
                            <?= $arResult["DESCRIPTION"]; ?>
                        </div>
                    </div>
                <? endif; ?>
                <div class="product-feature">
                    <span class="h4">Характеристики</span>
                    <table>

                            <?/*
                            foreach ( $arParams["VIEW_PROP"] as $code ):
                                if ( \Ekasoft\Base\Tools\Validate::isEmpty($arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"][$code]["VALUE"]) ) {
                                    continue;
                                }
                                ?>
                                <tr>
                                    <th>
                                        <span><?= $arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"][$code]["NAME"] ?>:</span>
                                    </th>
                                    <td> <?= $arResult["OFFERS"][$arResult["OFFER_ID"]]["PROPERTIES"][$code]["VALUE"]; ?></td>
                                </tr>
                            <? endforeach;*/ ?>

                            <?foreach($arResult['PROPERTIES_TO_SHOW'] as $k => $v){?>
                                <?if($v['NAME'] && $v['SHOW']=='Y'){?>
                                    <tr><th colspan="2" class="prop-group-name"><?=$v['NAME'];?></th></tr>
                                <?}?>
                                <?foreach($v['CHILDREN'] as $key => $val){?>
                                    <?if($val['NAME'] && $val['VALUE']){?>
                                        <tr>
                                            <th>
                                                <span><?=$val['NAME'];?>:</span>
                                            </th>
                                            <td><?=$val['VALUE'];?></td>
                                        </tr>
                                    <?}?>
                                <?}?>
                                <tr><td></td><td></td></tr>  
                            <?}?>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="related-products categories-list-tovar-e products-list">
    <div class="container">
        <h3><a href="#" title="Товары в коллекции">Товары в коллекции</a> <span class="count"><?= count($arResult["OFFERS"]) - 1; ?></span></h3>
        <div class="related-slider">
            <?
            //prent($arResult["OFFERS"]);
            foreach ( $arResult["COLLECTION"] as $arOffer ):
                $arOffer['PRICES'] = $arResult['COLLECTION_PRICE'];
                if ( $arOffer["ID"] == $arResult["OFFER_ID"] ) {
                    continue;
                }
                ?>
                <div class="slide">
                    <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arOffer, "FILES" => $arResult["FILES"], "BASKET_BLOCK" => "N", "QUANTITY" => array('QUANTITY' => 1), "OT" => "Y")); ?>
                </div>
            <? endforeach; ?>
            <?
            foreach ( $arResult["OFFERS"] as $arOffer ):
                if ( $arOffer["ID"] == $arResult["OFFER_ID"] ) {
                    continue;
                }
                ?>
                <div class="slide">
                    <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arOffer, "FILES" => $arResult["FILES"], "BASKET_BLOCK" => "N", "QUANTITY" => $arResult["QUANTITIES"][$arOffer["ID"]])); ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</section>
<? //prent($arResult['SOPUT']); ?>
<? if($arResult['SOPUT']) { ?>
<section class="related-products categories-list-tovar-e products-list">
    <div class="container">
        <h3><a href="#" title="">Сопутствующие товары</a> <span class="count"><?= count($arResult["SOPUT"]); ?></span></h3>
        <div class="related-slider">
            <?
            foreach ( $arResult["SOPUT"] as $arOffer ):
                if ( $arOffer["ID"] == $arResult["OFFER_ID"] ) {
                    continue;
                }
                ?>
                <div class="slide">
                    <? $APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arOffer, "FILES" => $arResult["FILES"], "BASKET_BLOCK" => "N", "QUANTITY" => $arResult["QUANTITIES"][$arOffer["ID"]])); ?>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</section>
<? } ?>