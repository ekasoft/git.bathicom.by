<?if(count($arResult["ITEMS"]) > 0 ):?>
<div class="category-list">
    <div class="row">
        <?foreach( $arResult["ITEMS"] as $id):
            $resId = CIBlockSection::GetByID($id);
            if($ar_res = $resId->GetNext()) {
                $arSection = $ar_res;
            }
           
            //$arSection = \EkaSoft\Base\Core\Structure::getInstance()->getSection($id);
        ?>
            <div class="col-lg-4 col-md-6">
                <div class="item-categor">
                    <a href="<?= $arSection["SECTION_PAGE_URL"];?>" title="<?= $arSection["NAME"];?>">
                        <span class="img"><img src="<?= imageResize(array("WIDTH" => 29, "HEIGHT" => 50), $arResult['IMAGES_TO_SHOW'][$arSection["PICTURE"]]["SRC"]);?>" alt="<?= $arSection["NAME"];?>" class="img-responsive"></span>
                        <span class="coll"><?= $arSection["ELEMENT_CNT"];?></span>
                        <span class="titles"><?= $arSection["NAME"];?></span>
                    </a>
                </div>
            </div>
        <?endforeach;?>
    </div>
</div>
<?endif;?>
