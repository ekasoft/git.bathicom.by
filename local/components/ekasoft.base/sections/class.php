<?php

if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BSections extends \BaseModuleComponent {
    
    public function onPrepareComponentParams( $arParams ) {
        $arParams['CACHE_TIME'] = 86400*30;
        return parent::onPrepareComponentParams($arParams);
        
    }
    
    public function executeComponent() {
        if( $this->startResultCache() ) {

            
          //  $this->arResult["ITEMS"] = \EkaSoft\Base\Core\Structure::getInstance()->getChild($this->arParams["SECTION_ID"]);
          //  $this->arResult["FILES"] = \EkaSoft\Base\Core\Structure::getInstance()->getFiles();
            $this->arResult["ITEMS"] = array();
            $arFilter = array('IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'SECTION_ID'=>$this->arParams["SECTION_ID"]);
            $arSelect = array('ID','PICTURE');
            $db_list = CIBlockSection::GetList(array('SORT'=>'ASC'), $arFilter, false);
            while($ar_result = $db_list->GetNext())
            {
                $this->arResult["ITEMS"][] = $ar_result['ID'];
                //$this->arResult['IMAGES'][$ar_result['PICTURE']] = $ar_result['PICTURE'];
                $res = CFile::GetList(false, array('ID' => $ar_result['PICTURE']));
                if($res_arr = $res->GetNext()){
                    $this->arResult['IMAGES_TO_SHOW'][$res_arr['ID']] = $res_arr;
                    $this->arResult['IMAGES_TO_SHOW'][$res_arr['ID']]['SRC'] = '/upload/'.$res_arr['SUBDIR'].'/'.$res_arr['FILE_NAME'];
                }
            }
            
               

            
            //prent($this->arResult["FILES"],true);
            $this->includeComponentTemplate();
            
            //$this->endResultCache();
        }        
    }
}