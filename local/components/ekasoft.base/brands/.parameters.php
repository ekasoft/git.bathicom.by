<?

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

if ( !CModule::IncludeModule("iblock") )
    return;

use Bitrix\Iblock;

$arTypesEx = CIBlockParameters::GetIBlockTypes(Array("-" => " "));

$arIBlocks = Array();
$db_iblock = CIBlock::GetList(Array("SORT" => "ASC"), Array("SITE_ID" => $_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"] != "-" ? $arCurrentValues["IBLOCK_TYPE"] : "")));
while ( $arRes = $db_iblock->Fetch() )
    $arIBlocks[$arRes["ID"]] = $arRes["NAME"];

$propertyIterator = Iblock\PropertyTable::getList(array(
          'select' => array('ID', 'IBLOCK_ID', 'NAME', 'CODE', 'PROPERTY_TYPE', 'MULTIPLE', 'LINK_IBLOCK_ID', 'USER_TYPE'),
          'filter' => array('=IBLOCK_ID' => $arCurrentValues['IBLOCK_ID'], '=ACTIVE' => 'Y'),
          'order' => array('SORT' => 'ASC', 'NAME' => 'ASC')
        ));
while ( $property = $propertyIterator->fetch() ) {
    $propertyCode = (string) $property['CODE'];
    if ( $propertyCode == '' )
        $propertyCode = $property['ID'];
    $propertyName = '[' . $propertyCode . '] ' . $property['NAME'];
    $propertyType[$property["CODE"]] = $property["PROPERTY_TYPE"];
    if ( $property['PROPERTY_TYPE'] != Iblock\PropertyTable::TYPE_FILE ) {
        $arProperty[$propertyCode] = $propertyName;

        if ( $property['MULTIPLE'] == 'Y' )
            $arProperty_X[$propertyCode] = $propertyName;
        elseif ( $property['PROPERTY_TYPE'] == Iblock\PropertyTable::TYPE_LIST )
            $arProperty_X[$propertyCode] = $propertyName;
        elseif ( $property['PROPERTY_TYPE'] == Iblock\PropertyTable::TYPE_ELEMENT && (int) $property['LINK_IBLOCK_ID'] > 0 )
            $arProperty_X[$propertyCode] = $propertyName;
    }
    else {
        if ( $property['MULTIPLE'] == 'N' )
            $arProperty_F[$propertyCode] = $propertyName;
    }

    if ( $property['PROPERTY_TYPE'] == Iblock\PropertyTable::TYPE_LIST || $property['PROPERTY_TYPE'] == Iblock\PropertyTable::TYPE_STRING )
        $arProperty_LS[$propertyCode] = $propertyName;

    if ( $property['PROPERTY_TYPE'] == Iblock\PropertyTable::TYPE_NUMBER )
        $arProperty_N[$propertyCode] = $propertyName;
}

$arDopParameters = array();
if ( isset($_REQUEST["current_values"]["PROPERTIES"]) && count($_REQUEST["current_values"]["PROPERTIES"]) > 0 ) {
    foreach ( $_REQUEST["current_values"]["PROPERTIES"] as $code ) {
        $type = 'STRING';
        $values = '';
        switch ( $propertyType[$code] ) {
            case Iblock\PropertyTable::TYPE_LIST :
                $type = "LIST";
                $dbl = \CIBlockPropertyEnum::getList(array("SORT" => "ASC"), array("CODE" => $code));
                while ( $row = $dbl->fetch() ) {
                    $values[$row["ID"]] = $row["VALUE"];
                }
                break;
        }

        $arDopParameters["PROPERTY_" . $code] = array(
          "PARENT" => "BASE",
          "NAME" => "Значение св-ва " . $arProperty[$code],
          "TYPE" => $type,
          "VALUES" => $values
        );
    }
}

unset($propertyCode, $propertyName, $property, $propertyIterator);

$obEntity = array(
  1 => "Простой элемент",
  2 => "Товар",
  3 => "Торговое предложение",
);

$obModel = array(
  1 => "Простой элемент",
  2 => "Элемент со набором всех свойств",
  3 => "Товар",
  4 => "Справочник"
);

$obDecorator = array(
  1 => "Базовые изменения",
  2 => "Обработка полных свойств",
  3 => "Товар"
);

$arComponentParameters = array(
  "GROUPS" => array(
    "EKA_OBJECTS" => array(
      "NAME" => "Объекты обработки",
      "SORT" => 100,
    )
  ),
  "PARAMETERS" => array(
    "AJAX_MODE" => array(),
    "IBLOCK_TYPE" => Array(
      "PARENT" => "BASE",
      "NAME" => "Где лежит инфоблока",
      "TYPE" => "LIST",
      "VALUES" => $arTypesEx,
      "DEFAULT" => "news",
      "REFRESH" => "Y",
     ),
    "IBLOCK_ID" => Array(
      "PARENT" => "BASE",
      "NAME" => "Инфоблок",
      "TYPE" => "LIST",
      "VALUES" => $arIBlocks,
      "DEFAULT" => '={$_REQUEST["ID"]}',
      "ADDITIONAL_VALUES" => "Y",
      "REFRESH" => "Y",
    ),
    "PROPERTIES" => array(
      "PARENT" => "BASE",
      "NAME" => "Фильтровать по свойствам",
      "TYPE" => "LIST",
      "VALUES" => $arProperty,
      "SIZE" => (count($arProperty) > 5 ? 8 : 3),
      "MULTIPLE" => "Y",
      "REFRESH" => "Y",
    ),
    "EKA_ENTITY" => array(
      "PARENT" => "EKA_OBJECTS",
      "NAME" => "Сущность",
      "TYPE" => "LIST",
      "VALUES" => $obEntity,
    ),
    "EKA_MODEL" => array(
      "PARENT" => "EKA_OBJECTS",
      "NAME" => "Данные из базы",
      "TYPE" => "LIST",
      "VALUES" => $obModel,
    ),
    "EKA_DECORATOR" => array(
      "PARENT" => "EKA_OBJECTS",
      "NAME" => "Оъект обработки данных",
      "TYPE" => "LIST",
      "VALUES" => $obDecorator,
    ),
    "CACHE_TIME" => Array("DEFAULT" => 36000000),
  )
);

$arComponentParameters["PARAMETERS"] = array_merge($arComponentParameters["PARAMETERS"], $arDopParameters);
