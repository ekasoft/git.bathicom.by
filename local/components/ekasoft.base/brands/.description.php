<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Компонент брендов",
    "DESCRIPTION" => "Компонент работает с брендами из highload",
    "ICON" => "/images/strings.gif",
    "PATH" => array(
        "ID" => "ekasoft",
        "NAME" => "Ека-софт",
        "CHILD" => array(
            "ID" => "elements",
            "NAME" => "Вывод элементов highload"
        )
    ),
);
?>
