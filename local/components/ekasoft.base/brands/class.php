<?php

if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BGetBrands extends BaseModuleComponent {
    
    public function onPrepareComponentParams($arParams) {      
        return parent::onPrepareComponentParams($arParams);
    }
    
    public function executeComponent() {
        
        if( $this->startResultCache() ) {
            $this->objDecorator->addCollectFiles(array("UF_FILE"));
        
            $this->arResult = $this->getElements();

            $this->includeComponentTemplate();
            
            $this->endResultCache();
        }
    }
    
}