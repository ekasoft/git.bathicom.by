<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BCatalog extends \BaseModuleComponent {

    protected $obEntitySKU;

    public function onPrepareComponentParams( $arParams ) {
        //prent($_REQUEST);
        $arParams['SMART_FILTER_PATH'] = htmlspecialchars($_REQUEST['SMART_FILTER_PATH']);
        $arParams['PAGEN_1'] = intval($_REQUEST['PAGEN_1']);
        $arParams["CACHE_TIME"] = 86400*30;
        $arParams["EKA_MODEL"] = \EkaSoft\Base\Fabrica\Model::ELEMENT_FULL;
        $arParams["EKA_DECORATOR"] = \EkaSoft\Base\Fabrica\Decorator::ELEMENT_FULL;
        $arParams["EKA_ENTITY"] = \EkaSoft\Base\Fabrica\Entity::TOVAR;

        
        $arFilter = Array('IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'CODE'=>$_REQUEST["SECTION_CODE"]);
            $arSelect = array('ID');
            $db_list = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
            if($ar_result = $db_list->GetNext())
            {
                $arParams["SECTION_ID"] = $ar_result['ID'];
            } 

        //$arParams["SECTION_ID"] = \EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->getIDByCode($_REQUEST["SECTION_CODE"]);
        $arParams["ELEMENT_CODE"] = $_REQUEST["ELEMENT_CODE"];

        if ( $_REQUEST["ajax"]["count"] != '' ) {
            $arParams["ELEMENT_COUNT"] = (int) $_REQUEST["ajax"]["count"];
        } else {
            $arParams["ELEMENT_COUNT"] = 24;
        }
        
        $arParams["GOODS"] = $_REQUEST["SKU_CODE"];

		if(empty($arParams["SECTION_ID"]))
		{
                    
			if (Bitrix\Main\Loader::includeModule('iblock'))
			{
				\Bitrix\Iblock\Component\Tools::process404(
					'����� ����������� � ��������'
					,true
					,true
					,true
					,'/404.php'
				);
			}
		}
	
		if($_REQUEST['PAGEN_1'] > 1)
		{
			list($can,) = explode('?', getenv('REQUEST_URI'), 2);
			$GLOBALS['APPLICATION']->AddHeadString('<link rel="canonical" href="http://bathicom.by'.$can.'" />', true);
		}
                
//if(getenv('REMOTE_ADDR') == '188.162.14.7')
//{
//	 print_r($arParams); exit;
//}

        return parent::onPrepareComponentParams($arParams);
        
    }
    
    public function getQuantity($IDs) {
        \CModule::IncludeModule("catalog");
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()));
        while( $row = $dbl->fetch() ) {
            $this->arResult["QUANTITIES"][$row["ID"]] = $row;
        }
    }
    
    public function addViewed($id) {
        global $USER;
        $user_id = CSaleBasket::GetBasketUserID();
        if($user_id  > 0) {
           \Bitrix\Catalog\CatalogViewedProductTable::refresh($id, $user_id);
        }
    }
    
    public function executeComponent() {
        if ( $this->startResultCache() ) {
            
            $this->objFilter = new \EkaSoft\Base\Filter\SmartFilter($this->arParams);
            
            $this->objModel->addSelect(array("CATALOG_GROUP_1"));

            $this->objModel->setFilter($this->objFilter->getFilter());  
            
            if ( $this->arParams["ELEMENT_CODE"] != '' ) {
                $this->objModel->addFilter(array("CODE" => $this->arParams["ELEMENT_CODE"]));
            }
            
            $key = $this->arParams["ELEMENT_CODE"] != '' ? false : true;
            
            $this->arResult = $this->getElements( $key );            
         
            
            foreach($this->arResult['ITEMS'] as $k => $v){
                if($v['PROPERTIES']['COUNTRY']['VALUE_ENUM']){
                    $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $v['PROPERTIES']['COUNTRY']['VALUE_ENUM']);
                    $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    if($object = $nr->GetNextElement())
                    {
                        $arF = $object->GetFields();
                        if($arF['DETAIL_PICTURE']){
                           $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                           $this->arResult['ITEMS'][$k]['ICON_COUNTRY'] = $image['SRC'];
                        }
                    }
                }
            }
            
             
            $objDecoratorSKU = new EkaSoft\Base\Decorator\Tovar($this);
            $objModel = new Ekasoft\Base\Model\Tovar($objDecoratorSKU);		
			if((!$this->objEntity->getIDs()))
			{
				if (Bitrix\Main\Loader::includeModule('iblock'))
				{
					\Bitrix\Iblock\Component\Tools::process404(
						'����� ����������� � ��������'
						,true
						,true
						,true
						,'/404.php'
					);
				}
			}
                       
                       
            $objModel->setFilter(array("IBLOCK_ID" => 11, "ACTIVE" => "Y", "=PROPERTY_CML2_LINK" => $this->objEntity->getIDs()));
            
            $objEntitySKU = new EkaSoft\Base\Entity\TovarSKU($objModel, $objDecoratorSKU);
            $objDecoratorSKU->initEntity($objEntitySKU);

            $objEntitySKU->setOfferActive("CODE", $this->arParams["GOODS"]);

            $this->arResult["OFFERS"] = $objEntitySKU->getItems();
            $this->arResult["OTHER_OFFERS"] = $objEntitySKU->getOffers();
            $this->getQuantity( $objEntitySKU->getIDs());
            $this->arResult["FILES"] = $this->arResult["FILES"] + $objEntitySKU->getFiles();

            //$this->arResult["SECTION"] = EkaSoft\Base\Core\Structure::getInstance()->getSection($this->arParams["SECTION_ID"]);
            $resId = CIBlockSection::GetByID($this->arParams["SECTION_ID"]);
            if($ar_res = $resId->GetNext()) {
                $this->arResult["SECTION"] = $ar_res;
            }

            $this->arResult["NAV_PARAMS"] = $this->objModel->getCDBResult();
            global $APPLICATION;
            $APPLICATION->SetPageProperty("count_elements", $this->objModel->SelectedRowsCount());
            
            $this->includeComponentTemplate();
        }
      
        //$this->addViewed($this->arResult["ITEMS"][0]["ID"]);

        $this->seoParams();
    }

}
