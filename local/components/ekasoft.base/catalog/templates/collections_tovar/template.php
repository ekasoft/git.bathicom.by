<div class="seo-block">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                    <?if($arResult["ITEMS"][0]['PROPERTIES']['SALE_ICON']['VALUE'] == 'Y'){?>
                        <img class="col-sticker" src="/local/img/sale-icon.png">
                    <?}?>
                    <?if($arResult["ITEMS"][0]['PROPERTIES']['IN_HIT']['VALUE'] == 'Y'){?>
                        <img class="col-sticker" src="/local/img/hit-icon.png">
                    <?}?>   
                    <?if($arResult["ITEMS"][0]['ICON_COUNTRY']){?>
                        <img class="col-sticker" src="<?=$arResult["ITEMS"][0]['ICON_COUNTRY'];?>">
                    <?}?>  

                <div class="seoslider">
                    <?if( count($arResult["ITEMS"][0]["GALLERY"]) > 0 ):?>
                        <?foreach($arResult["ITEMS"][0]["GALLERY"] as $iGallery):?>
                            <img src="<?= imageResize(array("WIDTH" => 570, "HEIGHT" => 267), $arResult["FILES"][$iGallery]["SRC"]);?>" alt="<?= $arResult["ITEMS"][0]["NAME"]?>" class="img-responsive" />
                        <?endforeach;?>
                    <?else:?>
                        <img src="<?= imageResize(array("WIDTH" => 570, "HEIGHT" => 267), NO_IMAGE_VERTICAL_SRC);?>" />
                    <?endif;?>
                </div>
            </div>
            <div class="col-md-6">
                <h3><?= $arResult["ITEMS"][0]["NAME"];?></h3>
                <p><?= $arResult["ITEMS"][0]["PREVIEW_TEXT"];?></p>
            </div>
        </div>
    </div>
</div>
<? $countOffers = count($arResult["OFFERS"][$arResult["ITEMS"][0]["ID"]]);
if( $countOffers > 0 ):?>
    <section class="categories-section-e">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title">Товары коллекции <span><?= $countOffers;?></span></div>
                </div>
                <div class="col-md-12">
                    <div class="categories-list-tovar-e products-list">
                        <div class="row">
                            <?foreach($arResult["OFFERS"][$arResult["ITEMS"][0]["ID"]] as $arItem):?>
                                <div class="col-lg-3 col-md-4 col-sm-6">
                                    <?$APPLICATION->IncludeComponent("ekasoft.view:element", "item", Array("RESULT" => $arItem, "FILES" => $arResult["FILES"], "QUANTITY" => $arResult["QUANTITIES"][$arItem["ID"]]));?>
                                </div>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?endif;?>