
<section class="detail-section-01">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <div class="product-viewer-wrap">
                    <div class="prod-photo">
                        <img src="<?= imageResize(array("WIDTH" => 358, "HEIGHT" => 358), $arResult["FILES"][$arResult["OFFERS"][0]["DETAIL_PICTURE"]]["SRC"]); ?>" data-zoom-image="images/product-618x1010.jpg" alt="img" class="img-responsive zoom-img"/>
                    </div>
                    <? if ( $arResult["OFFERS"][0]["PROPERTIES"][\EkaSoft\Base\Core\Config::PROP_GALLERY]["VALUE"] != '' ): ?>
                        <div id="gallery" class="products-preview">
                            <ul>
                                <? foreach ( $arResult["OFFERS"][0]["PROPERTIES"][\EkaSoft\Base\Core\Config::PROP_GALLERY]["VALUE"] as $value ): ?>
                                    <li>
                                        <a href="#" class="elevatezoom-gallery active" data-update="" data-image="<?= imageResize(array("WIDTH" => 358, "HEIGHT" => 358), $arResult["FILES"][$value]["SRC"]); ?>" data-zoom-image="<?= imageResize(array("WIDTH" => 618, "HEIGHT" => 1010), $arResult["FILES"][$value]["SRC"]); ?>">
                                            <img src="<?= imageResize(array("WIDTH" => 65, "HEIGHT" => 65), $arResult["FILES"][$value]["SRC"]); ?>" alt="img" width="65" height="65">
                                        </a>
                                    </li>
                                <? endforeach; ?>
                            </ul>
                        </div>
                    <? endif; ?>
                </div>
            </div>
            <div class="col-lg-8 col-sm-6">
                <div class="top-info">
                    <div class="row">
                        <div class="col-lg-8 col-md-12 paddbot20">
                            <h1><?= $arResult["OFFERS"][0]["NAME"]; ?></h1>
                            <ul>
                                <li>Бренд: 561469</li>
                                <li>Коллекция: Белый</li>
                                <li>Тип: Мрамор</li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="buy-info">
                                <div class="buy-head">
                                    <div class="rating-wrap">
                                        <div class="rating"></div>
                                    </div>
                                    <span class="reviews">15</span>
                                    <a class="view" href="#rewievs">Посмотреть отзывы</a>
                                </div>

                                <div class="price">
                                    <span class="large">8 999 990</span>
                                    <span class="small">000</span>
                                </div>
<?prent($arResult["OFFERS"]);?>
                                <div class="share">
                                    <span class="old">9 999 900 000</span>
                                    <span class="discount">Экономия 1 000 000 000</span>
                                </div>

                                <div class="parameters">
                                    <input class="count-input" min="1" value="1" type="number" placeholder=""/>
                                    <div class="units">
                                        <ul>
                                            <li>м2</li>
                                            <li class="active">шт</li>
                                        </ul>
                                    </div>
                                    <a class="button" href="#">В корзину</a>
                                </div>
                                <div class="buy-one-click">
                                    <a class="buy" href="#">Купить в один клик</a>
                                    <form>
                                        <input type="submit" value="Ок" /><input type="text" class="masktel" placeholder="+375 (XX) XXX-XX-XX" />
                                        <span class="bt-close-one-click"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <? if ( $arResult["OFFERS"][0]["DETAIL_TEXT"] != '' ): ?>
                    <div class="product-description">
                        <span class="h4">
                            <a class="title" href="#">Описание</a>
                        </span>
                        <div class="text">
                            <?= $arResult["OFFERS"][0]["DETAIL_TEXT"]; ?>
                        </div>
                    </div>
                <? endif; ?>
                <div class="product-feature">
                    <span class="h4">Прочие характеристики</span>
                    <table>
                        <tr>
                            <th>
                                <span>Назначение:</span>
                            </th>
                            <td>для ванной комнаты</td>
                        </tr>
                        <tr>
                            <th>
                                <span>Материал:</span>
                            </th>
                            <td>керамическая</td>
                        </tr>
                        <tr>
                            <th>
                                <span>Поверхность:</span>
                            </th>
                            <td>глазурованная</td>
                        </tr>
                        <tr>
                            <th>
                                <span>Рельеф:</span>
                            </th>
                            <td>да</td>
                        </tr>
                        <tr>
                            <th>
                                <span>Цвет:</span>
                            </th>
                            <td>Белый</td>
                        </tr>
                        <tr>
                            <th>
                                <span>Рисунок:</span>
                            </th>
                            <td>мозаика</td>
                        </tr>
                        <tr>
                            <th>
                                <span>М2 в коробке:</span>
                            </th>
                            <td>44</td>
                        </tr>
                        <tr>
                            <th>
                                <span>Поставщик:</span>
                            </th>
                            <td>ООО Церсанит Трейд</td>
                        </tr>
                        <tr>
                            <th>
                                <span>Адрес поставщика:</span>
                            </th>
                            <td>Ул. Молодежная 15, пгт Фряново, Щелковский р-н,
                                Московская обл., 141147, РФ</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="related-products products-list">
    <div class="container">
        <h3><a href="#" title="Товары в коллекции">Товары в коллекции</a> <span class="count"><?= count($arResult["OTHER_OFFERS"]); ?></span></h3>
        <div class="related-slider">
            <div class="slide">
                <div class="product-item">
                    <div class="img-wrap">
                        <img src="../images/product-prev-01.png" alt="img" width="176" height="92"/>
                        <div class="mark gray">Нет на складе</div>
                    </div>
                    <div class="text-wrap">
                        <span class="h5">
                            <a href="#">Акриловая ванна M103 из качественных сплетений</a>
                        </span>
                        <div class="rating-wrap">
                            <div class="rating"></div>
                            <span class="count">15</span>
                        </div>
                        <div class="price">8 999 990 <span class="small">000</span></div>
                    </div>
                </div>
            </div>

            <div class="slide">
                <div class="product-item">
                    <div class="img-wrap">
                        <img src="../images/product-prev-01.png" alt="img" width="176" height="92"/>
                        <div class="discount">-10%</div>
                    </div>
                    <div class="text-wrap">
                        <span class="h5">
                            <a href="#">Акриловая ванна Pupa</a>
                        </span>
                        <div class="rating-wrap">
                            <div class="rating"></div>
                            <span class="count">15</span>
                        </div>
                        <div class="price">8 999 990 <span class="small">000</span></div>
                        <div class="share">
                            <span class="old">9 999 900 000</span>
                            <span class="discount">Экономия 1 000 000 000</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="slide">
                <div class="product-item">
                    <div class="img-wrap">
                        <div class="product-images">
                            <div class="img">
                                <img src="../images/product-prev-02.png" alt="img" width="176" height="87"/>
                            </div>
                            <div class="img">
                                <img src="../images/product-prev-01.png" alt="img" width="176" height="92"/>
                            </div>
                        </div>
                    </div>
                    <div class="text-wrap">
                        <span class="h5">
                            <a href="#">Акриловая ванна G. W. TigerWelcamstrefa</a>
                        </span>
                        <div class="rating-wrap">
                            <div class="rating"></div>
                            <span class="count">15</span>
                        </div>
                        <div class="price">8 999 990 <span class="small">000</span></div>
                    </div>
                </div>
            </div>

            <div class="slide">
                <div class="product-item">
                    <div class="img-wrap">
                        <div class="product-images">
                            <div class="img">
                                <img src="../images/product-prev-03.png" alt="img" width="174" height="136"/>
                            </div>
                            <div class="img">
                                <img src="../images/product-prev-02.png" alt="img" width="176" height="87"/>
                            </div>
                        </div>
                    </div>
                    <div class="text-wrap">
                        <span class="h5">
                            <a href="#">Ванна Nasy правосторонняя, с уклоном</a>
                        </span>
                        <div class="rating-wrap">
                            <div class="rating"></div>
                            <span class="count">15</span>
                        </div>
                        <div class="price">8 999 990 <span class="small">000</span></div>
                    </div>
                </div>
            </div>

            <div class="slide">
                <div class="product-item">
                    <div class="img-wrap">
                        <img src="../images/product-prev-03.png" alt="img" width="174" height="136"/>
                    </div>
                    <div class="text-wrap">
                        <span class="h5">
                            <a href="#">Ванна Nasy правосторонняя, с уклоном</a>
                        </span>
                        <div class="rating-wrap">
                            <div class="rating"></div>
                            <span class="count">15</span>
                        </div>
                        <div class="price">8 999 990 <span class="small">000</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>