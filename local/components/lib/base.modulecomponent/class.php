<?php

if ( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true )
    die();

use EkaSoft\Base;

class BaseModuleComponent extends \CBitrixComponent {
    /* объект модели        */

    protected $objModel;
    /* объект декоратора    */
    protected $objDecorator;
    /* объект сущности      */
    protected $objEntity;

    public function createObjects( $arParams ) {
        if ( $arParams["EKA_DECORATOR"] == '' ) {
            $arParams["EKA_DECORATOR"] = 3;
        }

        if ( $arParams["EKA_MODEL"] == '' ) {
            $arParams["EKA_MODEL"] = 3;
        }

        if ( $arParams["EKA_ENTITY"] == '' ) {
            $arParams["EKA_ENTITY"] = 3;
        }

        $this->objDecorator = Base\Fabrica\Decorator::getObject($this)->getClass($arParams["EKA_DECORATOR"]);
        $this->objModel = Base\Fabrica\Model::getObject($this->objDecorator)->getClass($arParams["EKA_MODEL"]);

        $this->objEntity = Base\Fabrica\Entity::getObject($this->objModel, $this->objDecorator)->getClass($arParams["EKA_ENTITY"]);

        $this->objDecorator->initEntity($this->objEntity);

        return $arParams;
    }

    public function onPrepareComponentParams( $arParams ) {
        $this->createObjects($arParams);
        $this->initParams($arParams);
        $this->initVars();

        return $arParams;
    }

    public function initParams( $arParams ) {
        $this->objEntity->initParams($arParams);
    }

    public function getElements( $key = true ) {
        return $this->objEntity->getResult($key);
    }

    public function initVars() {
        $this->arResult["FILES"] = array();
    }

    public function getBasket() {
        return \EkaSoft\Base\Core\Context::getInstance()->get('BASKET');
    }

    public function seoParams() {
        $this->seoTitle();
        $this->addBreadCrumbs();
    }

    public function seoTitle() {
        global $APPLICATION;
        if ( count($this->arResult["ITEMS"]) == 1 ) {
            $arItems = $this->arResult["ITEMS"];
            $first = array_shift($arItems);
            unset($arItems);
			$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($first["IBLOCK_ID"], $first["ID"]);
			$this->arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();

			if ($this->arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
				$APPLICATION->SetPageProperty("title", $this->arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]);
			elseif(isset($this->arResult["SECTION"]["NAME"]))
				$APPLICATION->SetPageProperty("title", $this->arResult["SECTION"]["NAME"]);

			$browserTitle = \Bitrix\Main\Type\Collection::firstNotEmpty(
				$this->arResult["SECTION"], $arParams["BROWSER_TITLE"]
				,$this->arResult["IPROPERTY_VALUES"], "SECTION_META_TITLE"
			);
			if (is_array($browserTitle))
				$APPLICATION->SetTitle(implode(" ", $browserTitle));
			elseif ($browserTitle != "")
				$APPLICATION->SetTitle($browserTitle);

			$metaKeywords = \Bitrix\Main\Type\Collection::firstNotEmpty(
				$this->arResult["SECTION"], $arParams["META_KEYWORDS"]
				,$this->arResult["IPROPERTY_VALUES"], "SECTION_META_KEYWORDS"
			);
			if (is_array($metaKeywords))
				$APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords));
			elseif ($metaKeywords != "")
				$APPLICATION->SetPageProperty("keywords", $metaKeywords);

			$metaDescription = \Bitrix\Main\Type\Collection::firstNotEmpty(
				$this->arResult["SECTION"], $arParams["META_DESCRIPTION"]
				,$this->arResult["IPROPERTY_VALUES"], "SECTION_META_DESCRIPTION"
			);
			if (is_array($metaDescription))
				$APPLICATION->SetPageProperty("description", implode(" ", $metaDescription), $arTitleOptions);
			elseif ($metaDescription != "")
				$APPLICATION->SetPageProperty("description", $metaDescription, $arTitleOptions);

//            $APPLICATION->SetTitle($first["NAME"]);
        } else if ( isset($this->arResult["SECTION"]["NAME"]) ) {
			$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($this->arResult["SECTION"]["IBLOCK_ID"], $this->arResult["SECTION"]["ID"]);
			$this->arResult["IPROPERTY_VALUES"] = $ipropValues->getValues();

			if ($this->arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != "")
				$APPLICATION->SetPageProperty("title", $this->arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]);
			elseif(isset($this->arResult["SECTION"]["NAME"]))
				$APPLICATION->SetPageProperty("title", $this->arResult["SECTION"]["NAME"]);
                        
                        prent($this->arResult["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]);
			$browserTitle = \Bitrix\Main\Type\Collection::firstNotEmpty(
				$this->arResult["SECTION"], $arParams["BROWSER_TITLE"]
				,$this->arResult["IPROPERTY_VALUES"], "SECTION_META_TITLE"
			);
			if (is_array($browserTitle))
				$APPLICATION->SetTitle(implode(" ", $browserTitle));
			elseif ($browserTitle != "")
				$APPLICATION->SetTitle($browserTitle);

			$metaKeywords = \Bitrix\Main\Type\Collection::firstNotEmpty(
				$this->arResult["SECTION"], $arParams["META_KEYWORDS"]
				,$this->arResult["IPROPERTY_VALUES"], "SECTION_META_KEYWORDS"
			);
			if (is_array($metaKeywords))
				$APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords));
			elseif ($metaKeywords != "")
				$APPLICATION->SetPageProperty("keywords", $metaKeywords);

			$metaDescription = \Bitrix\Main\Type\Collection::firstNotEmpty(
				$this->arResult["SECTION"], $arParams["META_DESCRIPTION"]
				,$this->arResult["IPROPERTY_VALUES"], "SECTION_META_DESCRIPTION"
			);
			if (is_array($metaDescription))
				$APPLICATION->SetPageProperty("description", implode(" ", $metaDescription), $arTitleOptions);
			elseif ($metaDescription != "")
				$APPLICATION->SetPageProperty("description", $metaDescription, $arTitleOptions);

//            $APPLICATION->SetTitle($this->arResult["SECTION"]["NAME"]);
//           
//            print_r($this->arResult);
        }
//        print_r($this->arResult);
    }

    public function addBreadCrumbs() {
        $kostyl = 0;
        $pathHistory = array();
        $index = $this->arParams["SECTION_ID"];
        while ( $index != 0 ) {
            if ( $kostyl == 40 ) {
                break;
            }
            foreach ( \EkaSoft\Base\Core\Structure::getInstance()->getStructure() as $pid => $arSections ) {
                if ( isset($arSections[$index]) ) {
                    $pathHistory[] = $arSections[$index];
                    $index = $pid;
                    break;
                }
            }
            ++$kostyl;
        }

        global $APPLICATION;
        $count = count($pathHistory);
        for ( $i = $count - 1; $i >= 0; --$i ) {
            $url = $pathHistory[$i]["NAME"] != $this->arResult["SECTION"]["ID"] ? $pathHistory[$i]["SECTION_PAGE_URL"] : "";
            $APPLICATION->AddChainItem($pathHistory[$i]["NAME"], $url);
        }

        // карточка ТП (сантехника, ванны)
        if ( isset($this->arResult["ITEMS"]["ID"]) && !isset($this->arResult["OFFERS"]) ) {
            $APPLICATION->AddChainItem($this->arResult["ITEMS"]["NAME"]);
        } elseif ( isset($this->arResult["OFFERS"]) && isset($this->arResult["OFFER_ID"]) ) {
            $first = array_shift($this->arResult["ITEMS"]);
            $APPLICATION->AddChainItem($first["NAME"], $first["DETAIL_PAGE_URL"]);
            $APPLICATION->AddChainItem($this->arResult["OFFERS"][$this->arResult["OFFER_ID"]]["NAME"]);
        } elseif ( isset($this->arResult["ITEMS"][0]["ID"]) ) {
            $APPLICATION->AddChainItem($this->arResult["ITEMS"][0]["NAME"]);
        } elseif ( isset($this->arResult["OFFERS"]) ) {
            
        }
    }

}
