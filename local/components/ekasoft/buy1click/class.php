<?php

if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

\CBitrixComponent::includeComponentClass("lib:base.component");

class Buy1Click extends \BaseComponent {
    
    public function onPrepareComponentParams($arParams) {
        
        if( isset($arParams["ONE_CLICK"]) ) {
            $arParams["ORDER"]["IBLOCK_ID"] = 12;
            $arParams["ORDER"]["PROPERTY_VALUES"][114] = $arParams["ONE_CLICK"]["PRODUCT_ID"];
            $arParams["ORDER"]["NAME"] =  $arParams["ONE_CLICK"]["PHONE"] . ' - ' . date("Y-m-d H:i:s");
            $arParams["ORDER"]["CODE"] = $arParams["ONE_CLICK"]["PHONE"];
        }
        
        return $arParams;
    }

    public function addBasket() {
        $this->addElement($this->arParams["ORDER"]);
        $this->sendEmail();
    }
    
    public function sendEmail() {
        $arProduct = $this->getProduct();
        $arFields = array(
          "TOVAR" => $arProduct["NAME"],
          "LINK" => "http://bathicom.by/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=".$arProduct["IBLOCK_ID"]."&type=catalog&ID=".$arProduct["ID"]."&lang=ru&find_section_section=".$arProduct["SECTION_ID"]."&WF=Y",
          "PHONE" => $this->arParams["ORDER"]["CODE"]
        );
        \CEvent::Send("BUY_1_CLICK", "s1", $arFields);
        $this->arResult["SUCCESS"] = true;
    }
    
    public function getProduct() {
        return \CIBlockElement::GetByID($this->arParams["ONE_CLICK"]["PRODUCT_ID"])->fetch();
    }
    
    public function executeComponent() {
        if( isset($this->arParams["ORDER"]) ) {
            $this->addBasket();
        }
        $this->includeComponentTemplate();
    }
    
}