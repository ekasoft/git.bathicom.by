<div class="buy-one-click">
    <div class="insertAjax">
        <? if (!$arResult["SUCCESS"]): ?>
            <a class="buy" href="javascript:void(0);">Купить в один клик</a>
            <form action="/local/components/ekasoft/buy1click/ajax.php" method="post">
                <input type="submit" value="Ok" class="jsBuyOneClick" />
                <input type="text" class="masktel" name="ONE_CLICK[PHONE]" placeholder="+375 (XX) XXX-XX-XX" />
                <input type="hidden" name="ONE_CLICK[PRODUCT_ID]" value="<?= $arParams["ID"]; ?>" />
                <span class="bt-close-one-click"></span>
            </form>
        <? else: ?>
            <div>
                <a class="buy" href="javascript:void(0);">Заявка принята</a>
            </div>
        <? endif; ?>
    </div>
</div>