<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Покупка в 1 клик",
    "DESCRIPTION" => "Покупка в 1 клик",
    "ICON" => "/images/strings.gif",
    "PATH" => array(
        "ID" => "ekasoft",
        "NAME" => "Ека-софт",
        "CHILD" => array(
            "ID" => "sale",
            "NAME" => "Покупка в 1 клик"
        )
    ),
);
?>
