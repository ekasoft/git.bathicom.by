<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="col-md-3" id="left-col-cat">
   <?//prent($arParams['URI_PARTS'][0]);?>
      <?foreach($arResult['SECTIONS_FINAL'] as $k => $v){?>
    <a href="/brands/<?=$v['CODE'];?>/" class="a-task">
        <div class="task-menu-page<?if(strpos($arParams['URI_PARTS'][0],$v['CODE'])){?> active<?
$APPLICATION->AddChainItem($v['NAME'], "/brands/".$v['CODE'].'/');
}?>">
            <img class="img-1" src="<?=$v['IMAGE'];?>">
            <img class="img-2" src="<?=$v['IMAGE_WHITE'];?>">
            <span class="name"><?=$v['NAME'];?></span>
            <span class="triangle"></span>
        </div>
    </a>
    <?}?>
    <?//prent($arResult['B_SECTIONS_TO_SHOW']);?>
     <?/*  
         <?foreach($arResult['SECTIONS_FINAL'] as $k => $v){?>
         <?if(!$arParams['BRAND_CODE']){?>
            <a href="/brands/<?=$v['CODE'];?>/" class="a-task">
            <div class="task-menu-page">
                <img class="img-1" src="<?=$v['IMAGE'];?>">
                <img class="img-2" src="<?=$v['IMAGE_WHITE'];?>">
                <span class="name"><?=$v['NAME'];?></span>
            <span class="triangle"></span>
        </div>     </a>
        <?}else{?>
            <?if(!in_array($v['ID'], $arResult['B_SECTIONS_TO_SHOW'])){?>  
                <a href="#" class="a-task">
                <div class="task-menu-page opacity-task">
                    <img class="img-1" src="<?=$v['IMAGE'];?>">
                    <img class="img-2" src="<?=$v['IMAGE_WHITE'];?>">
                    <span class="name"><?=$v['NAME'];?></span>
                    <span class="triangle"></span>
                </div>  </a>
            <?}elseif(in_array($v['ID'], $arParams['IBLOCK_11_FRIENDS_SECTIONS'])){?>    
                <a href="/katalog/<?=$v['CODE'];?>/filter/brandtp-is-<?=$arParams['BRAND_CODE'];?>/apply/" class="a-task">
                <div class="task-menu-page">
                    <img class="img-1" src="<?=$v['IMAGE'];?>">
                    <img class="img-2" src="<?=$v['IMAGE_WHITE'];?>">
                    <span class="name"><?=$v['NAME'];?></span>
                    <span class="triangle"></span>
                </div>   </a>
            <?}else{?>
                <a href="/katalog/<?=$v['CODE'];?>/filter/brand-is-<?=$arParams['BRAND_CODE'];?>/apply/" class="a-task">
                <div class="task-menu-page">
                    <img class="img-1" src="<?=$v['IMAGE'];?>">
                    <img class="img-2" src="<?=$v['IMAGE_WHITE'];?>">
                    <span class="name"><?=$v['NAME'];?></span>
                    <span class="triangle"></span>
                </div>  </a>     
            <?}?>                  
        <?}?>
      <?}?> 
         */?>
        
   
    
</div>
<div class="col-md-9 jsAjaxCatalog" id="left-col-cat"> 
    

 <div class="products-list categories-list-tovar-e">
                    <div class="row brand-page">
                        <?if($arResult['BRAND'] && !$arParams['BRANDS_CODE']){?>
                            <?foreach($arResult['BRAND'] as $k => $v){?>
                                <?if($v['IMAGES']['src']){?>
                                    <div class="col-lg-3 col-md-6 brands-cart">
                                        <a href="<?=$arParams['URI_PARTS'][0].$v['CODE'];?>-brand/"><div class="img-wrap brands-img"><img src="<?=$v['IMAGES']['src']?>"></div></a>                                   
                                        <a href="<?=$arParams['URI_PARTS'][0].$v['CODE'];?>-brand/"><?=$v['NAME'];?></a>
                                    </div>
                                <?}?>
                            <?}?>
                        <?}elseif($arResult['BRAND_ONLY_ONE']){?>
                        <?
$APPLICATION->AddChainItem($arResult['BRAND_ONLY_ONE']['NAME'], $arResult['BRAND_ONLY_ONE']['CODE']);
?>
                            <div class="col-md-9 brands-cart">
                                <?=$arResult['BRAND_ONLY_ONE']['TEXT'];?>
                            </div>
                            <div class="col-md-3 img-wrap brand-detail">
                                <img src="<?=$arResult['BRAND_ONLY_ONE']['IMAGES']['src'];?>">
                            </div>
                        <?}?>
                    </div>
                </div>  
    <?if($arResult['SUBSECT']){?>
        <div class="category-list">
            <div class="row">
                <?foreach($arResult['SUBSECT'] as $k => $v){?>
                <div class="col-lg-4 col-md-6">
                    <div class="item-categor">
                        <a href="<?=$v['LINK'];?>" title="<?=$v['NAME'];?>">
                            <span class="img"><img src="<?=$v['IMAGES']['SHOW']['src'];?>" alt="<?=$v['NAME'];?>" class="img-responsive"></span>
                            <span class="coll"><?=$v['COUNT'];?></span>
                            <span class="titles"><?=$v['NAME'];?></span>
                        </a>
                    </div>
                </div>
                <?}?>
            </div>
        </div>
    <?}?>


    <?if($arResult['B_SECTIONS_TO_SHOW'] && !$arParams['SECTION_CODE']){?>
        <div class="category-list">
            <div class="row">
                <?foreach($arResult['B_SECTIONS_TO_SHOW'] as $k => $v){?>
                <div class="col-lg-4 col-md-6">
                    <div class="item-categor">
                        <a href="/brands/<?=$arResult['SECTIONS_FINAL'][$v]['CODE'];?>/<?=$arResult['BRAND_ONLY_ONE']['CODE'];?>-brand/" title="<?=$arResult['SECTIONS_FINAL'][$v]['NAME'];?>">
                            <span class="img"><img src="<?=$arResult['SECTIONS_FINAL'][$v]['IMAGE'];?>" alt="<?=$arResult['SECTIONS_FINAL'][$v]['NAME'];?>" class="img-responsive"></span>
                            <span class="titles"><?=$arResult['SECTIONS_FINAL'][$v]['NAME'];?></span>
                        </a>
                    </div>
                </div>
                <?}?>
            </div>
        </div>
    <?}?>
    