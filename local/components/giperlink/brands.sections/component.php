<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');
$arParams['SECTION_CODE'] = htmlspecialchars($_REQUEST['SECTION_CODE']);
$arParams['SECTION_ID'] = intval($_REQUEST['SECTION_ID']);
$arParams['IBLOCK_ID'] = intval($_REQUEST['IBLOCK_ID']);
$arParams['IBLOCK_10_SECTIONS'] = array(73,64);
$arParams['IBLOCK_11_FRIENDS_SECTIONS'] = array(44,57);
$arParams['URI'] = $_SERVER['REQUEST_URI'];
$arParams['URI_PARTS'] = explode('?',$arParams['URI']);   
$arParams['BRAND_CODE'] = htmlspecialchars($_REQUEST['BRAND_CODE']);
$arParams['BRAND_SECTION_FILTER'] = htmlspecialchars($_REQUEST['BRAND_SECTION_FILTER']);
$arResult = array();
if($this->StartResultCache(false)) {     
    
    if($arParams['BRAND_CODE']){
        $arFilter = array('IBLOCK_ID' => 10, 'XML_ID' => $arParams['BRAND_CODE'], 'PROPERTY_ID' => 86);
        $props = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC", array()), $arFilter);
        if($enum_fields = $props->GetNext())
        {
           $arParams['BRAND_IB_10_ID'] = $enum_fields['ID'];
        }
        
        $arFilter = array('IBLOCK_ID' => 11, 'XML_ID' => $arParams['BRAND_CODE'], 'PROPERTY_ID' => 94);
        $props = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC", array()), $arFilter);
        if($enum_fields = $props->GetNext())
        {
           $arParams['BRAND_IB_11_ID'] = $enum_fields['ID'];
        }
    }
    
    //забираем все товары 10-го инфоблока
    $arElementFilter = array('IBLOCK_ID' => 10,'ACTIVE' => 'Y','SECTION_ID' => $arParams['IBLOCK_10_SECTIONS'], 'INCLUDE_SUBSECTIONS' => 'Y');
    $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_BRAND');
    $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
    $arResult['BRANDS'] = array();
    while($ob = $res->GetNextElement()){  
        $arFields = $ob->GetFields();
        //prent($arFields);
        if(!$arParams['SECTION_CODE']){
            $arResult['BRANDS'][$arFields['PROPERTY_BRAND_VALUE']] = $arFields['PROPERTY_BRAND_VALUE'];
        }
        $arResult['SECTIONS'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
    }

    //забираем все товары 11-го инфоблока
    $arElementFilter = array('IBLOCK_ID' => 11,'ACTIVE' => 'Y');
    $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_CML2_LINK', 'PROPERTY_BRANDTP');
    $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
    while($ob = $res->GetNextElement()){  
        $arFields = $ob->GetFields();
        if($arFields['PROPERTY_BRANDTP_VALUE']){
            if(!$arParams['SECTION_CODE']){
                $arResult['BRANDS'][$arFields['PROPERTY_BRANDTP_VALUE']] = $arFields['PROPERTY_BRANDTP_VALUE'];    
            }
        }
        $arResult['SECTIONS_ELEVEN'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
    }

    //забираем бренды в случае выбранного раздела
    if($arParams['SECTION_CODE']){
        if($arParams['IBLOCK_ID'] == 10){

            $arElementFilter = array('IBLOCK_ID' => 10,'ACTIVE' => 'Y','SECTION_CODE' => $arParams['SECTION_CODE'], 'INCLUDE_SUBSECTIONS' => 'Y', '!PROPERTY_BRAND_VALUE' => false);
            $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_BRAND');
            $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
            while($ob = $res->GetNextElement()){  
                $arFields = $ob->GetFields();
                $arResult['BRANDS'][$arFields['PROPERTY_BRAND_VALUE']] = $arFields['PROPERTY_BRAND_VALUE'];
            }

        }elseif($arParams['IBLOCK_ID'] == 11){

            $arElementFilter = array('IBLOCK_ID' => 11,'ACTIVE' => 'Y','SECTION_CODE' => $arParams['SECTION_CODE'], 'INCLUDE_SUBSECTIONS' => 'Y');
            $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_CML2_LINK', 'PROPERTY_BRANDTP');
            $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);
            $arResult['IBLOCK_11_ITEMS'] = array(); //обнуляем массивы
            $arResult['IBLOCK_11_FRIENDS_IDS'] = array();
            while($ob = $res->GetNextElement()){  
                $arFields = $ob->GetFields();
                if($arFields['PROPERTY_BRANDTP_VALUE']){
                    $arResult['BRANDS'][$arFields['PROPERTY_BRANDTP_VALUE']] = $arFields['PROPERTY_BRANDTP_VALUE'];    
                }
            }
            //prent($arResult['BRANDS']);
        }

    }


    //дерево разделов 10-го инфоблока   
         
        $arResult['MAP'] = array();
        $arFilter = Array('IBLOCK_ID' => 10, 'ACTIVE' => 'Y');
        $db_list = CIBlockSection::GetList(Array("left_margin" => "asc"), $arFilter, true, array("ID", "NAME", "CODE", "DEPTH_LEVEL","PICTURE", "IBLOCK_SECTION_ID"));
        while ($ar_result = $db_list->GetNext()) {
            $arResult['MAP'][$ar_result['ID']] = $ar_result;
        }   
      if($arResult['SECTIONS']){ 
        foreach($arResult['SECTIONS'] as $k => $v){
            if($arResult['MAP'][$k]['DEPTH_LEVEL'] == 3){                
                $arResult['SECTIONS_TO_SHOW'][$arResult['MAP'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']; 
            }elseif($arResult['MAP'][$k]['DEPTH_LEVEL'] == 2){
                $arResult['SECTIONS_TO_SHOW'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID'];
            }elseif($arResult['MAP'][$k]['DEPTH_LEVEL'] == 1){
                $arResult['SECTIONS_TO_SHOW'][$arResult['MAP'][$k]['ID']] = $arResult['MAP'][$k]['ID'];
            }
        }        
    }
    
    
    //дерево разделов 11-го инфоблока
     if($arResult['SECTIONS_ELEVEN']){     
        $arResult['MAP_ELEVEN'] = array();
        $arFilter = Array('IBLOCK_ID' => 11, 'ACTIVE' => 'Y');
        $db_list = CIBlockSection::GetList(Array("left_margin" => "asc"), $arFilter, true, array("ID", "NAME", "CODE", "DEPTH_LEVEL","PICTURE", "IBLOCK_SECTION_ID"));
        while ($ar_result = $db_list->GetNext()) {
            $arResult['MAP_ELEVEN'][$ar_result['ID']] = $ar_result;
        }   
       
        foreach($arResult['SECTIONS_ELEVEN'] as $k => $v){
            if($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 3){                
                $arResult['ELEVEN_FIRST'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']; 
            }elseif($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 2){
                $arResult['ELEVEN_FIRST'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID'];
            }elseif($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 1){
                $arResult['ELEVEN_FIRST'][$arResult['MAP_ELEVEN'][$k]['ID']] = $arResult['MAP_ELEVEN'][$k]['ID'];
            }
        }
        
        if($arResult['ELEVEN_FIRST']){
            $arFilter = array('IBLOCK_ID' => 11, 'ACTIVE' => 'Y', 'ID' => $arResult['ELEVEN_FIRST']); 
            $arSelect = array('ID', 'IBLOCK_ID', 'UF_SECTION_FRIEND');
            $rsSect = CIBlockSection::GetList(false,$arFilter,false,$arSelect);
            while ($arSect = $rsSect->GetNext())
            {
                $arResult['SECTIONS_TO_SHOW'][$arSect['UF_SECTION_FRIEND']] = $arSect['UF_SECTION_FRIEND'];
            }
        }
    }
     
    
    if($arResult['SECTIONS_TO_SHOW']){
        $arOrder = array('SORT' => 'ASC');
        $arFilter = array('IBLOCK_ID' => 10, 'ID' => $arResult['SECTIONS_TO_SHOW'], 'ACTIVE' => 'Y');
        $arSelect = array('ID','NAME','UF_IMAGE_DISCOUNT','UF_IMAGE_WHITE','SECTION_PAGE_URL','SORT');
        $res = CIBlockSection::GetList($arOrder,$arFilter,false,$arSelect);
        while ($fin_result = $res->GetNext()) {
            $arResult['SECTIONS_FINAL'][$fin_result['ID']] = $fin_result;
            $rsFile = CFile::GetByID($fin_result["UF_IMAGE_DISCOUNT"]);
            if($arFile = $rsFile->Fetch()){
                $arResult['SECTIONS_FINAL'][$fin_result['ID']]['IMAGE'] = '/upload/'.$arFile['SUBDIR'].'/'.$arFile['FILE_NAME'];
            }
            $whFile = CFile::GetByID($fin_result["UF_IMAGE_WHITE"]);
            if($awFile = $whFile->Fetch()){
                $arResult['SECTIONS_FINAL'][$fin_result['ID']]['IMAGE_WHITE'] = '/upload/'.$awFile['SUBDIR'].'/'.$awFile['FILE_NAME'];
            }
        }   
    }



    if(!$arParams['BRAND_CODE']){    

            //случай, когда бренд не выбран, мы запрашиваем все бренды
            $arSelect = Array("ID", "NAME", "CODE", "DETAIL_PICTURE");
            $arFilter = Array("IBLOCK_ID"=>15, "ACTIVE"=>"Y", "NAME" => $arResult['BRANDS']) ;
            $res = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();
                $arResult['BRAND'][$arFields['ID']]['ID'] = $arFields['ID'];
                $arResult['BRAND'][$arFields['ID']]['NAME'] = $arFields['NAME'];
                $arResult['BRAND'][$arFields['ID']]['CODE'] = $arFields['CODE'];
                if($arFields['DETAIL_PICTURE']){
                    $img = CFile::GetFileArray($arFields['DETAIL_PICTURE']);
                    if($img){
                        $arResult['BRAND'][$arFields['ID']]['IMAGES'] = CFile::ResizeImageGet($img, array('width'=>100, 'height'=>100), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                    }
                }        
            }
    }else{
        //случай, когда выбран бренд
        $arSelect = Array("ID", "NAME", "CODE", "DETAIL_PICTURE", "DETAIL_TEXT");
        $arFilter = Array("IBLOCK_ID"=>15, "ACTIVE"=>"Y", "CODE" => $arParams['BRAND_CODE']) ;
        $res = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
        if($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arResult['BRAND_ONLY_ONE']['ID'] = $arFields['ID'];
            $arResult['BRAND_ONLY_ONE']['NAME'] = $arFields['NAME'];
            $arResult['BRAND_ONLY_ONE']['CODE'] = $arFields['CODE'];
            $arResult['BRAND_ONLY_ONE']['TEXT'] = $arFields['DETAIL_TEXT'];
            if($arFields['DETAIL_PICTURE']){
                $img = CFile::GetFileArray($arFields['DETAIL_PICTURE']);
                if($img){
                    $arResult['BRAND_ONLY_ONE']['IMAGES'] = CFile::ResizeImageGet($img, array('width'=>100, 'height'=>100), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                }
            }        
        }
        
        
         //забираем все товары 10-го инфоблока
        $arElementFilter = array('IBLOCK_ID' => 10,'ACTIVE' => 'Y','SECTION_ID' => $arParams['IBLOCK_10_SECTIONS'], 'INCLUDE_SUBSECTIONS' => 'Y', 'PROPERTY_BRAND' => $arParams['BRAND_IB_10_ID']);
        $arElementSelect = array('ID', 'IBLOCK_ID' ,'NAME', 'IBLOCK_SECTION_ID');
        $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
        while($ob = $res->GetNextElement()){  
            $arFields = $ob->GetFields();
            $arResult['B_SECTIONS'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
        }
        //prent($arResult['B_SECTIONS']);

        //забираем все товары 11-го инфоблока
        $arElementFilter = array('IBLOCK_ID' => 11,'ACTIVE' => 'Y','PROPERTY_BRANDTP' => $arParams['BRAND_IB_11_ID']);
        $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_CML2_LINK');
        $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
        while($ob = $res->GetNextElement()){  
            $arFields = $ob->GetFields();
            $arResult['B_SECTIONS_ELEVEN'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
            $arResult['B_IBLOCK_11_ITEMS'][$arFields['IBLOCK_SECTION_ID']][$arFields['ID']]['NAME'] = $arFields['NAME'];
            $arResult['B_IBLOCK_11_FRIENDS_IDS'][$arFields['PROPERTY_CML2_LINK_VALUE']] = $arFields['PROPERTY_CML2_LINK_VALUE'];
        }
        
        

        foreach($arResult['B_SECTIONS'] as $k => $v){
            if($arResult['MAP'][$k]['DEPTH_LEVEL'] == 3){   
                $arResult['B_SUBS_TO_CHECK'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID'];
                $arResult['B_SECTIONS_TO_SHOW'][$arResult['MAP'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']; 
            }elseif($arResult['MAP'][$k]['DEPTH_LEVEL'] == 2){
                $arResult['B_SUBS_TO_CHECK'][$k] = $k;
                $arResult['B_SECTIONS_TO_SHOW'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID'];
            }elseif($arResult['MAP'][$k]['DEPTH_LEVEL'] == 1){
                $arResult['B_SECTIONS_TO_SHOW'][$arResult['MAP'][$k]['ID']] = $arResult['MAP'][$k]['ID'];
            }
        }
        
        
        foreach($arResult['B_SECTIONS_ELEVEN'] as $k => $v){
            if($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 3){                
                $arResult['B_SECTIONS_TO_KNOW'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID'];
                $arResult['B_SUBS_TO_CHECK_11'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['CODE']] = $arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['CODE'];
            }elseif($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 2){
                $arResult['B_SUBS_TO_CHECK_11'][$arResult['MAP_ELEVEN'][$k]['CODE']] = $arResult['MAP_ELEVEN'][$k]['CODE'];
                $arResult['B_SECTIONS_TO_KNOW'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID'];
            }elseif($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 1){
                $arResult['B_SECTIONS_TO_KNOW'][$arResult['MAP_ELEVEN'][$k]['ID']] = $arResult['MAP_ELEVEN'][$k]['ID'];
            }
        }
        
        if($arResult['B_SECTIONS_TO_KNOW']){
            $arFilter = array('IBLOCK_ID' => 11, 'ACTIVE' => 'Y', 'ID' => $arResult['B_SECTIONS_TO_KNOW']); 
            $arSelect = array('IBLOCK_ID', 'ID', 'UF_SECTION_FRIEND');
            $rsSect = CIBlockSection::GetList(array(),$arFilter, false, $arSelect);
            while ($arSect = $rsSect->GetNext())
            {
                $arResult['B_SECTIONS_TO_SHOW'][$arSect['UF_SECTION_FRIEND']] = $arSect['UF_SECTION_FRIEND'];
            }
        }
        
        
    }
    
    if($arParams['BRAND_CODE'] && $arParams['SECTION_ID'] && $arParams['IBLOCK_ID']){
        if($arParams['IBLOCK_ID'] == 10){
            $arFilter = array('IBLOCK_ID' => 10, 'SECTION_ID' => $arParams['SECTION_ID']);
            $arSelect = array('ID','NAME','CODE','PICTURE','SECTION_PAGE_URL');
            $rsSect = CIBlockSection::GetList(false,$arFilter,false,$arSelect);
            while ($sect = $rsSect->GetNext())
            {
               // prent($arResult['B_SUBS_TO_CHECK']);
                if($arResult['B_SUBS_TO_CHECK'][$sect['ID']]){
                    $arResult['SUBSECT'][$sect['ID']] = $sect;
                    $arResult['SUBSECT'][$sect['ID']]['LINK'] = $sect['SECTION_PAGE_URL'].'filter/brand-is-'.$arParams['BRAND_CODE'].'/apply/';
                    if($sect['PICTURE']){
                        $rsFile = CFile::GetFileArray($sect['PICTURE']);
                        $arResult['SUBSECT'][$sect['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($rsFile, array('width'=>29, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                        $arResult['SUBSECT'][$sect['ID']]['IMAGES']['FULL'] = $rsFile;
                    }
                }

            }
        }elseif($arParams['IBLOCK_ID'] == 11){
            $arFilter = array('IBLOCK_ID' => 11, 'ACTIVE' => 'Y', 'ID' => $arParams['SECTION_ID']); 
            $arSelect = array('IBLOCK_ID', 'ID', 'UF_SECTION_FRIEND');
            $rsSect = CIBlockSection::GetList(array(),$arFilter, false, $arSelect);
            if($arSect = $rsSect->GetNext())
            {                                
                
                
                $arFilter = array('IBLOCK_ID' => 10, 'SECTION_ID' => $arSect['UF_SECTION_FRIEND']);
                $arSelect = array('ID','NAME','CODE','PICTURE','SECTION_PAGE_URL');
                $rsSect = CIBlockSection::GetList(false,$arFilter,false,$arSelect);
                while ($sect = $rsSect->GetNext())
                {
                    if($arResult['B_SUBS_TO_CHECK_11'][$sect['CODE']]){
                        $arResult['SUBSECT'][$sect['ID']] = $sect;
                        $arResult['SUBSECT'][$sect['ID']]['LINK'] = $sect['SECTION_PAGE_URL'].'filter/brandtp-is-'.$arParams['BRAND_CODE'].'/apply/';
                        if($sect['PICTURE']){
                            $rsFile = CFile::GetFileArray($sect['PICTURE']);
                            $arResult['SUBSECT'][$sect['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($rsFile, array('width'=>29, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                            $arResult['SUBSECT'][$sect['ID']]['IMAGES']['FULL'] = $rsFile;
                        }
                    }

                }
            }
        }

       // prent($arResult['SUBSECT']);
    }elseif($arParams['SECTION_ID'] && $arParams['IBLOCK_ID']){
        if($arParams['IBLOCK_ID'] == 10){
           $arFilter = array('IBLOCK_ID' => 10, 'SECTION_ID' => $arParams['SECTION_ID']);
           $arSelect = array('ID','NAME','CODE','PICTURE','SECTION_PAGE_URL');
           $rsSect = CIBlockSection::GetList(false,$arFilter,false,$arSelect);
           while ($sect = $rsSect->GetNext())
           {

                $arResult['SUBSECT'][$sect['ID']] = $sect;
                $arResult['SUBSECT'][$sect['ID']]['LINK'] = $sect['SECTION_PAGE_URL'];
                if($sect['PICTURE']){
                    $rsFile = CFile::GetFileArray($sect['PICTURE']);
                    $arResult['SUBSECT'][$sect['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($rsFile, array('width'=>29, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                    $arResult['SUBSECT'][$sect['ID']]['IMAGES']['FULL'] = $rsFile;
                }

           }
        }elseif($arParams['IBLOCK_ID'] == 11){
            $arFilter = array('IBLOCK_ID' => 11, 'ACTIVE' => 'Y', 'ID' => $arParams['SECTION_ID']); 
            $arSelect = array('IBLOCK_ID', 'ID', 'UF_SECTION_FRIEND');
            $rsSect = CIBlockSection::GetList(array(),$arFilter, false, $arSelect);
            if($arSect = $rsSect->GetNext())
            {                                
                
                
                $arFilter = array('IBLOCK_ID' => 10, 'SECTION_ID' => $arSect['UF_SECTION_FRIEND']);
                $arSelect = array('ID','NAME','CODE','PICTURE','SECTION_PAGE_URL');
                $rsSect = CIBlockSection::GetList(false,$arFilter,false,$arSelect);
                while ($sect = $rsSect->GetNext())
                {
     
                        $arResult['SUBSECT'][$sect['ID']] = $sect;
                        $arResult['SUBSECT'][$sect['ID']]['LINK'] = $sect['SECTION_PAGE_URL'];
                        if($sect['PICTURE']){
                            $rsFile = CFile::GetFileArray($sect['PICTURE']);
                            $arResult['SUBSECT'][$sect['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($rsFile, array('width'=>29, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                            $arResult['SUBSECT'][$sect['ID']]['IMAGES']['FULL'] = $rsFile;
                        }
                }
            }
        }
    }

   
    $this->SetResultCacheKeys(array(
        "MAP",
        "MAP_ELEVEN",
        "BRAND",
    ));
    
    $this->IncludeComponentTemplate();
}?>
