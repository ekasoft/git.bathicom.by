<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');
$arParams['SECTION_CODE'] = htmlspecialchars($_REQUEST['SECTION_CODE']);
$arParams['SECTION_ID'] = htmlspecialchars($_REQUEST['SECTION_ID']);
$arParams['IBLOCK_10_SECTIONS'] = array(73,64);
$arParams['IBLOCK_11_FRIENDS_SECTIONS'] = array(44,57);

$arResult = array();
if($this->StartResultCache()) {
    
    //забираем все товары 10-го инфоблока
    $arElementFilter = array('IBLOCK_ID' => 10,'ACTIVE' => 'Y','SECTION_ID' => $arParams['IBLOCK_10_SECTIONS'], 'INCLUDE_SUBSECTIONS' => 'Y', 'PROPERTY_CHEAPER_VALUE' => 'YES');
    $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID');
    $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
    while($ob = $res->GetNextElement()){  
        $arFields = $ob->GetFields();
        //$arResult['ITEMS'][$arFields['IBLOCK_SECTION_ID']][$arFields['ID']]['NAME'] = $arFields['NAME'];
        //в отдельный массив забираем айдишники разделов с товарами подешевле
        $arResult['SECTIONS'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
    }
    
    //забираем все товары 11-го инфоблока
    $arElementFilter = array('IBLOCK_ID' => 11,'ACTIVE' => 'Y', 'INCLUDE_SUBSECTIONS' => 'Y', 'PROPERTY_CHEAPER_VALUE' => 'YES');
    $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_CML2_LINK');
    $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
    while($ob = $res->GetNextElement()){  
        $arFields = $ob->GetFields();
        $arResult['IBLOCK_11_ITEMS'][$arFields['IBLOCK_SECTION_ID']][$arFields['ID']]['NAME'] = $arFields['NAME'];
        $arResult['IBLOCK_11_FRIENDS_IDS'][$arFields['PROPERTY_CML2_LINK_VALUE']] = $arFields['PROPERTY_CML2_LINK_VALUE'];
        //$arResult['SECTIONS'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
    }
        //забираем друзей из 10-го инфоблока (связанных с товарами из 11-го)
        if($arResult['IBLOCK_11_FRIENDS_IDS']){
            $arElementFilter = array('IBLOCK_ID' => 10,'ACTIVE' => 'Y','SECTION_ID' => $arParams['IBLOCK_11_FRIENDS_SECTIONS'], 'INCLUDE_SUBSECTIONS' => 'Y', 'ID' => $arResult['IBLOCK_11_FRIENDS_IDS']);
            $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID');
            $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
            while($ob = $res->GetNextElement()){  
                $arFields = $ob->GetFields();
                $arResult['SECTIONS'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
            }
        }
    
     
    //дерево разделов    
    if($arResult['SECTIONS']){     
        $map = array();
        $arFilter = Array('IBLOCK_ID' => 10, 'ACTIVE' => 'Y');
        $db_list = CIBlockSection::GetList(Array("left_margin" => "asc"), $arFilter, true, array("ID", "NAME", "CODE", "DEPTH_LEVEL","PICTURE", "IBLOCK_SECTION_ID"));
        while ($ar_result = $db_list->GetNext()) {
            $map[$ar_result['ID']] = $ar_result;
        }   
        //проверяем в каких разделах 1-го уровня есть товары подешевле
        foreach($arResult['SECTIONS'] as $k => $v){
            if($map[$k]['DEPTH_LEVEL'] == 3){                
                $arResult['SECTIONS_TO_SHOW'][$map[$map[$map[$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']] = $map[$map[$map[$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['CODE']; 
            }elseif($map[$k]['DEPTH_LEVEL'] == 2){
                $arResult['SECTIONS_TO_SHOW'][$map[$map[$k]['IBLOCK_SECTION_ID']]['ID']] = $map[$map[$k]['IBLOCK_SECTION_ID']]['CODE'];
            }elseif($map[$k]['DEPTH_LEVEL'] == 1){
                $arResult['SECTIONS_TO_SHOW'][$map[$k]['ID']] = $map[$k]['CODE'];
            }
        }        
    }
    //prent($arResult['SECTIONS_TO_SHOW']);

   
    $this->SetResultCacheKeys(array(
        "TEST",
    ));
    
    $this->IncludeComponentTemplate();
}?>
