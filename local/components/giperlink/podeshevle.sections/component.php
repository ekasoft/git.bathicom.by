<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');
$arParams['SECTION_CODE'] = htmlspecialchars($_REQUEST['SECTION_CODE']);
$arParams['SECTION_ID'] = intval($_REQUEST['SECTION_ID']);
$arParams['IBLOCK_ID'] = intval($_REQUEST['IBLOCK_ID']);
$arParams['IBLOCK_10_SECTIONS'] = array(73,64);
$arParams['IBLOCK_11_FRIENDS_SECTIONS'] = array(44,57);
$arParams['URI'] = $_SERVER['REQUEST_URI'];
$arParams['URI_PARTS'] = explode('?',$arParams['URI']);      
//prent($arParams['URI_PARTS']);
//prent($arParams);
$arResult = array();
if($this->StartResultCache()) {
    
    //забираем все товары 10-го инфоблока
    $arElementFilter = array('IBLOCK_ID' => 10,'ACTIVE' => 'Y','SECTION_ID' => $arParams['IBLOCK_10_SECTIONS'], 'INCLUDE_SUBSECTIONS' => 'Y', 'PROPERTY_CHEAPER_VALUE' => 'YES');
    $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID');
    $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
    while($ob = $res->GetNextElement()){  
        $arFields = $ob->GetFields();
        $arResult['SECTIONS'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
    }
    
    //забираем все товары 11-го инфоблока
    $arElementFilter = array('IBLOCK_ID' => 11,'ACTIVE' => 'Y', 'INCLUDE_SUBSECTIONS' => 'Y', 'PROPERTY_CHEAPER_VALUE' => 'YES');
    $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID', 'PROPERTY_CML2_LINK');
    $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
    while($ob = $res->GetNextElement()){  
        $arFields = $ob->GetFields();
        $arResult['SECTIONS_ELEVEN'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
        $arResult['IBLOCK_11_ITEMS'][$arFields['IBLOCK_SECTION_ID']][$arFields['ID']]['NAME'] = $arFields['NAME'];
        $arResult['IBLOCK_11_FRIENDS_IDS'][$arFields['PROPERTY_CML2_LINK_VALUE']] = $arFields['PROPERTY_CML2_LINK_VALUE'];
        //$arResult['SECTIONS'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
    }
        //забираем друзей из 10-го инфоблока (связанных с товарами из 11-го)
        if($arResult['IBLOCK_11_FRIENDS_IDS']){
            $arElementFilter = array('IBLOCK_ID' => 10,'ACTIVE' => 'Y','SECTION_ID' => $arParams['IBLOCK_11_FRIENDS_SECTIONS'], 'INCLUDE_SUBSECTIONS' => 'Y', 'ID' => $arResult['IBLOCK_11_FRIENDS_IDS']);
            $arElementSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID');
            $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
            while($ob = $res->GetNextElement()){  
                $arFields = $ob->GetFields();
                $arResult['SECTIONS'][$arFields['IBLOCK_SECTION_ID']] = $arFields['IBLOCK_SECTION_ID'];
            }
        }
    
     
    //дерево разделов    
    if($arResult['SECTIONS']){     
        $arResult['MAP'] = array();
        $arFilter = Array('IBLOCK_ID' => 10, 'ACTIVE' => 'Y');
        $db_list = CIBlockSection::GetList(Array("left_margin" => "asc"), $arFilter, true, array("ID", "NAME", "CODE", "DEPTH_LEVEL","PICTURE", "IBLOCK_SECTION_ID"));
        while ($ar_result = $db_list->GetNext()) {
            $arResult['MAP'][$ar_result['ID']] = $ar_result;
        }   
        //проверяем в каких разделах 1-го уровня есть товары подешевле
        foreach($arResult['SECTIONS'] as $k => $v){
            if($arResult['MAP'][$k]['DEPTH_LEVEL'] == 3){                
                $arResult['SECTIONS_TO_SHOW'][$arResult['MAP'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']; 
            }elseif($arResult['MAP'][$k]['DEPTH_LEVEL'] == 2){
                $arResult['SECTIONS_TO_SHOW'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID'];
            }elseif($arResult['MAP'][$k]['DEPTH_LEVEL'] == 1){
                $arResult['SECTIONS_TO_SHOW'][$arResult['MAP'][$k]['ID']] = $arResult['MAP'][$k]['ID'];
            }
        }        
    }
    
    if($arResult['SECTIONS_TO_SHOW']){
        $arOrder = array('SORT' => 'ASC');
        $arFilter = array('IBLOCK_ID' => 10, 'ID' => $arResult['SECTIONS_TO_SHOW'], 'ACTIVE' => 'Y');
        $arSelect = array('ID','NAME','UF_IMAGE_DISCOUNT','UF_IMAGE_WHITE','SECTION_PAGE_URL','SORT');
        $res = CIBlockSection::GetList($arOrder,$arFilter,false,$arSelect);
        while ($fin_result = $res->GetNext()) {
            $arResult['SECTIONS_FINAL'][$fin_result['ID']] = $fin_result;
            $rsFile = CFile::GetByID($fin_result["UF_IMAGE_DISCOUNT"]);
            if($arFile = $rsFile->Fetch()){
                $arResult['SECTIONS_FINAL'][$fin_result['ID']]['IMAGE'] = '/upload/'.$arFile['SUBDIR'].'/'.$arFile['FILE_NAME'];
            }
            $whFile = CFile::GetByID($fin_result["UF_IMAGE_WHITE"]);
            if($awFile = $whFile->Fetch()){
                $arResult['SECTIONS_FINAL'][$fin_result['ID']]['IMAGE_WHITE'] = '/upload/'.$awFile['SUBDIR'].'/'.$awFile['FILE_NAME'];
            }
        }   
    }
    
    if($arParams['SECTION_CODE'] && $arParams['IBLOCK_ID']){
        //prent($arParams['SECTION_CODE']);
        //prent($arParams['IBLOCK_ID']);
        $arElementFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'],'ACTIVE' => 'Y','SECTION_CODE' => $arParams['SECTION_CODE'], 'INCLUDE_SUBSECTIONS' => 'Y', 'PROPERTY_CHEAPER_VALUE' => 'YES');
        $arElementSelect = array('ID', 'NAME', 'CODE', 'IBLOCK_SECTION_ID');
        $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
        while($ob = $res->GetNextElement()){  
            $arFields = $ob->GetFields();
            $arResult['SUBSECTIONS'][$arFields['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID'] = $arFields['IBLOCK_SECTION_ID'];
            $arResult['SUBSECTIONS'][$arFields['IBLOCK_SECTION_ID']]['CODE'] = $arFields['CODE'];
            if($arResult['SUBSECTIONS'][$arFields['IBLOCK_SECTION_ID']]['COUNT']){
                $arResult['SUBSECTIONS'][$arFields['IBLOCK_SECTION_ID']]['COUNT']++;
            }else{
                $arResult['SUBSECTIONS'][$arFields['IBLOCK_SECTION_ID']]['COUNT'] = 1;
            }
        } 
        //prent($arResult['SUBSECTIONS']);
        
        if($arParams['IBLOCK_ID'] == 10){
        //выясняем какого уровня текущий раздел
        foreach($arResult['MAP'] as $k => $v){
            if($v['CODE'] == $arParams['SECTION_CODE']){
                $arParams['DEPTH_LEVEL'] = $v['DEPTH_LEVEL'];
            }   
        }
        
            if($arParams['DEPTH_LEVEL'] == 1){
                foreach($arResult['SUBSECTIONS'] as $k => $v){
                    if($arResult['MAP'][$k]['DEPTH_LEVEL'] == 3){
                        $arResult['SUBSECTIONS_TO_SHOW'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID'];
                        if($arResult['SUBSECTIONS_TO_COUNT'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID']]['COUNT']){
                            $arResult['SUBSECTIONS_TO_COUNT'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID']]['COUNT'] +=  $arResult['SUBSECTIONS'][$k]['COUNT'];
                        }else{
                            $arResult['SUBSECTIONS_TO_COUNT'][$arResult['MAP'][$arResult['MAP'][$k]['IBLOCK_SECTION_ID']]['ID']]['COUNT'] =  $arResult['SUBSECTIONS'][$k]['COUNT'];
                        }
                    }else{
                        $arResult['SUBSECTIONS_TO_SHOW'][$k] = $k;
                        $arResult['SUBSECTIONS_TO_COUNT'][$k]['COUNT'] = $arResult['SUBSECTIONS'][$k]['COUNT'];
                    }
                }
            }elseif($arParams['DEPTH_LEVEL'] == 2){
                foreach($arResult['SUBSECTIONS'] as $k => $v){
                    if($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] != $arParams['DEPTH_LEVEL']){
                        $arResult['SUBSECTIONS_TO_SHOW'][$k] = $k;
                        $arResult['SUBSECTIONS_TO_COUNT'][$k]['COUNT'] = $arResult['SUBSECTIONS'][$k]['COUNT'];
                    }
                }
            } 
        }
        
        if($arParams['IBLOCK_ID'] == 11){
            $arResult['MAP_ELEVEN'] = array();
            //отдельно собираем дерево для 11-го инфоблока
            $arFilter = Array('IBLOCK_ID' => 11, 'ACTIVE' => 'Y');
            $db_list = CIBlockSection::GetList(Array("left_margin" => "asc"), $arFilter, true, array("ID", "NAME", "CODE", "DEPTH_LEVEL","PICTURE", "IBLOCK_SECTION_ID"));
            while ($ar_result = $db_list->GetNext()) {
                $arResult['MAP_ELEVEN'][$ar_result['ID']] = $ar_result;
            }   
            //проверяем в каких разделах 1-го уровня есть товары подешевле
            foreach($arResult['SECTIONS_ELEVEN'] as $k => $v){
                if($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 3){                
                    $arResult['SECTIONS_TO_SHOW_ELEVEN'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['ID'];                    
                }elseif($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 2){
                    $arResult['SECTIONS_TO_SHOW_ELEVEN'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID'];
                }elseif($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 1){
                    $arResult['SECTIONS_TO_SHOW_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['ID']] = $arResult['MAP_ELEVEN'][$k]['ID'];
                }
            }     
            //выясняем какого уровня текущий раздел
            foreach($arResult['MAP_ELEVEN'] as $k => $v){
                if($v['CODE'] == $arParams['SECTION_CODE']){
                    $arParams['DEPTH_LEVEL'] = $v['DEPTH_LEVEL'];
                }   
            }
            
            if($arParams['DEPTH_LEVEL'] == 1){
                foreach($arResult['SUBSECTIONS'] as $k => $v){
                    if($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] == 3){
                        $arResult['SUBSECTIONS_TO_SHOW'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID']] = $arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID'];
                        if($arResult['SUBSECTIONS_TO_COUNT'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID']]['COUNT']){
                            $arResult['SUBSECTIONS_TO_COUNT'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID']]['COUNT'] = $arResult['SUBSECTIONS_TO_SHOW'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID']]['COUNT'] + 1;
                        }else{
                            $arResult['SUBSECTIONS_TO_COUNT'][$arResult['MAP_ELEVEN'][$arResult['MAP_ELEVEN'][$k]['IBLOCK_SECTION_ID']]['ID']]['COUNT'] =  $arResult['SUBSECTIONS'][$k]['COUNT'];
                        }
                    }else{
                        $arResult['SUBSECTIONS_TO_SHOW'][$k] = $k;
                        $arResult['SUBSECTIONS_TO_COUNT'][$k]['COUNT'] = $arResult['SUBSECTIONS'][$k]['COUNT'];
                    }
                }
            }elseif($arParams['DEPTH_LEVEL'] == 2){
                foreach($arResult['SUBSECTIONS'] as $k => $v){
                    if($arResult['MAP_ELEVEN'][$k]['DEPTH_LEVEL'] != $arParams['DEPTH_LEVEL']){
                        $arResult['SUBSECTIONS_TO_SHOW'][$k] = $k;   
                        $arResult['SUBSECTIONS_TO_COUNT'][$k]['COUNT'] = $arResult['SUBSECTIONS'][$k]['COUNT'];
                    }
                }
            } 
        }
        
        
        //запрашиваем разделы
    if($arParams['IBLOCK_ID'] == 10){    
        if($arResult['SUBSECTIONS_TO_SHOW']){
            $arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arResult['SUBSECTIONS_TO_SHOW'], 'ACTIVE' => 'Y');
            $arSelect = array('ID','NAME','PICTURE','CODE','SECTION_PAGE_URL');
            $res = CIBlockSection::GetList($arOrder,$arFilter,false,$arSelect);
            while ($sect = $res->GetNext()) {
                $arResult['SUBSECT'][$sect['ID']] = $sect;
                $arResult['SUBSECT'][$sect['ID']]['COUNT'] = $arResult['SUBSECTIONS_TO_COUNT'][$sect['ID']]['COUNT'];
                $arResult['SUBSECT'][$sect['ID']]['LINK'] = str_replace("katalog", "podeshevle", $sect['SECTION_PAGE_URL']);
                if($sect['PICTURE']){
                    $rsFile = CFile::GetFileArray($sect['PICTURE']);
                    $arResult['SUBSECT'][$sect['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($rsFile, array('width'=>29, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                    $arResult['SUBSECT'][$sect['ID']]['IMAGES']['FULL'] = $rsFile;
                }
            }
        }
    }elseif($arParams['IBLOCK_ID'] == 11){
        
        if($arResult['SUBSECTIONS_TO_SHOW']){
            
                 
            $arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arResult['SUBSECTIONS_TO_SHOW'], 'ACTIVE' => 'Y');
            $arSelect = array('ID','IBLOCK_ID','NAME','CODE','UF_SECTION_FRIEND');
            $res = CIBlockSection::GetList($arOrder,$arFilter,false,$arSelect);
            while ($sect = $res->GetNext()) {
                $arResult['SUBSECT'][$sect['ID']] = $sect;
                $arResult['SUBSECT'][$sect['ID']]['COUNT'] = $arResult['SUBSECTIONS_TO_COUNT'][$sect['ID']]['COUNT'];
                $arResult['SUBSECT'][$sect['ID']]['LINK'] = $arParams['URI_PARTS'][0].$sect['CODE'].'/';
                $arResult['ELEVEN_SUB_FRIENDS'][$sect['UF_SECTION_FRIEND']] = $sect['UF_SECTION_FRIEND'];
            }
            
            //запрашиваем контент из 10-го инфоблока
            if($arResult['ELEVEN_SUB_FRIENDS']){
                $arFilter = array('IBLOCK_ID' => 10, 'ID' => $arResult['ELEVEN_SUB_FRIENDS'], 'ACTIVE' => 'Y');
                $arSelect = array('ID','NAME','PICTURE','CODE','SECTION_PAGE_URL');
                $res = CIBlockSection::GetList(false,$arFilter,false,$arSelect);
                $sect = array();
                while ($sect = $res->GetNext()) {
                    $arResult['ELEVEN_CONTENT'][$sect['ID']]['LINK'] = str_replace("katalog", "podeshevle", $sect['SECTION_PAGE_URL']);
                    if($sect['PICTURE']){
                        $rsFile = CFile::GetFileArray($sect['PICTURE']);
                        $arResult['ELEVEN_CONTENT'][$sect['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($rsFile, array('width'=>29, 'height'=>50), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                        $arResult['ELEVEN_CONTENT'][$sect['ID']]['IMAGES']['FULL'] = $rsFile;
                    }
                }
                //связываем полученные массивы
                foreach($arResult['SUBSECT'] as $k => $v){
                    foreach($arResult['ELEVEN_CONTENT'] as $ke => $va){
                        if($v['UF_SECTION_FRIEND'] == $ke){
                            $arResult['SUBSECT'][$k]['IMAGES'] = $va['IMAGES'];
                        }
                    }               
                }
            }
        }
    }
          
    }
    
    


   
    $this->SetResultCacheKeys(array(
        "SECTIONS_TO_SHOW",
        "SECTIONS_FINAL",
        "SECTIONS",
        "IBLOCK_11_ITEMS",
        "IBLOCK_11_FRIENDS_IDS",
        "MAP",
        "SUBSECT"
    ));
    
    $this->IncludeComponentTemplate();
}?>
