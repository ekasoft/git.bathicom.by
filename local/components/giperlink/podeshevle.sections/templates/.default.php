<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="col-md-3" id="left-col-cat">
    <?foreach($arResult['SECTIONS_FINAL'] as $k => $v){?>
    <a href="/podeshevle/<?=$v['CODE'];?>/" class="a-task">
        <div class="task-menu-page<?if(strpos($arParams['URI_PARTS'][0],$v['CODE'])){?> active<?}?>">
            <img class="img-1" src="<?=$v['IMAGE'];?>">
            <img class="img-2" src="<?=$v['IMAGE_WHITE'];?>">
            <span class="name"><?=$v['NAME'];?></span>
            <span class="triangle"></span>
        </div>
    </a>
    <?}?>
</div>
<div class="col-md-9 jsAjaxCatalog" id="left-col-cat"> 
    <?if($arResult['SUBSECT']){?>
        <div class="category-list">
            <div class="row">
                <?foreach($arResult['SUBSECT'] as $k => $v){?>
                <div class="col-lg-4 col-md-6">
                    <div class="item-categor">
                        <a href="<?=$v['LINK'];?>" title="<?=$v['NAME'];?>">
                            <span class="img"><img src="<?=$v['IMAGES']['SHOW']['src'];?>" alt="<?=$v['NAME'];?>" class="img-responsive"></span>
                            <span class="coll"><?=$v['COUNT'];?></span>
                            <span class="titles"><?=$v['NAME'];?></span>
                        </a>
                    </div>
                </div>
                <?}?>
            </div>
        </div>
    <?}?>

        