<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');

if(htmlspecialchars($_REQUEST['TP_SECTION_CODE']) == 'plitki'){
    $arParams['SECTION_ID'] = 268;
}
if(htmlspecialchars($_REQUEST['TP_SECTION_CODE']) == 'bathroom-furniture'){
    $arParams['SECTION_ID'] = 279;
}

if($this->StartResultCache(false)) {     
    $arFilter = array('IBLOCK_ID' => 11, 'ACTIVE' => 'Y', 'SECTION_ID' => $arParams['SECTION_ID'], 'DEPTH_LEVEL' => 2); 
    $arSelect = array('IBLOCK_ID', 'ID', 'NAME', 'CODE', 'DEPTH_LEVEL','IBLOCK_SECTION_ID','PICTURE');
    $rsSect = CIBlockSection::GetList(array(),$arFilter, false, $arSelect);
    while ($arSect = $rsSect->GetNext())
    {
        $arResult['SECTIONS'][$arSect['ID']] = $arSect;
        if($arSect['PICTURE']){
            $img = CFile::GetFileArray($arSect['PICTURE']);
            $arResult['SECTIONS'][$arSect['ID']]['IMAGE'] = $img;
        }
    }
   
    $this->SetResultCacheKeys(array(
        "SECTIONS"
    ));
    
    $this->IncludeComponentTemplate();
}?>
