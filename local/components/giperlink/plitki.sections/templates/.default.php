<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="category-list">
    <div class="row">
        
        <?foreach($arResult['SECTIONS'] as $k => $v){?>
        <div class="col-lg-4 col-md-6">
            <div class="item-categor">
                <a href="/katalog/<?=htmlspecialchars($_REQUEST['TP_SECTION_CODE']);?>/<?=$v['CODE'];?>/" title="<?=$v['NAME'];?>">
                    <span class="img"><img src="<?=$v['IMAGE']['SRC'];?>" alt="<?=$v['NAME'];?>" class="img-responsive"></span>
                    <span class="coll"></span>
                    <span class="titles"><?=$v['NAME'];?></span>
                </a>
            </div>
        </div>
        <?}?>        
        
    </div>
</div>