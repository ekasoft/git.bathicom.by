<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');
$arResult = array();
if($this->StartResultCache(false)) {
    $IBLOCK_ID = 10;
    $properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>$IBLOCK_ID));
    while ($prop_fields = $properties->GetNext())
    {
      echo $prop_fields["ID"]." - ".$prop_fields["NAME"]."<br>";
    }
    
    $this->IncludeComponentTemplate();


}