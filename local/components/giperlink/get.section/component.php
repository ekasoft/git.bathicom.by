<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');

$arParams['SECTION_CODE'] = htmlspecialchars($_REQUEST['SECTION_CODE']);
//prent($arParams['SECTION_CODE']);

$arResult = array();
if($this->StartResultCache()) {
    
    if($arParams['SECTION_CODE']) {
        $arFilter = array('IBLOCK_ID' => 10, 'CODE' => $arParams['SECTION_CODE']);
        $arSelect = array('IBLOCK_ID','ID');
        $res = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
        $item = array();
        if($ob = $res->GetNextElement()) {
            $item = $ob->GetFields();
            $arResult['SECTION'] = $item;
        }
    }
    //prent($arResult['SECTION']);
    
    $this->SetResultCacheKeys(array(
        "SECTION",
    ));
    
    $this->IncludeComponentTemplate();
}