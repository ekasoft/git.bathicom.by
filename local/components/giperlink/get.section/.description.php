<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => 'Получить раздел',
	"DESCRIPTION" => 'Получить раздел',
	"ICON" => "/images/sections_list.gif",
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>
