<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule('iblock');
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');

$arParams['PRODUCT_ID'] = intval($_POST['productId']);
$arParams['NOTES'] = htmlspecialchars($_POST['notes']);

$arFields = array(
   "NOTES" => $arParams['NOTES']
);

$res = CSaleBasket::Update($arParams['PRODUCT_ID'], $arFields);
if($res){
    $arParams['RES'] = 'It is good!';
}

echo json_encode($arParams);
?>
