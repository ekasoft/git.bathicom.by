<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?if($arResult['ITEMS']){?>
    <?foreach($arResult['ITEMS'] as $k => $v){?>
    <?if($_REQUEST['deb'] == 'Y'){?>
    <?prent($v);?>
    <?}?>
        <div class="col-lg-4 col-md-6">
            <div class="product-item">
                <div class="img-wrap">
                    <a href="<?=$v['DETAIL_PAGE_URL'];?>" title="<?=$v['NAME'];?>">
                        <img src="<?if($v['IMAGES']['SHOW']['src']){ echo $v['IMAGES']['SHOW']['src'];}else{echo '/upload/cacheResize/b01/63c/133991f08787637c3d8d9c9f9f29e79d.png';}?>" alt="<?=$v['NAME'];?>" title="<?=$v['NAME'];?>">
                        <?if($v["PRICES"]["DISCOUNT"]){?>
                            <div class="discount-perc"><span>-<?=$v['PRICES']['DISCOUNT_PERCENT'];?>%</span></div>
                        <?}?>
                        <?if($v['ICONS']){?>
                            <?foreach($v['ICONS'] as $keya => $url){?>
                            <img src="<?=$url;?>">
                            <?}?>
                        <?}?>
                    </a>
                    <?if($v['QUANTITY'] == 0 || $v['QUANTITY'] == ''){?>
                        <div class="mark gray" style="display: block;">Нет на складе</div>
                    <?}?>
                       <?/* <div class="mark green" style="display: block;">Товар в корзине</div> */?>
                    <?if($v['HOVER_PROPS']){?>    
                        <div class="content-task" style="bottom: 100%;">
                            <div class="description-task">
                                <div class="introtext-task">
                                    <ul>
                                        <?foreach($v['HOVER_PROPS'] as $key => $val){?>
                                            <li><em><?=$v['PROPS'][$val]['NAME']?></em><span><?=$v['PROPS'][$val]['VALUE']?></span></li>
                                        <?}?>
                                    </ul>	
                                </div>
                                <a class="btn button_detail" href="<?=$v['DETAIL_PAGE_URL'];?>">Подробнее</a>
                            </div>
                        </div>
                    <?}?>
                </div>
                <div class="text-wrap">
                    <span class="h5">
                        <a href="<?=$v['DETAIL_PAGE_URL'];?>" title="<?=$v['NAME'];?>"><?=$v['NAME'];?></a>
                    </span>
                    
                    <ul class="description-params">
                        <?if($v['DESC_PROPS']){?>
                        <?foreach($v['DESC_PROPS'] as $key => $val){?>
                            <?if($v['PROPS'][$val]['VALUE']){?> 
                            <li><?=$v['PROPS'][$val]['NAME']?>: <?=$v['PROPS'][$val]['VALUE']?></li>
                            <?}?>
                        <?}?>
                        <?}?>
                    </ul>                   
                    <div class="price">
                        <span class="large"><?=$v['PRICES']['SHOW']['FORMAT'];?> руб.</span>
                        <?/*<br><span class="old byr"><?=  number_format($v['PRICES']['SHOW']['FORMAT']*10000,0,'',' ');?> руб.</span>*/?>
                    </div>
                    <? if ($v["PRICES"]["DISCOUNT"]): ?>
                        <div class="share">
                            <span class="old"><?=$v['PRICES']['SHOW']["OLD_PRICE"]; ?> руб.</span>
                            <span class="discount">Экономия <?=$v['PRICES']['SHOW']['DISCOUNT'];?> руб.</span>
                        </div>
                    <? endif; ?>
                </div>
                <div class="form-wrap-new">
                    <form class="parameters" method="post" action="/local/include/scripts/addbasket.php">
                        <?if($v['QUANTITY'] != 0 && $v['QUANTITY'] != ''){?>
                            <input type="hidden" name="ELEMENT_ID" value="<?=$v['ID'];?>">
                            <input type="hidden" name="MAX_QUANTITY" value="<?=$v['QUANTITY'];?>">
                            <input type="hidden" name="IBLOCK_ID" value="<?=$v['IBLOCK_ID'];?>">
                            <input class="count-input" min="1" value="1" name="QUANTITY"  max="<?=$v['QUANTITY'];?>" type="number" placeholder=""/>
                            <div class="units">
                                <ul>
                                    <li class="active">шт</li>
                                </ul>
                            </div>
                            <a class="button jsAddBasket" href="javascript:void(0);" title="В корзину">В корзину</a>
                        <?}else{?>
                            <div class="">
                                <span class="buy">Под заказ</span>
                            </div>
                        <?}?>
                    </form>       
                </div>
                <?if($v['QUANTITY'] != 0 && $v['QUANTITY'] != ''){?>
                <div class="button-block">
                    <div class="buy-one-click">
                        <div class="insertAjax">
                        <a class="buy" href="javascript:void(0);">Купить в один клик</a>
                        <form action="/local/components/ekasoft/buy1click/ajax.php" method="post">
                            <input type="submit" value="Ok" class="jsBuyOneClick">
                            <input type="text" class="masktel" name="ONE_CLICK[PHONE]" placeholder="+375 (XX) XXX-XX-XX">
                            <input type="hidden" name="ONE_CLICK[PRODUCT_ID]" value="<?=$v['ID']?>">
                            <span class="bt-close-one-click"></span>
                        </form>
                        </div>
                    </div>  
                </div>
                <?}?>
            </div> 
        </div> 
    <?}?>
<?}?>