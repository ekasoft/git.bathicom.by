<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');
$arParams['SECTION_CODE'] = htmlspecialchars($_REQUEST['SECTION_CODE']);
$arParams['SECTION_ID'] = intval($_REQUEST['SECTION_ID']);
$arParams['IBLOCK_ID'] = intval($_REQUEST['IBLOCK_ID']);

$arResult = array();
if($this->StartResultCache(false)) {   
    
    if(!$arParams['SECTION_CODE']){ //случай, когда не выбран раздел
        
        //главная страница
        if(!$arParams['IBLOCK_ID']){
            
            $arElementFilter = array('IBLOCK_ID' => 10,'ACTIVE' => 'Y','PROPERTY_CHEAPER_VALUE' => 'YES', ">CATALOG_QUANTITY" => 1);
            $arElementSelect = array('ID','IBLOCK_ID','NAME','IBLOCK_SECTION_ID','PREVIEW_PICTURE','DETAIL_PAGE_URL','PROPERTY_*');
            $arNav = array('nTopCount' => 12);
            $res = CIBlockElement::GetList(false,$arElementFilter,false,$arNav,$arElementSelect); 
            while($ob = $res->GetNextElement()){  
                $arFields = $ob->GetFields();
                $IDs[$arFields['ID']] = $arFields['ID'];
                $arResult['ITEMS'][$arFields['ID']] = $arFields;
                $arProps = $ob->GetProperties();
                $arResult['ITEMS'][$arFields['ID']]['PROPS'] = $arProps;
                if($arFields['PREVIEW_PICTURE']){
                    $img = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
                    $arResult['ITEMS'][$arFields['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($img, array('width'=>220, 'height'=>190), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                    $arResult['ITEMS'][$arFields['ID']]['IMAGES']['FULL'] = $img;
                }

                //забираем свойства для нависания
                $arSFilter = array('ID' => $arFields['IBLOCK_SECTION_ID'], 'IBLOCK_ID' => 10);
                $arSSelect = array('ID','IBLOCK_ID','UF_HOVER_PROPS','UF_DESC_PARAMS');
                $db_list = CIBlockSection::GetList(false,$arSFilter,false,$arSSelect);
                while($ar_result = $db_list->GetNext())
                {
                   $arResult['ITEMS'][$arFields['ID']]['HOVER_PROPS'] = $ar_result['UF_HOVER_PROPS'];
                   $arResult['ITEMS'][$arFields['ID']]['DESC_PROPS'] = $ar_result['UF_DESC_PARAMS'];
                }

                //забираем цену
                $arResult['ITEMS'][$arFields['ID']]['PRICES'] = prices($arFields['ID']);

                 //иконки
                if($arProps['SALE_ICON']['VALUE'] == 'Y'){
                    $arResult['ITEMS'][$arFields['ID']]['ICONS'][1] = '/local/img/sale-icon.png';
                }
                if($arProps['IN_HIT']['VALUE'] == 'Y'){
                    $arResult['ITEMS'][$arFields['ID']]['ICONS'][2] = '/local/img/hit-icon.png';
                }

                if($arProps['COUNTRY']['VALUE_ENUM']){
                    $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $arProps['COUNTRY']['VALUE_ENUM']);
                    $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    if($object = $nr->GetNextElement())
                    {
                        $arF = $object->GetFields();
                        if($arF['DETAIL_PICTURE']){
                           $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                           $arResult['ITEMS'][$arFields['ID']]['ICONS'][3] = $image['SRC'];
                        }
                    }
                }


            }
            
            
              //отдельно забираем все товары 11-го инфоблока
            $arElementFilter = array('IBLOCK_ID' => 11,'ACTIVE' => 'Y','PROPERTY_CHEAPER_VALUE' => 'YES', ">CATALOG_QUANTITY" => 1);
            $arElementSelect = array();
            $arNav = array('nTopCount' => 12);
            $res = CIBlockElement::GetList(false, $arElementFilter, false, $arNav, $arElementSelect);  
            while($ob = $res->GetNextElement()){  

                $arFields = $ob->GetFields();
                $IDs[$arFields['ID']] = $arFields['ID'];
                $arResult['ITEMS'][$arFields['ID']] = $arFields;
                $arProps = $ob->GetProperties();
                $arResult['ITEMS'][$arFields['ID']]['PROPS'] = $arProps;
                if($arFields['PREVIEW_PICTURE']){
                    $img = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
                    $arResult['ITEMS'][$arFields['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($img, array('width'=>220, 'height'=>190), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                    $arResult['ITEMS'][$arFields['ID']]['IMAGES']['FULL'] = $img;
                }
                //забираем св-ва для нависания
                $arSFilter = array('ID' => $arFields['IBLOCK_SECTION_ID'], 'IBLOCK_ID' => 11);
                $arSSelect = array('ID','IBLOCK_ID','UF_HOVER_PROPS','UF_DESC_PARAMS');
                $db_list = CIBlockSection::GetList(false,$arSFilter,false,$arSSelect);
                while($ar_result = $db_list->GetNext())
                {
                   $arResult['ITEMS'][$arFields['ID']]['HOVER_PROPS'] = $ar_result['UF_HOVER_PROPS'];
                   $arResult['ITEMS'][$arFields['ID']]['DESC_PROPS'] = $ar_result['UF_DESC_PARAMS'];
                }
                //забираем цену
                $arResult['ITEMS'][$arFields['ID']]['PRICES'] = prices($arFields['ID']);

                 //иконки
                if($arProps['SALE_ICON']['VALUE'] == 'Y'){
                    $arResult['ITEMS'][$arFields['ID']]['ICONS'][1] = '/local/img/sale-icon.png';
                }
                if($arProps['IN_HIT']['VALUE'] == 'Y'){
                    $arResult['ITEMS'][$arFields['ID']]['ICONS'][2] = '/local/img/hit-icon.png';
                }

                if($arProps['COUNTRY']['VALUE_ENUM']){
                    $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $arProps['COUNTRY']['VALUE_ENUM']);
                    $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    if($object = $nr->GetNextElement())
                    {
                        $arF = $object->GetFields();
                        if($arF['DETAIL_PICTURE']){
                           $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                           $arResult['ITEMS'][$arFields['ID']]['ICONS'][3] = $image['SRC'];
                        }
                    }
                }

            }
            
        }else{
       
            //забираем все товары 10-го инфоблока
            $arElementFilter = array('IBLOCK_ID' => 10,'ACTIVE' => 'Y','PROPERTY_CHEAPER_VALUE' => 'YES');
            $arElementSelect = array('ID','IBLOCK_ID','NAME','IBLOCK_SECTION_ID','PREVIEW_PICTURE','DETAIL_PAGE_URL','PROPERTY_*');
            $res = CIBlockElement::GetList(false,$arElementFilter,false,false,$arElementSelect); 
            while($ob = $res->GetNextElement()){  
                $arFields = $ob->GetFields();
                $IDs[$arFields['ID']] = $arFields['ID'];
                $arResult['ITEMS'][$arFields['ID']] = $arFields;
                $arProps = $ob->GetProperties();
                $arResult['ITEMS'][$arFields['ID']]['PROPS'] = $arProps;
                if($arFields['PREVIEW_PICTURE']){
                    $img = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
                    $arResult['ITEMS'][$arFields['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($img, array('width'=>220, 'height'=>190), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                    $arResult['ITEMS'][$arFields['ID']]['IMAGES']['FULL'] = $img;
                }

                //забираем свойства для нависания
                $arSFilter = array('ID' => $arFields['IBLOCK_SECTION_ID'], 'IBLOCK_ID' => 10);
                $arSSelect = array('ID','IBLOCK_ID','UF_HOVER_PROPS','UF_DESC_PARAMS');
                $db_list = CIBlockSection::GetList(false,$arSFilter,false,$arSSelect);
                while($ar_result = $db_list->GetNext())
                {
                   $arResult['ITEMS'][$arFields['ID']]['HOVER_PROPS'] = $ar_result['UF_HOVER_PROPS'];
                   $arResult['ITEMS'][$arFields['ID']]['DESC_PROPS'] = $ar_result['UF_DESC_PARAMS'];
                }

                //забираем цену
                $arResult['ITEMS'][$arFields['ID']]['PRICES'] = prices($arFields['ID']);

                 //иконки
                if($arProps['SALE_ICON']['VALUE'] == 'Y'){
                    $arResult['ITEMS'][$arFields['ID']]['ICONS'][1] = '/local/img/sale-icon.png';
                }
                if($arProps['IN_HIT']['VALUE'] == 'Y'){
                    $arResult['ITEMS'][$arFields['ID']]['ICONS'][2] = '/local/img/hit-icon.png';
                }

                if($arProps['COUNTRY']['VALUE_ENUM']){
                    $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $arProps['COUNTRY']['VALUE_ENUM']);
                    $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    if($object = $nr->GetNextElement())
                    {
                        $arF = $object->GetFields();
                        if($arF['DETAIL_PICTURE']){
                           $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                           $arResult['ITEMS'][$arFields['ID']]['ICONS'][3] = $image['SRC'];
                        }
                    }
                }


            }

            //отдельно забираем все товары 11-го инфоблока
            $arElementFilter = array('IBLOCK_ID' => 11,'ACTIVE' => 'Y','PROPERTY_CHEAPER_VALUE' => 'YES');
            $arElementSelect = array();
            $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
            while($ob = $res->GetNextElement()){  

                $arFields = $ob->GetFields();
                $IDs[$arFields['ID']] = $arFields['ID'];
                $arResult['ITEMS'][$arFields['ID']] = $arFields;
                $arProps = $ob->GetProperties();
                $arResult['ITEMS'][$arFields['ID']]['PROPS'] = $arProps;
                if($arFields['PREVIEW_PICTURE']){
                    $img = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
                    $arResult['ITEMS'][$arFields['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($img, array('width'=>220, 'height'=>190), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                    $arResult['ITEMS'][$arFields['ID']]['IMAGES']['FULL'] = $img;
                }
                //забираем св-ва для нависания
                $arSFilter = array('ID' => $arFields['IBLOCK_SECTION_ID'], 'IBLOCK_ID' => 11);
                $arSSelect = array('ID','IBLOCK_ID','UF_HOVER_PROPS','UF_DESC_PARAMS');
                $db_list = CIBlockSection::GetList(false,$arSFilter,false,$arSSelect);
                while($ar_result = $db_list->GetNext())
                {
                   $arResult['ITEMS'][$arFields['ID']]['HOVER_PROPS'] = $ar_result['UF_HOVER_PROPS'];
                   $arResult['ITEMS'][$arFields['ID']]['DESC_PROPS'] = $ar_result['UF_DESC_PARAMS'];
                }
                //забираем цену
                $arResult['ITEMS'][$arFields['ID']]['PRICES'] = prices($arFields['ID']);

                 //иконки
                if($arProps['SALE_ICON']['VALUE'] == 'Y'){
                    $arResult['ITEMS'][$arFields['ID']]['ICONS'][1] = '/local/img/sale-icon.png';
                }
                if($arProps['IN_HIT']['VALUE'] == 'Y'){
                    $arResult['ITEMS'][$arFields['ID']]['ICONS'][2] = '/local/img/hit-icon.png';
                }

                if($arProps['COUNTRY']['VALUE_ENUM']){
                    $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $arProps['COUNTRY']['VALUE_ENUM']);
                    $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    if($object = $nr->GetNextElement())
                    {
                        $arF = $object->GetFields();
                        if($arF['DETAIL_PICTURE']){
                           $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                           $arResult['ITEMS'][$arFields['ID']]['ICONS'][3] = $image['SRC'];
                        }
                    }
                }

            }
        
        }
        
    }elseif($arParams['IBLOCK_ID']){
        $arElementFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'SECTION_CODE' => $arParams['SECTION_CODE'], 'ACTIVE' => 'Y', 'INCLUDE_SUBSECTIONS' => 'Y', 'PROPERTY_CHEAPER_VALUE' => 'YES');
        $arElementSelect = array('ID','IBLOCK_ID','NAME','IBLOCK_SECTION_ID','PREVIEW_PICTURE','DETAIL_PAGE_URL','PROPERTY_*');
        $res = CIBlockElement::GetList(false,$arElementFilter,false,false,$arElementSelect); 
        while($ob = $res->GetNextElement()){  
            $arFields = $ob->GetFields();
            $IDs[$arFields['ID']] = $arFields['ID'];
            $arResult['ITEMS'][$arFields['ID']] = $arFields;
            $arProps = $ob->GetProperties();
            $arResult['ITEMS'][$arFields['ID']]['PROPS'] = $arProps;
            $arParams['SECTION_ID'] = $arFields['IBLOCK_SECTION_ID'];
            if($arFields['PREVIEW_PICTURE']){
                $img = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
                $arResult['ITEMS'][$arFields['ID']]['IMAGES']['SHOW'] = CFile::ResizeImageGet($img, array('width'=>220, 'height'=>190), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
                $arResult['ITEMS'][$arFields['ID']]['IMAGES']['FULL'] = $img;
            }

            //забираем свойства для нависания
            $arSFilter = array('ID' => $arFields['IBLOCK_SECTION_ID'], 'IBLOCK_ID' => $arParams['IBLOCK_ID']);
            $arSSelect = array('ID','IBLOCK_ID','UF_HOVER_PROPS','UF_DESC_PARAMS');
            $db_list = CIBlockSection::GetList(false,$arSFilter,false,$arSSelect);
            while($ar_result = $db_list->GetNext())
            {
               $arResult['ITEMS'][$arFields['ID']]['HOVER_PROPS'] = $ar_result['UF_HOVER_PROPS'];
               $arResult['ITEMS'][$arFields['ID']]['DESC_PROPS'] = $ar_result['UF_DESC_PARAMS'];
            }
            
            //иконки
            if($arProps['SALE_ICON']['VALUE'] == 'Y'){
                $arResult['ITEMS'][$arFields['ID']]['ICONS'][1] = '/local/img/sale-icon.png';
            }
            if($arProps['IN_HIT']['VALUE'] == 'Y'){
                $arResult['ITEMS'][$arFields['ID']]['ICONS'][2] = '/local/img/hit-icon.png';
            }
            
            if($arProps['COUNTRY']['VALUE_ENUM']){
                $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $arProps['COUNTRY']['VALUE_ENUM']);
                $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($object = $nr->GetNextElement())
                {
                    $arF = $object->GetFields();
                    if($arF['DETAIL_PICTURE']){
                       $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                       $arResult['ITEMS'][$arFields['ID']]['ICONS'][3] = $image['SRC'];
                    }
                }
            } 
            
            
            //prent($arProps);
                //забираем цену
            $arResult['ITEMS'][$arFields['ID']]['PRICES'] = prices($arFields['ID']);

                //забираем количество
            $cres = CCatalogProduct::GetByID($arFields['ID']);
            $arResult['ITEMS'][$arFields['ID']]['QUANTITY'] = $cres['QUANTITY'];

        }
        
    }
    
    if($IDs){
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()), false, false, array('QUANTITY', 'ID'));
        while( $row = $dbl->fetch() ) {
            $arResult['ITEMS'][$row['ID']]['QUANTITY'] = $row['QUANTITY'];
        }
    }
    //prent($arResult['ITEMS']);
     
    $this->SetResultCacheKeys(array(
        "ITEMS",
    ));
    
    $this->IncludeComponentTemplate();
}?>
