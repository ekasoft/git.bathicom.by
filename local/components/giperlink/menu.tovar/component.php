<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule('iblock');
$arResult = array();
if($this->StartResultCache(10)) {
    // Запрос разделов инфоблока меню
    $map = array();
    $arFilter = Array('IBLOCK_ID' => 10, 'ACTIVE' => 'Y');
    $db_list = CIBlockSection::GetList(Array("left_margin" => "asc"), $arFilter, true, array("ID", "NAME", "CODE", "DEPTH_LEVEL","PICTURE", "IBLOCK_SECTION_ID", "SECTION_PAGE_URL"));
    while ($ar_result = $db_list->GetNext()) {
        $map[$ar_result['ID']] = $ar_result;
    }

    //prent($map);

    $map_sec = array();

    foreach ($map as $key => $val) {
        if($val['DEPTH_LEVEL'] == 3) {
            $parent_id = $val['IBLOCK_SECTION_ID'];
            $parent_id2 = $map[$parent_id]['IBLOCK_SECTION_ID'];
            $map_sec[$parent_id2]['CHILDS'][$parent_id]['CHILDS'][$val['ID']] = array(
                'ID' => $val['ID'],
                'NAME' => $val['NAME'],
                'URL' => $val['CODE'],
                'LINK' => $val['SECTION_PAGE_URL'],
                'DEPTH_LEVEL' => 3,
                'PARENT_ID' => $val['IBLOCK_SECTION_ID'],
            );
        } elseif($val['DEPTH_LEVEL'] == 2) {
            $parent_id = $val['IBLOCK_SECTION_ID'];
            $map_sec[$parent_id]['CHILDS'][$val['ID']] = array(
                'ID' => $val['ID'],
                'NAME' => $val['NAME'],
                'URL' => $val['CODE'],
                'LINK' => $val['SECTION_PAGE_URL'],
                'DEPTH_LEVEL' => 2,
            );
        } elseif($val['DEPTH_LEVEL'] == 1) {
            $map_sec[$key] = array(
                'ID' => $val['ID'],
                'NAME' => $val['NAME'],
                'URL' => $val['CODE'],
                'LINK' => $val['SECTION_PAGE_URL'],
            );
        }
    }
    //prent($map_sec);
    $arResult['SECTION'] = $map_sec[$arParams['SECTION_ID']];
    
    
    $arSelect = Array("ID", "NAME", "DETAIL_PICTURE", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_IN_HIT", "IBLOCK_SECTION_ID", "PROPERTY_BRAND", "PROPERTY_SALE_ICON","PROPERTY_COUNTRY");
    $arFilter = Array("IBLOCK_ID"=>10, "ACTIVE"=>"Y", "PROPERTY_IN_HIT_VALUE"=>"Y", "SECTION_ID" => $arParams['SECTION_ID'], "INCLUDE_SUBSECTIONS" => "Y");
    $res = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);
    $IDs = array();
    while($oba = $res->GetNextElement())
    {
        $ob = $oba->GetFields();
        $props = $oba->GetProperties();
        $arResult['HITS'][$ob['ID']] = $ob;
        if($map[$ob['IBLOCK_SECTION_ID']]['DEPTH_LEVEL'] == 2){
            $arResult['HITS'][$ob['ID']]['SECT_CLASS'] = $map[$ob['IBLOCK_SECTION_ID']]['CODE'];            
        }
        if($map[$ob['IBLOCK_SECTION_ID']]['DEPTH_LEVEL'] == 3){
            $arResult['HITS'][$ob['ID']]['SECT_CLASS'] = $map[$map[$ob['IBLOCK_SECTION_ID']]['IBLOCK_SECTION_ID']]['CODE'];           
        }
        if($ob['DETAIL_PICTURE']) {
            $img = CFile::GetFileArray($ob['DETAIL_PICTURE']);
            $arResult['HITS'][$ob['ID']]['IMG'] = CFile::ResizeImageGet($img, array('width'=>213, 'height'=>163), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
        }elseif($ob['PREVIEW_PICTURE']){
            $img = CFile::GetFileArray($ob['PREVIEW_PICTURE']);
            $arResult['HITS'][$ob['ID']]['IMG'] = CFile::ResizeImageGet($img, array('width'=>213, 'height'=>163), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
        }
        
        $arResult['HITS'][$ob['ID']]['PRICE'] = CPrice::GetList(array(), array("PRODUCT_ID" => $ob['ID'],"CATALOG_GROUP_ID" => 1))->Fetch();
        $arResult['HITS'][$ob['ID']]['PRICE_BYN'] = CCurrencyRates::ConvertCurrency($arResult['HITS'][$ob['ID']]['PRICE']['PRICE'], $arResult['HITS'][$ob['ID']]['PRICE']['CURRENCY'], "BYR");
        $arResult['HITS'][$ob['ID']]['PRICE_BYN_TO_SHOW'] = number_format($arResult['HITS'][$ob['ID']]['PRICE_BYN'], 2, '.', ' ');
        $arResult['HITS'][$ob['ID']]['PRICE_BYR_TO_SHOW'] = number_format($arResult['HITS'][$ob['ID']]['PRICE_BYN']*10000, 0, '.', ' ');  
        $arResult['HITS'][$ob['ID']]['PRICE_INFO'] = prices($arResult['HITS'][$ob['ID']]);
        $IDs[$ob['ID']] = $ob['ID']; 
        
        //иконки
        if($props['SALE_ICON']['VALUE'] == 'Y'){
            $arResult['HITS'][$ob['ID']]['ICONS'][1] = '/local/img/sale-icon.png';
        }
        // это и так хит, поэтому не нужно условия
        $arResult['HITS'][$ob['ID']]['ICONS'][2] = '/local/img/hit-icon.png';


        if($props['COUNTRY']['VALUE_ENUM']){
            $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
            $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $props['COUNTRY']['VALUE_ENUM']);
            $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if($object = $nr->GetNextElement())
            {
                $arF = $object->GetFields();
                if($arF['DETAIL_PICTURE']){
                   $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                   $arResult['HITS'][$ob['ID']]['ICONS'][3] = $image['SRC'];
                }
            }
        }
        
        
    }
    
    if($IDs){
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $IDs, false, false, array()), false, false, array('QUANTITY', 'ID'));
        while( $row = $dbl->fetch() ) {
            $arResult['HITS'][$row['ID']]['QUANTITY'] = $row['QUANTITY'];
        }
    }


    $this->IncludeComponentTemplate();


}