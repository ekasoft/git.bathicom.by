<h1>NOW</h1>
                            <ul class="menu-level">
                                <? foreach ( $arResult["SECTIONS_GOODS"][$arItem["ID"]] as $arSectionsGoods ): ?>
                                    <li>
                                        <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>"><?= $arSectionsGoods["NAME"]; ?></a>
                                        <div class="topmenu-innermenu2">
                                            <div class="title-colection"><?= $arSectionsGoods["NAME"]; ?></div>
                                            <div class="slider-colection">
                                                <div class="slide-block">
                                                    <div class="item-slid">
                                                        <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>">
                                                            <img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200, "MODE" => "inv"), $arResult["FILES"][$arSectionsGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arSectionsGoods["NAME"]; ?>" class="img-responsive" />
                                                            <div class="info-block">
                                                                <p class="title-info"><?= $arSectionsGoods["NAME"]; ?></p>
                                                                <p class="price">от 30 500 <span>000</span></p>
                                                            </div>
                                                        </a>
                                                        <div class="topmenu-innermenu3">
                                                            <div class="col-md-12">
                                                                <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>" class="close-bt"></a>
                                                                <div class="title-colection"><?= $arSectionsGoods["NAME"]; ?></div>
                                                                <div class="row">
                                                                    <div class="col-md-7">
                                                                        <div>
                                                                            <img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200, "MODE" => "inv"), $arResult["FILES"][$arSectionsGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" class="img-responsive" />
                                                                        </div>
                                                                        <div class="colection-description"> 
                                                                            <?= substr($arSectionsGoods["PREVIEW_TEXT"], 0, 280); ?>
                                                                        </div>
                                                                        <div>
                                                                            <a href="<?= $arSectionsGoods["DETAIL_PAGE_URL"]; ?>" title="<?= $arSectionsGoods["NAME"]; ?>" class="bt-show-colection">Посмотреть коллекцию</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5 jq-listcoll">
                                                                        <div class="menu-list-colection">
                                                                            <div class="title-colection2">Плитка в <?= $arSectionsGoods["NAME"]; ?></div>
                                                                            <ul>
                                                                                <? foreach ( $arResult["OFFERS"][$arSectionsGoods["ID"]] as $arOffer ): ?>
                                                                                    <li>
                                                                                        <a href="<?= $arOffer["DETAIL_PAGE_URL"]; ?>" title="<?= $arOffer["NAME"]; ?>">
                                                                                            <span class="img-warp"><img src="<?= imageResize(array("WIDTH" => 262, "HEIGHT" => 200, "MODE" => "inv"), $arResult["FILES"][$arSectionsGoods["PREVIEW_PICTURE"]]["SRC"]); ?>" alt="<?= $arOffer["NAME"]; ?>" /></span>
                                                                                            <span class="name"><?= $arOffer["NAME"]; ?></span>
                                                                                        </a>
                                                                                    </li>
                                                                                <? endforeach; ?>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <? endforeach; ?>
                            </ul>   
  <?/*   <div class="col-md-8 col-lg-10 js-hit-towarov">
        <div class="title-menu-hit">Хиты продаж<i class="fa fa-caret-right"></i></div>
        <div class="products-list products_list-list new-style-list">
           foreach( $arResult["HITS"] as $arItem):?>
                <?$APPLICATION->IncludeComponent(
"ekasoft.view:element", 
"item", 
array(
    "RESULT" => $arItem,
    "FILES" => $arResult["FILES"],
    "TEMPLATE" => "vertical",
    "QUANTITY" => $arResult["QUANTITIES"][$arItem["ID"]],
    "COMPONENT_TEMPLATE" => "item",
    "GET_SECTION_CODE" => "Y"
),
false
);?>
            <?endforeach;?>
        </div>
    </div>
</div>*/?>   