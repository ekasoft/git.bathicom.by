<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="modal modal-vcenter fade in" id="rassrochka-popup" aria-hidden="false" style="display: none;">    
    <div class="modal-dialog popup-ras">
        <div class="modal-content rass-modal">
            <div class="modal-cont-wrap rass-wrap">
                <div class="head-pop">
                    <span>Рассрочка на <?=RASSROCH_MONTH;?> месяца</span>
                    <img src="/local/images/canc-gray.png" class="canc-gray">
                    <img src="/local/images/canc-blue.png" class="canc-blue">
                </div>
                <div class="item-block">
                    <div class="img-popup"><img src="<?=$arResult['ITEM']['IMAGE']['src'];?>"></img></div>
                    <div class="name-popup"><?=$arResult['ITEM']['NAME'];?></div>
                    <div class="first-pay price-pop">
                        <div class="pop-number">
                            <?=$arResult['ITEM']['FIRST_PAY'];?>
                            <span> руб.</span>
                        </div>
                        <div class="pop-title">Первый взнос</div>              
                    </div>
                    <div class="month-pay price-pop">
                        <div class="pop-number"><?=$arResult['ITEM']['REG_PAY'];?>
                            <span> руб.</span>
                        </div>
                        <div class="pop-title">Ежемесячный платёж</div>              
                    </div>
                    <div class="sum-popup price-pop">
                        <div class="pop-number"><?=$arResult['ITEM']['PRICE_BYN_TO_SHOW'];?>
                            <span> руб.</span>
                        </div>
                        <div class="pop-title">Итого</div>              
                    </div>
                </div>
                    
                <div class="pop-to-cart"><span class="pop-span">Для получения банковской рассрочки вам необходимо предоставить лишь паспорт и справку о доходах<span><div class="buy-info">
                                <form class="parameters"  method="post" action="/local/include/scripts/addbasket.php">
                                        <input type="hidden" name="ELEMENT_ID" value="<?=$arResult['ITEM']['ID'];?>">
                                        <input type="hidden" name="MAX_QUANTITY" value="1">
                                        <input type="hidden" name="IBLOCK_ID" value="<?=$arResult['ITEM']['IBLOCK_ID'];?>">  
                                        <input type="hidden" name="RASSROCHKA" value="RASSROCHKA">
                                        <div class="jq-number" style="display: none;">
                                            <div class="jq-number__field" style="display: none;">
                                                <input class="count-input" min="1" value="1" name="QUANTITY" max="1" type="number" placeholder="" style="display: none;">
                                            </div>
                                        </div>
                                    <a class="button jsAddBasket" href="javascript:void(0);" title="В корзину">В корзину</a>
                                </form>
                            </div></span></span></div>
        <div class="pop-footer"><span>Мы предлагаем выгодные условия на покупку множества полезных и популярных товаров в рассрочку на <?=RASSROCH_MONTH;?> месяца</span></div>
            </div>
        </div>
    </div>      
</div>

<!-- halva-popup -->

<div class="modal modal-vcenter fade in" id="halva-popup" aria-hidden="false" style="display: none;">    
    <div class="modal-dialog popup-ras">
        <div class="modal-content rass-modal">
            <div class="modal-cont-wrap rass-wrap">
                <div class="head-pop">
                    <span>Рассрочка на <?=RASSROCH_MONTH;?> месяца при оплате картой "Халва"</span>
                    <img src="/local/images/canc-gray.png" class="canc-gray">
                    <img src="/local/images/canc-blue.png" class="canc-blue">
                </div>
                <div class="item-block">
                    <div class="img-popup"><img src="<?=$arResult['ITEM']['IMAGE']['src'];?>"></img></div>
                    <div class="name-popup"><?=$arResult['ITEM']['NAME'];?></div>
                    <div class="first-pay price-pop">
                        <div class="pop-number">
                           0
                            <span> руб.</span>
                        </div>
                        <div class="pop-title">Первый взнос</div>              
                    </div>
                    <div class="month-pay price-pop">
                        <div class="pop-number"><?=$arResult['ITEM']['HALVA_REG_PAY'];?>
                            <span> руб.</span>
                        </div>
                        <div class="pop-title">Ежемесячный платёж</div>              
                    </div>
                    <div class="sum-popup price-pop">
                        <div class="pop-number"><?=$arResult['ITEM']['PRICE_BYN_TO_SHOW'];?>
                            <span> руб.</span>
                        </div>
                        <div class="pop-title">Итого</div>              
                    </div>
                </div>
                    
                <div class="pop-to-cart"><span class="pop-span"><span><div class="buy-info">
                                <form class="parameters"  method="post" action="/local/include/scripts/addbasket.php">
                                        <input type="hidden" name="ELEMENT_ID" value="<?=$arResult['ITEM']['ID'];?>">
                                        <input type="hidden" name="MAX_QUANTITY" value="1">
                                        <input type="hidden" name="IBLOCK_ID" value="<?=$arResult['ITEM']['IBLOCK_ID'];?>">  
                                        <input type="hidden" name="RASSROCHKA" value="HALVA">
                                        <div class="jq-number" style="display: none;">
                                            <div class="jq-number__field" style="display: none;">
                                                <input class="count-input" min="1" value="1" name="QUANTITY" max="1" type="number" placeholder="" style="display: none;">
                                            </div>
                                        </div>
                                    <a class="button jsAddBasket" href="javascript:void(0);" title="В корзину">В корзину</a>
                                </form>
                            </div></span></span></div>
        <div class="pop-footer"><span>Мы предлагаем выгодные условия на покупку множества полезных и популярных товаров в рассрочку на <?=RASSROCH_MONTH;?> месяца</span></div>
            </div>
        </div>
    </div>      
</div>

        