<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
CModule::IncludeModule('iblock');
$arParams['ELEMENT_CODE'] = htmlspecialchars($_REQUEST['ELEMENT_CODE']);
$arParams['IBLOCK_ID'] = htmlspecialchars($_REQUEST['IBLOCK_ID']);
$arParams['SKU_CODE'] = htmlspecialchars($_REQUEST['SKU_CODE']);
if($arParams['SKU_CODE']){
    $arParams['IBLOCK_ID'] = 11;
}

$arResult = array();
if($this->StartResultCache()) {
  
    $arElementFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'],'ACTIVE' => 'Y');
    if($arParams['SKU_CODE']){
        $arElementFilter['CODE'] = $arParams['SKU_CODE'];
    }else{
        $arElementFilter['CODE'] = $arParams['ELEMENT_CODE'];
    }
    $arElementSelect = array('IBLOCK_ID','ID','NAME','IBLOCK_SECTION_ID','DETAIL_PICTURE','PREVIEW_PICTURE');
    $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
    if($ob = $res->GetNext()){ 
        $arResult['ITEM'] = $ob;
        if($ob['DETAIL_PICTURE']){
            $img = CFile::GetFileArray($ob['DETAIL_PICTURE']);
            $arResult['ITEM']['IMAGE'] = CFile::ResizeImageGet($img, array('width'=>90, 'height'=>90), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
        }elseif($ob['PREVIEW_PICTURE']){
            $img = CFile::GetFileArray($ob['PREVIEW_PICTURE']);
            $arResult['ITEM']['IMAGE'] = CFile::ResizeImageGet($img, array('width'=>90, 'height'=>90), BX_RESIZE_IMAGE_PROPORTIONAL_ALT,true);
        }
        $arResult['ITEM']['PRICE'] = CPrice::GetList(array(), array("PRODUCT_ID" => $ob['ID'],"CATALOG_GROUP_ID" => 1))->Fetch();
        $arResult['ITEM']['PRICE_BYN'] = CCurrencyRates::ConvertCurrency($arResult['ITEM']['PRICE']['PRICE'], "USD", "BYR");
        $arResult['ITEM']['PRICE_BYN_TO_SHOW'] = number_format($arResult['ITEM']['PRICE_BYN'], 2, '.', ' ');
        $arResult['ITEM']['FIRST_PAY'] = number_format($arResult['ITEM']['PRICE_BYN']*FIRST_PAY_PERCENT/100, 2, '.', ' ');
        $arResult['ITEM']['REG_PAY'] = number_format(($arResult['ITEM']['PRICE_BYN'] - $arResult['ITEM']['PRICE_BYN']*FIRST_PAY_PERCENT/100)/RASSROCH_MONTH, 2, '.', ' ');
        $arResult['ITEM']['HALVA_REG_PAY'] = number_format($arResult['ITEM']['PRICE_BYN']/RASSROCH_MONTH, 2, '.', ' ');
    }

    $this->SetResultCacheKeys(array(
        "ITEM",
    ));
    
    $this->IncludeComponentTemplate();
}?>
