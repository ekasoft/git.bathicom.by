<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');

$arParams['IBLOCK_ID'] = intval($_REQUEST['IBLOCK_ID']);
if($arParams['IBLOCK_ID'] == 11){
    $arParams['SECTION_CODE'] = htmlspecialchars($_REQUEST['TP_SECTION_CODE']);
}else{
    $arParams['SECTION_CODE'] = htmlspecialchars($_REQUEST['SECTION_CODE']);
}
$arParams['ELEMENT_CODE'] = htmlspecialchars($_REQUEST['ELEMENT_CODE']);
if(!$arParams['IBLOCK_ID']){
    $arParams['IBLOCK_ID'] = 10;
}

//prent($arParams);
$arResult = array();
if($this->StartResultCache()) {
    
    if($arParams['SECTION_CODE']) {
        $arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'CODE' => $arParams['SECTION_CODE']);
        $arSelect = array('IBLOCK_ID','ID');
        $res = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
        $item = array();
        if($ob = $res->GetNextElement()) {
            $item = $ob->GetFields();
            $arResult['SECTION'] = $item;
        }
    }
    //забираем навигационную цепочку для текущего раздела
    $arNavSelect = array('IBLOCK_SECTION_ID','ID','NAME','CODE');
    $nav = CIBlockSection::GetNavChain($arParams['IBLOCK_ID'], $arResult['SECTION']['ID'],$arNavSelect);
    $i = 1;//счётчик уровня раздела каталога
    while($cout = $nav->GetNext()){
        
        $arResult['NAV']['LEVEL_'.$i]['PARENT_ID'] = $cout['IBLOCK_SECTION_ID'];
        $arResult['NAV']['LEVEL_'.$i]['NAME'] = $cout['NAME'];
        $arResult['NAV']['LEVEL_'.$i]['ID'] = $cout['ID'];
        $arResult['NAV']['LEVEL_'.$i]['CODE'] = $cout['CODE'];
  
    
        //забираем соседние разделы      
        $arNeighboursSelect = array('ID','NAME','CODE');
        $arNeighboursFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'SECTION_ID' => $cout['IBLOCK_SECTION_ID'], 'ACTIVE' => 'Y', '!ID' => $cout['ID']);
        $arNeighboursOrder = array('SORT' => 'ASC');
        $neighbours = CIBlockSection::GetList($arNeighboursOrder,$arNeighboursFilter,false,$arNeighboursSelect);
        while($ar_result = $neighbours->GetNext()){
           $arResult['NAV']['LEVEL_'.$i]['NEIGHBOURS'][$ar_result['ID']]['NAME'] = $ar_result['NAME'];  
           $arResult['NAV']['LEVEL_'.$i]['NEIGHBOURS'][$ar_result['ID']]['CODE'] = $ar_result['CODE'];
        }  
        $i++;   
    }

    //Получаем название элемента, если это страница товара
    if($arParams['ELEMENT_CODE']){
        $arElementFilter = array('CODE' => $arParams['ELEMENT_CODE']);
        $arElementSelect = array('NAME');
        $res = CIBlockElement::GetList(false, $arElementFilter, false, false, $arElementSelect);  
        if($ob = $res->GetNextElement()){  
           $arFields = $ob->GetFields();
           $arResult['NAV']['ELEMENT_NAME'] = $arFields['NAME'];
        }     
    }
    
    $this->SetResultCacheKeys(array(
        "SECTION",
        "NAV",
    ));
    
    $this->IncludeComponentTemplate();
}?>
