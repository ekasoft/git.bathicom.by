<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<div class="breadcrumbs">
    <div class="container">
        <ul>
            <li id="bx_breadcrumb_0" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemref="bx_breadcrumb_1">
                <a href="/" title="Главная" itemprop="url"><span itemprop="title">Главная</span></a>
            </li>
            <?if($arResult['NAV']['LEVEL_1']['CODE'] && $arResult['NAV']['LEVEL_1']['NAME']){?>
                <li id="bx_breadcrumb_1" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" <?if($arResult['NAV']['LEVEL_1']['NEIGHBOURS']){?>class="bc-triangle"<?}?>>
                    <a href="/katalog/<?=$arResult['NAV']['LEVEL_1']['CODE'];?>/" title="<?=$arResult['NAV']['LEVEL_1']['NAME'];?>" itemprop="url"><span itemprop="title"><?=$arResult['NAV']['LEVEL_1']['NAME'];?></span><span class="triangl<?if(!$arResult['NAV']['LEVEL_2']){?> last-gl<?}?>"></span></a>
                    <div class="neighbours-wrap">
                    <ul class="neighbours">
                        <?if($arResult['NAV']['LEVEL_1']['NEIGHBOURS']){?>
                            <?foreach($arResult['NAV']['LEVEL_1']['NEIGHBOURS'] as $k => $v){?>
                                <?//prent($v);?>
                                <li><a href="/katalog/<?=$v['CODE'];?>/"><?=$v['NAME'];?></a></li>
                            <?}?>
                        <?}?>
                    </ul>
                    </div>
                </li>
            <?}?>
            <?if($arResult['NAV']['LEVEL_2']['CODE'] && $arResult['NAV']['LEVEL_2']['NAME']){?>
                <li id="bx_breadcrumb_2" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" <?if($arResult['NAV']['LEVEL_2']['NEIGHBOURS']){?>class="bc-triangle"<?}?>>
                    <?if($arResult['NAV']['LEVEL_2']['NEIGHBOURS']){?>
                    <a href="/katalog/<?=$arResult['NAV']['LEVEL_1']['CODE'];?>/<?=$arResult['NAV']['LEVEL_2']['CODE'];?>/" title="<?=$arResult['NAV']['LEVEL_2']['NAME'];?>" itemprop="url"><span itemprop="title"><?=$arResult['NAV']['LEVEL_2']['NAME'];?></span><span class="triangl<?if(!$arResult['NAV']['LEVEL_3']){?> last-gl<?}?>"></span></a>
                    <div class="neighbours-wrap">
                    <ul class="neighbours">
                        <?foreach($arResult['NAV']['LEVEL_2']['NEIGHBOURS'] as $k => $v){?>
                            <li><a href="/katalog/<?=$arResult['NAV']['LEVEL_1']['CODE'];?>/<?=$v['CODE'];?>/"><?=$v['NAME'];?></a></li>
                        <?}?>
                    </ul>
                    </div>
                    <?}else{?>
                    <span itemprop="title"><?=$arResult['NAV']['LEVEL_2']['NAME'];?></span>
                    <?}?>
                </li>
            <?}?>
            <?if($arResult['NAV']['LEVEL_3']['CODE'] && $arResult['NAV']['LEVEL_3']['NAME']){?>
                <li id="bx_breadcrumb_3" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb" itemprop="child" <?if($arResult['NAV']['LEVEL_3']['NEIGHBOURS']){?>class="bc-triangle"<?}?>>
                    <?if($arResult['NAV']['LEVEL_3']['NEIGHBOURS']){?>
                    <a href="/katalog/<?=$arResult['NAV']['LEVEL_1']['CODE'];?>/<?=$arResult['NAV']['LEVEL_2']['CODE'];?>/<?=$arResult['NAV']['LEVEL_3']['CODE'];?>/" title="<?=$arResult['NAV']['LEVEL_3']['NAME'];?>" itemprop="url"><span itemprop="title"><?=$arResult['NAV']['LEVEL_3']['NAME'];?></span><span class="triangl<?if(!$arParams['ELEMENT_CODE']){?> last-gl<?}?>"></span></a>
                    <div class="neighbours-wrap">
                    <ul class="neighbours">                        
                        <?foreach($arResult['NAV']['LEVEL_3']['NEIGHBOURS'] as $k => $v){?>
                            <li><a href="/katalog/<?=$arResult['NAV']['LEVEL_1']['CODE'];?>/<?=$arResult['NAV']['LEVEL_2']['CODE'];?>/<?=$v['CODE'];?>/"><?=$v['NAME'];?></a></li>
                        <?}?>                        
                    </ul>
                    </div>
                    <?}else{?>
                    <span itemprop="title"><?=$arResult['NAV']['LEVEL_3']['NAME'];?></span>
                    <?}?>
                </li>   
                <?if($arParams['ELEMENT_CODE']){?>
                <li class="bx-breadcrumb-item">
                    <span><?=$arResult['NAV']['ELEMENT_NAME'];?></span>
                </li>
                <?}?>
            <?}?>
        </ul>
    </div>
</div>