<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

CModule::IncludeModule('iblock');
CModule::IncludeModule('catalog');
CModule::IncludeModule('sale');

$arParams['PARAMS'] = array(
    0 => array(
        'ID' => 10,
        'EXCLUDE_SECTIONS' => array(64,73),
    ),
    1 => array(
        'ID' => 11,
    ),
);

$arResult = array();
if($this->StartResultCache()) {
    foreach($arParams['PARAMS'] as $k => $v) {
        if($v['ID']) {
            $arFilter = array('IBLOCK_ID' => $v['ID'],array(
                "LOGIC" => "OR",
                array("catalog_PRICE_1" => 0),
                array("catalog_PRICE_1" => false),
            ));
            if($v['EXCLUDE_SECTIONS']) {
                $arFilter['SECTION_ID'] = $v['EXCLUDE_SECTIONS'];
                $arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
            }
            //prent($arFilter);
            $arSelect = array('ID','NAME','CODE','catalog_PRICE_1');
            $res = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);  
            while($ob = $res->GetNext()){
                $arResult['TROUBLES'][$ob['ID']] = $ob;
                $el = new CIBlockElement;
                $arLoadProductArray = Array("ACTIVE" => "N");
                $PRODUCT_ID = $ob['ID'];
                //prent($ob['ID']);
                $res2 = $el->Update($PRODUCT_ID, $arLoadProductArray);
            }
        }
    }
    /*
    $_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www";
    $date = date('h:i:s m.d.Y');
    echo $date;
    $file = $_SERVER['DOCUMENT_ROOT'].'/log.txt';
    $current = file_get_contents($file);
    $current .= $date."\n";

    file_put_contents($file, $current);*/
    
    $this->SetResultCacheKeys(array(
        "TROUBLES"
    ));
    
    //$this->IncludeComponentTemplate();
}?>
