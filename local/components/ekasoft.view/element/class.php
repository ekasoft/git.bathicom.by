<?php

if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();



class ViewElement extends \CBitrixComponent { 
    
    public function onPrepareComponentParams($arParams) {
        $arParams["BASKET_VIEW"] = array(
          "plitka"  => 0,
          "mebel"   => 0
        );
        $arParams['CACHE_TIME'] = 86400*30;
        $arParams["VIEW_MAIN_PROP"] = array("BRAND", "COLLECTION_REF");
        $arParams["VIEW_PROP"] = array("ARTNUMBER", "WIDTH", "COLOR", "LENGTH", "HEIGHT", "ADDRESS_PRODUCER", "NAME_SUPPLIER", "MEASURE", "COLOR_CODE");
        
        
        return $arParams;
    }
    
    public function getSection($sid) {
        //prent($sid);
        $dl = true;
        $row = array();
        while($dl) {
            $row = \Bitrix\Iblock\SectionTable::getRowById($sid);
            if($row['DEPTH_LEVEL'] == 2) {
                $dl = false;
                return $row['CODE'];
            }
            if($row['DEPTH_LEVEL'] > 2) {
                $sid = $row['IBLOCK_SECTION_ID'];
            }
        }
    }
    
    public function executeComponent() {
        if( $this->startResultCache() ) {
            
            $this->arResult["ITEMS"] = $this->arParams["RESULT"];

            $this->arResult["FILES"] = $this->arParams["FILES"];
            $this->arResult["QUANTITY"] = $this->arParams["QUANTITY"];

            $basket = \EkaSoft\Base\Core\Context::getInstance()->get('BASKET');

            if( isset($this->arResult["ITEMS"][0]) ) {
                $this->arResult["ITEMS"] = $this->arResult["ITEMS"][0];    
            }

                if($this->arResult["ITEMS"]['PROPERTIES']['COUNTRY']['VALUE_ENUM']){
                    $arSelect = Array("ID", "NAME", "DETAIL_PICTURE");
                    $arFilter = Array("IBLOCK_ID"=>16, "ACTIVE"=>"Y", "NAME" => $this->arResult["ITEMS"]['PROPERTIES']['COUNTRY']['VALUE_ENUM']);
                    $nr = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    if($object = $nr->GetNextElement())
                    {
                        $arF = $object->GetFields();
                        if($arF['DETAIL_PICTURE']){
                           $image = CFile::GetFileArray($arF['DETAIL_PICTURE']);
                           $this->arResult["ITEMS"]['ICON_COUNTRY'] = $image['SRC'];
                        }
                    }          
                }           
            
            $arFilter = Array('IBLOCK_ID'=>$this->arParams['RESULT']['IBLOCK_ID'], 'ID'=>$this->arParams['RESULT']['IBLOCK_SECTION_ID']);
            $arSelect = array('ID', 'NAME', 'UF_DESC_PARAMS','UF_HOVER_PROPS');
            CModule::IncludeModule('iblock');
            $db_list = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
            if($ar_result = $db_list->GetNext())
            {
                $this->arResult["ITEMS"]["DESC_PROPS"] = $ar_result['UF_DESC_PARAMS'];
                $this->arResult["ITEMS"]["HOVER_PROPS"] = $ar_result['UF_HOVER_PROPS'];
            }    
               


            if($this->arParams["GET_SECTION_CODE"]) {
                $this->arResult["SECTION"] = $this->getSection($this->arResult["ITEMS"]['IBLOCK_SECTION_ID']);
            }
            if(empty($this->arResult["ITEMS"][1]) and $this->arResult["ITEMS"]["ID"] < 1)
            {
                            if (Bitrix\Main\Loader::includeModule('iblock'))
                            {
                                    \Bitrix\Iblock\Component\Tools::process404(
                                            'Товар отсутствует в каталоге'
                                            ,true
                                            ,true
                                            ,true
                                            ,'/404.php'
                                    );
                            }
            } else {
                    if( in_array($this->arResult["ITEMS"]["ID"], $basket) ) {
                        $this->arResult["IN_BASKET"] = "Y";
                    }

                    $this->includeComponentTemplate($this->arParams["TEMPLATE"]);

            }
            $this->endResultCache();
        }
    }
}