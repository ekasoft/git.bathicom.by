<div class="price">
    <?= $arResult["ITEMS"]["PRICES"]["PRICE"] == 0 || $arResult["ITEMS"]["PRICES"]["PRICE"] == '' ? '' : $arResult["ITEMS"]["PRICES"]["HTML"]["PRICE"]; ?>
    <? if ($arResult["ITEMS"]["PRICES"]["PRICE"] != 0 && $arResult["ITEMS"]["PRICES"]["PRICE"] != ''): ?>
    <? endif; ?>
</div>
<? if ($arResult["ITEMS"]["PRICES"]["DISCOUNT"]): ?>
    <div class="share">
        <span class="old"><?= $arResult["ITEMS"]["PRICES"]["HTML"]["OLD_PRICE"]; ?> руб.</span>
        <span class="discount">Экономия <?= $arResult["ITEMS"]["PRICES"]["HTML"]["DISCOUNT"]; ?> руб.</span>
    </div>
<? endif; ?>