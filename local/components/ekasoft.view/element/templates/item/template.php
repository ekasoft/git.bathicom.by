 <?//prent($arParams);?>
<div class="product-item">
    <div class="img-wrap">
        <a href="<?= $arResult["ITEMS"]["DETAIL_PAGE_URL"];?>" title="<?= $arResult["ITEMS"]["NAME"];?>">
            <img src="<?= imageResize(array("WIDTH" => 220, "HEIGHT" => 190), $arResult["FILES"][$arResult["ITEMS"]["PREVIEW_PICTURE"]]["SRC"]) ?>" alt="<?= $arResult["ITEMS"]["NAME"]; ?>" width="176" height="92" title="<?= $arResult["ITEMS"]["NAME"]; ?>"/>
                <?if($arResult["ITEMS"]["PROPERTIES"]['SALE_ICON']['VALUE'] == 'Y'){?>
                    <img src="/local/img/sale-icon.png">
                <?}?>
                <?if($arResult["ITEMS"]["PROPERTIES"]['IN_HIT']['VALUE'] == 'Y'){?>
                    <img src="/local/img/hit-icon.png">
                <?}?>
                <?if($arResult["ITEMS"]["ICON_COUNTRY"]){?>
                    <img src="<?=$arResult["ITEMS"]["ICON_COUNTRY"];?>">
                <?}?>
        </a>
        <?if( $arResult["QUANTITY"]["QUANTITY"] == 0 ):?>
            <div class="mark gray">Нет на складе</div>
        <?elseif( $arResult["IN_BASKET"] == "Y" ):?>
            <div class="mark green">Товар в корзине</div>
        <?endif;?>
            <?if($arResult["ITEMS"]['HOVER_PROPS']){?>    
                <div class="content-task">
                    <div class="description-task">
                        <div class="introtext-task">
                            <ul>
                                <?foreach($arResult["ITEMS"]['HOVER_PROPS'] as $k => $v){?>
                                    <?if ( isset($arResult["ITEMS"]["PROPERTIES"][$v]["VALUE"]) && $arResult["ITEMS"]["PROPERTIES"][$v]["VALUE"] != '' ){?>
                                        <li><em><?= $arResult["ITEMS"]["PROPERTIES"][$v]["NAME"] ?></em><span><?= $arResult["ITEMS"]["PROPERTIES"][$v]["VALUE"]; ?></span></li>
                                    <?}?>
                                <?}?>
                            </ul>	
                        </div>
                        <a class="btn button_detail" href="<?= $arResult["ITEMS"]["DETAIL_PAGE_URL"]; ?>">Подробнее</a>
                    </div>
                </div>
            <?}?>
        <!--div class="discount">-10%</div-->
    </div>
    <div class="text-wrap">
        <span class="h5">
            <a href="<?= $arResult["ITEMS"]["DETAIL_PAGE_URL"]; ?>" title="<?= $arResult["ITEMS"]["NAME"]; ?>"><?= $arResult["ITEMS"]["NAME"]; ?></a>
        </span>
        <!--div class="rating-wrap">
            <div class="rating"></div>
            <span class="count">15</span>
        </div-->
        <ul class="description-params">

                <?
                    foreach ( $arParams["VIEW_MAIN_PROP"] as $prop ):
                        if ( isset($arResult["ITEMS"]["PROPERTIES"][$prop]["VALUE"]) && $arResult["ITEMS"]["PROPERTIES"][$prop]["VALUE"] != '' ):
                            ?>
                            <li><?= $arResult["ITEMS"]["PROPERTIES"][$prop]["NAME"] ?>: <?= $arResult["ITEMS"]["PROPERTIES"][$prop]["VALUE"]; ?></li>
                            <?
                        endif;
                    endforeach; 
                ?>

                <?/*foreach($arResult["ITEMS"]['DESC_PROPS'] as $k => $v){?>
                    <?if(isset($arResult["ITEMS"]["PROPERTIES"][$v]["VALUE"]) && $arResult["ITEMS"]["PROPERTIES"][$v]["VALUE"] != ''){?>
                        <li><?= $arResult["ITEMS"]["PROPERTIES"][$v]["NAME"] ?>: <?= $arResult["ITEMS"]["PROPERTIES"][$v]["VALUE"]; ?></li>
                    <?}?>
                <?}*/?>

        </ul>
        <? include "price.php"; ?>
    </div>
    <?if( $arParams["BASKET_BLOCK"] != "N"):?>
        <div class="form-wrap-new">
            <?include "basket.php";?>
        </div>
        <div class="button-block">
            <?include "oneclick.php";?>
        </div>
    <?endif;?>
</div>