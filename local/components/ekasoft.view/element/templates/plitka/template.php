<div class="col-lg-3 col-md-4 col-sm-6">
    <div class="product-item">
        <div class="img-wrap">
            <img src="<?= imageResize(array("WIDTH" => 176, "HEIGHT" => 92), $arResult["FILES"][$arResult["ITEMS"]["PREVIEW_PICTURE"]]["SRC"])?>" alt="<?= $arResult["ITEMS"]["NAME"];?>" width="176" height="92" title="<?= $arResult["ITEMS"]["NAME"];?>"/>
            <!--div class="discount">-10%</div-->
        </div>
        <div class="text-wrap">
            <span class="h5">
                <a href="<?= $arResult["ITEMS"]["DETAIL_PAGE_URL"];?>" title="<?= $arResult["ITEMS"]["NAME"];?>"><?= $arResult["ITEMS"]["NAME"];?></a>
            </span>
            <div class="rating-wrap">
                <div class="rating"></div>
                <span class="count">15</span>
            </div>
            <?include "price.php";?>
        </div>
        <div class="button-block">
            <div class="parameters">
                <input class="count-input" min="1" value="1" type="number" placeholder=""/>
                <div class="units">
                    <ul>
                        <li>м2</li>
                        <li class="active">шт</li>
                    </ul>
                </div>
                <a class="button" href="#">В корзину</a>
            </div>
            <!--div class="buy-one-click">
                <a class="buy" href="#">Купить в один клик</a>
                <form>
                    <input type="submit" value="ok" /><input type="text" class="masktel" placeholder="+375 (XX) XXX-XX-XX" />
                </form>
            </div-->
        </div>
    </div>
</div>
