<form class="parameters" method="post" action="/local/include/scripts/addbasket.php">
    <input type="hidden" name="ELEMENT_ID" value="<?= $arResult["ITEMS"]["ID"]; ?>" />
    <input type="hidden" name="MAX_QUANTITY" value="<?= $arResult["QUANTITY"]["QUANTITY"]; ?>" />
    <input type="hidden" name="IBLOCK_ID" value="<?= $arResult["ITEMS"]["IBLOCK_ID"];?>" />
    <? if ( $arResult["QUANTITY"]["QUANTITY"] != 0 && $arResult["QUANTITY"]["QUANTITY"] != '' ): 
        $iMaxQuantity = empty($arResult["QUANTITY"]["QUANTITY"]) ? 1 : $arResult["QUANTITY"]["QUANTITY"];
    ?>
        <input class="count-input" min="1" value="1" name="QUANTITY"  max="<?= empty($arResult["QUANTITY"]["QUANTITY"]) ? 1 : $arResult["QUANTITY"]["QUANTITY"]; ?>" type="number" placeholder=""/>
        <?if( $arResult["ITEMS"]["PROPERTIES"]["COEFFICIENT"]["VALUE"] > 0 && $arResult["ITEMS"]["PROPERTIES"]["MEASURE"]["VALUE"] == "м2" ):?>
            <input class="text-count-input hide" value="1" name="TRANSFER" max="<?= $iMaxQuantity * $arResult["ITEMS"]["PROPERTIES"]["COEFFICIENT"]["VALUE"] ?>" type="text" placeholder=""/>
        <?endif;?>
        <?if( $arResult["ITEMS"]["PROPERTIES"]["MEASURE"]["VALUE"] != '' ):?>
            <div class="units" <?= $arResult["ITEMS"]["PROPERTIES"]["COEFFICIENT"]["VALUE"] > 0 ? 'data-cof="'.$arResult["ITEMS"]["PROPERTIES"]["COEFFICIENT"]["VALUE"].'"' : '';?>>
                <ul>
                    <?if( $arResult["ITEMS"]["PROPERTIES"]["COEFFICIENT"]["VALUE"] > 0 && $arResult["ITEMS"]["PROPERTIES"]["MEASURE"]["VALUE"] == "м2"):?>
                        <li class="changeMeasure active j_sht" data-measure="j_m2">шт</li>
                        <li class="changeMeasure j_m2" data-measure="j_sht"><?= $arResult["ITEMS"]["PROPERTIES"]["MEASURE"]["VALUE"];?></li>
                    <?else:?>
                        <li class="active">шт<? $arResult["ITEMS"]["PROPERTIES"]["MEASURE"]["VALUE"];?></li>
                    <?endif;?>
                </ul>
            </div>
        <?endif;?>
        <a class="button jsAddBasket" href="javascript:void(0);" title="В корзину">В корзину</a>
    <? else: ?>
        <div class="">
            <span class="buy">Под заказ</span>
        </div>
    <? endif; ?>
</form>