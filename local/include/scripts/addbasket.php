<?php

require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";

\CModule::includeModule("sale");
\CModule::IncludeModule("ekasoft.base");

$iProduct = (int) $_REQUEST["ELEMENT_ID"];
$iCount = (int) $_REQUEST["QUANTITY"];
$iBlock = (int) $_REQUEST["IBLOCK_ID"];
$iNotes = htmlspecialchars($_REQUEST["RASSROCHKA"]);

$template = htmlspecialchars($_REQUEST["TEMPLATE"]);

$objDecorator = new EkaSoft\Base\Decorator\Tovar(null);
$objModel = new Ekasoft\Base\Model\Tovar($objDecorator);
$objEntity = new EkaSoft\Base\Entity\TovarSKU($objModel, $objDecorator);
$objDecorator->initEntity($objEntity);

$objEntity->initParams(array("ID" => $iProduct, "IBLOCK_ID" => $iBlock));

$arProduct = $objEntity->getResult(false);

$arFields = array(
  "PRODUCT_ID" => $iProduct,
  "RPODUCT_XML_ID" => $arProduct[0]["XML_ID"],
  "PRODUCT_PRICE_ID" => 0,
  "PRICE" => $arProduct[0]["PRICES"]["PRICE"],
  "CURRENCY" => "BYR",
  "QUANTITY" => $iCount,
  "LID" => 's1',
  "NAME" => $arProduct[0]["NAME"],
  "DETAIL_PAGE_URL" => $arProduct[0]["DETAIL_PAGE_URL"],
  "CAN_BUY" => "Y",
);

if($iNotes){
    $arFields["NOTES"] = $iNotes;
}

\CSaleBasket::Add($arFields);

\CModule::IncludeModule("catalog");
$dbl = \CCatalogProduct::GetList(array(), array("ID" => $arProduct[0]["ID"], false, false, array()));
while ( $row = $dbl->fetch() ) {
    $quanti = $row;
}

\EkaSoft\Base\Core\Process::update('BASKET');

global $APPLICATION;
$APPLICATION->IncludeComponent("ekasoft.view:element", "item", 
        Array(
          "RESULT" => $arProduct,
          "FILES" => $objEntity->getFiles(),
          "QUANTITY" => $quanti,
          "TEMPLATE" => $template,
        )
);