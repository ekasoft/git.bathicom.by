<?require_once $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php";
$APPLICATION->IncludeComponent(
    "bitrix:search.page",
    "search",
    array(
        "RESTART" => "Y",
        "NO_WORD_LOGIC" => "N",
        "CHECK_DATES" => "N",
        "USE_TITLE_RANK" => "Y",
        "DEFAULT_SORT" => "rank",
        "FILTER_NAME" => "",
        "arrFILTER" => array(
            0 => "no",
        ),
        "arrFILTER_iblock_stores" => array(
            0 => "all",
        ),
        "arrFILTER_iblock_promotions" => array(
            0 => "all",
        ),
        "arrFILTER_iblock_news" => array(
            0 => "all",
        ),
        "SHOW_WHERE" => "N",
        "SHOW_WHEN" => "Y",
        "PAGE_RESULT_COUNT" => "9999",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "N",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "36000000",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Результаты поиска",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "catalog",
        "USE_LANGUAGE_GUESS" => "N",
        "USE_SUGGEST" => "N",
        "SHOW_RATING" => "",
        "RATING_TYPE" => "",
        "PATH_TO_USER_PROFILE" => "",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "search",
        "arrFILTER_iblock_info" => array(
            0 => "5",
        ),
        "arrFILTER_iblock_catalog" => array(
            0 => "10",
            1 => "11",
        ),
        "STRUCTURE_FILTER" => "structure",
        "NAME_TEMPLATE" => "",
        "SHOW_LOGIN" => "Y"
    ),
    false
);