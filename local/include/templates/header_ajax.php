<!DOCTYPE html>
<html lang="<?= ToLower(LANGUAGE_ID); ?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="format-detection" content="telephone=no"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="<?= SITE_CHARSET ?>" />

        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
        <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
      

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="robots" content="index, follow" />

        <?
        $APPLICATION->ShowHead();
        $APPLICATION->ShowMeta("robots", false, $bXhtmlStyle);
        $APPLICATION->ShowMeta("keywords", false, $bXhtmlStyle);
        $APPLICATION->ShowMeta("description", false, $bXhtmlStyle);
        $APPLICATION->ShowHeadStrings();
        ?>

        <title><?= $APPLICATION->ShowTitle(true); ?></title>

        <?
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/bootstrap.min.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/lightbox.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/jquery.mCustomScrollbar.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/pickmeup.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/js/nouislider/jquery.nouislider.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/bootstrap-select.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/normalize.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/styles.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/styles_sk.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/main.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/slick.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/app.min.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/dev.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/gl.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/jquery.fancybox.css");
        Bitrix\Main\Page\Asset::getInstance()->addCss("/local/css/jquery.fancybox-thumbs.css");

        Bitrix\Main\Page\Asset::getInstance()->AddJs("https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/bootstrap.min.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/nouislider/jquery.nouislider.min.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/scripts_sk.js");
        
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/modernizr.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.maskedinput.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.touchSwipe.min.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.carouFredSel-6.2.1-packed.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.pickmeup.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/parallax.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.dotdotdot.min.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/lightbox.min.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.mousewheel.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/bootstrap-select.min.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/slick.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.elevatezoom.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.formstyler.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/bootstrap-rating.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.validate.min.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.form.min.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/scripts.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/scripts_gl.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/ajax.js");
    //  Bitrix\Main\Page\Asset::getInstance()->AddJs("https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/common.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/dev.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.fancybox.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.fancybox-thumbs.js");
        Bitrix\Main\Page\Asset::getInstance()->AddJs("/local/js/jquery.fancybox-media.js");
        ?>

        <? $APPLICATION->ShowCSS(true); ?>
        
        <? include $_SERVER["DOCUMENT_ROOT"] . "/metricHeader.php"; ?>
    </head>
    <body>
        <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
        <span class="popup-menu-bg"></span>
        <span class="popup-search-bg"></span>
        <header class="v2">
            <div class="fixed-header">
            <div class="phone-row">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 right-to-left phones">
                            <span class="city">+375 17 276-75-77</span>
                            <span>
                                <span class="velcom"></span>
                                <span class="mts"></span>
                                <span class="life"></span>
                                <span class="mobile">7577</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mosaic-row">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <a href="/" id="logo">
                                <img src="/local/img/logo-min.png" alt="logo" width="291" height="54">
                            </a>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="work-time-basket">
                                <div id="working-hours">
                                    <?
                                    $APPLICATION->IncludeFile(
                                            SITE_DIR . "include/opening-times.php", Array(), Array("MODE" => "html")
                                    );
                                    ?>
                                </div>
                                <div id="basket" class="delay_04s">
                                    <?
                                    $APPLICATION->IncludeComponent(
                                            "bitrix:sale.basket.basket.small", "cart", array(
                                      "PATH_TO_BASKET" => "/cart/",
                                      "PATH_TO_ORDER" => "/order/",
                                      "SHOW_DELAY" => "N",
                                      "SHOW_NOTAVAIL" => "N",
                                      "SHOW_SUBSCRIBE" => "N",
                                      "COMPONENT_TEMPLATE" => "cart"
                                            ), false
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?
                        $APPLICATION->IncludeComponent(
                          "bitrix:search.title", "bottom", array(
                          "SHOW_INPUT" => "Y",
                          "INPUT_ID" => "title-search-input",
                          "CONTAINER_ID" => "title-search",
                          "PAGE" => "/search/",
                          "NUM_CATEGORIES" => "1",
                          "TOP_COUNT" => "5",
                          "ORDER" => "date",
                          "USE_LANGUAGE_GUESS" => "Y",
                          "CHECK_DATES" => "N",
                          "SHOW_OTHERS" => "N",
                          "CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
                          "CATEGORY_0" => array(
                            0 => "no",
                          ),
                          "CATEGORY_0_iblock_catalog" => array(
                            0 => "all",
                          ),
                          "COMPONENT_TEMPLATE" => "top"
                                ), false
                        );
                        ?>
                        <div class="search-icon hidden-xs"></div>
                    </div>
                </div>
            </div>
            </div>
            <div class="min-search-block">
                <?
                $APPLICATION->IncludeComponent(
                        "bitrix:search.title", "bottom", array(
                  "SHOW_INPUT" => "Y",
                  "INPUT_ID" => "title-search-input",
                  "CONTAINER_ID" => "title-search",
                  "PAGE" => "/search/",
                  "NUM_CATEGORIES" => "1",
                  "TOP_COUNT" => "5",
                  "ORDER" => "date",
                  "USE_LANGUAGE_GUESS" => "Y",
                  "CHECK_DATES" => "N",
                  "SHOW_OTHERS" => "N",
                  "CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
                  "CATEGORY_0" => array(
                    0 => "no",
                  ),
                  "CATEGORY_0_iblock_catalog" => array(
                    0 => "all",
                  ),
                  "COMPONENT_TEMPLATE" => "top"
                        ), false
                );
                ?>
            </div>
            <div class="container main-buttons-box ind-buttons-box posinit">
                
                <div class="row posinit">
                    
                    <div class="col-xs-12 col-sm-6 col-md-3 js-top-menu-element-ajax posinit ajax-menu">
                        <a  href="/katalog/plitka/" data-href="#plitka" class="but but1 but-red bg bg-red delay_04s1 a-url" title="Плитка" data-id="44" data-type="ekasoft.base:menu.collections" data-temp=".ajax">
                            <div class="img-block">
                                <img src="/local/img/plitka-w-min.png" alt="Плитка" />
                                <img src="/local/img/plitka-c-min.png" alt="Плитка" />
                            </div>
                            <span class="text">Плитка</span>
                            <span class="clearfix"></span>
                        </a>
                        <nav class="header-menu-top plitka" id="plitka">
                        <div class="container" id="show-menu44">
                            <div class="l-wrapper">
                                <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                  <symbol id="s--circle">
                                    <circle r="10" cx="20" cy="20"></circle>
                                  </symbol> 
                                </svg>
                                <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g class="g-circles g-circles--v3">
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>  
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                </svg> 
                            </div>
                        </div>
                        </nav>
                    </div>
                    
                    <div class="col-xs-12 col-sm-6 col-md-3 js-top-menu-element-ajax posinit ajax-menu">
                        <a  href="/katalog/santekhnika/" data-href="#santekhnika" class="but but1 but-green bg bg-green delay_04s1 a-url" title="Сантехника" data-id="73" data-type="giperlink:menu.tovar" data-temp="">
                            <div class="img-block">
                                <img src="/local/img/santekhnika-net-min.png" alt="Сантехника" />
                                <img src="/local/img/santekhnika-da-min.png" alt="Сантехника" />
                            </div>
                            <span class="text">Сантехника</span>
                            <span class="clearfix"></span>
                        </a>
                        <nav class="header-menu-top santekhnika" id="santekhnika">
                        <div class="container" id="show-menu73">
                            <div class="l-wrapper">
                                <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                  <symbol id="s--circle">
                                    <circle r="10" cx="20" cy="20"></circle>
                                  </symbol> 
                                </svg>
                                <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g class="g-circles g-circles--v3">
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>  
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                </svg> 
                            </div>
                        </div>
                        </nav>
                    </div>
                    
                    <div class="col-xs-12 col-sm-6 col-md-3 js-top-menu-element-ajax posinit ajax-menu">
                        <a  href="/katalog/vanny/" data-href="#vanny" class="but but1 but-blue bg bg-blue delay_04s1 a-url" title="Ванны" data-id="64" data-type="giperlink:menu.tovar" data-temp="">
                            <div class="img-block">
                                <img src="/local/img/vanny-net-min.png" alt="Ванны" />
                                <img src="/local/img/vanny-da-min.png" alt="Ванны" />
                            </div>
                            <span class="text">Ванны</span>
                            <span class="clearfix"></span>
                        </a>
                        <nav class="header-menu-top vanny" id="vanny">
                        <div class="container" id="show-menu64">
                            <div class="l-wrapper">
                                <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                  <symbol id="s--circle">
                                    <circle r="10" cx="20" cy="20"></circle>
                                  </symbol> 
                                </svg>
                                <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g class="g-circles g-circles--v3">
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>  
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                </svg> 
                            </div>
                        </div>
                        </nav>
                    </div>  
                    
                    <div class="col-xs-12 col-sm-6 col-md-3 js-top-menu-element-ajax posinit ajax-menu">
                        <a  href="/katalog/mebel-dlya-vannoy/" data-href="#mebel-dlya-vannoy" class="but but1 but-orange bg bg-orange delay_04s1 a-url" title="Мебель для ванной" data-id="57" data-type="ekasoft.base:menu.collections" data-temp=".ajax">
                            <div class="img-block">
                                <img src="/local/img/mebel-net-min.png" alt="Мебель для ванной" />
                                <img src="/local/img/mebel-da-min.png" alt="Мебель для ванной" />
                            </div>
                            <span class="text">Мебель для ванной</span>
                            <span class="clearfix"></span>
                        </a>
                        <nav class="header-menu-top mebel-dlya-vannoy" id="mebel-dlya-vannoy">
                        <div class="container" id="show-menu57">
                            <div class="l-wrapper">
                                <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                  <symbol id="s--circle">
                                    <circle r="10" cx="20" cy="20"></circle>
                                  </symbol> 
                                </svg>
                                <svg viewBox="0 0 120 120" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <g class="g-circles g-circles--v3">
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>  
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                      <g class="g--circle">
                                        <use xlink:href="#s--circle" class="u--circle"/>
                                      </g>
                                </svg> 
                            </div>
                        </div>
                        </nav>
                    </div>
                    
                </div> 
                
            </div>
            
            <?/*
            $APPLICATION->IncludeComponent(
                    "bitrix:catalog.section.list", "buttons-box", array(
              "COMPONENT_TEMPLATE" => "tree",
              "IBLOCK_TYPE" => "catalog",
              "IBLOCK_ID" => "2",
              "SECTION_ID" => $_REQUEST["SECTION_ID"],
              "SECTION_CODE" => "",
              "COUNT_ELEMENTS" => "N",
              "TOP_DEPTH" => "2",
              "SECTION_FIELDS" => array(
                0 => "",
                1 => "",
              ),
              "SECTION_USER_FIELDS" => array(
                0 => "",
                1 => "",
              ),
              "VIEW_MODE" => "LIST",
              "SHOW_PARENT_NAME" => "N",
              "SECTION_URL" => "",
              "CACHE_TYPE" => "N",
              "CACHE_TIME" => "36000000",
              "CACHE_GROUPS" => "Y",
              "ADD_SECTIONS_CHAIN" => "N"
                    ), $component
            );*/
            ?>
            <div class="container mosaic-button-row hidden-xs">
                <div class="row">
                    <div class="col-md-12">
                        <div class="button-wrapper">
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar navbar-default navbar-static-top hidden-xs" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse">
                        <?
                        $APPLICATION->IncludeComponent(
                                "bitrix:menu", "top", array(
                          "COMPONENT_TEMPLATE" => "top",
                          "ROOT_MENU_TYPE" => "top",
                          "MENU_CACHE_TYPE" => "N",
                          "MENU_CACHE_TIME" => "86400",
                          "MENU_CACHE_USE_GROUPS" => "Y",
                          "MENU_CACHE_GET_VARS" => array(),
                          "MAX_LEVEL" => "1",
                          "CHILD_MENU_TYPE" => "left",
                          "USE_EXT" => "N",
                          "DELAY" => "N",
                          "ALLOW_MULTI_SELECT" => "N"
                                ), false
                        );
                        ?>
                    </div>
                </div>
            </div>
            <div class="preims hidden-xs">
                <div class="container">
                    <ul>
                        <li class="preim-1"><a href="/nashi-preimushchestva/#prices">Только честные цены</a></li>
                        <li class="preim-2"><a href="/nashi-preimushchestva/#garantii">Гарантия качества</a></li>
                        <li class="preim-3"><a href="/nashi-preimushchestva/#dostavka">Быстрая профессиональная доставка</a></li>
                        <li class="preim-4"><a href="/nashi-preimushchestva/#servis">Качественная установка и сервис</a></li>
                        <li class="preim-5"><a href="/nashi-preimushchestva/#oplata">Удобные способы оплаты</a></li>
                    </ul>
                </div>
            </div>
        </header>
        