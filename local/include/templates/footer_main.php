<footer>
    <div class="bottom-buttons">
        <!-- line -->
        <div class="line"></div>
        <!-- bottom buttons-->
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <a class="thank main_btn delay_04s" data-toggle="modal" data-target="#thank-popup">
                        <span class="text">
                            <span class="img"></span>
                            Поблагодарить
                        </span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <a class="complain main_btn delay_04s" data-toggle="modal" data-target="#complain-popup">
                        <span class="text">
                            <span class="img"></span>
                            Пожаловаться
                        </span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <a class="advice main_btn delay_04s" data-toggle="modal" data-target="#advice-popup">
                        <span class="text">
                            <span class="img"></span>
                            Посоветовать
                        </span>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <a class="ask main_btn delay_04s" data-toggle="modal" data-target="#ask-popup">
                        <span class="text">
                            <span class="img"></span>
                            Спросить
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-4 general-information">
                    <p><span class="copy">&copy;&nbsp;</span><?= date("Y") ?></p>
                    <br>

                    <p><b><?
                            $APPLICATION->IncludeFile(
                                    SITE_DIR . "include/company-name.php", Array(), Array("MODE" => "html")
                            );
                            ?></b></p>
                    <br>

                    <p>Адрес:</p>

                    <p><?
                            $APPLICATION->IncludeFile(
                                    SITE_DIR . "include/address-minsk.php", Array(), Array("MODE" => "html")
                            );
                            ?>
                    </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4 v2">
                    <div class="mobile-phone-block">
                        <span class="icons">
                            <span class="velcom"></span>
                            <span class="mts"></span>
                            <span class="life"></span>
                        </span>
                        <span class="mobile">7577</span>
                    </div>
                    <div class="clr"></div>
                    <div class="city-phone-block">
                        <span class="city"><span>+375 17</span> 276-75-77</span>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-4">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:main.include", "", Array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => $APPLICATION->GetTemplatePath("/local/include/_footer3.php"),
                        "EDIT_TEMPLATE" => ""
                        )
                    );?>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- popups -->

<!-- rassrochka-popup -->

    <?$APPLICATION->IncludeComponent("giperlink:rassrochka.popup", "", array());?>

<!-- thank-popup -->
<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "footer", array(
        "WEB_FORM_ID" => "1",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "N",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => ""
    ),
    false
);?>
<!-- complain-popup -->
<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "footer", array(
        "WEB_FORM_ID" => "2",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "N",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => ""
    ),
    false
);?>
<!-- advice-popup -->
<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "footer", array(
        "WEB_FORM_ID" => "3",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "N",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => ""
    ),
    false
);?>
<!-- ask-popup -->
<?$APPLICATION->IncludeComponent("bitrix:form.result.new", "footer", array(
        "WEB_FORM_ID" => "4",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "USE_EXTENDED_ERRORS" => "N",
        "SEF_MODE" => "Y",
        "SEF_FOLDER" => "/",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "3600",
        "LIST_URL" => "",
        "EDIT_URL" => "",
        "SUCCESS_URL" => "",
        "CHAIN_ITEM_TEXT" => "",
        "CHAIN_ITEM_LINK" => ""
    ),
    false
);?>
<!-- ok-popup -->
<div class="modal modal-vcenter fade" id="ok-popup">
    <div class="modal-dialog min-popup">
        <div class="modal-content">
            <div class="ok">
                <div class="modal-cont-wrap">
                    <div class="modal-header">
                        <button type="button" class="close delay_04s" data-dismiss="modal" aria-hidden="true"></button>
                        <p>Спасибо, ваша заявка будет обработана в ближайшее время!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- basket-add-popup -->
<div class="modal modal-vcenter fade" id="basket-add-popup">
    <div class="modal-dialog min-popup">
        <div class="modal-content">
            <div class="modal-cont-wrap">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <span class="modal-title"></span>
                </div>
                <div class="modal-body">
                </div>
            </div>
        </div>
    </div>
</div>
 <link href='http://fonts.googleapis.com/css?family=Exo+2:800,900&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,300,700,400italic&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
 <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
 <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css?v=1460533778032">
<?
//$APPLICATION->ShowHeadScripts();

include $_SERVER["DOCUMENT_ROOT"] . "/metricFooter.php";
?>
</body>
</html>