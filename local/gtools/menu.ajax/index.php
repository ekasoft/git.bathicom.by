<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); ?>

<?php $APPLICATION->IncludeComponent('ekasoft.base:menu.sections','.menu.ajax',array(
                        "IBLOCK_TYPE" => "catalog",
                        "IBLOCK_ID" => "10",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "CACHE_TYPE" => "A",
                        "CACHE_TIME" => "36000000",
                        "SECTION_TYPE" => htmlspecialchars($_REQUEST['sectionType']),
                        "SECTION_ID" => intval($_REQUEST['sectionId']),
                        "TEMPLATE" => htmlspecialchars($_REQUEST['templateType']),
                            ), false
                    ); ?>