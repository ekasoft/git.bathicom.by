'use strict';
var timeoutID = setTimeout(console.log('setTimeout'), 0);
$(function() {
    var bodyWidth = $(window).width();
    console.log(bodyWidth);


  $('.product-viewer-wrap .products-preview a').on('click', function(e){
    e.preventDefault();
    var main_img = $('.zoom-img'),
      source = $(this);
    main_img.attr('src', source.attr('data-image'));
    main_img.attr('data-zoom-image', source.attr('data-zoom-image'));
  });

  $('.rating').rating();

    if (bodyWidth > 1030) {
        $('.zoom-img').elevateZoom({
            gallery: "gallery",
            galleryActiveClass: "active"
        });
    };
  $('input.count-input[type="number"]').styler();

  $('.product-description .title').on('click', function(e){
    e.preventDefault();
    var self = $(this),
      description = self.parents('.product-description'),
      text = description.find('.text');
    self.toggleClass('active');
    text.slideToggle();
  });

  $('.related-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
          breakpoint: 769,
          settings: {

              slidesToShow: 2
          }
      },
      {
          breakpoint: 480,
          settings: {
              slidesToShow: 1
          }
      }
    ]
  });

  $('.viewed-slider').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
      responsive: [
          {
              breakpoint: 1100,
              settings: {
                  slidesToShow: 4
              }
          },
          {
              breakpoint: 769,
              settings: {
                  slidesToShow: 2
              }
          },
          {
              breakpoint: 480,
              settings: {
                  slidesToShow: 1
              }
          }
      ]
  });

    $('.slider-colection').slick({
        slide: '.slide-block'
    });

    $('.product-images').slick({
        slide: '.img'
    });

    $('.seoslider').slick({
        slide: 'img'
    });

    $(document).on('click','.buy-one-click',function(){
        $(this).addClass('active');
        return false;
    });

    $(document).on('click','.bt-close-one-click',function(){
        $(this).parents('.buy-one-click').removeClass('active');
        return false;
    });


  $.fn.equalheight = function () {
    Array.max = function (array) {
      return Math.max.apply(Math, array);
    };
    var heights = this.map(function () {
      return $(this).outerHeight();
    }).get();
    return this.outerHeight(Array.max(heights));
  };

    $('.related-slider h5').equalheight();
    $('.related-slider .product-item').equalheight();
    $('.viewed-slider h5').equalheight();
    $('.viewed-slider .product-item').equalheight();


    $('.masktel').mask("+375 (99) 999-99-99");
    /*
    if (bodyWidth > 1248) {
    $('.js-top-menu-element a.a-url').mouseenter(function(){
        bodyWidth = $('body').width();
        var that = $(this);
        clearTimeout(timeoutID);
        timeoutID = setTimeout(function(){
            $('.header-menu-top').hide();
            $('.main-buttons-box a.a-url').removeClass('active');
            that.addClass('active');
            var blockShow = that.attr('href'),
                topPos = that.parents('.js-top-menu-element').offset().top+that.parents('.js-top-menu-element').height();
            $(blockShow).show().css('top',topPos);
            $(blockShow).css('top',topPos);
            $('.popup-menu-bg').show();
            if (bodyWidth > 1180) {
                var thet = that;
                if (that.parents('.js-top-menu-element').find('.jq-showmenublock').length > 0) {
                    $.ajax({
                            url: './menu-l1.html',
                            dataType: 'html',
                        })
                        .done(function (data) {
                            //console.log("success");
                            thet.parents('.js-top-menu-element').find('.jq-showmenublock').html(data);
                        })
                        .fail(function () {
                            //console.log("error");
                        })
                        .always(function () {
                            //console.log("complete");
                        });
                }
                if (that.parents('.js-top-menu-element').find('.js-hit-towarov').length > 0) {
                    $.ajax({
                            url: './menu-h1.html',
                            dataType: 'html',
                        })
                        .done(function (data) {
                            //console.log("success");
                            thet.parents('.js-top-menu-element').find('.js-hit-towarov').html(data);
                        })
                        .fail(function () {
                            //console.log("error");
                        })
                        .always(function () {
                            //console.log("complete");
                        });
                }
            }
        },300);
        //return false;
    });
    $('.header-menu-top, .js-top-menu-element').mouseleave(function(){
        clearTimeout(timeoutID);

        $('.main-buttons-box a').removeClass('active');
        $('.header-menu-top').hide();
        $('.popup-menu-bg').hide();
    });

    $('.popup-menu-bg').mouseenter(function(){
        clearTimeout(timeoutID);
        $('.main-buttons-box a').removeClass('active');
        $('.header-menu-top').hide();
        $('.popup-menu-bg').hide();
    });

    $('.menu-first-level li a').on('mouseenter',function(){
        $(this).parents('.menu-first-level').find('li').removeClass('active');
        $(this).parents('li').addClass('active');
        var urlHref = $(this).prop('href');
        $.ajax({
            url: urlHref,
            dataType: 'html',
        })
        .done(function(data) {
            console.log("success");
            $('.menu-second-level').html(data);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

        if (bodyWidth > 1180) {
            $.ajax({
                    url: './menu-l1.html',
                    dataType: 'html'
                })
                .done(function (data) {
                    //console.log("success");
                    $('.jq-showmenublock').html(data);
                })
                .fail(function () {
                    //console.log("error");
                })
                .always(function () {
                    //console.log("complete");
                });
        }
        return false;
    });

    $(document).on('mouseenter','.menu-first-level2 li a',function(){
        $(this).parents('.menu-first-level2').find('li').removeClass('active');
        $(this).parents('li').addClass('active');
        var urlHref = $(this).prop('href');

        $.ajax({
            url: urlHref,
            dataType: 'html'
        })
        .done(function(data) {
            console.log("success");
            $('.js-hit-towarov').html(data);
            if (bodyWidth < 1180) {
                $('.js-hit-towarov2').remove();
            }
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

        return false;
    });


        $(document).on('mouseenter', '.menu-second-level li a', function () {
            $(this).parents('.menu-second-level').find('li').removeClass('active');
            $(this).parents('li').addClass('active');
            var urlHref = $(this).prop('href');
            $.ajax({
                    url: urlHref,
                    dataType: 'html',
                })
                .done(function (data) {
                    console.log("success");
                    $('.jq-showmenublock').html(data);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });

            return false;
        });


        $(document).on('mouseenter', '.menu-second-level2 li a', function () {
            $(this).parents('.menu-second-level2').find('li').removeClass('active');
            $(this).parents('li').addClass('active');
            var urlHref = $(this).prop('href');
            $.ajax({
                    url: urlHref,
                    dataType: 'html'
                })
                .done(function (data) {
                    console.log("success");
                    $('.js-hit-towarov2').html(data);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
            return false;
        });

        $(document).on('click', '.slider-colection .item-slid a', function (event) {
            var urlHref = $(this).prop('href');
            $.ajax({
                    url: urlHref,
                    dataType: 'html',
                })
                .done(function (data) {
                    console.log("success");
                    $('.jq-showmenublock').html(data);
                    var urlattr = $('.menu-second-level li.active a').attr('href');
                    $('.jq-showmenublock a.close-bt').attr('href', urlattr);
                    console.log('find a ' + urlattr + ' ');
                    console.log($('.jq-showmenublock a.close-bt').attr('href'));
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
            return false;
        });

        $(document).on('click', '.jq-showmenublock a.close-bt', function (event) {
            console.log('close click');
            var urlHref = $(this).prop('href');
            $.ajax({
                    url: urlHref,
                    dataType: 'html',
                })
                .done(function (data) {
                    console.log("success");
                    $('.jq-showmenublock').html(data);
                })
                .fail(function () {
                    console.log("error");
                })
                .always(function () {
                    console.log("complete");
                });
            return false;
        });
    }
*/
    $(document).on('mouseenter','.colections-list .colections-item', function(){
        $(this).find('.colections-item-slider').slick({
            slide: '.slide-item'
        });
    });

    $(document).on('mouseenter','.colections-item-slider .colections-item-slider-item', function(){
        var thet = $(this),
            jsBlock = $(this).parents('.colections-item-hover').find('.js-left-part'),
            htmlCode = thet.find('.showtovar').html();
        if (thet.find('.showtovar').length > 0) {
            jsBlock.html(htmlCode);
        } else {
           console.log('error');
        }
        
        // var urlHref = $(this).prop('href');
        // var that = $(this);
        // $.ajax({
        //         url: urlHref,
        //         dataType: 'html'
        //     })
        //     .done(function(data) {
        //         console.log("success");
        //         that.parents('.colections-item-hover').find('.js-left-part').html(data);
        //         that.parents('.colections-item-hover').find('.rating').rating();
        //     })
        //     .fail(function() {
        //         console.log("error");
        //     })
        //     .always(function() {
        //         console.log("complete");
        //     });
        return false;
    });

    $(document).on('submit','#searchform',function(event){
        event.preventDefault();
        $('.search-result-block').show();
        var heigthmain = $('.search-result-block').height()-150;
        $('.search-result-block .products-list.products_list-list').height(heigthmain);
        $('.popup-search-bg').show();
        return false;
    });

    $(document).on('click','.popup-search-bg, .search-result-block .cancel-bt',function(event){
        event.preventDefault();
        $('.search-result-block').hide();
        $('.popup-search-bg').hide();
        return false;
    });

    var map;
    function initMap() {
        var customMapType = new google.maps.StyledMapType([{"featureType":"all","elementType":"geometry","stylers":[{"color":"#d7ecf1"}]},{"featureType":"all","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"gamma":0.01},{"lightness":20}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"saturation":-31},{"lightness":-33},{"weight":2},{"gamma":0.8}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":30},{"saturation":30}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":20}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"lightness":20},{"saturation":-20}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":10},{"saturation":-30}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"saturation":25},{"lightness":25}]},{"featureType":"water","elementType":"all","stylers":[{"lightness":-20}]}], {
            name: 'Custom Style'
        });
        var customMapTypeId = 'custom_style',
            myLatLng = {lat: 53.9463152, lng: 27.6860694};
 

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            scrollwheel: false,
            center: myLatLng,  // Brooklyn.
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
            }
        });
        var image = 'images/gogle-map.png';
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: image,
            title: 'Hello World!'
        });

        map.mapTypes.set(customMapTypeId, customMapType);
        map.setMapTypeId(customMapTypeId);
    };

    initMap();

    $('#contact-form').validate({
        rules:{
            name:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            email:{
                required: true,
                minlength: 2,
                maxlength: 64,
                email: true
            },
            messag: {
                required: true,
                minlength: 20,
            }
        },
        messages: {
            name: {
                required: "неверно",
                minlength: "неверно",
                maxlength: "неверно"
            },
            email: {
                required: "неверно",
                minlength: "неверно",
                maxlength: "неверно",
                email: "неверно",
            },
            messag: {
                required: "неверно",
                minlength: "неверно"
            }
        }

    })
});

$(window).load(function(){
    var bodyWidth = $('body').width();
    if (bodyWidth > 1030) {
        var leftCatH = $('#left-col-cat').height(),
            rightCatH = $('#right-col-cat').height();
        if (leftCatH < rightCatH) {
            $('#left-col-cat').css('min-height',rightCatH);
        } else {
            $('#right-col-cat').css('min-height',leftCatH);
        }
    }
});

$(window).resize(function(){
    var bodyWidth = $('body').width();
    if (bodyWidth > 1030) {
        var leftCatH = $('#left-col-cat').height(),
            rightCatH = $('#right-col-cat').height();
        if (leftCatH < rightCatH) {
            $('#left-col-cat').css('min-height',rightCatH);
        } else {
            $('#right-col-cat').css('min-height',leftCatH);
        }
    }
});