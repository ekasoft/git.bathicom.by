'use strict';
var timeoutID = setTimeout(console.log('setTimeout'), 0);
$(function() {
    var bodyWidth = $(window).width();
    console.log(bodyWidth);



  var selectE = $('.select-e');
  if (selectE.length){
      var showPopupCls = 'show-popup';

      selectE.each(function(index){
          var self = $(this);
          var popup = self.find('.select-e__popup');
          popup.attr('data-index', index);
      });

      selectE.on('click', 'a.show-popup-btn', function(e){
          e.preventDefault();
          var self = $(this);
          var popup = self.next('.select-e__popup');
          if (popup.length){
              var index = popup.attr('data-index');

              $('.select-e__popup').each(function(ind, item){
                  var itemself = $(item);
                  var indexPopup = itemself.attr('data-index');
                  if (indexPopup != index){
                      itemself.removeClass(showPopupCls);
                  }
              });

              popup.toggleClass(showPopupCls);
          }
      });

      $(document).on('click', function(e){
          var target = $(e.target);
          if (target.is('.select-e__popup') || target.closest('.select-e__popup').length || target.is('a.show-popup-btn') || target.closest('a.show-popup-btn').length) return;
          $('.select-e__popup').removeClass(showPopupCls);
      });
  }


  $('.product-viewer-wrap .products-preview a').on('click', function(e){
    e.preventDefault();
    var main_img = $('.zoom-img'),
      source = $(this);
    main_img.attr('src', source.attr('data-image'));
    main_img.attr('data-zoom-image', source.attr('data-zoom-image'));
    if(source.hasClass('video')){
        $('div.zoomContainer').hide();
        $('div.mark').hide();
        $('.prod-photo a').hide();
       // $('.prod-photo a.num-'+source.attr('data-num')).attr("href", source.attr('data-href'));
        $('.prod-photo a.num-'+source.attr('data-num')).show();
        $('.prod-photo').css({'height' : '363px'});
        $('a.cloud-zoom').hide();
        $('.prod-photo a img').css({'margin-top' : '46px'});
        $('div.big-vid').show();
    }else{
        $('div.big-vid').hide();
        $('.prod-photo a img').css({'margin-top' : '0px'});
        $('div.zoomContainer').show();
        $('div.show').hide();
    }
  });
  
    jQuery("[rel='fancybox-video']").fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        nextEffect  : 'none',
        prevEffect  : 'none',
        padding : 0,
        helpers : {
            thumbs : false,
            media : {},
            overlay: {
                locked: false
            }
        }
    });

  $('.rating').rating();

    if (bodyWidth > 1030) {
        if ($.fn.elevateZoom){
            $('.zoom-img').elevateZoom({
                gallery: "gallery",
                galleryActiveClass: "active",
                zoomWindowWidth: 358,
                zoomWindowHeight: 358
            });
        }

        if ($.fn.CloudZoom){
            $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
        }
    }
    $('.cloud-zoom').on('click', function(e){
        e.preventDefault();
    });
    
  $('input.count-input[type="number"]').styler();

  $('.product-description .title').on('click', function(e){
    e.preventDefault();
    var self = $(this),
      description = self.parents('.product-description'),
      text = description.find('.text');
    self.toggleClass('active');
    text.slideToggle();
  });

  $('.related-slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
          breakpoint: 769,
          settings: {

              slidesToShow: 2
          }
      },
      {
          breakpoint: 480,
          settings: {
              slidesToShow: 1
          }
      }
    ]
  });

  $('.viewed-slider').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
      responsive: [
          {
              breakpoint: 1100,
              settings: {
                  slidesToShow: 4
              }
          },
          {
              breakpoint: 769,
              settings: {
                  slidesToShow: 2
              }
          },
          {
              breakpoint: 480,
              settings: {
                  slidesToShow: 1
              }
          }
      ]
  });

    // $('.slider-colection').slick({
    //     slide: '.slide-block'
    // });

    $('.product-images').slick({
        slide: '.img'
    });

    $('.seoslider').slick({
        slide: 'img'
    });

    $(document).on('click','.buy-one-click',function(){
        $(this).addClass('active');
        return false;
    });

    $(document).on('click','.bt-close-one-click',function(){
        $(this).parents('.buy-one-click').removeClass('active');
        return false;
    });


  $.fn.equalheight = function () {
    Array.max = function (array) {
      return Math.max.apply(Math, array);
    };
    var heights = this.map(function () {
      return $(this).outerHeight();
    }).get();
    return this.outerHeight(Array.max(heights));
  };

    $('.related-slider h5').equalheight();
    $('.related-slider .product-item').equalheight();
    $('.viewed-slider h5').equalheight();
    $('.viewed-slider .product-item').equalheight();


    $('.masktel').mask("+375 (99) 999-99-99");
    if (bodyWidth > 1248) {
    $('.js-top-menu-element a.a-url').mouseenter(function(){
        var windowWidth = $(window).width();
        if (windowWidth <= 1280) return;

        bodyWidth = $('body').width();
        var that = $(this);
        clearTimeout(timeoutID);
        timeoutID = setTimeout(function(){
            $('.header-menu-top').hide();
            $('.main-buttons-box a.a-url').removeClass('active');
            that.addClass('active');

            var blockShow = that.attr('data-href'),
                topPos = that.parents('.js-top-menu-element').offset().top+that.parents('.js-top-menu-element').height();
                topPos -= 93;
            if( $("#bx-panel").length ) {
                topPos -= 40;
            }
            $(blockShow).show().css('top',topPos);
            $(blockShow).css('top',topPos);
            $('.popup-menu-bg').show();
            if (that.parents('.js-top-menu-element').find('.jq-showmenublock').length > 0) {
                /*
                that.parents('.js-top-menu-element').find('.jq-showmenublock .slider-colection').slick({
                    slide: '.slide-block'
                });
                */
            }
            if (that.parents('.js-top-menu-element').find('.js-hit-towarov').length > 0) {

            }

        },300);
    });
    
    $('.js-top-menu-element-ajax a.a-url').click(function(e){
        e.preventDefault();
        var windowWidth = $(window).width();
        if (windowWidth <= 1280) return;
       
        bodyWidth = $('body').width();
        var that = $(this);
        var sectionType = jQuery(this).attr("data-type");
        var sectionId = jQuery(this).attr("data-id");
        var templateType = jQuery(this).attr("data-temp");
        //jQuery('.header-menu-top .container').html('');
        var beforeHtml = jQuery('.header-menu-top #show-menu'+sectionId+' .row');
        if(beforeHtml.length == 0){ 
            askMenu(sectionType, sectionId, templateType);
        }

            $('.header-menu-top').hide();
            $('.main-buttons-box a.a-url').removeClass('active');
            that.addClass('active');
            var blockShow = that.attr('data-href'),
                topPos = that.parents('.js-top-menu-element-ajax').offset().top+that.parents('.js-top-menu-element-ajax').height();
                 topPos -= 93;
            if( $("#bx-panel").length ) {
                topPos -= 40;
            }
            $(blockShow).show().css('top',topPos);
            $(blockShow).css('top',topPos);
            $('.popup-menu-bg').show();
            if (that.parents('.js-top-menu-element-ajax').find('.jq-showmenublock').length > 0) {
                /*
                that.parents('.js-top-menu-element-ajax').find('.jq-showmenublock .slider-colection').slick({
                    slide: '.slide-block'
                });
                */
            }
            if (that.parents('.js-top-menu-element-ajax').find('.js-hit-towarov').length > 0) {

            }
       
    });
    
    
    

    $('.header-menu-top, .js-top-menu-element').mouseleave(function(){
        clearTimeout(timeoutID);

        $('.main-buttons-box a').removeClass('active');
        $('.header-menu-top').hide();
        $('.popup-menu-bg').hide();

        $('.infoCollection').hide();
        $('.popuplarCollection').show();
    });

    $('.popup-menu-bg').mouseenter(function(){
        clearTimeout(timeoutID);
        $('.main-buttons-box a').removeClass('active');
        $('.header-menu-top').hide();
        $('.popup-menu-bg').hide();
    });


        $('.menu-first-level li a').on('mouseenter',function(){
            $(this).parents('.menu-first-level').find('li').removeClass('active');
            $(this).parents('li').addClass('active');
            var htmlInnerMenu = $(this).parents('li').find('.topmenu-innermenu').html(),
                thatMenu = $(this).parents('.header-menu-top').find('.menu-second-level');
            thatMenu.html('');
            thatMenu.html(htmlInnerMenu);
            return false;
        });

        $(document).on('mouseenter','.menu-first-level2 li a',function(){
            $(this).parents('.menu-first-level2').find('li').removeClass('active');
            $(this).parents('li').addClass('active');
            var scode = jQuery(this).attr('data-code');
            var htmlInnerMenu = $(this).parents('li').find('.topmenu-innermenu').html(),
                //thatMenu = $(this).parents('.header-menu-top').find('.js-hit-towarov');
                thatMenu = $(this).parents('.header-menu-top').find('.subs');
                $(this).parents('.header-menu-top').find('.subs').css('display','inline-block');
                $(this).parents('.header-menu-top').find('.js-hit-towarov').addClass('hover');
                $(this).parents('.header-menu-top').find('.js-hit-towarov').find('.product-item').hide();
                if($(this).parents('.header-menu-top').find('.js-hit-towarov').find('.product-item.'+scode).length) {
                    $(this).parents('.header-menu-top').find('.js-hit-towarov').find('.product-item.'+scode).show();
                    $(this).parents('.header-menu-top').find('.js-hit-towarov').show();
                } else {
                    $(this).parents('.header-menu-top').find('.js-hit-towarov').hide();
                }
            thatMenu.html('');
            thatMenu.html(htmlInnerMenu);
            return false;
        });

        $(document).on('mouseenter', '.menu-second-level li a', function () {
            $(this).parents('.menu-second-level').find('li').removeClass('active');
            $(this).parents('li').addClass('active');
            // var urlHref = $(this).prop('href');
            var htmlInnerMenu = $(this).parents('li').find('.topmenu-innermenu2').html(),
                thatMenu = $(this).parents('.header-menu-top').find('.jq-showmenublock');
            thatMenu.html('');
            thatMenu.html(htmlInnerMenu);
            $(this).parents('.header-menu-top').find('.jq-showmenublock .slider-colection').slick({
                slide: '.slide-block'
            });

            return false;
        });
        
        $('div.product-item div.img-wrap').mouseenter(function(){
            $(this).children('div.content-task').animate({ bottom:"0"}, 100);
            $(this).children('div.mark').hide();
            
        });
        
        $('div.product-item div.img-wrap').mouseleave(function(){
            $(this).children('div.content-task').animate({ bottom:"100%"}, 100);
            $(this).children('div.mark').show();
        });
            
        $(document).on('mouseenter', '.menu-second-level2 li a', function () {
            $(this).parents('.menu-second-level2').find('li').removeClass('active');
            $(this).parents('li').addClass('active');
            var htmlInnerMenu = $(this).parents('li').find('.topmenu-innermenu2').html(),
                thatMenu = $(this).parents('.header-menu-top').find('.js-hit-towarov2');
            thatMenu.html('');
            thatMenu.html(htmlInnerMenu);
            return false;

            return false;
        });

        $(document).on('click', '.jq-showmenublock .slider-colection .item-slid a', function (event) {
            var htmlInnerMenu = $(this).parents('.item-slid').find('.topmenu-innermenu3').html(),
                thatMenu = $(this).parents('.header-menu-top').find('.infoCollection');

            $('.popuplarCollection').hide();
            $('.infoCollection').show();

            thatMenu.html('');
            thatMenu.html(htmlInnerMenu);

            return false;
        });

        $(document).on('click', '.jq-showmenublock a.close-bt', function (event) {

            $('.infoCollection').hide();
            $('.popuplarCollection').show();

            return false;
        });

    }
    $(document).on('mouseenter','.colections-list .colections-item', function(){
        $(this).find('.colections-item-slider').slick({
            slide: '.slide-item'
        });
    });

    $(document).on('mouseenter','.colections-item-slider .colections-item-slider-item', function(){
        var thet = $(this),
            itemHover = $(this).parents('.colections-item-hover'),
            linkCloseCollection = itemHover.find('.collection-item-new-close-btn'),
            jsBlock = itemHover.find('.js-left-part'),
            htmlCode = thet.find('.showtovar').html();

        linkCloseCollection.show();

        if (!jsBlock.data('default-html')){
            jsBlock.data('default-html', jsBlock.html());
        }

        if (thet.find('.showtovar').length > 0) {
            jsBlock.html(htmlCode);
        } else {
           console.log('error');
        }

        // var urlHref = $(this).prop('href');
        // var that = $(this);
        // $.ajax({
        //         url: urlHref,
        //         dataType: 'html'
        //     })
        //     .done(function(data) {
        //         console.log("success");
        //         that.parents('.colections-item-hover').find('.js-left-part').html(data);
        //         that.parents('.colections-item-hover').find('.rating').rating();
        //     })
        //     .fail(function() {
        //         console.log("error");
        //     })
        //     .always(function() {
        //         console.log("complete");
        //     });
        return false;
    });

    $(document).on('mouseleave', '.colections-item-hover', function(){
        var self = $(this),
            linkCloseCollection = self.find('.collection-item-new-close-btn'),
            jsBlock = self.find('.js-left-part');

        linkCloseCollection.hide();

        collectionDefaultHtml(jsBlock);
    });

    $(document).on('click', '.collection-item-new-close-btn', function(e){
        e.preventDefault();

        var self = $(this);
        var jsBlock = $(this).parents('.colections-item-hover').find('.js-left-part');

        self.hide();

        collectionDefaultHtml(jsBlock);
    });

/*
    $(document).on('click','.popup-search-bg, .search-result-block .cancel-bt',function(event){
      
        event.preventDefault();
        var input = $('#search-input');
        input.val('');

        
        $('.search-result-block').remove();
        $('.popup-search-bg').hide();
        
        return false;
        
    });
    */

    $(document).on('click', '.popup-search-bg, .js-search-close-popup', function(e){
        e.preventDefault();
        $('.search-result-block').remove();
        $('.popup-search-bg').hide();
    });

    var map;
    function initMap() {
        var customMapType = new google.maps.StyledMapType([{"featureType":"all","elementType":"geometry","stylers":[{"color":"#d7ecf1"}]},{"featureType":"all","elementType":"labels","stylers":[{"visibility":"simplified"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"gamma":0.01},{"lightness":20}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"saturation":-31},{"lightness":-33},{"weight":2},{"gamma":0.8}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":30},{"saturation":30}]},{"featureType":"poi","elementType":"geometry","stylers":[{"saturation":20}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"lightness":20},{"saturation":-20}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":10},{"saturation":-30}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"saturation":25},{"lightness":25}]},{"featureType":"water","elementType":"all","stylers":[{"lightness":-20}]}], {
            name: 'Custom Style'
        });
        var customMapTypeId = 'custom_style',
            myLatLng = {lat: 53.9463152, lng: 27.6860694};


        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            scrollwheel: false,
            center: myLatLng,  // Brooklyn.
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, customMapTypeId]
            }
        });
        var image = 'images/gogle-map.png';
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: image,
            title: 'Hello World!'
        });

        map.mapTypes.set(customMapTypeId, customMapType);
        map.setMapTypeId(customMapTypeId);
    };

    if( $("#map").length ) {
        initMap();
    }

    $('#contact-form').validate({
        rules:{
            name:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            email:{
                required: true,
                minlength: 2,
                maxlength: 64,
                email: true
            },
            messag: {
                required: true,
                minlength: 20,
            }
        },
        messages: {
            name: {
                required: "неверно",
                minlength: "неверно",
                maxlength: "неверно"
            },
            email: {
                required: "неверно",
                minlength: "неверно",
                maxlength: "неверно",
                email: "неверно",
            },
            messag: {
                required: "неверно",
                minlength: "неверно"
            }
        }

    })
});

$(window).load(function(){
    var bodyWidth = $('body').width();
    if (bodyWidth > 1030) {
        var leftCatH = $('#left-col-cat').height(),
            rightCatH = $('#right-col-cat').height();
            console.log(leftCatH);
            console.log(rightCatH);
        if (leftCatH < rightCatH) {
            $('#left-col-cat').css('min-height',rightCatH);
        } else {
            $('#right-col-cat').css('min-height',leftCatH);
        }
    }
});

$(window).resize(function(){
    var bodyWidth = $('body').width();
    if (bodyWidth > 1030) {
        var leftCatH = $('#left-col-cat').height(),
            rightCatH = $('#right-col-cat').height();
        if (leftCatH < rightCatH) {
            $('#left-col-cat').css('min-height',rightCatH);
        } else {
            $('#right-col-cat').css('min-height',leftCatH);
        }
    }
});

$( window ).load(function() {
    if($('#gallery ul li a').length && $('.video').length<2) {
        $('#gallery ul li:first-child a').trigger('click');
    }else{
        var tube = $('.video').length;
        $('#gallery ul li:nth-child('+tube+') a').trigger('click');
    }
});

function collectionDefaultHtml(block){
    if (block.data('default-html')){
        block.html(block.data('default-html'));
    }
}

function setSelectionRange(input, selectionStart, selectionEnd) {
    if (input.setSelectionRange) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    }
    else if (input.createTextRange) {
        var range = input.createTextRange();
        range.collapse(true);
        range.moveEnd('character', selectionEnd);
        range.moveStart('character', selectionStart);
        range.select();
    }
}

function setCaretToPos (input, pos) {
    setSelectionRange(input, pos, pos);
}

function askMenu(sectionType, sectionId, templateType) {
    var hostname = window.location.hostname;
    var request = $.ajax({
        url: "http://"+hostname+"/local/gtools/menu.ajax/",
        type: "POST",
        global: false,
        data: { sectionType: sectionType, sectionId: sectionId, templateType: templateType},
        //dataType: "json"
    });

    request.done(function(msg) {
        var cartobj = jQuery.parseJSON(msg);
        console.log(cartobj);
        var beforeHtml = jQuery('.header-menu-top #show-menu'+sectionId+' .row');
        jQuery('.header-menu-top #show-menu'+sectionId).html(cartobj.HTML);

        
    });

    request.fail(function( jqXHR, textStatus ) {
        console.log( "Request failed: " + textStatus );
    });
};


function initSearchPopup($obj) {
    var $obj = $obj || $("body");

    var $searchForm = $(".search-form", $obj)
//    var timer = 0;
//    var searchProgress = false;

//    var doSearch = function() {
//        if (searchProgress) return;
//
//        searchProgress = true;
//        $searchForm.submit();
//        $("body").one("onAjaxReload", function() {
//            searchProgress = false;
//        });
//    }

    $('.popup-search-bg').show();
//    $("#search-input", $obj).on("keyup", function() {
//        clearTimeout(timer);
//        if ($(this).val().length > 2) {
//            setTimeout(doSearch, 1000);
//        }
//    });
    $("#search-input", $obj).focus();
    setCaretToPos($("#search-input", $obj)[0], $("#search-input", $obj).val().length);
}
