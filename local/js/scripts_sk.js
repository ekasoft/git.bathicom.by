$(document).ready(function () {
    if ($('[name="ADDRESS_NAME"]').length > 0){
        var ADDRESS_NAME = $('[name="ADDRESS_NAME"]').val();
        $(document).delegate('[name="'+ADDRESS_NAME+'"]', 'change', function(){
            var data_address_id = $(this).find("option:selected").data('id');
            $('.address-tab').hide();
            $('.address-tab[data-address-id="'+data_address_id+'"]').show();
        });
    }

    $('#index-page .mosaic-button-row a').on('click',function(e){
        e.preventDefault();
        $('#index-page .main-buttons-box .more').slideToggle().css({'display': 'inline-block'});
        $(window).resize();
    });

    $('.modal#ok-popup').on('hidden.bs.modal', function () {
        location.href = removeURLParameter(location.href, 'formresult');
    });

    function removeURLParameter(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts= url.split('?');
        if (urlparts.length>=2) {

            var prefix= encodeURIComponent(parameter)+'=';
            var pars= urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i= pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url= urlparts[0]+'?'+pars.join('&');
            return url;
        } else {
            return url;
        }
    }
});

