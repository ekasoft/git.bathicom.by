jQuery(document).ready(function(){
    jQuery('header.v2 .mosaic-row .search-icon').click(function() {
        jQuery('header.v2 .min-search-block').toggleClass('active');
    });
    jQuery('#order_form_content').on('click','[name="PAY_SYSTEM_ID"]',function(){
        var id = jQuery(this).val();
        if(id == 10) {
            jQuery('#ORDER_FORM').attr('action','/order/make/?gopay=Y');
        } else {
            jQuery('#ORDER_FORM').attr('action','/order/make/');
        }
        //console.log(id);
    });
    
    jQuery('.bx-filter .more .variants').on('click',function(){
        jQuery(this).next('.items').toggleClass('active');
    });
    
    jQuery('body').on('click',function(e){
        var cls = e.target.className;
        //console.log(cls);
        if (cls != 'variants' && cls != 'items active' && cls != 'col' && cls != '' && cls != 'bx-filter-param-text'){
           jQuery('.bx-filter .items').removeClass('active'); 
           //console.log(777);
        }
    });
});