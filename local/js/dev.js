function Progress(_obj) {
    this.obj = _obj;
    this.classProgress = 'opacityAjax';

    this.start = function () {
        $(this.obj).addClass(this.classProgress);
    }

    this.end = function () {
        $(this.obj).removeClass(this.classProgress)
    }
}

$(document).ready(function () {
    $(document).on('click', '.changeMeasure', function () {
        if ($(this).hasClass("active")) {
            return true;
        }

        $(this).addClass("active");
        var units = $(this).closest('.units');
        var measure = $(this).attr('data-measure');
        var form = $(this).closest('form');

        $('.' + measure, units).removeClass("active");

        var conf = parseFloat($(units).attr('data-cof'));
        var quantity = parseInt($('.count-input', form).val());
        var m2 = $('.text-count-input', form).val();
        if ($(this).hasClass('j_m2')) {
            var _m2 = quantity * conf;
            $('.text-count-input', form).val(_m2);
            $('.jq-number', form).addClass('hide');
            $('.text-count-input', form).removeClass('hide');
        } else if ($(this).hasClass('j_sht')) {
            var _m2 = Math.ceil(m2 / conf);
            $('.count-input', form).val(_m2);
            $('.jq-number', form).removeClass('hide');
            $('.text-count-input', form).addClass('hide');
        }
    })

    $(document).on('change', '.text-count-input', function () {
        var form = $(this).closest('form');
        var m2 = $(this).val();
        var units = $('.units', form);
        var conf = parseFloat($(units).attr('data-cof'));
        var _m2 = Math.ceil(m2 / conf);

        $('.count-input', form).val(_m2);
    })

    $(document).on("click", ".jsAddBasket", function () {
        var form = $(this).closest('form');
        var quantity = parseInt($('input[name="QUANTITY"]', form).val());
        var productHtml = $(this).closest('.product-item');
        var self = this;

        if (quantity <= 0 || typeof (quantity) != "number") {
            $('.count-input', form).val(1);
            return false;
        }

        if ($(this).closest('.jsDetailElement').length) {
            var callback = function (data) {
                updateBasket(quantity);
                if (quantity > 1) {
                    $('.count-input', form).val(1);
                }
                if (!$('.prod-photo', productHtml).find('.green').length) {
                    $('.prod-photo', productHtml).append('<div class="mark green">Товар в корзине</div>');
                }
            }
        } else {
            var callback = function (data) {
                updateItem(productHtml, data);
                updateBasket(quantity);
                updateBlock(productHtml);
            }
        }

        submitForm(productHtml, form, callback);
    });

    $(document).on('click', '.jsBuyOneClick', function () {
        var obj = $(this).closest('.insertAjax');
        var form = $(this).closest('form');
        var vals = $('input[name="ONE_CLICK[PHONE]"]', form).val();
        var btnOneClick = $(this).closest('.buy-one-click');

        if (vals.length == 0) {
            return false;
        }

        callback = function (data) {
            var _data = $('.insertAjax', data).html();
            btnOneClick.removeClass('active');
            updateItem(obj, _data);
        }

        submitForm(obj, form, callback);

        return false;
    });

    $(document).on('submit', 'form.smartfilter', function () {
        //var form = $(this);
        //var obj = $(this).closest('.insertAjax');
        var progress = new Progress(obj);
        progress.start();
//        var callback = function (data) {
//  //          var _data = $(data).find('.insertAjax');
//  //          updateItem(obj, _data);
//  //          updateBlock(obj);
//        }
//
//        submitForm(obj, form, callback);
//        return false;
    })

    $(document).on("change", ".jsSort", function () {
        var form = $(this).closest('form');
        var obj = $(this).closest('.jsAjaxCatalog');

        var callback = function (data) {
            //console.log(data);
            //var _data = $('.jsAjaxCatalog', data).html();
            var _data = $(data).filter('.jsAjaxCatalog').html();
            //console.log(_data);
            updateCatalog(obj, _data);
            updateBlock(obj);
        }
        submitForm(obj, form, callback);
    })

    $(document).on('click', '.jsTemplate', function () {
        if ($(this).hasClass('active')) {
            return false;
        }
        var self = this;
        var obj = $(this).closest('.jsAjaxCatalog');
        $(obj).addClass('opacityAjax');
        var callback = function (data) {
            //var _data = $('.jsAjaxCatalog', data).html();
            var _data = $(data).filter('.jsAjaxCatalog').html();
            updateCatalog(obj, _data);
            updateBlock(obj);
        }
        $.ajax({
            url: location.href,
            dataType: "html",
            type: 'post',
            data: {ajax: {template: $(self).attr('data-template')}},
            success: callback,
        }).done(function (data) {
            $(obj).removeClass('opacityAjax');
        });
    })

    $('.categories-section-e').on('click', '.instoke .checkbox-e input', function () {
        var pathname = window.location.pathname; // Returns path only 
        //var url = window.location.href; // Returns full UR
        //console.log(pathname);
        //console.log(url);
        if ($(this).prop('checked')) {
            //$('input[name="STORE"]').val('Y');
            pathname += '?STORE=Y';
        } else {
            //$('input[name="STORE"]').val('N');
            pathname += '?STORE=N';
        }

        //$('#set_filter').trigger('click');
        window.location.href = pathname;
        return true;
    });
    /*
    $(document).on('change', 'form.smartfilter input', function () {
        $(this).closest('form').find('#set_filter').trigger('click');
    });*/

    $(document).on('change', '.count-input', function () {
        var max = parseInt($(this).attr('max'));
        var val = parseInt($(this).val());
        if (val > max) {
            $(this).val(max);
        }

        return true;
    })

});


function submitForm(obj, form, callback) {

    if (callback === undefined) {
        callback = function (data) {
            console.log('void');
        };
    }
    var progress = new Progress(obj);
    progress.start();
    $.ajax({
        url: $(form).attr('action'),
        dataType: "html",
        type: $(form).attr('method'),
        data: $(form).serialize(),
        success: callback,
    }).done(function (data) {
        //console.log(data);
        progress.end();
    });
}


function updateBlock(productHtml) {
    if (('input.count-input[type="number"]', productHtml).length) {
        $('input.count-input[type="number"]', productHtml).styler();
    }
    if ($('.masktel', productHtml).length) {
        $('.masktel', productHtml).mask("+375 (99) 999-99-99");
    }
}

function updateItem(productHtml, data) {
    $(productHtml).html($(data).html());
}

function updateCatalog(obj, data) {
    $(obj).html(data);
}

function updateBasket(quantity) {
    var countBasket = parseInt($('#idBasket').text());
    if (quantity <= 0 || typeof (quantity) != "number" || isNaN(quantity)) {
        quantity = 1;
    }
    var newCount = countBasket + quantity;
    $('#idBasket').text(newCount);
}
