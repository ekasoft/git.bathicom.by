$(document).ready(function(){
/*custom-select*/
    //$('.selectpicker').selectpicker();
/* === CAROUFREDSEL === */

    if($('.main-slider').length) {
        $('.main-slider').carouFredSel({
            responsive: true,
            width: '100%',
            height: 'variable',
            scroll : {
                items: 1,
                pauseOnHover: true
            },
            prev: {
                button:'#slide-prev',
                key:'left'
            },
            next: {
                button:'#slide-next',
                key:'right'
            },
            circular: true,
                infinite: true,
                mousewheel: false,
                swipe: {
                onTouch:true,
                //onMouse:true
            },
            transition: true,
            items: {
                visible: 1
            },
            auto: {
                //play: false,
                pauseOnHover: true,
                timeoutDuration: 5000,
                duration: 1000
            }
        });
    }
    
    if(jQuery('.rassrochka-input').prop("checked")){
        console.log('it is');
        console.log(jQuery('.rassrochka-input:checked').parents('.one-product').children('div span.choose-rass').attr('id'));
        jQuery('.rassrochka-input:checked').parents('.one-product').children('div span.choose-rass').show();
    }else{
        console.log('it is not');
    }
    
    $('#add-product-slider').carouFredSel({
        responsive: true,
        width: '100%',
        height: 'variable',
        align: 'center',
        scroll : {
            items: 1
        },
        prev: {
            button:'#add-product-slide-prev',
            key:'left'
        },
        next: {
            button:'#add-product-slide-next',
            key:'right'
        },
        circular: true,
            infinite: true,
            mousewheel: false,
            swipe: {
            onTouch:true,
            //onMouse:true
        },
        transition: true,
        items: {
            visible: {
                min: 1,
                max: 4
            }
        },
        auto: {
            play: false
        }
    });

$(function() {
    $('#carousel').carouFredSel({
        responsive: true,
        circular: false,
        auto: false,
        items: {
            visible: 1,
           // width: 200,
        },
        scroll: {
            fx: 'crossfade'
        }
    });
 
    $('#thumbs').carouFredSel({
        responsive: true,
        circular: false,
        scroll : {
            items: 1,
        },
        infinite: false,
        auto: false,
        prev: '#prev',
        next: '#next',
        items: {
            visible: {
                min: 4,
                max: 6
            }
            //width: 150,
        }
    });
 
    $('#thumbs a').click(function() {
        $('#carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
        $('#thumbs a').removeClass('selected');
        $(this).addClass('selected');
        return false;
    });
});
/* parallax */
    if ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) || (/Android/i.test(navigator.userAgent) && /chrome/i.test(navigator.userAgent)) || (/Android/i.test(navigator.userAgent) && /firefox/i.test(navigator.userAgent)) || (/Android/i.test(navigator.userAgent) && /opera/i.test(navigator.userAgent))) {
        $('#scene').css({'display':'none'});
    }else{
        $('#scene').css({'display':'block'});
        if($("#scene").length) {
            var scene = document.getElementById('scene');
            var parallax = new Parallax(scene);
        }
    }




/*active set block*/
    /*$('.set-list .set-item').on('click', function(){
        $(this).closest('.set-list').find('.set-item').removeClass('active');
        $(this).addClass('active');
    })*/

/*reset select*/
    /*$('input.reset').on('click', function(evt){
        evt.preventDefault();

        $form1 = $(evt.target).closest('.filter-wrapper').find('.main-catalogue-filter form');
        $form1[0].reset();
        $form1.find('select').selectpicker('render');

        $form2 = $(evt.target).closest('.filter-wrapper').find('.additional-filter-wrapper form');
        $form2[0].reset();
        $form2.find('select').selectpicker('render');
    });*/

/*popup delete scroll in document*/
    /*$('.modal').on('shown.bs.modal', function () {
        $('html').css({'overflow':'hidden'});
    });

    $('.modal').on('hidden.bs.modal', function () {
        $('html').css({'overflow-x':'hidden', 'overflow-y':'auto'});
    });*/
/*select-color*/
    $(".select-color").on("click", function(){
        if ($('.select-opt-color').is(':visible')) {
            $('.select-opt-color').slideUp(200);
        }else{
            $(this).find('.select-opt-color').slideDown(200);
        }
    });
    $(".select-opt-color").on("click", function(){
    }, function(){
        $(this).slideUp(200);
    });
    var change_status = $('.select-opt-color a');
    for(var i=0,len=change_status.length;i<len;i++) {
        $(change_status[i]).on('click', function(x){
            return function() {
                var option = $(this).data('value');
                $(this).closest('.filter-item').find('[type="hidden"]').val(option);
                $(this).addClass('active').siblings('a').removeClass('active');
                $(this).closest('.select-color').find("span.selected").html($(this).html());
                $(".select-opt-color").slideUp(200);
                return false;
            }
        }(i));
    }
    $(function(){//close list (click over list)
        $(document).click(function(event) {
            if ($(event.target).closest(".select-color").length) return;
            $(".select-opt-color").slideUp(200);
            event.stopPropagation();
        });
    });

/* alignment text block in slider */
    if ((/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) || (/Android/i.test(navigator.userAgent) && /chrome/i.test(navigator.userAgent)) || (/Android/i.test(navigator.userAgent) && /firefox/i.test(navigator.userAgent))) {
    var slideWidth = $('.slide').width(),
        slideHeight = $('.slide').height(),
        blockWidth = $('.slide-info').width(),
        blockHeight = $('.slide-info').height();
        $('.slide-info-wrap').css({'margin-top': slideHeight/2 - blockHeight/2-30, 'margin-bottom': slideHeight/2 - blockHeight/2-30});
        $('#slide-prev, #slide-next').on('touch',function(){
            var slideWidth = $('.slide').width(),
                slideHeight = $('.slide').height()
                blockWidth = $('.slide-info').width(),
                blockHeight = $('.slide-info').height();
                $('.slide-info-wrap').css({'margin-top': slideHeight/2 - blockHeight/2-30, 'margin-bottom': slideHeight/2 - blockHeight/2-30});
        });
    };
        

/* show/hide .sub-cat-box */
    $('#index-page .main-buttons-box .but').on('click',function(){
        if (typeof $(this).attr('href') != "undefined")
            return;
        $('.sub-cat-box').slideUp(300).css({'opacity':0});
         $('.main-buttons-box .but').removeClass('active');
        if (!$(this).next().is(':visible')) {
            $(this).next().slideDown(300).css({'opacity':1});
            $(this).addClass('active');
        }
    });

/* margin .sub-cat-box */
    $('.ind-buttons-box .but').each(function(i){
        var contWidth = $('.container').width();
            wContainer = $('.container').offset().left,
            wBut = $(this).offset().left;
            marginB_C = wBut - wContainer;
        $(this).parent().find('.sub-cat-box').css({'width':contWidth, 'margin-left': - marginB_C + 15});
       
    });
    
        $(window).resize(function(){
            $('.ind-buttons-box .but').each(function(i){
            var contWidth = $('.container').width(),
                wContainer = $('.container').offset().left,
                wBut = $(this).offset().left;
                marginB_C = wBut - wContainer;
            $(this).parent().find('.sub-cat-box').css({'width':contWidth, 'margin-left': - marginB_C + 15});
        });
    });

/* dotted plugin... */
    $(".product-card-min .product-name-wrapper").dotdotdot({
        wrap: 'letter'
    });
    $(window).resize(function(){
        $(".product-card-min .product-name-wrapper").dotdotdot({
            wrap: 'letter'
        });
    })

/* +/- 1 produc in basket*/
    $('input.number-product').on('blur',function(){
        if ($(this).val()<=0) {
            $(this).prop('value','1');
        }
    });
    /*$('input.minus').click(function () {
        var $input = $(this).parent().find('input:text');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });*/
    /*$('input.plus').click(function () {
        var $input = $(this).parent().find('input:text');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });*/
/* number only */
    $('.number-only').keypress(function(e){
       if(e.which != 8) { if (!(e.which>=48 && e.which<=57)){
        return false;
        }};
    });
/* calendar number only . / */
    $('.calendar').keypress(function(e){
       if(e.which != 8) { if (!((e.which>=48 && e.which<=57) || (e.which==46) || (e.which==47))) {
        return false;
        }};
    });
/**/
    function centerModals($element) {
        var $modals;
        if ($element.length) {
            $modals = $element;
        } else {
            $modals = $('.modal-vcenter:visible');
        }
        $modals.each( function(i) {
            var $clone = $(this).clone().css('display', 'block').appendTo('body');
            var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
            top = top > 0 ? top : 0;
            $clone.remove();
            $(this).find('.modal-dialog').css("margin-top", top);
        });
    }
    $('.modal-vcenter').on('show.bs.modal', function(e) {
        centerModals($(this));
    });
    $(window).on('resize', centerModals);


/*=== masked ===*/  
$(function($){
   $(".phone").mask("+375(99)999-99-99");
});

/* show/hide additional filter*/
    $('.show-add-filter').on('click',function(){
        $(this).toggleClass('active');
        $(this).closest('.filter-wrapper').find('.additional-filter-wrapper').stop().slideToggle(300);
    })

/*validate form*/
    /*#thank-popup-form*/
    $("#thank-popup-form").validate({
        rules:{
            form_text_1:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            form_text_2:{
                required: true,
                minlength: 2,
                maxlength: 64,
                email: true
            },
            form_text_3:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            form_textarea_4:{
                required: true,
                minlength: 2,
                maxlength: 1000
            }
        },
        messages: {
            form_text_1: {
                required: "Ошибка в поле Имя"
            },
            form_text_2: {
                required: "Ошибка в поле Email"
            },
            form_text_3: {
                required: "Ошибка в поле Телефон"
            },
            form_textarea_4: {
                required: "Введите сообщение"
            }
        }
        /*errorPlacement: function(error, element) { }*/
    });
    $('#thank-popup-form .btn_validate').on('click',function() {
        $("#thank-popup-form").valid();
    });
    /*#thank-popup-form*/
    $("#complain-popup-form").validate({
        rules:{
            form_text_5:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            form_text_6:{
                required: true,
                minlength: 2,
                maxlength: 64,
                email: true
            },
            form_text_7:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            form_textarea_8:{
                required: true,
                minlength: 2,
                maxlength: 1000
            }
        },
        messages: {
            form_text_5: {
                required: "Ошибка в поле Имя"
            },
            form_text_6: {
                required: "Ошибка в поле Email"
            },
            form_text_7: {
                required: "Ошибка в поле Телефон"
            },
            form_textarea_8: {
                required: "Введите сообщение"
            }
        }
        /*errorPlacement: function(error, element) { }*/
    });
    $('#complain-popup-form .btn_validate').on('click',function() {
        $("#complain-popup-form").valid();
    });
    /*#thank-popup-form*/
    $("#advice-popup-form").validate({
        rules:{
            form_text_9:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            form_text_10:{
                required: true,
                minlength: 2,
                maxlength: 64,
                email: true
            },
            form_text_11:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            form_textarea_12:{
                required: true,
                minlength: 2,
                maxlength: 1000
            }
        },
        messages: {
            form_text_9: {
                required: "Ошибка в поле Имя"
            },
            form_text_10: {
                required: "Ошибка в поле Email"
            },
            form_text_11: {
                required: "Ошибка в поле Телефон"
            },
            form_textarea_12: {
                required: "Введите сообщение"
            }
        }
        /*errorPlacement: function(error, element) { }*/
    });
    $('#advice-popup-form .btn_validate').on('click',function() {
        $("#advice-popup-form").valid();
    });
    /*#thank-popup-form*/
    $("#ask-popup-form").validate({
        rules:{
            form_text_13:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            form_text_14:{
                required: true,
                minlength: 2,
                maxlength: 64,
                email: true
            },
            form_text_15:{
                required: true,
                minlength: 2,
                maxlength: 64
            },
            form_textarea_16:{
                required: true,
                minlength: 2,
                maxlength: 1000
            }
        },
        messages: {
            form_text_13: {
                required: "Ошибка в поле Имя"
            },
            form_text_14: {
                required: "Ошибка в поле Email"
            },
            form_text_15: {
                required: "Ошибка в поле Телефон"
            },
            form_textarea_16: {
                required: "Введите сообщение"
            }
        }
        /*errorPlacement: function(error, element) { }*/
    });
    $('#ask-popup-form .btn_validate').on('click',function() {
        $("#ask-popup-form").valid();
    });
    /*#ORDER_FORM*/
    $("#ORDER_FORM").validate({
        rules: {
            ORDER_PROP_1: {
                required: true,
                minlength: 2,
                maxlength: 64
            },
            ORDER_PROP_2: {
                required: true,
                minlength: 2,
                maxlength: 64,
                email: true
            },
            ORDER_PROP_3: {
                required: true,
                minlength: 2,
                maxlength: 64
            },
            ORDER_PROP_20: {
                required: true,
                minlength: 2,
                maxlength: 64
            },
            ORDER_PROP_22: {
                required: true,
                minlength: 2,
                maxlength: 64
            },
            ORDER_PROP_23: {
                required: true,
                minlength: 1,
                maxlength: 64
            },
            ORDER_PROP_27: {
                required: true,
                minlength: 10,
                maxlength: 10
            },
            ORDER_PROP_28: {
                required: true,
                minlength: 5,
                maxlength: 5
            },
            ORDER_PROP_29: {
                required: true,
                minlength: 5,
                maxlength: 5
            },
            ORDER_PROP_30_val: {
                required: true,
                minlength: 2,
                maxlength: 64
            }
        },
        messages: {
            ORDER_PROP_1: {
                required: "Ошибка в поле Фамилия"
            },
            ORDER_PROP_20: {
                required: "Ошибка в поле Имя"
            },
            ORDER_PROP_2: {
                required: "Ошибка в поле Email"
            },
            ORDER_PROP_3: {
                required: "Ошибка в поле Телефон"
            },
            ORDER_PROP_22: {
                required: ""
            },
            ORDER_PROP_23: {
                required: ""
            },
            ORDER_PROP_27: {
                required: ""
            },
            ORDER_PROP_28: {
                required: ""
            },
            ORDER_PROP_29: {
                required: ""
            },
            ORDER_PROP_30_val: {
                required: "Ошибка в поле Город"
            }
        },
        submitHandler: function (form) {
            submitForm('Y');
            return false;
        }
    });
    /*$('#ORDER_FORM .next-step').on('click',function() {
        $("#ORDER_FORM").valid();
    });*/


/*group checkboxes :checked*/
    $('.group-checkboxes .custom_check input').on('change',function(){
        $(this).closest('.group-checkboxes').find('input[type="checkbox"]').not(this).prop('checked', false);
    })

    /*$('.slope-param .custom_check input').on('change',function(){
        $(this).closest('.slope-param').find('input[type="checkbox"]').not(this).prop('checked', false);
    })*/

    $('.slope-param .custom_check input').on('click',function(){
        $(this).closest('.slope-param').find('input').prop('checked', false);
        $(this).prop('checked', true);
    })

//*rise on the floor*/
    $('.no-rise').prop({'checked':true, 'disabled':true});

    $('.group-checkboxes input').on('click',function(){
        $(this).closest('.additional-service').find('.no-rise').prop({'checked':false, 'disabled':false});
    })

    $('.rise').on('click',function(){
        $('.no-rise').prop({'checked':false, 'disabled':false});
        if ($(this).is(':checked')) {
            $(this).closest('.lifting-check-box').find('.slope-param').slideDown(300).addClass('active');
            $(this).closest('.lifting-check-box').find('.slope-param').find('.slope:first-child input').prop({'checked':true});
            $(this).closest('.lifting-check-box').find('.no-rise').prop('checked', false);
        }else{
            $(this).closest('.lifting-check-box').find('.slope-param').slideUp(300).removeClass('active');
            $(this).closest('.lifting-check-box').find('.slope-param').find('input').prop({'checked':false});
        }
    });
    $('.no-rise').on('click',function(){
        $(this).prop({'checked':true, 'disabled':true});
        $('.rise').closest('.lifting-check-box').find('.slope-param').slideUp(300).removeClass('active');
        $('.check-control input').prop('checked', false)
    });

    $('.check-control input').on('click', function(){
        $(this).each(function(i){
            if (!$('.check-control input').is(':checked')) {
                $('.no-rise').prop({'checked':true, 'disabled':true});
            }
        })
    });

/*recalc basket on service click/change*/
    $('.no-rise').on('click',function(){
        recalcBasketAjax({});
    });
//    $('.group-checkboxes .custom_check input,.slope-param .custom_check input,.slope-param .floor-box input').on('change',function(){
    $('.group-checkboxes .custom_check input,.lifting-check-box .custom_check input,.slope-param .floor-box input').on('change',function(){
        recalcBasketAjax({});
    });

/*delivery radio*/
    $('.pay-box-wrapper .custom_radio').on('click',function(){
        if ($('#delivery-check1').is(':checked')) {
            $('.inner-delivery').slideUp(300);
            $('.inner-no-delivery').slideDown(300);
        }
        if ($('#delivery-check2').is(':checked')) {
            $('.inner-no-delivery').slideUp(300);
            $('.inner-delivery').slideDown(300);
        }
    });
/*select-tab city in contacts*/
$('#citySelect').on('change', function (e) {
    $('#cityTab li a').eq($(this).val()).tab('show'); 
});

/**/
    $('.floor-box input').on('blur',function(){
        if ($(this).val() < 1) {
            $(this).prop('value', '1');
        }
    })
/**/
    if (!(navigator.userAgent.match(/iPhone|iPad|iPod/i))) {
        $('.modal').on('shown.bs.modal', function() {
            $(this).find('input.form-control:first').focus();
        });
    }
    
/**/
    jQuery(window).load(function() {
        $('.number-product').each(function(){
            if ($(this).val() == 1) {
                $(this).parent().find('.minus').prop('disabled', true);
            }else{
                $(this).parent().find('.minus').prop('disabled', false);
            }
        });
        $('.minus').on('click',function(){
            if ($(this).next().val() == 1) {
                $(this).prop('disabled', true);
            }
        });
        $('.plus').on('click',function(){
            if ($(this).prev().val() > 1) {
                $(this).parent().find('.minus').prop('disabled', false);
            }
        });
        $('.number-product').on('blur',function(){
            if ($(this).val() == 1) {
                $(this).parent().find('.minus').prop('disabled', true);
            }else{
                $(this).parent().find('.minus').prop('disabled', false);
            }
        })
    });
/*init calendar*/
    var datesArray = [];
    var d = new Date();
    for (i = -1; i > -390; i--) {
        var tempDay = new Date(); tempDay.setHours(0,0,0,0);
        tempDay.setDate(d.getDate()+i);
        datesArray.push(tempDay.getTime());
    }
    $(function () {
        $('.calendar').pickmeup({
            position : 'top',
            hide_on_select : true,
            //default_date : false,
            render: function(date) {
              if ($.inArray(date.getTime(), datesArray) > -1){
                        return {
                            disabled   : true,
                            class_name : 'disabled'
                        }
                    }
            }
        });
    });
/* masked time*/
    $(function($) {
        $.mask.definitions['H']='[012]';
        $.mask.definitions['M']='[012345]';
        $('.time').mask('H9:M9');
    });

/*scroll to product in catalogue*/
    if($('.main-catalogue-filter').length) {
        $(function(){
            $('html, body').animate({scrollTop: $('.main-catalogue-filter').offset().top-100}, 800);
        });
    };

/*fix :hover on iPhone*/
    var mobileHover = function () {
        $('*').on('touchstart', function () {
            $(this).trigger('hover');
        }).on('touchend', function () {
            $(this).trigger('hover');
        });
    };
    mobileHover();

/*popup delete scroll in document*/
    $('.modal').on('shown.bs.modal', function () {
        $('html').css({'overflow':'hidden'});
    });
    $('.modal').on('hidden.bs.modal', function () {
        if ($('.modal.fade.in').length > 0) {
        } else {
            $('html').css({'overflow-x':'hidden', 'overflow-y':'auto'});
        }
    });
    
/*breadcrumb ul.neighbours to show*/    
    jQuery('.breadcrumbs li.bc-triangle').on('mouseover',function() {
        jQuery(this).children('div.neighbours-wrap').show();
        jQuery(this).addClass('active');
    });
    
    jQuery('.breadcrumbs li.bc-triangle').on('mouseout',function() {
        jQuery(this).children('div.neighbours-wrap').hide();
        jQuery(this).removeClass('active');
    });
    
    jQuery('div.neighbours-wrap').on('mouseover',function() {
        jQuery(this).show();
        jQuery(this).parent().find('.breadcrumbs li.bc-triangle').addClass('active');
    });
    
    jQuery('div.neighbours-wrap').on('mouseout',function() {
        jQuery(this).hide();
        jQuery(this).parent().find('.breadcrumbs li.bc-triangle').removeClass('active');
    });

    jQuery('.canc-gray').on('mouseover',function() {
        jQuery(this).hide();
        jQuery('.canc-blue').show();
    });
    
    jQuery('.canc-blue').on('mouseleave',function() {
        jQuery(this).hide();
        jQuery('.canc-gray').show();
    });
    
    jQuery('.canc-blue').on('click',function() {
        jQuery('#rassrochka-popup').hide();
        jQuery('#halva-popup').hide();
    });
    
    jQuery('.halva-block').on('click',function() {
        jQuery('#halva-popup').show();
    });
    
    jQuery('.rassrochka-block').on('click',function() {
        jQuery('#rassrochka-popup').show();
    });
    
    jQuery('.pop-span span .buy-info form a').on('click',function() {
        jQuery('#rassrochka-popup').hide();
        jQuery('#halva-popup').hide();
    });
    
    jQuery('.one-product .checkbox-e input').on('change',function() {
        if(jQuery(this).prop("checked")){
            updateNotes(jQuery(this).attr('data-id'), jQuery(this).attr('data-type'));
            jQuery(this).parents('.checkbox-e').parents('.checkbox-block').children('.checkbox-e').children('input').prop("checked", false);
            jQuery(this).prop("checked",true);
            if(jQuery(this).attr('data-type') == "RASSROCHKA"){
                console.log('show must go on');
                jQuery(this).parents('.checkbox-e').parents('.checkbox-block').parents('.one-product').children('div.col-xs-12').children('.choose-rass').show();
            }else{
                jQuery(this).parents('.checkbox-e').parents('.checkbox-block').parents('.one-product').children('div.col-xs-12').children('.choose-rass').hide();
            }
        }else{
            jQuery(this).parents('.checkbox-e').parents('.checkbox-block').parents('.one-product').children('div.col-xs-12').children('.choose-rass').hide();
            updateNotes(jQuery(this).attr('data-id'), '');
        }
    });
    
});

function updateNotes(productId, notes){
    var hostname = window.location.hostname;
    var request = $.ajax({
        url: "http://"+hostname+"/update-notes/",
        type: "POST",
        global: false,
        data: { productId: productId, notes: notes },
    });

    request.done(function(msg) {
        var cartobj = jQuery.parseJSON(msg);
        console.log(cartobj);
    });

    request.fail(function( jqXHR, textStatus ) {
        console.log( "Request failed: " + textStatus );
    });
}
