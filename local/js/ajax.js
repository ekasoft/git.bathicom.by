//************* ajax*****************/
/* submit */
function addSubmitParams()
{
    if ($(this).is(':disabled') || $(this).is(".disabled"))
        return false;

    $('body').trigger('onAjaxReloadBeforeSubmitClick');

    var inpName = $(this).attr('name');
    var inpValue = $(this).val();
    if (typeof (inpName) !== "undefined") {
        $(this).closest("form").append('<input type="hidden" name="' + inpName + '" " value="' + inpValue + '"/>');
    }
}


function bitrixFormAjaxReplaseInit(data)
{
    if (!$('input[name=bxajaxidjQuery]', data).length)
        return;

    $(data).attr('action', $('input[name=FORM_ACTION]', data).val());

    $('input[name=FORM_ACTION]', data).remove();
    $('input[name=AJAX_CALLjQuery]', data).remove();

    $('input[name=bxajaxidjQuery]', data).attr('name', 'bxajaxid');

    $('input[type=submit],button[type=submit]', data).on("click", addSubmitParams);

    $(data).removeClass('replaceFormBitixAjax');
    $(data).submit(bitrixFormAjaxReplase);
}


function checkForAjaxForms()
{
    $("form.replaceFormBitixAjax").each(function() {
        bitrixFormAjaxReplaseInit(this);
    });
}

function bitrixFormAjaxReplase()
{

    var bxajaxObj = $("#comp_" + $('input[name=bxajaxid]', this).val());

    loader_Custom_ajax_shadow(bxajaxObj);
    var options = {
        url: $(this).attr("action"),
        dataType: "html",
        type: "POST",
        data: $(this).serialize()

    };

    $.ajax(options).done(function(data)
    {
        $(bxajaxObj).html(data);
        updateBlock(bxajaxObj);
        checkForAjaxForms();
        loader_Custom_ajax_remove()
        $('body').trigger('onAjaxReload', [bxajaxObj]);
    });
    return false
}

if (BX.ajax)
{
    BX._showWait = BX.showWait;
    BX.showWait = function(node, msg)
    {
        loader_Custom_ajax_shadow(node);
        BX._showWait(node, msg);
        return;
    }

    BX.ajax._UpdatePageData = BX.ajax.UpdatePageData;
    BX.ajax.UpdatePageData = function(arData)
    {
        BX.ajax._UpdatePageData(arData);
    }

    BX._closeWait = BX.closeWait;
    BX.closeWait = function(node, obMsg)
    {
        checkForAjaxForms(node);
        loader_Custom_ajax_remove();
        BX._closeWait(node, obMsg);
    }

    BX.ajax._submitComponentForm = BX.ajax.submitComponentForm;
    BX.ajax.submitComponentForm = function(obForm, container)
    {
        var bxajaxObj = $("#" + container)
        loader_Custom_ajax_shadow(bxajaxObj);
        BX.ajax._submitComponentForm(obForm, container);
        BX.addCustomEvent('onAjaxSuccess', FormAjaxDone);

        function FormAjaxDone()
        {
            loader_Custom_ajax_remove();
            $('body').trigger('onAjaxReload', [bxajaxObj]);
        }

    }


    BX.ajax._insertToNode = BX.ajax.insertToNode;
    BX.ajax.insertToNode = function(url, container)
    {
        var bxajaxObj = $("#" + container);
        loader_Custom_ajax_shadow(bxajaxObj);
        var options = {
            url: url,
            dataType: "html",
            type: "GET"
        };

        $.ajax(options).done(function(data)
        {
            var result = {"obj": bxajaxObj, "data": data};
            $('body').trigger('onAjaxBeforeInsert', [result]);
            loader_Custom_ajax_remove();
            $(result.obj).html(result.data);
            updateBlock($('body'));
            checkForAjaxForms();
            $('body').trigger('onAjaxReload', [bxajaxObj]);
        });
    }
}


function  loader_Custom_ajax_remove()
{
    $("form.loading").removeClass("loading");
}
/*jquery ajaxa  */


function loader_Custom_ajax_shadow(cont)
{
    var bxajaxObj = $(cont)

    if (!bxajaxObj.length)
       return;

    if (typeof ajaxloaderposition == "function") {
        ajaxloaderposition();
    }
    $("form", bxajaxObj).addClass("loading");

    $('body').trigger('onAjaxReloadBefore', [bxajaxObj]);
}

//
function windowHeight() {
    var de = document.documentElement;

    return self.innerHeight || (de && de.clientHeight) || document.body.clientHeight;
}

//
function windowWidth() {
    var de = document.documentElement;

    return self.innerWidth || (de && de.clientWidth) || document.body.clientWidth;
}

$(document).ready(function() {
    checkForAjaxForms();
});
