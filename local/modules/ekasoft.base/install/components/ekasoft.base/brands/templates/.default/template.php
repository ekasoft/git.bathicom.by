<div class="brends-main">
    <div class="centering">
        <h2>Бренды</h2>
        <div class="slider carousel-slick slider--brands">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <div class="item">
                    <img src="<?= imageResize(array('WIDTH' => 0, 'HEIGHT' => 50), $arResult["FILES"][$arItem["UF_FILE"]]["SRC"]);?>" alt="<?=$arItem["NAME"];?>" title="<?=$arItem["NAME"];?>" />
                </div>
            <?endforeach;?>
        </div>
    </div>
</div>