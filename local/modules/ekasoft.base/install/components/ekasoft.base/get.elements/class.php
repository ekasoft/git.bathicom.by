<?php

if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

\CBitrixComponent::includeComponentClass("lib:base.modulecomponent");

class BGetElements extends BaseModuleComponent {
    
    public function onPrepareComponentParams($arParams) {
        if( isset($arParams["PROPERTY_CODE"]) && $arParams["PROPERTY_VALUE"] != "" ) {
            $arParams["FILTER"]["PROPERTY_".$arParams["PROPERTY_CODE"]."_VALUE"] = $arParams["PROPERTY_VALUE"];
        }
        unset($arParams["PROPERTY_CODE"], $arParams["PROPERTY_VALUE"]);
        
        return parent::onPrepareComponentParams($arParams);
    }
    
    public function decoratePrice($arPrices) {
        return $arPrices;
    }
    
    public function executeComponent() {
        $this->arResult = $this->getElements();

        $this->includeComponentTemplate();
    }
    
}