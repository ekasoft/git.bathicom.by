<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Вывод элемента по фильтру",
    "DESCRIPTION" => "Вывод элементов по заданному фильтру из инфоблока",
    "ICON" => "/images/strings.gif",
    "PATH" => array(
        "ID" => "ekasoft",
        "NAME" => "Ека-софт",
        "CHILD" => array(
            "ID" => "elements",
            "NAME" => "Вывод элементов инфоблока"
        )
    ),
);
?>
