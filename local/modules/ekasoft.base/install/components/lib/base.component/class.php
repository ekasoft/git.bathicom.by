<?php

/**
 * @version 0.2
 * @author Koval Vitaliy  kov4l.v@gmail.com
 */


class BaseComponent extends \CBitrixComponent {

    protected $arOrder  = array();
    protected $arSelect = array();
    protected $arFilter = array();
    protected $arNavParams;
    
    protected $arOrderSection   = array();
    protected $arFilterSection  = array();
    
    protected $arImage  = array();
    protected $arError  = array();

    protected $objItem;
    
    protected function addElement($arFields) {
        \CModule::includeModule("iblock");

        $objElement = new \CIBlockElement;
        $ID = $objElement->add($arFields);

        return $ID;
    }
    
    protected function getSections($full = true) {
        \CModule::includeModule("iblock");
        
        $dbl = \CIBlockSection::getList(
                    $this->arOrderSection, $this->arFilterSection
        );
        $result = array();
        if( $full ) {
            while( $row = $this->decorateSection($dbl->getNext(true, false)) ) {
                $result[$row["ID"]] = $row;
            }
        } else {
            while( $row = $this->decorateSection($dbl->getNext(true, false)) ) {
                $result[] = $row;
            }
        }
        
        return $result;
    }
    
    protected function getElements($key = true, $full = false) {
        \CModule::IncludeModule("iblock");
        if( is_array($this->arParams["FILTER"]) ) {
            $this->arFilter = array_merge($this->arFilter, $this->arParams["FILTER"]);
        }
        if( is_array($this->arParams["PROPERTY"]) ) {
            $this->arFilter = array_merge($this->arFilter, $this->arParams["PROPERTY"]);
        }
        
        $dbl = \CIBlockElement::GetList(
                        $this->arOrder, $this->arFilter, false, $this->arNavParams, $this->arSelect
        );
        
        $result = array();

        if( $full ) {
            while( $row = $this->decorateFull($dbl->getNextElement()) ) {
                if( $key ) {
                    $result[$row["ID"]] = $row;
                } else {
                    $result[] = $row;
                }
            }
        } else {
            while( $row = $this->decorate($dbl->getNext(true, false)) ) {
                if( $key ) {
                    $result[$row["ID"]] = $row;
                } else {
                    $result[] = $row;
                }
            }
        }

        return $result;
    }

    protected function getPropValue($code =false, $id = false) {
        \CModule::includeModule("iblock");

        $arOrder = array(
            "SORT" => "ASC"
        );

        $arFilter = array(
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"]
        );

        if( $code ) {
            $arFilter["CODE"] = $code;
        } elseif( $id) {
            $arFilter["ID"] = $id;
        }

        $dbl = \CIBlockProperty::getList($arOrder, $arFilter);

        while( $row = $dbl->fetch(true, false) ) {
            prent($row);
        }
    }

    protected function getMultiPropValue($code = false, $id = false) {
        \CModule::includeModule("iblock");

        $arOrder = array(
            "SORT" => "ASC"
        );

        $arFilter = array(
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"]
        );

        if( $code ) {
            $arFilter["CODE"] = $code;
        } elseif( $id ) {
            $arFilter["PROPERTY_ID"] = $id;
        }

        $dbl = CIBlockPropertyEnum::getList($arOrder, $arFilter);
        $result = array();
        while( $row = $this->decorateMultiProp($dbl->fetch(true, false)) ) {
            $result[$row["ID"]] = $row;
        }

        return $result;
    }

    protected function decorateMultiProp($result) {
        if( empty($this->arParams["FILTER"]) || !$result ) {
            return $result;
        }
        
        if( isset($this->arParams["FILTER"]["PROPERTY_2"]) && in_array($result["ID"], $this->arParams["FILTER"]["PROPERTY_2"]) ) {
            $result["CHECKED"] = true;
        }
        
        if( isset($this->arParams["FILTER"]["PROPERTY_5"]) && $result["ID"] == $this->arParams["FILTER"]["PROPERTY_5"] ) {
            $result["CHECKED"] = true;
        }
        
        if( isset($this->arParams["FILTER"]["PROPERTY_6"]) && $result["ID"] == $this->arParams["FILTER"]["PROPERTY_6"] ) {
            $result["CHECKED"] = true;
        }

        return $result;
    }
    
    protected function decorate($result) {
        if( $result["PREVIEW_PICTURE"] > 0 ) {
            $this->arImage[] = $result["PREVIEW_PICTURE"];
        }
        
        if( $result["DETAIL_PICTURE"] > 0 ) {
            $this->arImage[] = $result["DETAIL_PICTURE"];
        }
        
        return $result;
    }
    
    protected function decorateFull($resultObj) {
        if( !$resultObj ) {
            return $resultObj;
        }
        $result = $resultObj->getFields();
        $result["PROP"] = $resultObj->getProperties();
        
        return $this->decorate($result);
    }
    
    protected function decorateSection($result) {
        if( $result["PICTURE"] > 0 ) {
            $this->arImage[] = $result["PICTURE"];
        }
        
        return $result;
    }
    
    protected function decorateFiles($result) {
        $uploadDir = \COption::GetOptionString("main", "upload_dir", "upload");
        return "/$uploadDir/" . $result["SUBDIR"] . "/" . $result["FILE_NAME"];
    }
    
    protected function makeImages() {
        if( count($this->arImage) == 0 ) {
            return false;
        }
        
        $dbl = \CFile::getList(array(), array("@ID" => implode(",", $this->arImage)));
        
        while( $res = $dbl->fetch() ) {
            $this->arResult["IMAGES"][$res["ID"]] = $this->decorateFiles($res);
        }
    }

    protected function setOrder() {
        $this->arOrder = array(
         "SORT" => "ASC",
         "NAME"  => "ASC",
        );
    }
    
    protected function setFilter() {
        $this->arFilter = array(
         "ACTIVE"    => "Y",
         "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
        );
        
        if( isset($this->arParams["CODE"]) && $this->arParams["CODE"] != '' ) {
            $this->arFilter["CODE"] = $this->arParams["CODE"];
        }
    }
    
    protected function setNavParams() {
        $this->arNavParams = false;
    }

    protected function setSelect() {
        $this->arSelect = array(
         "*"
        );
    }
    
    protected function setOrderSection() {
        $this->arOrderSection = $this->arOrder;
    }
    
    protected function setFilterSection() {
        $this->arFilterSection = $this->arFilter;
    }

    protected function init() {
        $this->setOrder();
        $this->setFilter();
        $this->setSelect();
        $this->setNavParams();
        
        $this->setOrderSection();
        $this->setFilterSection();
    }
}
