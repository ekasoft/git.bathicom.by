<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use EkaSoft\Base;

class BaseModuleComponent extends \CBitrixComponent {
    
    /* объект модели        */
    protected $objModel;
    /* объект декоратора    */
    protected $objDecorator;
    /* объект сущности      */
    protected $objEntity;
    
    protected $objFilter;
    
    public function createObjects($arParams) {
        if( $arParams["EKA_DECORATOR"] == '' ) {
            $arParams["EKA_DECORATOR"] = 3;
        }
        
        if( $arParams["EKA_MODEL"] == '' ) {
            $arParams["EKA_MODEL"] = 3;
        }
        
        if( $arParams["EKA_ENTITY"] == '' ) {
            $arParams["EKA_ENTITY"] = 3;
        }
        
        $this->objDecorator = Base\Fabrica\Decorator::getObject($this)->getClass($arParams["EKA_DECORATOR"]);
        $this->objModel     = Base\Fabrica\Model::getObject($this->objDecorator)->getClass($arParams["EKA_MODEL"], 2);

        $this->objEntity    = Base\Fabrica\Entity::getObject($this->objModel, $this->objDecorator)->getClass($arParams["EKA_ENTITY"]);

        $this->objDecorator->initEntity($this->objEntity);

        return $arParams;
    }
    
    public function onPrepareComponentParams($arParams) {
        $this->createObjects($arParams);
        $this->initParams($arParams);
        $this->initVars();
    }
    
    public function initVars() {
        $this->arResult["FILES"] = array();
    }
    
    public function initParams($arParams) {
        $this->objEntity->initParams($arParams);
    }
       
    public function getElements($key = true) {
        return $this->objEntity->getResult($key);
    }
    
}