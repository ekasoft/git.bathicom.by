<?php

if( !defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

class ViewElement extends \CBitrixComponent { 
    
    public function onPrepareComponentParams($arParams) {
        return $arParams;
    }
    
    public function executeComponent() {
        $this->arResult["ITEMS"] = $this->arParams["RESULT"];
        $this->arResult["FILES"] = $this->arParams["FILES"];

        $this->includeComponentTemplate();
    }
}