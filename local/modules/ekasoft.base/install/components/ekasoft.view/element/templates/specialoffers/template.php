<div class="js-element js-elementid<?= $arResult["ITEMS"]["ID"] ?> simple propvision1" data-elementid="<?= $arResult["ITEMS"]["ID"] ?>" data-detail="/catalog/bumazhno_belovaya_produktsiya/">
    <div class="inner">
        <div class="padd">
            <a href="#"><i class="icon da2qb"></i></a>
            <div class="name"><a href="<?= $arResult["ITEMS"]["DETAIL_PAGE_URL"] ?>" title="<?= html_entity_decode($arResult["ITEMS"]["NAME"]) ?>"><?= htmlspecialchars_decode($arResult["ITEMS"]["NAME"]) ?></a></div>
            <div class="pic"><a href="#"><img src="<?= imageResize(array("WIDTH" => 150, "HEIGHT" => 150, "MODE" => "in"), $arResult["FILES"][$arResult["ITEMS"]["DETAIL_PICTURE"]]["SRC"]); ?>" alt="Y<?= $arResult["ITEMS"]["NAME"] ?>" title="<?= $arResult["ITEMS"]["NAME"] ?>"></a></div>
            <? if (isset($arResult["ITEMS"]["ATTRIBUTES"])): ?>
                <div class="item__labels">
                    <? foreach ($arResult["ITEMS"]["ATTRIBUTES"] as $code => $value): ?>
                        <i class="icon ico_icon--<?= mb_strtolower($code) ?>"></i>
                    <? endforeach; ?>
                </div>
            <? endif; ?>
        </div>
        <div class="soloprice"><span class="price gen "><span>Цена:</span> <?= $arResult["ITEMS"]["PRICES"]["FORMAT"]; ?> руб.</span></div>
        <div class="popup padd">
            <noindex>
                <div class="buy">
                    <form class="add2basketform js-custom js-buyform<?= $arResult["ITEMS"]['ID'] ?> js-synchro clearfix" name="add2basketform">
                        <input type="hidden" name="ajax_basket" value="Y" />
                        <input type="hidden" name="action" value="ADD2BASKET">
                        <input type="hidden" name="id" class="js-add2basketpid" value="<?= $arResult["ITEMS"]['ID']; ?>">
                        <span class="quantity">
                            <a class="minus js-minus" href="javascript:void(0);" title="Уменьшить количество">-</a>
                            <input type="text" class="js-quantity" name="quantity" value="1" data-ratio="1">
                            <span class="js-measurename">шт</span>
                            <a class="plus js-plus" title="Увеличить количество" href="javascript:void(0);">+</a>
                        </span>
                        <a rel="nofollow" class="submit add2basket" href="#" title="В корзину">В корзину</a>
                        <a rel="nofollow" class="inbasket" href="/personal/cart/" title="В корзине">В корзине</a>
                        <input type="submit" name="submit" class="nonep" value="" />
                    </form>
                </div>
            </noindex>
        </div>
        <div class="description">
            <div class="text">
                <?= $arResult["ITEMS"]["PREVIEW_TEXT"]; ?>
            </div>
        </div>
    </div>
</div>