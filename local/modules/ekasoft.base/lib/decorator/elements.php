<?php

namespace EkaSoft\Base\Decorator;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class Elements extends \Ekasoft\Base\Abstracts\Decorator {
    
    public function __construct($_objComponent) {
        parent::__construct($_objComponent);
        
        $this->collectFilesCodes = array(
          "PREVIEW_PICTURE",
          "DETAIL_PICTURE",
        );
    }
    
    /**
     * Функция преобразует данные из Entity
     * @param array $row   - строчка из базы
     * @return array $row
     */
    public function decorateItem($row) {
        
        $this->objEntity->setItem($row);

        $this->objEntity->transfarmation();
        
        $this->collectFiles($row);

        // c версии 5.3.0 можно method_exists()
        if( method_exists(get_class($this->objComponent), "decorateItem") && $this->objComponent != null) {
            $row = $this->objComponent->decorateItem($row);
        }

        return $row;
    }
}