<?php

namespace EkaSoft\Base\Decorator;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class ElementsFull extends Elements {
    
    public function decorateItem($rowObj) {
        $row = $rowObj->getFields();
        $row["PROPERTIES"] = $rowObj->getProperties();

        $this->objEntity->setItem($row);
        
        $row = $this->objEntity->createPhotoGallery($row);
        
        return parent::decorateItem($row);
    }
}