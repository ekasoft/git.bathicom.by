<?php

namespace Ekasoft\Base\Abstracts;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

\CModule::IncludeModule("iblock");
abstract class Item {
    
    protected $objDecorator;
    
    protected $arOrder;
    public $arFilter;
    protected $arNavParams;
    protected $arSelect;

    protected $dbResult;
    
    protected function __construct($_objDecorator) {
        $this->objDecorator = $_objDecorator;
    }
    
    abstract function getElements($key = false);
    
    /**
     * Функция задания значения для сортировки
     * @param array $arOrder  - массив сортировки
     * @return object $this   - возвращает ссылку на себя
     */
    public function setOrder($arOrder = false) {       
        if( !$arOrder || !is_array($arOrder)) {
            return false;
        }
        
        $this->arOrder = $arOrder;
        
        return $this;
    }
    
    protected function initOrder() {
        $this->arOrder = array(
          "SORT" => "ASC",
          "NAME" => "ASC"
        );
        
        return $this;
    }
    
    /**
     * Функция добавления к массиву фильтра
     * @param array $arFilter  - массив для фильтра
     * @return object $this    - возвращает ссылку на себя
     */
    public function addFilter($arFilter = array()) {
        if( !is_array($arFilter) ) {
            return false;
        }
        
        if( count($this->arFilter) == 0 ) {
            $this->setFilter($arFilter);
            return $this;
        }
        
        $this->arFilter += $arFilter;
        //$this->arFilter = array_merge($this->arFilter, $arFilter);
        
        return $this;
    }
    
    public function setFilter($arFilter) {
        if( !is_array($arFilter)  ) {
            $this->initFilter();
        }

        $this->arFilter = $arFilter;
        
        return $this;
    }
    
    public function setFilterByCode($code, $value) {
        $this->arFilter[$code] = $value;
    }
    
    public function delFilterByCode($code) {
        unset($this->arFilter[$code]);
    }
    
    public function initFilter($arParams = array()) {
        $this->arFilter = array(
            "ACTIVE"    => "Y"  ,
        );
              
        $this->makeFilter($arParams);

        return $this;
    }
    
    public function initNavParams($arParams = array()) {
        $this->arNavParams = false;
        if( isset($arParams["ELEMENT_COUNT"]) ) {
            $this->arNavParams = array(
              "nPageSize" => $arParams["ELEMENT_COUNT"]
            );
        }
        
        if( isset($arParams["PAGEN"]) ) {
            $this->arNavParams["iNumPage"] = $arParams["PAGEN"];
        }
        
        return $this;
    }
    
    public function setNavParams($arParams = array()) {
        if( !is_array($arParams) ) {
            return false;
        }
        
        $this->arNavParams = $arParams;
    }
    
    protected function initSelect() {
        $this->arSelect = array("*");
        
        return $this;
    }
    
    /**
     * Функция добавления к массиву фильтра
     * @param array $arFilter  - массив для фильтра
     * @return object $this    - возвращает ссылку на себя
     */
    public function addSelect($arSelect = array()) {
        if( !is_array($arSelect) ) {
            return false;
        }

        $this->arSelect = array_merge($this->arSelect, $arSelect);
        
        return $this;
    }
    
    public function initParams($arParams) {
        $this->initOrder()
                ->initFilter($arParams)
                ->initNavParams($arParams)
                ->initSelect();
    }
    
    /**
     * Функция формирования фильтра из параметра компонента
     * @param array $arFilter  - массив параметра компонентов
     * @return object $this    - возвращает ссылку на себя
     */
    public function makeFilter($arParams) {
        if( isset($arParams["IBLOCK_ID"]) ) {
            $this->arFilter["IBLOCK_ID"] = $arParams["IBLOCK_ID"];
        }
        if( isset($arParams["ID"]) ) {
            $this->arFilter["ID"] = $arParams["ID"];
        }
        if( isset($arParams["FILTER"]) && is_array($arParams["FILTER"]) ) {
            $this->arFilter = array_merge($this->arFilter, $arParams["FILTER"]);
        }
        
        return $this;
    }
    
    public function setDecorator($_objDecorator) {
        $this->objDecorator = $_objDecorator;
    }
    
    public function getCDBResult() {
        return $this->dbResult;
    }
    
    public function SelectedRowsCount() {
        return $this->dbResult->SelectedRowsCount();
    }
}