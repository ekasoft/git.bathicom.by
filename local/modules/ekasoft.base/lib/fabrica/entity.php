<?php

namespace EkaSoft\Base\Fabrica;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class Entity implements \EkaSoft\Base\Interfaces\Fabrica {
    
    const ELEMENT   = 1;
    
    const TOVAR     = 2;
    
    const TOVARSKU  = 3;
    
    const STRUCTURE   = 4;
    
    private $objModel;
    private $objDecorator;
        
    private function __construct($_objModel, $_objDecorator) {
        $this->objModel = $_objModel;
        $this->objDecorator = $_objDecorator;
    }
    
    public function getObject($_objModel, $_objDecorator) {
        return new self($_objModel, $_objDecorator);
    }
    
    public function getClass($type) {
        switch ($type) {
            case self::ELEMENT :
                return new \EkaSoft\Base\Entity\BaseItem($this->objModel, $this->objDecorator);
            case self::TOVAR :
                return new \EkaSoft\Base\Entity\Tovar($this->objModel, $this->objDecorator);
            case self::TOVARSKU :
                return new \EkaSoft\Base\Entity\TovarSKU($this->objModel, $this->objDecorator);
            case self::STRUCTURE :
                return new \EkaSoft\Base\Entity\Structure($this->objModel, $this->objDecorator);
        }
    }
}