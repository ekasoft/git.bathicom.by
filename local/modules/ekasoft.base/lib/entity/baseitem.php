<?php
namespace EkaSoft\Base\Entity;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class BaseItem {

    /*  Объект модели      */
    protected $objModel;
    /*  Объект декоратора  */
    protected $objDecorator;
    
    /*  Массив id-шников файла  */
    protected $arFiles;
    
    protected $arItem;
    
    protected $arResult;
    
    public function __construct($_objModel, $_objDecorator) {
        $this->objModel     = $_objModel;
        $this->objDecorator = $_objDecorator;

        $this->arFiles = array();
    }
    
    /**
     * Функция накапливает id-шники файла
     * @param int|array $iFiles - id файла
     */
    public function addFiles($files) {
        if( is_array($files) ) {
            $this->arFiles = array_merge($this->arFiles, $files);
        } else {
            $this->arFiles[] = $files;
        }
    }
    
    public function getFiles() {
        if( count($this->arFiles) == 0 ) {
            return array();
        }
        //prent($this->arFiles);
        
        $dbl = \CFile::getList(array(), array("@ID" => implode(",", $this->arFiles)));
        $result = array();
        while( $row = $dbl->fetch() ) {
            $result[$row["ID"]] = $this->objDecorator->decorateFiles($row);
        }

        return $result;
    }
    
    public function setItem($arItem) {
        $this->arItem = $arItem;
    }
    
    public function getItem() {
        return $this->arItem;
    }
    
    public function initParams($arParams) {
        $this->objModel->initParams($arParams);
    }
    
    public function getItems($key) {
        return (count($this->arResult) <= 0 ) ? $this->objModel->getElements($key) : $this->arResult;
    }
    
    public function getResult($key = true) {
        return array(
          "ITEMS" => $this->getItems($key),
          "FILES" => $this->getFiles(),
        );
    }
    
    public function createPhotoGallery($row) {
        $row["GALLERY"] = array();
        
        if( $row["DETAIL_PICTURE"] != '' ) {
            $row["GALLERY"][] = $row["DETAIL_PICTURE"];
        }
        /*
        if( $row["PREVIEW_PICTURE"] != '' ) {
            $row["GALLERY"][] = $row["PREVIEW_PICTURE"];
        }*/
        
        if( $row["PROPERTIES"][\EkaSoft\Base\Core\Config::PROP_GALLERY]["VALUE"] != "" ) {
            $this->addFiles($row["PROPERTIES"][\EkaSoft\Base\Core\Config::PROP_GALLERY]["VALUE"]);
            $row["GALLERY"] = array_merge($row["GALLERY"], $row["PROPERTIES"][\EkaSoft\Base\Core\Config::PROP_GALLERY]["VALUE"]);
        }

        return $row;
    }
    
    public function getNavParams() {
        return $this->objDecorator->decoreateNavParams( $this->objModel->getCDBResult() );
    }
    
    public function getCDBResult() {
        return $this->objModel->getCDBResult();
    }
}