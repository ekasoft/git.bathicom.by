<?php
namespace EkaSoft\Base\Entity;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class Tovar extends BaseItem {
    
    protected $IDs;
    
    protected $idSections;

    public function __construct($_objModel = null, $_objDecorator = null) {
        parent::__construct($_objModel, $_objDecorator);
    }
    
    public function transfarmation() {
        $this->IDs[] = (int)$this->arItem["ID"];
        $this->idSections[$this->arItem["IBLOCK_SECTION_ID"]][] = $this->arItem;
    }
    
    public function getIDs() {
        return $this->IDs;
    }
    
    public function getIDSections() {
        return $this->idSections;
    }
   
}