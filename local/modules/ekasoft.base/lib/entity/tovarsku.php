<?php

namespace EkaSoft\Base\Entity;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class TovarSKU extends BaseItem {

    protected $arOffers;
    protected $IDs;
    
    /* хранит код свойства, по которому определить активную ТПшку */
    protected $activeOfferCode = null;
    /* храинт значение активной ТПшки  */
    protected $activeOfferValue = null;
    
    public function __construct($_objModel = null, $_objDecorator = null) {
        parent::__construct($_objModel, $_objDecorator);
        $this->arOffers = array();
    }
    
    public function setOfferActive($code, $value) {
        $this->activeOfferCode = $code;
        $this->activeOfferValue = $value;
    }
    
    public function getAttributes() {
        return array(
            "HIT",
            "SALE",
            "NEW",
        );
    }
    
    public function getItems() {
        $this->objModel->getElements();
        return $this->arResult;
    }
    
    public function getOffers() {
        return $this->arOffers;
    }
    
    public function getResult($key = true) {
        return $this->objModel->getElements($key);
    }
    
    public function getQuantity() {
        \CModule::IncludeModule("catalog");
        $dbl = \CCatalogProduct::GetList(array(), array("ID" => $this->IDs, false, false, array()));
        $result = array();
        while ( $row = $dbl->fetch() ) {
            $result[$row["ID"]] = $row;
        }
        
        return $result;
    }
    
    public function transfarmation() {
        $this->IDs[] = (int)$this->arItem["ID"];
        if( isset($this->arItem["PROPERTIES"][\EkaSoft\Base\Core\Config::PROP_BIND]) && $this->arItem["PROPERTIES"][\EkaSoft\Base\Core\Config::PROP_BIND]["VALUE"] != '' ) {

            if( ($this->activeOfferCode == "" || $this->activeOfferValue == "") || 
                    ($this->activeOfferCode == null || $this->activeOfferValue ) == null
            ) {
                $this->arResult[$this->arItem["PROPERTIES"][\EkaSoft\Base\Core\Config::PROP_BIND]["VALUE"]][$this->arItem["ID"]] = $this->arItem;
            } elseif( $this->arItem[$this->activeOfferCode] == $this->activeOfferValue ) {
                $this->arResult[$this->arItem["ID"]] = $this->arItem;
            } else {
                $this->arOffers[$this->arItem["ID"]] = $this->arItem;
            }
        } else {
            if( ($this->activeOfferCode == "" || $this->activeOfferValue == "") || 
                    ($this->activeOfferCode == null || $this->activeOfferValue ) == null
            ) {
                $this->arResult[$this->arItem["PROPERTIES"][\EkaSoft\Base\Core\Config::PROP_BIND]["VALUE"]][$this->arItem["ID"]] = $this->arItem;
            } elseif( $this->arItem[$this->activeOfferCode] == $this->activeOfferValue ) {
                $this->arResult[$this->arItem["ID"]] = $this->arItem;
            } else {
                $this->arOffers[$this->arItem["ID"]] = $this->arItem;
            }
        }
    }

    public function getIDs() {
        return $this->IDs;
    }
    
    public function getPrices($iProduct) {
        return $this->objModel->getPrices($iProduct);
    }
}