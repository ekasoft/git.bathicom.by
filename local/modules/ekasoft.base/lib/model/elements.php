<?php

namespace Ekasoft\Base\Model;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class Elements extends \Ekasoft\Base\Abstracts\Item {
        
    protected $arItem;
    
    public function __construct($_objDecorator) {
        $this->objDecorator = $_objDecorator;
    }
      
    public function getElements($key = true) {
        $this->dbResult = \CIBlockElement::getList(
                $this->arOrder,
                $this->arFilter,
                false,
                $this->arNavParams,
                $this->arSelect
        );
        
        $result = array();
        while( $row = $this->dbResult->getNext(true, false) ) {
            $row = $this->objDecorator->decorateItem($row);
            
            if( $key ) {
                $result[$row["ID"]] = $row;
            } else {
                $result[] = $row;
            }
        }
        return $result;
    }
}