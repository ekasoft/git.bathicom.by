<?php

namespace Ekasoft\Base\Model;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class Section extends \Ekasoft\Base\Abstracts\Item {
    
    public function __construct($_objDecorator) {
        parent::__construct($_objDecorator);
    }
    
    public function initFilter($arParams = array()) {
        $this->arFilter = array(
            "ACTIVE"    => "Y"  ,
//            ">=ELEMENT_CNT" => 1,
            'CNT_ALL'=> true
        );
              
        $this->makeFilter($arParams);

        return $this;
    }
    
    public function initOrder() {
        $this->arOrder = array(
          "LEFT_MARGIN" => "ASC",
          "SORT"        => "ASC",
          "NAME"        => "ASC"
        );
        
        return $this;
    }
    
    public function initCnt() {
        $this->arBlncCnt = true;
        
        return $this;
    }
    
    public function initParams($arParams) {
        $this->initOrder()
                ->initFilter($arParams)
                ->initCnt()
                ->initNavParams($arParams)
                ->initSelect();
    }
    
    
    public function initSelect() {
        $this->arSelect = array(
          "ID",
          "IBLOCK_ID",
          "NAME",
          "CODE",
          "DEPTH_LEVEL",
          "DESCRIPTION",
          "PICTURE",
          "IBLOCK_SECTION_ID",
          "SECTION_PAGE_URL",
          "LEFT_MARGIN",
          "RIGHT_MARGIN",
          "ELEMENT_CNT"
        );
        
        return $this;
    }
    
    public function getElements($key = true) {
        $this->dbResult = \CIBlockSection::getList(
                $this->arOrder,
                $this->arFilter,
                $this->arBlncCnt,
                $this->arSelect
        );
        
        $result = array();
        while( $row = $this->dbResult->getNext(true, false) ) {

            $row = $this->objDecorator->decorateItemSection($row);
            
            if( $key ) {
                $result[$row["ID"]] = $row;
            } else {
                $result[] = $row;
            }
        }
        
        
        return $result;
    }
}