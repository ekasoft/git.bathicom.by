<?php

namespace Ekasoft\Base\Model;

\CModule::includeModule("catalog");

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class Tovar extends ElementsFull {
    
    public function __construct($_objDecorator) {
        parent::__construct($_objDecorator);
    }
    
    /**
     * Функция вычисляет оптимальную цену товара с учетом скидок
     * @param int $iProduct - id товара
     * @return array        - массив цен
     */
    public function getPrices($iProduct) {
        global $USER;

        $arPrices = \CCatalogProduct::getOptimalPrice($iProduct, 1, $USER->GetUserGroupArray(), "N");

        return $this->objDecorator->decoratePrice($arPrices);
    }
}