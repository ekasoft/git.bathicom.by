<?php

namespace Ekasoft\Base\Model;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class ElementsFull extends Elements {
    
    public function __construct($_objDecorator) {
        parent::__construct($_objDecorator);
    }

    public function getElements($key = true) {
        $this->dbResult = \CIBlockElement::getList(
                $this->arOrder,
                $this->arFilter,
                false,
                $this->arNavParams,
                $this->arSelect
        );
        //prent($this->arFilter);
        //prent($this->arNavParams);

        $result = array();
        while( $row = $this->dbResult->getNextElement(true, false) ) {
            $row = $this->objDecorator->decorateItem($row);
            //prent($row);
            if( $key == 1 ) {
                $result[$row["ID"]] = $row;
            } elseif( $key == 2) {
                $_key = isset($row["KEY"]) ? $row["KEY"] : $row["ID"];
                $result[$_key][] = $row;
            } else {
                $result[] = $row;
            }
        }
        //prent($result);
        return $result;
    }
}