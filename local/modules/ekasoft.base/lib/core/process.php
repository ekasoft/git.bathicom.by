<?php

namespace EkaSoft\Base\Core;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

\CModule::IncludeModule("sale");

class Process {
    
    static function run() {
        if( !Context::getInstance()->get("BASKET") ) {
            Context::getInstance()->set("BASKET", \EkaSoft\Base\Basket::getIDs() );
        }
    }
    
    static function update($type) {
        Context::getInstance()->set($type, \EkaSoft\Base\Basket::getIDs() );
    }
    
}