<?php

namespace EkaSoft\Base\Core;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class Config {
    const CACHE_TIME = 3600;
    const CACHE_ID_STRUCTURE = 's1_structure';
    
    const PROP_BIND = "CML2_LINK";
    const PROP_GALLERY = "MORE_PHOTO";
}