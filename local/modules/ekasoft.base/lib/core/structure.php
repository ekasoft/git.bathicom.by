<?php

namespace EkaSoft\Base\Core;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class Structure {
    
    static private $instance = null;
    
    private $objCache;
    
    private $sections;
    private $structure;
    private $files;
    private $hashIDByCode;
    private $tree;
    
    private $isCache;
    
    private function __construct($sCacheID) {
//        $this->objCache = new \CPHPCache;
//        $sCacheID .= 'ss';
        $this->sections = array();
        $this->hashIDByCode = array();
        $this->tree = array();
        $this->images = array();
        $this->structure = array();
//        
//        $this->isCache = false;
//        
//        if( $this->objCache->initCache(Config::CACHE_TIME, $sCacheID, "structure") ) {
//            $_var = $this->objCache->getVars();prent($_var);
//            $this->isCache = true;
//            $this->sections = $_var["sections"];
//            $this->hashIDByCode = $_var["hashIDByCode"];
//            $this->tree = $_var["tree"];
//            $this->structure = $_var["structure"];
//            $this->files = $_var["files"];
//            
//            unset($_var);
//        } else {
//            $this->objCache->startDataCache(Config::CACHE_TIME, $sCacheID, "structure");
//        }
    }
    
    public function saveCache() {
//        $arFields = array(
//          "sections"        => $this->sections,
//          "hashIDByCode"    => $this->hashIDByCode,
//          "tree"            => $this->tree,
//          "structure"       => $this->structure,
//          "images"          => $this->images,
//        );
//        
//        $this->objCache->endDataCache($arFields);prent($arFields);die;
    }
    
    static public function getInstance($sCacheID = false) {
        if( self::$instance === null ) {
            self::$instance = new self($sCacheID);
        }
        
        return self::$instance;
    }
    
    public function addStructure($row) {
        $this->sections[$row["ID"]] = $row;
        $this->hashIDByCode[$row["CODE"]] = $row["ID"];
        $this->treeConstruction($row);
    }
    
    public function treeConstruction($row) {
        if( $row["IBLOCK_SECTION_ID"] == "" ) {
            $this->tree[$row["ID"]] = array();
        } else {
            $this->tree[$row["IBLOCK_SECTION_ID"]][] = $row["ID"];
        }
    }
    
    public function getChild($iSection) {
        return $this->tree[$iSection];
    }
    
    public function getIDByCode($sCode) {
        return isset($this->hashIDByCode[$sCode]) ? $this->hashIDByCode[$sCode] : false;
    }
    
    public function getSection($iID) {
        return isset($this->sections[$iID]) ? $this->sections[$iID] : false;
    }
    
    public function setStructure($structure) {
        $this->structure = $structure;
    }
    
    public function getStructure() {
        return $this->structure;
    }
    
    public function setFiles($files) {
        $this->files = $files;
    }
    
    public function getFiles() {
        return $this->files;
    }
    
}