<? if (!check_bitrix_sessid()) return; ?>
<?
global $errors;

if ($errors === false):
    echo CAdminMessage::ShowNote("Модуль установлен");
else:
    for ($i = 0; $i < count($errors); $i++)
        $alErrors .= $errors[$i] . "<br>";
    echo CAdminMessage::ShowMessage(Array("TYPE" => "ERROR", "MESSAGE" => "Ошибка при установке", "DETAILS" => $alErrors, "HTML" => true));
endif;
?>
<form action="<? echo $APPLICATION->GetCurPage() ?>">
    <p>
        <input type="hidden" name="lang" value="<? echo LANG ?>">
        <input type="submit" name="" value="<? echo GetMessage("MOD_BACK") ?>">
    </p>
</form>