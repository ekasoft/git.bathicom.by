<?
$module_id = "sam.sync";

if (class_exists("sam_sync"))
    return;

class sam_sync extends CModule
{

    public $MODULE_ID = "sam.sync";
	public $MODULE_VERSION;
	public $MODULE_VERSION_DATE;
	public $MODULE_NAME;
	public $MODULE_DESCRIPTION;
	public $MODULE_CSS;
	public $MODULE_GROUP_RIGHTS = "Y";

	public function __construct()
    {
        $arModuleVersion = array();
        include 'version.php';

        $this->MODULE_VERSION = '0';
        $this->MODULE_VERSION_DATE = '';

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = "Импорт из файлов";
        $this->MODULE_DESCRIPTION = "Гибкий импорт данных";
    }

    function DoInstall()
    {
        global $APPLICATION;
        $this->InstallFiles();
        $this->InstallDB();
        $GLOBALS["errors"] = $this->errors;
        $APPLICATION->IncludeAdminFile("Установка модуля", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/sam.easyfilter/install/step1.php");
    }

    function DoUninstall()
    {
        global $APPLICATION;

            $this->UnInstallDB();
            $this->UnInstallFiles();

            $GLOBALS["errors"] = $this->errors;
            $APPLICATION->IncludeAdminFile("Удаление модуля", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/sam.easyfilter/install/unstep1.php");

    }

    function InstallDB()
    {
        RegisterModule("sam.sync");
        return true;
    }

    function UnInstallDB($arParams = array())
    {
        UnRegisterModule("sam.sync");
        return true;
    }

    function InstallFiles()
    {
        return true;
    }

    function UnInstallFiles()
    {
        return true;
    }

}