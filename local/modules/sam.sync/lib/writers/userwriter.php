<?
namespace Sam\Sync\Writers;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Entity\EntityError;
use Bitrix\Main\Entity\Result;
use CUser;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class UserWriter
{
	private $entity;
	private $map;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	public function setLogger(LoggerInterface $logger) {
		$this->logger = $logger;
	}

	public function getLogger() {
		if (is_null($this->logger)) {
			return new NullLogger();
		}
		return $this->logger;

	}

	public function __construct()
	{
		$this->entity = new CUser();
	}

	public function setMap(array $map)
	{
		$this->map = $map;
	}

	public function write($item)
	{
		//throw new \Bitrix\Main\NotImplementedException("На данный момент невозможно импортировать пользователей");
		$item = $this->convert($item);
		$item = $this->prepareBeforeAdd($item);

		$this->getLogger()->debug("Пользователь обрабатывается", $item);

		$dbl = $this->entity->GetList($by, $order, array("XML_ID" => $item["XML_ID"]));
		if ($res = $dbl->fetch()) {
			$this->getLogger()->debug("Пользователь обновляется по xml_id", $item["XML_ID"]);
			$result = $this->entity->update($res["ID"], $item);
		} else {
			$this->getLogger()->debug("Пользователь создаётся", $item["XML_ID"]);
			$result = $this->entity->add($item);
		}

		if ($result) {
			$this->getLogger()->debug("Пользователь обработан");
		} else {
			$item["ERROR"] = $this->entity->LAST_ERROR;
			$this->getLogger()->error("Пользователь не обработан", $item);
			throw new \Exception("Пользователь не обработан");
		}

	}

	private function convert($item)
	{
		$result = array();
		foreach ($this->map as $k => $field) {
			$result[$k] = strlen($field["VALUE"]) ? $field["VALUE"] :  $item[$field["CODE"]];

			if (is_callable($field["FUNC"])) {
				$result[$k] = call_user_func($field["FUNC"], $result[$k]);
			}
		}

		return $result;
	}

	private function prepareBeforeAdd($item)
	{
		return $item;
	}

	public function beforeWriteFromTable() {

	}
}