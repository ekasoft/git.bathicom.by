<?
namespace Sam\Sync\Writers;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Entity\UpdateResult;
use Bitrix\Main\Loader;
use CIBlockSection;
use CUtil;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class SectionWriter
{
	private $entity;
	private $map;
	private $iblockID;

	private $addedCount;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	protected function getEntity()
	{
		return $this->entity;
	}

	public function setLogger(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	protected function getLogger()
	{
		if (is_null($this->logger)) {
			return new NullLogger();
		}
		return $this->logger;

	}

	public function __construct($iblockID)
	{
		Loader::includeModule("iblock");
		$this->iblockID = $iblockID;
		$this->entity = new CIBlockSection();
	}

	public function setMap(array $map)
	{
		$this->map = $map;
	}

	public function write($item)
	{
		$item = $this->convert($item);

		$item["IBLOCK_ID"] = $this->iblockID;
		$this->getLogger()->debug("Раздел обрабатывается", $item);

		$dbl = $this->getEntity()->GetList(false, array("XML_ID" => $item["XML_ID"], "IBLOCK_ID" => $this->iblockID));
		if ($res = $dbl->fetch()) {
			$this->getLogger()->debug("Раздел существует");
			$result = $this->getEntity()->Update($res["ID"], $item);
		} else {
			$this->getLogger()->debug("Раздел создаётся");
			$item = $this->generateCode($item);
			$result = $this->getEntity()->add($item);
			if ($result) {
				$this->addedCount++;
			}
		}

		if ($result) {
			$this->getLogger()->info("Раздел обработан");
		} else {
			$item["ERROR"] = $this->getEntity()->LAST_ERROR;
			$this->getLogger()->error("Раздел не обработан", $item);
			throw new \Exception("Раздел не обработан");
		}

	}

	private function convert($item)
	{
		$result = array();
		foreach ($this->map as $k => $field) {
			$result[$k] = strlen($field["VALUE"]) ? $field["VALUE"] :  $item[$field["CODE"]];

			if (is_callable($field["FUNC"])) {
				$result[$k] = call_user_func($field["FUNC"], $result[$k]);
			}
		}

		return $result;
	}

	private function translit($text)
	{
		$arParams = array("replace_space" => "-", "replace_other" => "-");
		return CUtil::translit($text, 'ru', $arParams);
	}

	public function beforeWriteFromTable() {
		$filter = array("IBLOCK_ID" => $this->iblockID, "ACTIVE" => "Y");
		$dbl = CIBlockSection::GetList(false, $filter, false, array("ID"));

		$obj = new CIBlockSection();
		while($res = $dbl->Fetch()) {
			$obj->Update($res["ID"], array("ACTIVE" => "N"));
		}
	}

	public function getIDByXmlID($xmlID) {
		if (empty($xmlID)) return false;

		$filter = array("XML_ID" => $xmlID, "IBLOCK_ID" => $this->iblockID);
		$select = array("ID");
		$res = $this->getEntity()->GetList(false, $filter, false, $select)->Fetch();

		return $res["ID"] ?: false;
	}

	private function generateCode($item) {
		$item["CODE"] = $this->translit($item["NAME"]);
		$this->getLogger()->debug("Символьный код сгенерирован", array("XML_ID" => $item["XML_ID"], "CODE" => $item["CODE"]));

		return $item;
	}
}