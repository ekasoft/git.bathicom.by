<?
namespace Sam\Sync\Writers;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Entity\UpdateResult;
use Bitrix\Main\Loader;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class PriceWriter
{
	/**
	 * @var \CPrice
	 */
	private $priceEntity;

	private $map;

	private $addedCount;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	protected function getPriceEntity()
	{
		return $this->priceEntity;
	}

	public function setLogger(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	protected function getLogger()
	{
		if (is_null($this->logger)) {
			return new NullLogger();
		}
		return $this->logger;

	}

	public function __construct()
	{
		$this->priceEntity = new \CPrice();
	}

	public function setMap(array $map)
	{
		$this->map = $map;
	}

	public function write($item)
	{
		global $APPLICATION;
		$this->getLogger()->debug("Цена обрабатывается", $item);

		$item = $this->convert($item);
		/**
		 * @TODO Move to callback
		 */
		$item = $this->prepareItem($item);
		$this->getLogger()->debug("Результат конвертации", $item);
		$filter =  array("PRODUCT_ID" => $item["PRODUCT_ID"], "CATALOG_GROUP_ID" => $item["CATALOG_GROUP_ID"]);
		$dbl = $this->getPriceEntity()->GetList(false, $filter);
		if ($res = $dbl->fetch()) {
			$this->getLogger()->debug("Цена существует");
			$priceResult = $this->getPriceEntity()->Update($res["ID"], $item, false);
		} else {
			$this->getLogger()->debug("Цена создаётся");
			$priceResult = $this->getPriceEntity()->Add($item);
		}


		if ($priceResult) {
			$this->getLogger()->info("Элемент обработан");
		} else {
			if ($APPLICATION->GetException()) {
				$item["ERROR"][] = $APPLICATION->GetException()->GetString();
			}
			if ($this->getPriceEntity()->LAST_ERROR) {
				$item["ERROR"][] = $this->getPriceEntity()->LAST_ERROR;
			}
			$this->getLogger()->error("Элемент не обработан", $item);
			throw new \Exception("Элемент не обработан");
		}

	}

	private function convert($item)
	{
		$result = array();
		foreach ($this->map as $k => $field) {
			$result[$k] = strlen($field["VALUE"]) ? $field["VALUE"] :  $item[$field["CODE"]];

			if (is_callable($field["FUNC"])) {
				$result[$k] = call_user_func($field["FUNC"], $result[$k]);
			}
		}

		return $result;
	}


	private function translit($text)
	{
		$arParams = array("replace_space" => "-", "replace_other" => "-");
		return \CUtil::translit($text, 'ru', $arParams);
	}

	public function beforeWriteFromTable() {

	}

	public function getIDByXmlID($xmlID) {
		if (empty($xmlID)) return false;

		$filter = array("XML_ID" => $xmlID);
		$select = array("ID");
		$res = \CIBlockElement::GetList(false, $filter, false, false, $select)->Fetch();

		return $res["ID"];
	}

	private function prepareItem($item)
	{
		if (empty($item["PRODUCT_ID"]) || empty($item["PRICE"])) {
			return $item;
		}

		$res = \CIBlockElement::GetByID($item["PRODUCT_ID"])->GetNextElement();
		$props = $res->GetProperties();
		if (empty($props["COEFFICIENT"]["VALUE"])) return $item;

		$item["PRICE"] = $item["PRICE"]/(1/$props["COEFFICIENT"]["VALUE"]);

		return $item;
	}
}