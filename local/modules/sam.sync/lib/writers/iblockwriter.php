<?
namespace Sam\Sync\Writers;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;
use CIBlockElement;
use CIBlockProperty;
use CIBlockPropertyEnum;
use CIBlockSection;
use CUtil;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class IblockWriter
{
	private $entity;
	private $map;
	private $iblockID;

	private $addedCount;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	protected function getEntity()
	{
		return $this->entity;
	}

	public function setLogger(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	protected function getLogger()
	{
		if (is_null($this->logger)) {
			return new NullLogger();
		}
		return $this->logger;

	}

	public function __construct($iblockID)
	{
		Loader::includeModule("iblock");
		$this->iblockID = $iblockID;
		$this->entity = new CIBlockElement();
	}

	public function setMap(array $map)
	{
		$this->map = $map;
	}

	public function write($item)
	{

		$item = $this->convert($item);

		$item["IBLOCK_ID"] = $item["IBLOCK_ID"] ?: $this->iblockID;
		$this->getLogger()->debug("Элемент обрабатывается", $item);

		if ($item["SUB_CATEGORY_NAME"]) {
			$item["IBLOCK_SECTION_ID"] = $this->createSubsectionIfNotExists($item["IBLOCK_SECTION_ID"], $item["SUB_CATEGORY_NAME"], $item["IBLOCK_ID"]);
		}

		$dbl = $this->getEntity()->GetList(false, array("XML_ID" => $item["XML_ID"], "IBLOCK_ID" => $item["IBLOCK_ID"]));
		if ($res = $dbl->fetch()) {

			if ($item["DELETE"] == "Y") {
				$this->getLogger()->debug("Элемент {$res["ID"]} будет удалён");
				$result = $this->getEntity()->Delete($res["ID"]);
			} else {
				$item = $this->fillRequiredFields($item, $res);
				$this->getLogger()->debug("Элемент существует");
				$props = $item["PROPERTY_VALUES"];
				$fields = $item;
				unset($fields["PROPERTY_VALUES"]);
				$result = $this->getEntity()->Update($res["ID"], $fields, false, false, false, false);
				$this->getEntity()->SetPropertyValuesEx($res["ID"], $item["IBLOCK_ID"], $props);
			}
		} else {
			$this->getLogger()->debug("Элемент создаётся");
			$result = $this->getEntity()->add($item);
			if ($result) {
				$this->addedCount++;
			}
		}

		if ($result) {
			$this->getLogger()->info("Элемент обработан");
		} else {
			$item["ERROR"] = $this->getEntity()->LAST_ERROR;
			$this->getLogger()->error("Элемент не обработан", $item);
			throw new \Exception("Элемент не обработан");
		}

	}

	protected function convert($item)
	{
		$result = array();
		$resultBefore = array();

		foreach ($this->map as $k => $field) {
			$resultBefore[$k] = $item[$k];
			if (is_callable($field["GENERATOR"]) || function_exists($field["GENERATOR"])) {
				$resultBefore[$k] = call_user_func($field["GENERATOR"], $item);
			}
		}

		foreach ($this->map as $k => $field) {
			if ($field["IS_PROPERTY"]) {
				$value = $this->convertIblockProperty($field["CODE"], $resultBefore[$k], $resultBefore["IBLOCK_ID"]);
				if (is_callable($field["FUNC"]) || function_exists($field["FUNC"])) {
					$value = call_user_func($field["FUNC"], $value);
				}
				if ($field["MULTIPLE"]) {
					$result["PROPERTY_VALUES"][$field["CODE"]][] = $value;
				} else {
					$result["PROPERTY_VALUES"][$field["CODE"]] = $value;
				}
			} else {
				$result[$field["CODE"]] = $resultBefore[$k];
				if (is_callable($field["FUNC"]) || function_exists($field["FUNC"])) {
					$result[$field["CODE"]] = call_user_func($field["FUNC"], $result[$field["CODE"]]);
				}
			}

		}

		if (empty($result["CODE"])) {
			$result["CODE"] = $this->translit($result["NAME"]);
		}

		return $result;
	}

	private function convertIblockProperty($code, $value, $iblockID) {
		$filter = array("CODE" => $code, "IBLOCK_ID" => $iblockID ?: $this->iblockID);
		$propertyParams = CIBlockProperty::GetList(false, $filter)->Fetch();
		$uType = $propertyParams["USER_TYPE"];
		$type = $propertyParams["PROPERTY_TYPE"];
		switch (true) {
			case $type == "L": return $this->convertToEnum($value, $propertyParams);
			case $type == "S" && $uType == "directory": return $this->convertToHL($value, $propertyParams);
			default: return $value;
		}
	}

	private function translit($text)
	{
		$arParams = array("replace_space" => "-", "replace_other" => "-");
		return CUtil::translit($text, 'ru', $arParams);
	}

	public function beforeWriteFromTable() {

	}

	private function convertToEnum($value, $propertyParams) {
		if (empty($value)) return false;

		$filter = array("VALUE" => $value, "PROPERTY_ID" => $propertyParams["ID"]);
		$enumValue = CIBlockPropertyEnum::GetList(false, $filter)->Fetch();
		if ($enumValue) return $enumValue["ID"];

		$newValueID = CIBlockPropertyEnum::Add(array("PROPERTY_ID" => $propertyParams["ID"], "VALUE" => $value));

		if ($newValueID) {
			$this->getLogger()->info("Создан вариант свойства", array($propertyParams["ID"],$newValueID, $value));
		} else {
			$this->getLogger()->error("Не удалось создать вариант свойства", array($propertyParams, $value));
		}

		return $newValueID;
	}

	private function convertToHL($value, $propertyParams) {
		if (empty($value)) return false;

		$hlItem = HighloadBlockTable::getList(
			array(
				'select' => array('TABLE_NAME', 'NAME', 'ID'),
				'filter' => array('=TABLE_NAME' => $propertyParams["USER_TYPE_SETTINGS"]["TABLE_NAME"])
			)
		)->fetch();
		$entity = HighloadBlockTable::compileEntity($hlItem);
		$entityDataClass = $entity->getDataClass();

		$directoryItem = $entityDataClass::getList(array("filter" => array("UF_NAME" => $value), "select" => array("ID", "UF_XML_ID")))->Fetch();
		if ($directoryItem) return $directoryItem["UF_XML_ID"];

		$addResult = $entityDataClass::add(array("UF_NAME" => $value));

		if ($addResult->isSuccess()) {
			$this->getLogger()->info("Создан элемент справочника", array($propertyParams["ID"],$addResult->getId() , $value));
			$entityDataClass::update($addResult->getId(), array("UF_XML_ID" => $hlItem["TABLE_NAME"] . $addResult->getId()));
			return $hlItem["TABLE_NAME"] . $addResult->getId();
		} else {
			$this->getLogger()->error("Не удалось создать элемент справочника", array($propertyParams, $value));
			return false;
		}
	}

	public function getIDByXmlID($xmlID) {
		if (empty($xmlID)) return false;

		$filter = array("XML_ID" => $xmlID, "IBLOCK_ID" => $this->iblockID);
		$select = array("ID");
		$res = \CIBlockSection::GetList(false, $filter, false, $select)->Fetch();

		return $res["ID"] ?: false;
	}

	private function createSubsectionIfNotExists($parentID, $name, $iblockID) {
		if (empty($parentID) || empty($name)) return false;

		$filter = array("IBLOCK_ID" => $this->iblockID, "SECTION_ID" => $parentID, "NAME" => $name);
		$select = array("ID", "NAME", "SECTION_ID");
		$res = CIBlockSection::GetList(false, $filter, false, $select)->Fetch();

		if ($res) {
			$this->getLogger()->info("Подраздел существует", $res);
			return $res["ID"];
		}

		$filter = array("IBLOCK_ID" => $iblockID, "ID" => $parentID);
		$select = array("ID", "NAME");
		$parentSection = CIBlockSection::GetList(false, $filter, false, $select)->Fetch();

		$obj = new \CIBlockSection();
		$fields = array("IBLOCK_SECTION_ID" => $parentID, "NAME" => $name, "IBLOCK_ID" => $iblockID, "CODE" => $this->translit($name . "-{$parentSection["NAME"]}"));

		$this->getLogger()->info("Создаётся подраздел", $fields);

		$sectionID = $obj->Add($fields);

		if ($sectionID) {
			$this->getLogger()->info("Подраздел создан", array($sectionID));
		} else {
			$fields["ERROR"] = $obj->LAST_ERROR;
			$this->getLogger()->error("Подраздел не создан", $fields);
			throw new \Exception("Элемент не обработан т.к. не создан подраздел");

		}

		return $sectionID;
	}

	public function getIblockIDByXmlID($item) {
		if (empty($item["XML_ID"])) return false;

		$filter = array("XML_ID" => $item["XML_ID"]);
		$select = array("IBLOCK_ID");
		$res = \CIBlockElement::GetList(false, $filter, false, false, $select)->Fetch();

		return $res["IBLOCK_ID"] ?: false;
	}

	private function fillRequiredFields($item, $res)
	{
		$item["CODE"] = $item["CODE"] ?: $res["CODE"];
		return $item;
	}
}