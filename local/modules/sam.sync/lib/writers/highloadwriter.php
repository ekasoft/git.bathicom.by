<?
namespace Sam\Sync\Writers;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Entity\Query;
use Bitrix\Main\Entity\UpdateResult;
use Bitrix\Main\Loader;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class HighloadWriter
{
	private $entity;
	private $map;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	protected function getEntity() {
		return $this->entity;
	}

	public function setLogger(LoggerInterface $logger) {
		$this->logger = $logger;
	}

	protected function getLogger() {
		if (is_null($this->logger)) {
			return new NullLogger();
		}
		return $this->logger;

	}

	public function __construct($hlID) {
		Loader::includeModule("highloadblock");
		$hl = HighloadBlockTable::getById($hlID)->fetch();
		$this->entity = HighloadBlockTable::compileEntity($hl);
	}

	public function setMap(array $map) {
		$this->map = $map;
	}

	public function write($item) {
		$item = $this->convert($item);
		$tableClass = $this->getEntity()->getDataClass();
		$this->getLogger()->debug("Элемент обрабатывается", $item);

		$query = new Query($this->entity);
		$dbl = $query->setSelect(array("*"))->setFilter(array("=UF_XML_ID" => $item["UF_XML_ID"]))->exec();
		if ($res = $dbl->fetch()) {
			if ($res["UF_CRC"] == $item["UF_CRC"]) {
				$this->getLogger()->debug("Элемент не изменился");
				$result = new UpdateResult();
			} else {
				$this->getLogger()->debug("Элемент обновляется");
				$result = $tableClass::update($res["ID"], $item);
			}
		} else {
			$this->getLogger()->debug("Элемент создаётся");
			$result = $tableClass::add($item);
		}

		if ($result->isSuccess()) {
			$this->getLogger()->info("Элемент обработан");
		} else {
			$item["ERROR"] = implode(", ", $result->getErrorMessages());
			$this->getLogger()->error("Элемент не обработан", $item);
			throw new \Exception("Элемент не обработан");
		}

	}

	private function convert($item)
	{
		$result = array();
		foreach ($this->map as $k => $field) {
			$result[$k] = strlen($field["VALUE"]) ? $field["VALUE"] :  $item[$field["CODE"]];

			if (is_callable($field["FUNC"])) {
				$result[$k] = call_user_func($field["FUNC"], $result[$k]);
			}
		}

		return $result;
	}

	public function beforeWriteFromTable() {

	}


}