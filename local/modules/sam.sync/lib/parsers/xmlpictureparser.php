<?
namespace Sam\Sync\Parsers;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Psr\Log\LoggerInterface;

class XMLPictureParser
{
	/**
	 * @var ArrayObject
	 */
	private $source;
	/**
	 * @var ArrayIterator
	 */
	private $sourceIterator;
	private $map;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	private $filePath;

	public function setLogger(LoggerInterface $logger) {
		$this->logger = $logger;
	}

	public function getLogger() {
		if (is_null($this->logger)) {
			return new \Psr\Log\NullLogger();
		}
		return $this->logger;

	}

	public function setSource($source, $params = false) {
		if (!file_exists($source)) {
			throw new \Exception("Файл импорта не найден");
		}
		
		$this->filePath = $source;

		$this->source = new \SimpleXMLElement(file_get_contents($source));
		if ($params["root_xpath"]) {
			//prent($this->source->xpath($params["root_xpath"]));
			$this->source = new \ArrayObject($this->source->xpath($params["root_xpath"]));
			$this->sourceIterator = $this->source->getIterator();
		} else {
			throw new \Exception("Work without xpath not implemented");
		}
	}

	public function setMap(array $map) {
		$this->map = $map;
	}

	public function getMap() {
		return $this->map;
	}

	public function next() {
		$item = $this->sourceIterator->current();
		if ($item) {
			$this->sourceIterator->next();
			return $this->convert($item);
		}

		return false;
	}

	/**
	 * @param $item \SimpleXMLElement
	 * @return array
	 */
	private function convert($item) {
		$result = array();
		$attributes = json_decode(json_encode($item), true);
		foreach ($this->getMap() as $k => $field) {
			if ($field["IGNORE"]) continue;
			foreach ((array)$attributes[$k] as $valueIndex => $value) {
				if ($valueIndex) {
					$fieldCode = $field["CODE"] . "_$valueIndex";
				} else {
					$fieldCode = $field["CODE"];
				}
				if (!empty($value) || strlen($field["VALUE"])) {
					$result[$fieldCode] = strlen($field["VALUE"]) ? $field["VALUE"] : $value;
				}
				if ($field["FUNC"] && function_exists($field["FUNC"])) {
					$result[$fieldCode] = call_user_func($field["FUNC"], $result[$fieldCode]);
				}
				$result[$fieldCode] = trim($result[$fieldCode]);

				if ($field["FILTER"] && function_exists($field["FILTER"])) {
					$filterResult = call_user_func($field["FILTER"], $result[$fieldCode]);

					if ($filterResult == false) return array();
				}
			}
		}
		//prent($result);die();
		return $result;
	}

	public function findFile($prefix = false) {
		if (empty($prefix)) return false;

		$files = glob($_SERVER["DOCUMENT_ROOT"] . "/upload/exch/$prefix*");
		sort($files);

		$this->filePath = array_shift($files);
		return $this->filePath;
	}

	public function deleteFile() {
		if (unlink($this->filePath)) {
			$this->getLogger()->debug("Файл удалён", array($this->filePath));
		} else {
			$this->getLogger()->error("Файл не удалён", array($this->filePath));
			throw new \Exception("Файл не был удалён");
		}
	}
}