<?
namespace Sam\Sync\Parsers;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Psr\Log\LoggerInterface;

class CSVParser
{
	private $source;
	private $map;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	public function setLogger(LoggerInterface $logger) {
		$this->logger = $logger;
	}

	public function getLogger() {
		if (is_null($this->logger)) {
			return new \Psr\Log\NullLogger();
		}
		return $this->logger;

	}

	public function setSource($source) {
		if (!file_exists($source)) {
			throw new \Exception("Файл импорта не найден");
		}

		$this->source = fopen($source, "r+");
	}

	public function setMap(array $map) {
		$this->map = $map;
	}

	public function getMap() {
		return $this->map;
	}

	public function next() {
		$item = fgetcsv($this->source, 2048, ";");
		return $item ? $this->convert($item) : false;
	}

	private function convert($item) {
		$result = array();
		foreach ($this->map as $k => $field) {
			$result[$field["CODE"]] = strlen($field["VALUE"]) ? $field["VALUE"] :  $item[$k];
			if ($field["FUNC"] && function_exists($field["FUNC"])) {
				$result[$field["CODE"]] = call_user_func($field["FUNC"], $result[$field["CODE"]]);
			}
			$result[$field["CODE"]] = trim($result[$field["CODE"]]);
 		}
		return $result;
	}
}