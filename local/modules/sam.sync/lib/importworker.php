<?
namespace Sam\Sync;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Application;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class ImportWorker
{
	public $dropFileAfterRead;
	private $parser;
	private $writer;
	/**
	 * @var Base
	 */
	private $tableEntity;
	private $tableName;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	private $importID;

	private $writeErrorCount;
	private $writeSuccessCount;
	private $allProcessCount;
	private $errorCount;

	public $oneStep = false;

	/**
	 * @param mixed $importID
	 */
	public function setImportID($importID)
	{
		$this->importID = $importID;
	}

	public function setLogger(LoggerInterface $logger)
	{
		$this->logger = $logger;
	}

	public function getLogger()
	{
		if (is_null($this->logger)) {
			return new NullLogger();
		}
		return $this->logger;
	}

	/**
	 * @param mixed $tableName
	 */
	public function setTableName($tableName)
	{
		$this->tableName = $tableName;
	}

	public function setParser($parser)
	{
		$this->parser = $parser;
	}

	public function setWriter($writer)
	{
		$this->writer = $writer;
	}

	public function createTable()
	{
		$map = $this->parser->getMap();
		$entityMap = $this->getEntityMap($map);

		$connection = Application::getConnection();
		$connection->createTable($this->getTableName(), $entityMap, array("ID"), array("ID"));
		$this->getLogger()->debug("Таблица создана", array("name" => $this->getTableName()));

	}

	public function getTableEntity()
	{
		if (is_null($this->tableEntity)) {
			$tableFields = $this->getEntityMap($this->parser->getMap());
			$entityParams = array("table_name" => $this->getTableName());
			$this->tableEntity = Base::compileEntity("Import" . $entityParams["table_name"], $tableFields, $entityParams);
		}

		return $this->tableEntity;
	}

	private function convertMapToEntity($map)
	{
		$entities = array();

		foreach ($map as $field) {
			switch ($field["TYPE"]) {
				case "INT":	$entities[$field["CODE"]] = new IntegerField($field["CODE"]);
					break;
				default:$entities[$field["CODE"]] = new StringField($field["CODE"]);
					break;
			}
		}

		return $entities;
	}

	private function getTableName()
	{
		return $this->tableName;
	}

	public function writeToTable()
	{
		$tableClass = $this->getTableEntity()->getDataClass();
		while (($res = $this->parser->next()) !== false) {
			$this->allProcessCount++;

			if (empty($res)) continue;

			try {
				$res["CRC"] = $this->getCRCByItem($res);
				$tableClass::add($res);
				$this->writeSuccessCount++;
			} catch (\Exception $e) {
				$info = array(
					"item" => $res,
					"error" => $e->getMessage(),
					"file" => $e->getFile(),
					"line" => $e->getLine()
				);
				$this->getLogger()->error("Ошибка при записи во временную таблицу", $info);
				$this->writeErrorCount++;
				throw new \Exception("Ошибка при записи во временную таблицу");
			}
		}
	}

	private function getCRCByItem($item) {
		$crcString = "";
		foreach ($this->parser->getMap() as $field) {
			if ($field["USE_IN_CRC"]) {
				$crcString .= $item[$field["CODE"]];
			}
		}

		return md5($crcString);
	}

	public function writeFromTable()
	{
		$tableClass = $this->getTableEntity()->getDataClass();

		$query = new \Bitrix\Main\Entity\Query($this->getTableEntity());
		$query->setSelect(array("*"));
		if (!$this->oneStep) {
			$query->setLimit(1000);
		}

		$dbl = $query->exec();
		while ($res = $dbl->Fetch()) {
			try {
				$this->writer->write($res);
			} catch (\Exception $e) {
				$this->errorCount++;
				$this->getLogger()->error("Ошибка при записи в приёмник", array("item" => $res, "error" => $e->getMessage()));

			}
			$tableClass::delete($res["ID"]);
		}

		$this->getLogger()->debug("Данные записаны из таблицы в приёмник", array("name" => $this->getTableName()));
		return $dbl->getSelectedRowsCount();
	}

	public function dropTable()
	{
		if ($this->tableExists()) {
			$connection = Application::getConnection();
			$connection->dropTable($this->getTableName());
			$this->getLogger()->debug("Таблица удалена", array("name" => $this->getTableName()));
		} else {
			$this->getLogger()->warning("Попытка удалить несуществующую таблицу", array("name" => $this->getTableName()));
		}
	}

	public function tableExists()
	{
		$connection = Application::getConnection();
		return $connection->isTableExists($this->getTableName());
	}

	public function tableIsEmpty()
	{
		$query = new \Bitrix\Main\Entity\Query($this->getTableEntity());
		$dbl = $query->setSelect(array("ID"))->setLimit(1)->exec();
		return $dbl->Fetch() === false;
	}

	public function run()
	{
		try {
			if (!$this->tableExists()) {
				$this->start();
				echo "progress", "\n", "Данные прочитаны во временную таблицу", "\n";
				if (!$this->oneStep) return;

			}

			if (!$this->tableIsEmpty()) {
				$count = $this->step();
				echo "progress", "\n", "Обработано $count записей", "\n";
				if ($this->errorCount) {
					$logName = SAM_SYNC_LOG_PATH . $this->importID . "-" . date("Y-m-d") . ".log";
					echo "Обработано с ошибками {$this->errorCount} записей", "\n";
					echo "Подробности в логе $logName", "\n";
				}
			}

			if ($this->tableIsEmpty()) {
				$this->complete();
				echo "success", "\n", "Импорт завершён";
				if (!$this->oneStep) return;
			}

		} catch (\Exception $e) {
			$info = array(
				"id" => $this->importID,
				"error" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			);
			$this->getLogger()->error("Импорт завершён неудачно", $info);

			$this->dropTable();
			echo "failure", "\n", $info["error"], "\n", $info["file"] . ":" . $info["line"];
		}

	}

	private function start()
	{
		$this->getLogger()->info("Импорт запущен", array("id" => $this->importID));

		$this->createTable();

		$this->writeToTable();
		$this->getLogger()->debug("Данные записаны во временную таблицу",
			array(
				"name" => $this->getTableName(),
				"count" => $this->allProcessCount,
				"success" => $this->writeSuccessCount,
				"error" => $this->writeErrorCount
			));

		$this->writer->beforeWriteFromTable();
		$this->getLogger()->debug("Подготовка данных сайта выполнена");

	}

	private function step()
	{
		return $this->writeFromTable();
	}

	private function complete()
	{
		if ($this->dropFileAfterRead) {
			$this->parser->deleteFile();
		}
		$this->dropTable();
		$this->getLogger()->info("Импорт завершён", array("id" => $this->importID));

	}

	private function getEntityMap($map) {
		$entityMap = $this->convertMapToEntity($map);
		$entityMap["ID"] = new IntegerField("ID", array(
			'primary' => true,
			'autocomplete' => true,
			'unique' => true));

		$entityMap["CRC"] = new StringField("CRC");

		return $entityMap;
	}

}