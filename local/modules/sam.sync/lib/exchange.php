<?
namespace Sam\Sync;

use Monolog\ErrorHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class Exchange {

    const LOGGER_ID = "exchange";
    static $ALLOW_MODES = array("checkauth", "import", "export");
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function defineConstants() {
        define('BX_SESSION_ID_CHANGE', false);
        define('BX_SKIP_POST_UNQUOTE', true);
        define('NO_AGENT_CHECK', true);
        define("STATISTIC_SKIP_ACTIVITY_CHECK", true);
        set_time_limit(600);
    }

    public function initLogger($id = self::LOGGER_ID) {
        $this->logger = new Logger($id);
        $this->logger->pushHandler(new RotatingFileHandler(SAM_SYNC_LOG_PATH . $id . ".log", 10,Logger::DEBUG));
        ErrorHandler::register($this->logger);
    }

    public function checkMode() {
        global $APPLICATION;

        if (!in_array($_REQUEST["mode"], self::$ALLOW_MODES)) {
            $APPLICATION->RestartBuffer();
            echo "failure\n";
            echo "Unknown mode.";
            die();
        }

        if ($_REQUEST["mode"] == "import" && empty($_REQUEST["type"])) {
            $APPLICATION->RestartBuffer();
            echo "failure\n";
            echo "Unknown command type.";
            die();
        }
    }

    public function doAuth() {
        global $USER;
        if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST["USER_LOGIN"]) && !empty($_POST["USER_PASSWORD"])) {
            /* @var $USER CUser */
            $result_message = $USER->Login($_POST["USER_LOGIN"], $_POST["USER_PASSWORD"]);
            if ($result_message !== true) {
                echo "failure\nWrong auth";
                $mode = "error";
            }
        }
    }

    public function checkAuth() {
        global $USER;
        $arCurrentGroups = $this->getSaleGroups();
        return $USER->IsAuthorized() && (array_intersect($USER->GetUserGroupArray(), $arCurrentGroups) || $USER->IsAdmin());
    }

    public function getSaleGroups() {
        $arCurrentGroups = array();
        $dbSiteGroupsList = \CSaleGroupAccessToSite::GetList(
            array(),
            array("SITE_ID" => SITE_ID)
        );
        while ($arSiteGroup = $dbSiteGroupsList->Fetch()) {
            $arCurrentGroups[] = IntVal($arSiteGroup["GROUP_ID"]);
        }

        return $arCurrentGroups;
    }

    public function getSession() {
        if ($this->checkAuth()) {
            echo "success\n";
            echo session_name() . "\n";
            echo session_id() . "\n";
            echo bitrix_sessid_get() . "\n";
            die();
        } else {
            echo "failure\nошибка авторизации";
            die();
        }
    }
}