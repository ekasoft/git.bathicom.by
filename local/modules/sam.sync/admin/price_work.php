<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Sam\Sync\ImportWorker;
use Sam\Sync\Parsers\CSVParser;
use Sam\Sync\Parsers\XMLParser;
use Sam\Sync\Writers\CatalogWriter;
use Sam\Sync\Writers\IblockWriter;
use Sam\Sync\Writers\PriceWriter;

function preparePrice($value) {
	return str_replace(",", ".", $value);
}

$importID = "price";
$logger = new Logger($importID);
$logger->pushHandler(new RotatingFileHandler(SAM_SYNC_LOG_PATH . "{$importID}.log", 10,Logger::DEBUG));
Monolog\ErrorHandler::register($logger);

/*
 * Создаём парсер
 */
$parser = new XMLParser();
$parser->setLogger($logger);
/*
 * Указываем источник
 */

$parser->setSource($_SERVER["DOCUMENT_ROOT"] . "/upload/exchange/exch/prices.xml", array("root_xpath" => "/prices/tovar"));
/*
 * Указываем карту полей источник->таблица
 */
$map = array(
	"price" => array("CODE" => "PRICE", "FUNC" => "preparePrice"),
	"code" => array("CODE" => "XML_ID"),
	"code_currency" => array("CODE" => "CURRENCY"),
	array("CODE" => "CATALOG_GROUP_ID", "VALUE" => 1)

);
$parser->setMap($map);
/*
 * Создаём воркер
 */
$worker = new ImportWorker();
$worker->setTableName("b_import_price_2");
$worker->setImportID($importID);
$worker->setLogger($logger);
/*
 * Устанавливаем воркеру парсер
 */
$worker->setParser($parser);
/*
 * Создаём writer
 */
$writer = new PriceWriter();
$writer->setLogger($logger);
/*
 * Указываем карту таблица->приёмник
 */
$map = array(
	"PRICE" => array("CODE" => "PRICE"),
	"CURRENCY" => array("CODE" => "CURRENCY"),
	"CATALOG_GROUP_ID" => array("CODE" => "CATALOG_GROUP_ID"),
	"PRODUCT_ID" => array("CODE" => "XML_ID", "FUNC" => array($writer, "getIDByXmlID"))
);
$writer->setMap($map);
/*
 * Устанавливаем воркеру writer
 */
$worker->setWriter($writer);
$worker->oneStep = true;
$worker->dropFileAfterRead = true;
$worker->run();
