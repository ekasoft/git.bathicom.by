<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Sam\Sync\ImportWorker;
use Sam\Sync\Parsers\XMLParser;
use Sam\Sync\Writers\CatalogWriter;

$importID = "quantity";
$logger = new Logger($importID);
$logger->pushHandler(new RotatingFileHandler(SAM_SYNC_LOG_PATH . "{$importID}.log", 10,Logger::DEBUG));
Monolog\ErrorHandler::register($logger);

/*
 * Создаём парсер
 */
$parser = new XMLParser();
$parser->setLogger($logger);
/*
 * Указываем источник
 */
$parser->setSource($_SERVER["DOCUMENT_ROOT"] . "/upload/exchange/exch/balances.xml", array("root_xpath" => "/balances/tovar"));
/*
 * Указываем карту полей источник->таблица
 */
$map = array(
    "code" => array("CODE" => "PRODUCT_XML_ID"),
    "balance" => array("CODE" => "COUNT"),

);
$parser->setMap($map);
/*
 * Создаём воркер
 */
$worker = new ImportWorker();
$worker->setTableName("b_import_quantity");
$worker->setImportID($importID);
$worker->setLogger($logger);
/*
 * Устанавливаем воркеру парсер
 */
$worker->setParser($parser);
/*
 * Создаём writer
 */
$writer = new CatalogWriter();
$writer->setLogger($logger);
/*
 * Указываем карту таблица->приёмник
 */
$map = array(
    "COUNT" => array("CODE" => "COUNT"),
    "PRODUCT_ID" => array("CODE" => "PRODUCT_XML_ID", "FUNC" => array($writer, "getIDByXmlID")),
);
$writer->setMap($map);
/*
 * Устанавливаем воркеру writer
 */
$worker->setWriter($writer);
$worker->oneStep = true;
$worker->dropFileAfterRead = true;
$worker->run();
