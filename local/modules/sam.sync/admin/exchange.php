<?
define("CLI", empty($_SERVER["DOCUMENT_ROOT"]));

if (CLI) {
	$_SERVER["DOCUMENT_ROOT"] = "/home/bitrix/www";
	$_REQUEST = array();
	parse_str(urldecode($argv[1]), $_REQUEST);
	$_REQUEST["CLI"] = CLI;

	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
	CModule::IncludeModule("sam.sync");
}

$noAuth = CLI;

$exchange = new Sam\Sync\Exchange();
$exchange->defineConstants();
$exchange->initLogger();

$exchange->checkMode();
$exchange->doAuth();

if ($_REQUEST["mode"] == "checkauth") {
	$exchange->getSession();
}
$APPLICATION->RestartBuffer();

if ($_REQUEST["mode"] == "import" && ($exchange->checkAuth() || $noAuth)) {
	switch ($_REQUEST["type"]) {
		case "goods":
			require_once "goods_work.php";
			require_once "product_work.php";
			break;
		case "price":
			require_once "price_work.php";
			break;
		case "balance":
			require_once "quantity_work.php";
			break;
		case "sections":
			require_once "section_work.php";
			break;
		case "picture":
			require_once "pictures_work.php";
			break;
		default:
			echo "failure\nневерный тип импорта";
			die();
			break;
	}
}

?>
