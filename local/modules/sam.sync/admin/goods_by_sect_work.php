<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Sam\Sync\ImportWorker;
use Sam\Sync\Parsers\CSVParser;
use Sam\Sync\Parsers\XMLParser;
use Sam\Sync\Writers\IblockWriter;

function tileFilter($value) {
	return !in_array($value, array("Мебель для ванной", "Плитка"));
}

$importID = "goods_by_sect";
$logger = new Logger($importID);
$logger->pushHandler(new RotatingFileHandler(SAM_SYNC_LOG_PATH . "{$importID}.log", 10,Logger::DEBUG));
Monolog\ErrorHandler::register($logger);

/*
 * Создаём парсер
 */
$parser = new XMLParser();
$parser->setLogger($logger);
/*
 * Указываем источник
 */

$parser->setSource($_SERVER["DOCUMENT_ROOT"] . "/upload/exch/goodsbysections.xml", array("root_xpath" => "/Tovars/Tovar"));
/*
 * Указываем карту полей источник->таблица
 */
$map = array(
	"Code_Tovar" => array("CODE" => "XML_ID"),
	"PodKategorii_Code" => array("CODE" => "PARENT_XML_ID"),

);
$parser->setMap($map);
/*
 * Создаём воркер
 */
$worker = new ImportWorker();
$worker->setTableName("b_import_goods_by_sect");
$worker->setImportID($importID);
$worker->setLogger($logger);
/*
 * Устанавливаем воркеру парсер
 */
$worker->setParser($parser);
/*
 * Создаём writer
 */
$writer = new IblockWriter(10);
$writer->setLogger($logger);
/*
 * Указываем карту таблица->приёмник
 */
$map = array(
	"XML_ID" => array("CODE" => "XML_ID"),
	"PARENT_XML_ID" => array("CODE" => "IBLOCK_SECTION_ID", "FUNC" => array($writer, "getIDByXmlID"))
);
$writer->setMap($map);
/*
 * Устанавливаем воркеру writer
 */
$worker->setWriter($writer);
$worker->run();
