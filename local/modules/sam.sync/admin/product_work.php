<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Sam\Sync\ImportWorker;
use Sam\Sync\Parsers\CSVParser;
use Sam\Sync\Parsers\XMLParser;
use Sam\Sync\Writers\IblockWriter;

function tileTpFilter($value) {
	return in_array($value, array("Мебель для ванной", "Плитка"));
}

function prepareFloat($value) {
	return str_replace(",", ".", $value);
}

$importID = "product";
$logger = new Logger($importID);
$logger->pushHandler(new RotatingFileHandler(SAM_SYNC_LOG_PATH . "{$importID}.log", 10,Logger::DEBUG));
Monolog\ErrorHandler::register($logger);

/*
 * Создаём парсер
 */
$parser = new XMLParser();
$parser->setLogger($logger);
/*
 * Указываем источник
 */

$parser->setSource($_SERVER["DOCUMENT_ROOT"] . "/upload/exchange/exch/goods.xml", array("root_xpath" => "/goods/tovar"));
/*
 * Указываем карту полей источник->таблица
 */
$map = array(
	"code" => array("CODE" => "XML_ID"),
	"active" => array("CODE" => "ACTIVE"),
	"name" => array("CODE" => "NAME"),
	"preview_text" => array("CODE" => "PREVIEW_TEXT"),
	"detailed_name" => array("CODE" => "DETAIL_TEXT"),
	"trademark" => array("CODE" => "TRADEMARK"),
	"collection" => array("CODE" => "COLLECTION"),
	//"special_offer" => array("CODE" => "SPECIAL_OFFER"),
	"article" => array("CODE" => "ARTICLE"),
	//"sets" => array("CODE" => "SETS"),
	//"IP_PROP46" => array("CODE" => "IP_PROP46"),
	"width" => array("CODE" => "WIDTH"),
	"color" => array("CODE" => "COLOR"),
	"color_code" => array("CODE" => "COLOR_CODE"),
	//"in_stock" => array("CODE" => "IN_STOCK"),
	"length" => array("CODE" => "LENGTH"),
	"height" => array("CODE" => "HEIGHT"),
	"depth" => array("CODE" => "DEPTH"),
	"name_supplier" => array("CODE" => "NAME_SUPPLIER"),
	//"IP_POST72" => array("CODE" => "IP_POST72"),
	"address_producer" => array("CODE" => "ADDRESS_PRODUCER"),
	"main_unit" => array("CODE" => "MAIN_UNIT"),
	"category" => array("CODE" => "CATEGORY", "FILTER" => "tileTpFilter"),
	"hide" => array("CODE" => "DELETE"),
	"pc_area" => array("CODE" => "COEFFICIENT", "FUNC" => "prepareFloat")
);
$parser->setMap($map);
/*
 * Создаём воркер
 */
$worker = new ImportWorker();
$worker->setTableName("b_import_tp");
$worker->setImportID($importID);
$worker->setLogger($logger);
/*
 * Устанавливаем воркеру парсер
 */
$worker->setParser($parser);
/*
 * Создаём writer
 */
$writer = new IblockWriter(11);
$writer->setLogger($logger);
/*
 * Указываем карту таблица->приёмник
 */
$map = array(
	"XML_ID" => array("CODE" => "XML_ID"),
	"ACTIVE" => array("CODE" => "ACTIVE"),
	"NAME" => array("CODE" => "NAME"),
	"PREVIEW_TEXT" => array("CODE" => "PREVIEW_TEXT"),
	"TRADEMARK" => array("CODE" => "BRAND", "IS_PROPERTY" => true),
	"COLLECTION" => array("CODE" => "COLLECTION_REF", "IS_PROPERTY" => true),
	"ARTICLE" => array("CODE" => "ARTNUMBER", "IS_PROPERTY" => true),
	"WIDTH" => array("CODE" => "WIDTH", "IS_PROPERTY" => true),
	"COLOR" => array("CODE" => "COLOR", "IS_PROPERTY" => true),
	"COLOR_CODE" => array("CODE" => "COLOR_CODE", "IS_PROPERTY" => true),
	"LENGTH" => array("CODE" => "LENGTH", "IS_PROPERTY" => true),
	"HEIGHT" => array("CODE" => "HEIGHT", "IS_PROPERTY" => true),
	"DEPTH" => array("CODE" => "DEPTH", "IS_PROPERTY" => true),
	"NAME_SUPPLIER" => array("CODE" => "NAME_SUPPLIER", "IS_PROPERTY" => true),
	"ADDRESS_PRODUCER" => array("CODE" => "ADDRESS_PRODUCER", "IS_PROPERTY" => true),
	"MAIN_UNIT" => array("CODE" => "MEASURE", "IS_PROPERTY" => true),
	"COEFFICIENT" => array("CODE" => "COEFFICIENT", "IS_PROPERTY" => true),
	"DELETE" => array("CODE" => "DELETE")
);
$writer->setMap($map);
/*
 * Устанавливаем воркеру writer
 */
$worker->setWriter($writer);
$worker->oneStep = true;
$worker->dropFileAfterRead = true;
$worker->run();
