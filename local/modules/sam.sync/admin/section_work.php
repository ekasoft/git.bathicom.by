<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Sam\Sync\ImportWorker;
use Sam\Sync\Parsers\XMLParser;
use Sam\Sync\Writers\SectionWriter;

$importID = "section";
$logger = new Logger($importID);
$logger->pushHandler(new RotatingFileHandler(SAM_SYNC_LOG_PATH . "{$importID}.log", 10,Logger::DEBUG));

Monolog\ErrorHandler::register($logger);

//Преобразовываем файл
require "xmlreformat.php";
/*
 * Создаём парсер
 */
$parser = new XMLParser();
$parser->setLogger($logger);
/*
 * Указываем источник
 */
$parser->setSource($_SERVER["DOCUMENT_ROOT"] . "/upload/exchange/exch/sections.xml", array("root_xpath" => "/root/category"));
/*
 * Указываем карту полей источник->таблица
 */
$map = array(
	"xml_id" => array("CODE" => "XML_ID"),
	"name" => array("CODE" => "NAME"),
	"parent_xml_id" => array("CODE" => "PARENT_XML_ID"),
	array("CODE" => "ACTIVE", "VALUE" => "Y")

);
$parser->setMap($map);
/*
 * Создаём воркер
 */
$worker = new ImportWorker();
$worker->setTableName("b_import_section");
$worker->setImportID($importID);
$worker->setLogger($logger);
/*
 * Устанавливаем воркеру парсер
 */
$worker->setParser($parser);
/*
 * Создаём writer
 */
$writer = new SectionWriter(10);
$writer->setLogger($logger);
/*
 * Указываем карту таблица->приёмник
 */
$map = array(
	"NAME" => array("CODE" => "NAME"),
	"XML_ID" => array("CODE" => "XML_ID"),
	"ACTIVE" => array("CODE" => "ACTIVE"),
	//"IBLOCK_SECTION_ID" => array("CODE" => "PARENT_XML_ID", "FUNC" => array($writer, "getIDByXmlID"))
);
$writer->setMap($map);
/*
 * Устанавливаем воркеру writer
 */
$worker->setWriter($writer);
$worker->oneStep = true;
$worker->dropFileAfterRead = true;
$worker->run();

//Удаляем исходный файл
unlink($_SERVER["DOCUMENT_ROOT"] . "/upload/exchange/exch/KATEGORII.xml");
