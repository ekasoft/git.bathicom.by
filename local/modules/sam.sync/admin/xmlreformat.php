<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

$source = new \SimpleXMLElement(file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/exchange/exch/KATEGORII.xml"));



$allCategories = array();
$source = json_decode(json_encode($source), true);
foreach ($source['Kategoria'] as $category) {
    $parentXMLID = "1" . $category['Code_Tovar'];

    $allCategories[] = array(
        "name" => $category['Naimenovanie'],
        "xml_id" => $parentXMLID,
        "parent_xml_id" => ""
    );

    foreach ($category['POD_KATEGORIY']['pod_Kategorii_Code'] as $k => $name) {
        $xmlID = $category['POD_KATEGORIY']['Pod_Kategorii_Naimenovanie'][$k];
        $allCategories[] = array(
            "name" => $name,
            "xml_id" => $xmlID,
            "parent_xml_id" => $parentXMLID
        );
    }
}

$dom = new DOMDocument("1.0");
$dom->preserveWhiteSpace = false;
$dom->encoding = "UTF-8";
$dom->formatOutput = true;

$root = $dom->createElement("root");
$dom->appendChild($root);

foreach ($allCategories as $category) {
    $categoryNode = $dom->createElement("category");
    foreach ($category as $nodeName => $value) {
        $categoryNode->appendChild($dom->createElement($nodeName, $value));
    }

    $root->appendChild($categoryNode);
}

$APPLICATION->RestartBuffer();

file_put_contents($_SERVER["DOCUMENT_ROOT"] . "/upload/exchange/exch/sections.xml", $dom->saveXML());
