<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Sam\Sync\ImportWorker;
use Sam\Sync\Parsers\XMLParser;
use Sam\Sync\Parsers\XMLPictureParser;
use Sam\Sync\Writers\IblockWriter;

function extractProdFromHtml($val) {
	preg_match("#>(.+)</a>#", $val, $matches);

	return $matches[1];
}

function resolvePicture($val) {
	$path = $_SERVER["DOCUMENT_ROOT"] . "/upload/exchange/exch/" . $val;
	if (!is_file($path)) return false;

	return CFile::MakeFileArray($path);
}

$importID = "picture";
$logger = new Logger($importID);
$logger->pushHandler(new RotatingFileHandler(SAM_SYNC_LOG_PATH . "{$importID}.log", 10,Logger::DEBUG));
Monolog\ErrorHandler::register($logger);

/*
 * Создаём парсер
 */
$parser = new XMLPictureParser();
$parser->setLogger($logger);
/*
 * Указываем источник
 */

$parser->setSource($_SERVER["DOCUMENT_ROOT"] . "/upload/exchange/exch/pictures.xml", array("root_xpath" => "/pictures/tovar"));
/*
 * Указываем карту полей источник->таблица
 */
$map = array(
	"code" => array("CODE" => "XML_ID"),
	"picture_code" => array("CODE" => "PICTURE"),
	array("CODE" => "PICTURE_1", "IGNORE" => true),
	array("CODE" => "PICTURE_2",  "IGNORE" => true),
	array("CODE" => "PICTURE_3",  "IGNORE" => true),
	array("CODE" => "PICTURE_4",  "IGNORE" => true),

);
$parser->setMap($map);
/*
 * Создаём воркер
 */
$worker = new ImportWorker();
$worker->setTableName("b_import_picture");
$worker->setImportID($importID);
$worker->setLogger($logger);
/*
 * Устанавливаем воркеру парсер
 */
$worker->setParser($parser);
/*
 * Создаём writer
 */
$writer = new IblockWriter();
$writer->setLogger($logger);
/*
 * Указываем карту таблица->приёмник
 */
$map = array(
	"IBLOCK_ID" => array("CODE" => "IBLOCK_ID", "GENERATOR" => array($writer,"getIblockIDByXmlID")),
	"XML_ID" => array("CODE" => "XML_ID"),
	"PICTURE" => array("CODE" => "PREVIEW_PICTURE", "FUNC" => "resolvePicture"),
	"PICTURE_1" => array("CODE" => "DETAIL_PICTURE", "FUNC" => "resolvePicture"),
	"PICTURE_2" => array("CODE" => "MORE_PHOTO", "FUNC" => "resolvePicture", "IS_PROPERTY" => true, "MULTIPLE" => true),
	"PICTURE_3" => array("CODE" => "MORE_PHOTO", "FUNC" => "resolvePicture", "IS_PROPERTY" => true, "MULTIPLE" => true),
	"PICTURE_4" => array("CODE" => "MORE_PHOTO", "FUNC" => "resolvePicture", "IS_PROPERTY" => true, "MULTIPLE" => true),
);
$writer->setMap($map);
/*
 * Устанавливаем воркеру writer
 */
$worker->setWriter($writer);
$worker->oneStep = true;
$worker->dropFileAfterRead = true;

//Извлекаем картинки из архива

shell_exec("unzip -o ~/www/upload/exchange/exch/goods_files.zip -d ~/www/upload/exchange/exch/goods_files");
$worker->run();
