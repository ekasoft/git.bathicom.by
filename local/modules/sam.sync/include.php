<?

define("SAM_SYNC_MODULE_NAME", "sam.sync");
define("SAM_SYNC_MODULE_PATH", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . SAM_SYNC_MODULE_NAME . "/");
define("SAM_SYNC_LOG_PATH", $_SERVER["DOCUMENT_ROOT"] . "/local/modules/" . SAM_SYNC_MODULE_NAME . "/admin/logs/");

\Bitrix\Main\Loader::IncludeModule("iblock");
\Bitrix\Main\Loader::IncludeModule("catalog");
\Bitrix\Main\Loader::IncludeModule("sale");

require_once SAM_SYNC_MODULE_PATH . "vendor/autoload.php";

