<?
class CPriceEvents
{
    const DEFAULT_CURRENCY = "BYR";

    public static function OnOffersPriceUpdate($ID, $arFields) {
        self::saveMinPriceByOfferID($ID);
    }

    public static function OnOffersPriceAdd($ID, $arFields) {
        self::saveMinPriceByOfferID($ID);
    }

    private static function saveMinPriceByOfferID($ID) {
        if (empty($ID)) return;

        $offerPrice = CPrice::GetByID($ID);

        if (empty($offerPrice)) return;

        $filter = array("ID" => $offerPrice["PRODUCT_ID"], "IBLOCK_ID" => 11);
        $select = array("ID", "IBLOCK_ID", "PROPERTY_CML2_LINK", "ACTIVE" => "Y");
        $offer = CIBlockElement::GetList(false, $filter, false, false, $select)->Fetch();
        $productID = $offer["PROPERTY_CML2_LINK_VALUE"];

        if (empty($productID)) return;

        $filter = array("IBLOCK_ID" => 11, "PROPERTY_CML2_LINK" => $productID);
        $select = array("ID", "CATALOG_GROUP_1");

        $dbl = CIBlockElement::GetList(false, $filter, false, false, $select);
        while ($res = $dbl->Fetch()) {
            if (ceil($res["CATALOG_PRICE_1"])) {
                if ($res["CATALOG_CURRENCY"] != self::DEFAULT_CURRENCY) {
                    $res["CATALOG_PRICE_1"] =  CCurrencyRates::ConvertCurrency($res["CATALOG_PRICE_1"], $res["CATALOG_CURRENCY_1"], self::DEFAULT_CURRENCY);
                }
                $offers[$res["ID"]] = $res["CATALOG_PRICE_1"];
            }
        }

        $minPrice = min($offers);

        CIBlockElement::SetPropertyValuesEx($productID, 10, array("PRICE" => $minPrice));
    }
}