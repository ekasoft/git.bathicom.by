<?
require_once dirname(__FILE__) . "/include/constants.php";
require_once dirname(__FILE__) . "/include/tools.php";
require_once dirname(__FILE__) . "/include/eventlist.php";
/**
 * Created by PhpStorm.
 * Developer: SKUBACH, 2015
 */

use \Bitrix\Main\Page\Asset;
use \Bitrix\Highloadblock as HL;
use \Bitrix\Main\Entity;

define("GOODS_IBLOCK", 2);
define("OFFERS_IBLOCK", 3);
define("SERVICES_IBLOCK", 4);
define("LIFT_HIGHLOAD_IBLOCK", 5);
define("COLOR_HIGHLOAD_IBLOCK", 6);
define("ADDRESS_IBLOCK", 9);
define("RASSROCH_MONTH", 3);
define("FIRST_PAY_PERCENT", 20);
//Asset::getInstance()->moveJs('main');
\CModule::includeModule("ekasoft.base");
\EkaSoft\Base\Core\Process::run();
/*
AddEventHandler("sale", "OnBeforeOrderAdd", "OnBeforeOrderAddHandler");
// округляем цену до сотен в белорусских рублях в заказе
function OnBeforeOrderAddHandler(&$arFields)
{
    if ($arFields['CURRENCY'] == "BYR"){
        $arFields['PRICE'] = round($arFields['PRICE'], -2);
    }
}*/

/*
AddEventHandler("currency", "CurrencyFormat", "CurrencyFormatHandler");
// округляем цену до сотен в белорусских рублях
function CurrencyFormatHandler($price, $currency)
{
    CModule::IncludeModule('currency');
    if ($currency == "BYR"){
        $price = round($price, -2);
    }

    if (!isset($price) || $price === '')
        return '';

    $currency = Bitrix\Currency\CurrencyManager::checkCurrencyID($currency);
    if ($currency === false)
        return '';

    $arCurFormat = CAllCurrencyLang::GetFormatDescription($currency);
    $intDecimals = $arCurFormat['DECIMALS'];
    if (CAllCurrencyLang::isAllowUseHideZero() && $arCurFormat['HIDE_ZERO'] == 'Y')
    {
        if (round($price, $arCurFormat["DECIMALS"]) == round($price, 0))
            $intDecimals = 0;
    }
    $price = number_format($price, $intDecimals, $arCurFormat['DEC_POINT'], $arCurFormat['THOUSANDS_SEP']);
    if ($arCurFormat['THOUSANDS_VARIANT'] == CAllCurrencyLang::SEP_NBSPACE)
        $price = str_replace(' ', '&nbsp;', $price);

    return str_replace('#', $price, $arCurFormat['FORMAT_STRING']);
}*/

// разбивает price, на цену и код валюты
if (!function_exists("getPriceCode")) {
    function getPriceCode($price, $currencyCode, $currencyLang = false)
    {
        if (!$currencyLang) {
            if (CModule::IncludeModule('currency')) {
                $currencyLang = CCurrencyLang::GetByID($currencyCode, LANGUAGE_ID);
            }
        }
        $currencyCode = str_replace("#", "", $currencyLang['FORMAT_STRING']);
        return array(
            "price" => str_replace($currencyCode, "", $price),
            "code" => $currencyCode,
            "currencyLang" => $currencyLang
        );
    }
}

// получить список услуг для категории
if (!function_exists("getServicesBySectionId")) {
    function getServicesBySectionId($sectionId)
    {
        CModule::IncludeModule('catalog');
        $db_list = CIBlockSection::GetList(array(), array(
            "IBLOCK_ID" => GOODS_IBLOCK, // инфоблок товаров
            'ID' => $sectionId // id категории
        ), false, array("UF_SERVICES"));
        if ($ar_result = $db_list->GetNext()) {
            if (!empty($ar_result['UF_SERVICES'])) {
                $res = CIBlockElement::GetList(array(
                    "SORT" => "ASC",
                    "PROPERTYSORT_RISE_ON_THE_FLOOR" => "ASC"
                ), array(
                    "IBLOCK_ID" => SERVICES_IBLOCK, // инфоблок услуг
                    "ACTIVE" => "Y",
                    "ID" => $ar_result['UF_SERVICES']
                ), false, false, array());
                $services = array();
                while ($ob = $res->GetNextElement()) {
                    $arFields = $ob->GetFields();
//                    $arProperties = $ob->GetProperties();
                    $arProduct = CCatalogProduct::GetByIDEx($arFields["ID"]);
//                    $arFields['PROPERTIES'] = $arProperties;
//                    $arFields['PRODUCT'] = $arProduct;
                    $services[] = $arProduct;
//                    $services[] = $arFields;
                }
                return $services;
            }
        }
        return false;
    }
}

// получаем список для услуги Подьем
if (!function_exists("getLiftServices")) {
    function getLiftServices($id_key = false)
    {
        if (CModule::IncludeModule("highloadblock")) {
            $hlblock = HL\HighloadBlockTable::getById(LIFT_HIGHLOAD_IBLOCK)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $entity_table_name = $hlblock['TABLE_NAME'];

            $sTableID = 'tbl_' . $entity_table_name;
            $rsData = $entity_data_class::getList(array(
                "select" => array('*'), //выбираем все поля
                "filter" => array(),
                "order" => array('UF_SORT' => "ASC") // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
            ));
            $rsData = new CDBResult($rsData, $sTableID);
            $lift = array();
            while ($arRes = $rsData->Fetch()) {
                if ($id_key) {
                    $lift[$arRes['ID']] = $arRes;
                } else {
                    $lift[] = $arRes;
                }
            }
            return $lift;
        }
        return false;
    }
}

// получаем список Color
if (!function_exists("getColors")) {
    function getColors()
    {
        if (CModule::IncludeModule("highloadblock")) {
            $hlblock = HL\HighloadBlockTable::getById(COLOR_HIGHLOAD_IBLOCK)->fetch();
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entity_data_class = $entity->getDataClass();
            $entity_table_name = $hlblock['TABLE_NAME'];

            $sTableID = 'tbl_' . $entity_table_name;
            $rsData = $entity_data_class::getList(array(
                "select" => array('*'), //выбираем все поля
                "filter" => array(),
                "order" => array('UF_SORT' => "ASC") // сортировка по полю UF_SORT, будет работать только, если вы завели такое поле в hl'блоке
            ));
            $rsData = new CDBResult($rsData, $sTableID);
            $colors = array();
            while ($arRes = $rsData->Fetch()) {
                $colors[$arRes['UF_XML_ID']] = $arRes;
            }
            return $colors;
        }
        return false;
    }
}

AddEventHandler('sale', 'OnBeforeBasketDelete', 'ddOnBeforeBasketDelete');
// удаляем услуги, закрепленные за товаром, при удалении товара из корзины
function ddOnBeforeBasketDelete($basketId)
{
    CModule::IncludeModule('sale');
    $dbProp = CSaleBasket::GetPropsList(
        array(),
        array(
            "BASKET_ID" => $basketId,
            "CODE" => "SERVICES"
        ),
        false,
        false,
        array('NAME', 'VALUE', 'CODE')
    );
    while ($arProp = $dbProp->Fetch()) {
        $servicesIds = explode(',', $arProp['VALUE']);
        $servicesIds = array_diff($servicesIds, array('', null));
        foreach ($servicesIds as $servicesId) {
            $serviceArr = CSaleBasket::GetByID($servicesId);
            if (intval($serviceArr['QUANTITY']) > 1) {
                $arFields = array(
                    "QUANTITY" => intval($serviceArr['QUANTITY']) - 1,
                );
                CSaleBasket::Update($servicesId, $arFields);
            } else {
                CSaleBasket::Delete($servicesId);
            }
        }
    }
}

if (!function_exists('getSortOfWord')) {
    function getSortOfWord($num, $let1, $let4, $let5, $let10)
    {
        if (((int)substr(number_format($num, 0, ".", ""), -2) > 9) and ((int)substr(number_format($num, 0, ".", ""), -2) < 21)) return $let10;
        if ((int)substr(number_format($num, 0, ".", ""), -1, 1) == 1) return $let1;
        if (((int)substr(number_format($num, 0, ".", ""), -1, 1) < 5) and ((int)substr(number_format($num, 0, ".", ""), -1, 1) > 1)) return $let4;
        if (((int)substr(number_format($num, 0, ".", ""), -1, 1) > 4) or ((int)substr(number_format($num, 0, ".", ""), -1, 1) == 0)) return $let5;
    }
}

if (!function_exists('getSectionColors')) {
    function getSectionColors()
    {
        $colors = array();
        $rsRes = CUserFieldEnum::GetList(
            array(),
            array(
                "SELECT" => array("UF_COLOR"),
            ));
        while ($arRes = $rsRes->GetNext()) {
            $colors[$arRes['ID']] = $arRes;
        }
        return $colors;
    }
}

function oldPrice($price,$formatted) {
    $price = floatval(str_replace(',', '.', $price));
    if($formatted) {
        $price = floatval(str_replace(' ', '', $price));
    }
    $price = $price*10000;
    $price = number_format($price, 0, '', ' ').' <span class="curr">руб.</span>';
    return $price;
}

function getSectionIdByCode($code,$iblock_id) {
    // Получаем слайды
    if($code && $iblock_id) {
        $arFilter = array('IBLOCK_ID' => $iblock_id, 'CODE' => $code);
        $arSelect = array('IBLOCK_ID','ID');
        $res = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
        $item = array();
        if($ob = $res->GetNextElement()) {
            $item = $ob->GetFields();
            return $item['ID'];
        }
    }
}


AddEventHandler("catalog", "OnPriceUpdate", array("MyClass","OnPriceUpdateHandler"));
//проверяем наличие цены при обновлении и деактивируем в случае отсутствия цены
class MyClass 
{ 
    function OnPriceUpdateHandler($id, $arFields)
    {
        CModule::IncludeModule('iblock');
        if($arFields['PRICE'] == 0){
            $arLoadProductArray = Array("ACTIVE" => "N");
        } else {
            $arLoadProductArray = Array("ACTIVE" => "Y");
        }
        CModule::IncludeModule('iblock');
        $el = new CIBlockElement;
        //$arLoadProductArray = Array("ACTIVE" => "N");
        $PRODUCT_ID = $arFields['PRODUCT_ID'];
        $res = $el->Update($PRODUCT_ID, $arLoadProductArray);

    }
}

AddEventHandler("catalog", "OnPriceDelete", array("PriceDeleteClass","OnPriceDeleteHandler"));
//деактивируем товар при удалении цены
class PriceDeleteClass 
{ 
    function OnPriceDeleteHandler($id)
    {
        $arPrice = CPrice::GetByID($id);
        CModule::IncludeModule('iblock');
        if($arPrice['PRODUCT_ID']){
            $el = new CIBlockElement;
            $arLoadProductArray = Array("ACTIVE" => "N");
            $PRODUCT_ID = $arPrice['PRODUCT_ID'];
            $res = $el->Update($PRODUCT_ID, $arLoadProductArray);
        }
    }
}

AddEventHandler("catalog", "OnPriceAdd", array("PriceAddClass","OnPriceAddHandler"));
//деактивируем товар при добавлении нулевой цены
class PriceAddClass 
{ 
    function OnPriceAddHandler($id, $arFields)
    {
        CModule::IncludeModule('iblock');
        if($arFields['PRICE'] == 0){
            $arLoadProductArray = Array("ACTIVE" => "N");
        } else {
            $arLoadProductArray = Array("ACTIVE" => "Y");
        }
        CModule::IncludeModule('iblock');
        $el = new CIBlockElement;
        $PRODUCT_ID = $arFields['PRODUCT_ID'];
        $res = $el->Update($PRODUCT_ID, $arLoadProductArray);
    }
}

/*
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("ElementAfterAddClass", "OnAfterIBlockElementAddHandler"));
class ElementAfterAddClass
{
    function OnAfterIBlockElementAddHandler($arFields){
        if($arFields && !$arFields['PRICE']){
            CModule::IncludeModule('iblock');
            $el = new CIBlockElement;
            $arLoadProductArray = Array("ACTIVE" => "N");
            $PRODUCT_ID = $arFields['PRODUCT_ID'];
            $res = $el->Update($PRODUCT_ID, $arLoadProductArray);
        }
    }
}*/


//AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("OptimClass", "CheckPriceTask"));
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("OptimClass", "CheckPriceTask"));
class OptimClass
{
  function CheckPriceTask(&$arFields){
    if($arFields['ID']){
        CModule::IncludeModule('catalog');
        $arFilter = array('PRODUCT_ID' => $arFields['ID']);
        $arSelect = array('ID','PRICE');
        $db_res = CPrice::GetList(false, $arFilter, false, false, $arSelect);
        if ($ar_res = $db_res->Fetch())
        {
            if(!$ar_res["PRICE"] || $ar_res["PRICE"] == 0){
                $el = new CIBlockElement;
                $arLoadProductArray = Array("ACTIVE" => "N");
                $PRODUCT_ID = $arFields['ID'];
                $res = $el->Update($PRODUCT_ID, $arLoadProductArray);
            }
        }else{
            $el = new CIBlockElement;
            $arLoadProductArray = Array("ACTIVE" => "N");
            $PRODUCT_ID = $arFields['ID'];
            $res = $el->Update($PRODUCT_ID, $arLoadProductArray);
        }
    }
  }  
}




AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("OneMoreClass", "OnAfterIBlockElementUpdateHandler"));
class OneMoreClass
{
    // создаем обработчик события "OnAfterIBlockElementUpdate"
    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
        if($arFields['IBLOCK_ID'] == 11 && !$arFields['IBLOCK_SECTION']['0']){
            $arFilter = array('XML_ID' => 'CML2_LINK');
            $db_props = CIBlockElement::GetProperty(11, $arFields['ID'], array("sort" => "asc"), $arFilter);
            if($ar_props = $db_props->Fetch()){
                if($ar_props['VALUE']){
                    $res = CIBlockElement::GetByID($ar_props['VALUE']);
                    if($ar_res = $res->GetNext()){
                        if($ar_res['IBLOCK_SECTION_ID']){
                            $res = CIBlockSection::GetByID($ar_res['IBLOCK_SECTION_ID']);
                            if($my_res = $res->GetNext()){
                                if($my_res['CODE']){
                                    $arFilter = Array('IBLOCK_ID'=>11, 'CODE'=>$my_res['CODE']);
                                    $arSelect = array('ID');
                                    $db_list = CIBlockSection::GetList(false, $arFilter, true, $arSelect);
                                    if($ar_result = $db_list->GetNext())
                                    {
                                        $el = new CIBlockElement;
                                        $arLoadProductArray = Array("IBLOCK_SECTION" => $ar_result['ID']);
                                        $PRODUCT_ID = $arFields['ID'];
                                        $res = $el->Update($PRODUCT_ID, $arLoadProductArray);
                                    }
                                }
                            }            
                        }
                    }
                }
            }
        }
    }
}

AddEventHandler("sale", "OnOrderSave", array("OrderClass","OnOrderSaveHandler"));
//создание нового заказа
class OrderClass
{
    function OnOrderSaveHandler($orderId, $arFields, $arOrder, $isNew){
        if($isNew){
            
            //запрашиваем данные о способе доставки
            $arDelivery = array();
            if($arFields['DELIVERY_ID']){
                $arDeliv = CSaleDelivery::GetByID($arFields['DELIVERY_ID']);
                if ($arDeliv)
                {
                    $arDelivery = $arDeliv;
                } 
            }   
            //запрашиваем свойства товаров из корзины
            foreach($arOrder['BASKET_ITEMS'] as $k => $v){
                $ids[] = $v['PRODUCT_ID'];
            }
            $arFilter = Array("ID" => $ids);
            $res = CIBlockElement::GetList(Array(), $arFilter);
            while($ob = $res->GetNextElement())
            {
                $arFee = $ob->GetFields();
                $arProps = $ob->GetProperties();
                $basketItems[$arFee['ID']] = $arFee;
                if($arFee['IBLOCK_SECTION_ID'] && $arFee['IBLOCK_ID']){
                    $arNavSelect = array('IBLOCK_SECTION_ID','ID','NAME','CODE');
                    $nav = CIBlockSection::GetNavChain($arFee['IBLOCK_ID'],$arFee['IBLOCK_SECTION_ID'],$arNavSelect);
                    if($cout = $nav->GetNext()){
                        $basketItems[$arFee['ID']]['SECTION_FIRST'] = $cout['CODE'];
                    }
                }
                $basketItems[$arFee['ID']]['PROPS'] = $arProps;
            }
            
            $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . '/upload/exchange/orders/'.$orderId.'.xml', "w") or die("Unable to open file!");
                $txt = '<?xml version="1.0" encoding="UTF-8"?>';
                $txt .= '<order>';
                    $txt .= '<id>'.$orderId.'</id>';
                    
                    //данные о покупателе
                    $txt .= '<client>';
                        $txt .= '<name>'.$arOrder['ORDER_PROP'][20].'</name>';
                        $txt .=	'<lastname>'.$arOrder['ORDER_PROP'][1].'</lastname>';
                        $txt .=	'<patronymic>'.$arOrder['ORDER_PROP'][21].'</patronymic>';
                        $txt .=	'<email>'.$arOrder['ORDER_PROP'][2].'</email>';
                        $txt .=	'<phone>'.$arOrder['ORDER_PROP'][3].'</phone>';
                        $txt .=	'<street>'.$arOrder['ORDER_PROP'][22].'</street>';
                        $txt .=	'<building>'.$arOrder['ORDER_PROP'][23].'</building>';
                        $txt .=	'<housing>'.$arOrder['ORDER_PROP'][25].'</housing>';
                        $txt .=	'<porch>'.$arOrder['ORDER_PROP'][26].'</porch>';
                        $txt .=	'<flat>'.$arOrder['ORDER_PROP'][24].'</flat>';

                        //данные о доставке
                        $txt .=	'<delivery>';
                            $txt .=	'<id>'.$arFields['DELIVERY_ID'].'</id>';
                            $txt .=	'<name>'.$arDelivery['NAME'].'</name>';
                            $txt .=	'<price>'.$arDelivery['PRICE'].'</price>';
                            $txt .=	'<currency>'.$arDelivery['CURRENCY'].'</currency>';
                        $txt .= '</delivery>';

                        $txt .=	'<delivery_time_from>'.$arOrder['ORDER_PROP'][28].'</delivery_time_from>';
                        $txt .=	'<delivery_time_to>'.$arOrder['ORDER_PROP'][29].'</delivery_time_to>';
                        $txt .=	'<comment>'.$arOrder['USER_DESCRIPTION'].'</comment>';
                        $txt .=	'<sum>'.$arFields['PRICE'].'</sum>';
                    $txt .= '</client>';

                    // данные о товарах в заказе
                    $txt .= '<products>';			
                        foreach ($arOrder['BASKET_ITEMS'] as $k => $v){
                                $txt .=	'<product>';
                                        $txt .=	'<id>'.$basketItems[$v['PRODUCT_ID']]['XML_ID'].'</id>'; // возможно нужен внешний код - уточнить
                                        $txt .=	'<name>'.str_replace('&', '&amp;', $v['NAME']).'</name>';
                                        $txt .=	'<quantity>'.$v['QUANTITY'].'</quantity>';
                                        $txt .=	'<price>'.$v['PRICE'].'</price>';
                                        $txt .=	'<currency>'.$v['CURRENCY'].'</currency>';
                                $txt .= '</product>';
                        }				
                    $txt .= '</products>';
                $txt .= '</order>';
                
            fwrite($myfile, $txt);
            fclose($myfile);
            /*
            $newfile = fopen($_SERVER['DOCUMENT_ROOT'] . '/upload/exchange/orders/'.$orderId.'_products_characteristics.xml', "w") or die("Unable to open file!");
            $output = '<?xml version="1.0" encoding="UTF-8"?>';
            $output .= '<order_products_characteristics>';
            foreach($basketItems as $k=>$v){
                $output .= '<product>';
                    $output .= '<id>'.$v['XML_ID'].'</id>';
                    $output .= '<props>';
                        foreach ($v['PROPS'] as $key => $val){
                            $output .= '<prop>';
                                $output .= '<id>'.$val['ID'].'</id>';
                                $output .= '<name>'.str_replace('&', '&amp;', $val['NAME']).'</name>';
                                $output .= '<value>'.str_replace('&', '&amp;', $val['VALUE']).'</value>';
                            $output .= '</prop>';
                        }
                    $output .= '</props>';                   
                $output .= '</product>';
            }
            $output .= '</order_products_characteristics>';
            
            fwrite($newfile, $output);
            fclose($newfile);*/
            
            
            $lastfile = fopen($_SERVER['DOCUMENT_ROOT'] . '/upload/exchange/orders/'.$orderId.'_products.xml', "w") or die("Unable to open file!");
            $xml_res = '<?xml version="1.0" encoding="UTF-8"?>';
            $xml_res .= '<order_products>';
            foreach($basketItems as $k=>$v){
                if($v['ID'] == $v['EXTERNAL_ID']){
                    $xml_res .= '<product>';
                        $xml_res .= '<id>'.$v['ID'].'</id>';
                        $xml_res .= '<name>'.$v['NAME'].'</name>';
                        $xml_res .= '<name_supplier>'.$v['PROPS']['NAME_SUPPLIER']['VALUE'].'</name_supplier>';
                        $xml_res .= '<artnumber>'.$v['PROPS']['ARTNUMBER']['VALUE'].'</artnumber>';
                        $xml_res .= '<section>'.$v['SECTION_FIRST'].'</section>';
                    $xml_res .= '</product>';
                }
            }
            $xml_res .= '</order_products>';
            fwrite($lastfile, $xml_res);
            fclose($lastfile);
            
        }
    }
}

function prices($id,$isTP) {
    CModule::IncludeModule('catalog');
   
    if($isTP == 'Y') {
        CModule::IncludeModule('iblock');
        $arFilter = array('IBLOCK_ID' => 6, 'PROPERTY_CML2_LINK' => $id);
        $arSelect = array('IBLOCK_ID','ID','CODE');
        $res = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
        $itm = array();
        if($ob = $res->GetNextElement()) {
            $itm = $ob->GetFields();
            $id = $itm['ID'];
        }
    }

    // Запрос цен
    
    // Выведем все курсы, отсортированные по дате
    $db_rate = CCurrencyRates::GetList();
    while($ar_rate = $db_rate->Fetch()) {
        $rates[$ar_rate['CURRENCY']] = $ar_rate;
    }
   
    global $USER;
    $arPrice = CCatalogProduct::GetOptimalPrice($id, 1, $USER->GetUserGroupArray());
    $item['SHOW']['FORMAT'] = number_format($arPrice['DISCOUNT_PRICE'],2,'.',' ');
    $item['SHOW']['DISCOUNT'] = number_format($arPrice['RESULT_PRICE']['DISCOUNT'],2,'.',' ');
    $item['SHOW']['OLD_PRICE'] = number_format($arPrice['RESULT_PRICE']['BASE_PRICE'],2,'.',' ');
    $item['DISCOUNT'] = $arPrice['DISCOUNT'];
    if($arPrice['RESULT_PRICE']['PERCENT']){
        $item['DISCOUNT_PERCENT'] = number_format(floor($arPrice['RESULT_PRICE']['PERCENT']),0);
    }    
    
    return $item;
}