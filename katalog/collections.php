<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
?>
<section class="categories-section-e jsAjaxReplace">
    <div class="container">
        <div class="row insertAjax">
            <div class="col-md-12">
                <div class="main-h1"><h1><? $APPLICATION->ShowTitle(); ?></h1><!--span>28</span--></div>
            </div>
            <? 
                /*
                $arFilter = array('IBLOCK_ID' => 10, 'CODE' => $_REQUEST['SECTION_CODE']);
                $arSelect = array('IBLOCK_ID','ID');
                $res = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
                $item = array();
                if($ob = $res->GetNextElement()) {
                    $item = $ob->GetFields();
                }*/
            ?>
            <?
            $arFilter = Array('IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'CODE'=>$_REQUEST["SECTION_CODE"]);
            $arSelect = array('ID');
            $db_list = CIBlockSection::GetList(false, $arFilter, false, $arSelect);
            if($ar_result = $db_list->GetNext())
            {
                $iSection = $ar_result['ID'];
            } 
            //prent($iSection.'||',true);
            ?>
           
            <? //$iSection = \EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->getIDByCode($_REQUEST["SECTION_CODE"]); ?>
            <? //prent($iSection,true);?>
            <? //prent($_REQUEST); ?>
            
            <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.smart.filter", 
	".default", 
	array(
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"DISPLAY_ELEMENT_COUNT" => "Y",
		"FILTER_NAME" => "arrFilter",
		"FILTER_VIEW_MODE" => "vertical",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "catalog",
		"INSTANT_RELOAD" => "N",
		"PAGER_PARAMS_NAME" => "arrPager",
		"PRICE_CODE" => array(
                    0 => "BASE",
                ),
                "CONVERT_CURRENCY" => "Y",
                "CURRENCY_ID" => "BYR",
		"SAVE_IN_SESSION" => "N",
		"SECTION_CODE" => "",
		"SECTION_DESCRIPTION" => "-",
		"SECTION_ID" => $iSection,
		"SECTION_TITLE" => "-",
		"SEF_MODE" => "Y",
		"TEMPLATE_THEME" => "blue",
		"XML_EXPORT" => "N",
		"COMPONENT_TEMPLATE" => ".default",
		"POPUP_POSITION" => "right",
		"SEF_RULE" => "/katalog/".($_REQUEST['SECTION_CODE_1']?$_REQUEST['SECTION_CODE_1']."/":"")."#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		"SECTION_CODE_PATH" => "",
		"SMART_FILTER_PATH" => $_REQUEST['SMART_FILTER_PATH']
	),
	false
);?>
            
            
            <? //exit(); ?>
            <?/*<div class="col-md-3 hidden-xs" id="left-col-cat">
                <div class="full-filters-e">
                    <div class="block-filter">
                        <div class="title">Тип</div>
                        <div class="checkbox-e"><input type="checkbox" id="cb1" /> <label for="cb1">Прямоугольные раздвижные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb2" /> <label for="cb2">Полукрыглые распашные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb3" /> <label for="cb3">Прямоугольные распашные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb4" /> <label for="cb4">Квадратные раздвижные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb5" /> <label for="cb5">Прямоугольные распашные</label></div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Цена (минимальная)</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="от"><input type="text" name="" id="" placeholder="до">
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Производитель</div>
                        <div class="checkbox-e"><input type="checkbox" id="cb6" /> <label for="cb6">Armatura</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb7" /> <label for="cb7">Canela Spring</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb8" /> <label for="cb8">Cersanit</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb9" /> <label for="cb9">Cordivari</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb10" /> <label for="cb10">Emalia</label></div>
                        <div class="select-e">
                            <a href="#">Все 58 вариантов <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Производитель</div>
                        <div class="checkbox-e"><input type="checkbox" id="cb11" /> <label for="cb11">Armatura</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb12" /> <label for="cb12">Canela Spring</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb13" /> <label for="cb13">Cersanit</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb14" /> <label for="cb14">Cordivari</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb15" /> <label for="cb15">Emalia</label></div>
                        <div class="select-e">
                            <a href="#">Все 35 вариантов <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Высот.а, см</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="150"><input type="text" name="" id="" placeholder="240">
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Ширина, см</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="60"><input type="text" name="" id="" placeholder="220">
                        </div>
                    </div>
                    <div class="block-filter no-border">
                        <div class="title">Глубина, см</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="50"><input type="text" name="" id="" placeholder="160">
                        </div>
                    </div>
                    <div class="title2">Дополнительные параметры</div>
                    <div class="find-all">Найдено 568 товаров</div>
                    <div class="block-filter no-border">
                        <a href="#" class="bt-filter-reset">Сбросить все фильтры</a>
                    </div>
                </div>
            </div>*/?>
 
            
            <div class="col-md-9" id="right-col-cat">
                <?if($_REQUEST['SECTION_CODE']=='plitka' || $_REQUEST['SECTION_CODE_1']=='plitka'){?>

             <?/*       <div class="categories-filter-order-e">
                        <div class="choose-plitka firsten">
                            <div class="collect active"><a href="<?=($_REQUEST['SMART_FILTER_PATH'])?'/katalog/plitka/filter/'.$_REQUEST['SMART_FILTER_PATH'].'/apply/':'/katalog/plitka/';?>">Коллекции</a></div>
                            <div class="plitk"><a href="<?=($_REQUEST['SMART_FILTER_PATH'])?'/katalog/plitki/filter/'.$_REQUEST['SMART_FILTER_PATH'].'/apply/':'/katalog/plitki/';?>">Плитка</a></div>
                        </div>
                    </div>  */?>

                    <div class="categories-filter-order-e">
                        <div class="choose-plitka firsten">
                            <div class="collect active"><a href="#">Коллекции</a></div>
                            <div class="plitk"><a href="<?if($_REQUEST['SMART_FILTER_PATH']){
                            $arTemp = explode('filter/',$_SERVER['REQUEST_URI']);
                            echo str_replace('plitka','plitki',$arTemp[0]);
                            }else{
                            echo str_replace('plitka','plitki',$_SERVER['REQUEST_URI']);}?>">Плитка</a></div>
                        </div>
                    </div>

                <?}?>  
             
                <?if($_REQUEST['SECTION_CODE']=='mebel-dlya-vannoy' || $_REQUEST['SECTION_CODE_1']=='mebel-dlya-vannoy'){?>
                
      <?/*      <div class="categories-filter-order-e">
                    <div class="choose-plitka firsten">
                        <div class="collect active"><a href="<?=($_REQUEST['SMART_FILTER_PATH'])?'/katalog/mebel-dlya-vannoy/filter/'.$_REQUEST['SMART_FILTER_PATH'].'/apply/':'/katalog/mebel-dlya-vannoy/';?>">Коллекции</a></div>
                        <div class="plitk"><a href="<?=($_REQUEST['SMART_FILTER_PATH'])?'/katalog/bathroom-furniture/filter/'.$_REQUEST['SMART_FILTER_PATH'].'/apply/':'/katalog/bathroom-furniture/';?>">Товары</a></div>
                    </div>
                </div>  */?>
                <div class="categories-filter-order-e">
                    <div class="choose-plitka firsten">
                        <div class="collect active"><a href="#">Коллекции</a></div>
                        <div class="plitk"><a href="<?if($_REQUEST['SMART_FILTER_PATH']){
                        $arTemp = explode('filter/',$_SERVER['REQUEST_URI']);
                        echo str_replace('mebel-dlya-vannoy','bathroom-furniture',$arTemp[0]);
                        }else{
                        echo str_replace('mebel-dlya-vannoy','bathroom-furniture',$_SERVER['REQUEST_URI']);}?>">Товары</a></div>
                    </div>
                </div>
                
                
                <?}?>
                
               
                <?$APPLICATION->IncludeComponent("ekasoft.base:sections", "", Array("SECTION_ID" => $iSection));?>
                
                <?//$APPLICATION->IncludeComponent("ekasoft.base:sections", "", Array("SECTION_ID" => \EkaSoft\Base\Core\Structure::getInstance( \EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE )->getIDByCode($_REQUEST["SECTION_CODE"])));?>
                
                    <?
                $APPLICATION->IncludeComponent(
                        "ekasoft.base:catalog", "collections", Array("IBLOCK_ID" => 10), false
                );
                ?>
                
            </div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>