<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
//$APPLICATION->SetTitle("каталог");
?>
<section class="categories-section-e">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title"><? $APPLICATION->ShowTitle(); ?><!--span>28</span--></div>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "ekasoft", Array(
	"COMPONENT_TEMPLATE" => "visual_vertical",
		"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
		"IBLOCK_ID" => "10",	// Инфоблок
		"SECTION_ID" => \EkaSoft\Base\Core\Structure::getInstance( \EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE )->getIDByCode($_REQUEST["SECTION_CODE"]),	// ID раздела инфоблока
		"SECTION_CODE" => $_REQUEST["SECTION_CODE"],	// Код раздела
		"FILTER_NAME" => "arrFilter",	// Имя выходящего массива для фильтрации
		"HIDE_NOT_AVAILABLE" => "N",	// Не отображать товары, которых нет на складах
		"TEMPLATE_THEME" => "blue",	// Цветовая тема
		"DISPLAY_ELEMENT_COUNT" => "Y",	// Показывать количество
		"SEF_MODE" => "N",	// Включить поддержку ЧПУ
		"CACHE_TYPE" => "A",	// Тип кеширования
		"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
		"CACHE_GROUPS" => "Y",	// Учитывать права доступа
		"SAVE_IN_SESSION" => "N",	// Сохранять установки фильтра в сессии пользователя
		"INSTANT_RELOAD" => "N",	// Мгновенная фильтрация при включенном AJAX
		"PAGER_PARAMS_NAME" => "arrPager",	// Имя массива с переменными для построения ссылок в постраничной навигации
		"PRICE_CODE" => "",	// Тип цены
		"CONVERT_CURRENCY" => "N",	// Показывать цены в одной валюте
		"XML_EXPORT" => "N",	// Включить поддержку Яндекс Островов
		"SECTION_TITLE" => "-",	// Заголовок
		"SECTION_DESCRIPTION" => "-",	// Описание
		"FILTER_VIEW_MODE" => "vertical",
		"POPUP_POSITION" => "left",	// Позиция для отображения всплывающего блока с информацией о фильтрации
	),
	false
);?>
            <?/*<div class="col-md-3 hidden-xs" id="left-col-cat">
                <div class="full-filters-e">
                    <div class="block-filter">
                        <div class="title">Тип</div>
                        <div class="checkbox-e"><input type="checkbox" id="cb1" /> <label for="cb1">Прямоугольные раздвижные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb2" /> <label for="cb2">Полукрыглые распашные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb3" /> <label for="cb3">Прямоугольные распашные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb4" /> <label for="cb4">Квадратные раздвижные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb5" /> <label for="cb5">Прямоугольные распашные</label></div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Цена (минимальная)</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="от"><input type="text" name="" id="" placeholder="до">
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Производитель</div>
                        <div class="checkbox-e"><input type="checkbox" id="cb6" /> <label for="cb6">Armatura</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb7" /> <label for="cb7">Canela Spring</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb8" /> <label for="cb8">Cersanit</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb9" /> <label for="cb9">Cordivari</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb10" /> <label for="cb10">Emalia</label></div>
                        <div class="select-e">
                            <a href="#">Все 58 вариантов <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Производитель</div>
                        <div class="checkbox-e"><input type="checkbox" id="cb11" /> <label for="cb11">Armatura</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb12" /> <label for="cb12">Canela Spring</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb13" /> <label for="cb13">Cersanit</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb14" /> <label for="cb14">Cordivari</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb15" /> <label for="cb15">Emalia</label></div>
                        <div class="select-e">
                            <a href="#">Все 35 вариантов <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Высот.а, см</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="150"><input type="text" name="" id="" placeholder="240">
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Ширина, см</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="60"><input type="text" name="" id="" placeholder="220">
                        </div>
                    </div>
                    <div class="block-filter no-border">
                        <div class="title">Глубина, см</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="50"><input type="text" name="" id="" placeholder="160">
                        </div>
                    </div>
                    <div class="title2">Дополнительные параметры</div>
                    <div class="find-all">Найдено 568 товаров</div>
                    <div class="block-filter no-border">
                        <a href="#" class="bt-filter-reset">Сбросить все фильтры</a>
                    </div>
                </div>
            </div>*/?>
            <div class="col-md-9" id="right-col-cat">
                <?$APPLICATION->IncludeComponent("ekasoft.base:sections", "", Array("SECTION_ID" => \EkaSoft\Base\Core\Structure::getInstance( \EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE )->getIDByCode($_REQUEST["SECTION_CODE"])));?>
                <?
                $APPLICATION->IncludeComponent(
                        "ekasoft.base:catalog", "tovar", Array("IBLOCK_ID" => 10), false
                );
                ?></div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>