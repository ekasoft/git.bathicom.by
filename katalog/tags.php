<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Теги");
?>
<section class="categories-section-e jsAjaxReplace">
    <div class="container">
        <div class="row insertAjax">
            <div class="col-md-12">
                <div class="main-h1"><h1><? $APPLICATION->ShowTitle(); ?></h1><!--span>28</span--></div>
            </div>
            <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.smart.filter", 
	"ekasoft", 
	array(
		"COMPONENT_TEMPLATE" => "ekasoft",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "10",
		"SECTION_ID" => \EkaSoft\Base\Core\Structure::getInstance(\EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE)->getIDByCode(),
		"SECTION_CODE" => '',
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"TEMPLATE_THEME" => "blue",
		"DISPLAY_ELEMENT_COUNT" => "Y",
		"SEF_MODE" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_GROUPS" => "Y",
		"SAVE_IN_SESSION" => "N",
		"INSTANT_RELOAD" => "N",
		"PAGER_PARAMS_NAME" => "arrPager",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"CONVERT_CURRENCY" => "Y",
		"XML_EXPORT" => "N",
		"SECTION_TITLE" => "-",
		"SECTION_DESCRIPTION" => "-",
		"FILTER_VIEW_MODE" => "vertical",
		"POPUP_POSITION" => "left",
		"CURRENCY_ID" => "BYR"
	),
	false
);?>
            <?/*<div class="col-md-3 hidden-xs" id="left-col-cat">
                <div class="full-filters-e">
                    <div class="block-filter">
                        <div class="title">Тип</div>
                        <div class="checkbox-e"><input type="checkbox" id="cb1" /> <label for="cb1">Прямоугольные раздвижные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb2" /> <label for="cb2">Полукрыглые распашные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb3" /> <label for="cb3">Прямоугольные распашные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb4" /> <label for="cb4">Квадратные раздвижные</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb5" /> <label for="cb5">Прямоугольные распашные</label></div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Цена (минимальная)</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="от"><input type="text" name="" id="" placeholder="до">
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Производитель</div>
                        <div class="checkbox-e"><input type="checkbox" id="cb6" /> <label for="cb6">Armatura</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb7" /> <label for="cb7">Canela Spring</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb8" /> <label for="cb8">Cersanit</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb9" /> <label for="cb9">Cordivari</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb10" /> <label for="cb10">Emalia</label></div>
                        <div class="select-e">
                            <a href="#">Все 58 вариантов <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Производитель</div>
                        <div class="checkbox-e"><input type="checkbox" id="cb11" /> <label for="cb11">Armatura</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb12" /> <label for="cb12">Canela Spring</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb13" /> <label for="cb13">Cersanit</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb14" /> <label for="cb14">Cordivari</label></div>
                        <div class="checkbox-e"><input type="checkbox" id="cb15" /> <label for="cb15">Emalia</label></div>
                        <div class="select-e">
                            <a href="#">Все 35 вариантов <i class="fa fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Высот.а, см</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="150"><input type="text" name="" id="" placeholder="240">
                        </div>
                    </div>
                    <div class="block-filter">
                        <div class="title">Ширина, см</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="60"><input type="text" name="" id="" placeholder="220">
                        </div>
                    </div>
                    <div class="block-filter no-border">
                        <div class="title">Глубина, см</div>
                        <div class="tow-input">
                            <input type="text" name="" id="" placeholder="50"><input type="text" name="" id="" placeholder="160">
                        </div>
                    </div>
                    <div class="title2">Дополнительные параметры</div>
                    <div class="find-all">Найдено 568 товаров</div>
                    <div class="block-filter no-border">
                        <a href="#" class="bt-filter-reset">Сбросить все фильтры</a>
                    </div>
                </div>
            </div>*/?>
            <div class="col-md-9" id="right-col-cat">
                <?//$APPLICATION->IncludeComponent("ekasoft.base:sections", "", Array("SECTION_ID" => \EkaSoft\Base\Core\Structure::getInstance( \EkaSoft\Base\Core\Config::CACHE_ID_STRUCTURE )->getIDByCode()));?>
                <?
                $APPLICATION->IncludeComponent(
                        "ekasoft.base:catalog.tag", "collections", Array("IBLOCK_ID" => 10, "TAG_CODE" => $_REQUEST['ELEMENT_CODE']), false
                );
                ?></div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>