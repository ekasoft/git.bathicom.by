<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KFFNB3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KFFNB3');</script>
<!-- End Google Tag Manager -->

<script>
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}
function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
$(function() {
	var source = getUrlVars()["source"];
	if(source=='webmart'){		
		setCookie('webmart_phones', 1, {
    		expires: 3600*24*30,
			path: '/'
  		});
	}
	if(getCookie('webmart_phones')==1){
$(".phones .velcom").hide();
$(".phones .life").hide();
		$(".phones .city").html("+375 17 369-75-32");
$(".phones .mobile").html("+375 29 836-75-77");
		$(".city-phone-block .city").html("<span>+375 17</span> 369-75-32");
$(".mobile-phone-block").hide();
$(".main-footer .v2").append('<div class="city-phone-block"><span class="city"><span>+375 29</span> 836-75-77</span></div>');
$(".phones-row .velcom").hide();
$(".phones-row .life").hide();
$(".phones-row .btc .phone").html('<span class="code">+375 17 </span>369-75-32');
$(".phones-row .mts .phone").html('<span class="code">+375 29 </span>836-75-77');
		$(".bg-white.padd-30.b-r_2 b").each(function(){
			if($(this).html()=="7577 Velcom, МТС, Life:)"){
				$(this).html("+375 17 369-75-32; +375 29 836-75-77");
			}
		}
	);
	}
});
<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter41833024 = new Ya.Metrika({ id:41833024, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/41833024" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</script>