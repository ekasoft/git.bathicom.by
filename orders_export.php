<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Экспорт заказов в \"1С:Предприятие\" ");
?><?$APPLICATION->IncludeComponent(
	"jlab:sale.export.1c",
	"",
	Array(
		"1C_SITE_NEW_ORDERS" => "s1",
		"CHANGE_STATUS_FROM_1C" => "N",
		"EXPORT_ALLOW_DELIVERY_ORDERS" => "N",
		"EXPORT_FINAL_ORDERS" => "A",
		"EXPORT_PAYED_ORDERS" => "N",
		"FILE_SIZE_LIMIT" => "204800",
		"GROUP_PERMISSIONS" => array("1"),
		"IMPORT_NEW_ORDERS" => "N",
		"INTERVAL" => "30",
		"REPLACE_CURRENCY" => "руб.",
		"SITE_LIST" => "s1",
		"USE_ZIP" => "Y"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>