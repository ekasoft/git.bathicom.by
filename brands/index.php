<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Бренды");
?>
<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "ekasoft", array());?>  
<?
$APPLICATION->AddChainItem("Бренды", "/brands/");
?>
<section class="categories-section-e">
    <div class="container">
        <div class="row insertAjax">
            <div class="col-md-12">
                <div class="main-h1"><h1><?$APPLICATION->ShowTitle();?></h1></div>
            </div>
            <?$APPLICATION->IncludeComponent("giperlink:brands.sections", "", array());?>      
            </div>                        
        </div>
    </div>
</section>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>